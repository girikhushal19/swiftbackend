var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors=require("cors");
const crons=require("node-cron");
require("dotenv").config();
require("./config/firebase.config.js")
require("./config/database").connect();
const {
  checkifappointmentiscomplete
 
}=require("./modules/cronjobs");
var indexRouter = require('./routes/index');
var adminpanelrouter=require('./routes/admin-panel/index');

var app = express();
app.use(cors());
// view engine setup


// app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());

app.set("view engine", "ejs")
app.use(express.urlencoded({limit: '50mb', extended: true, parameterLimit: 50000}));
app.use(express.static(path.join(__dirname, "views")));
app.use("/static/public",express.static("views/admin-panel/public"))
app.use('/public/uploads', express.static('public/uploads'));
app.use("/api", indexRouter);
// console.log("indexRouter",indexRouter)
app.use('/admin-panel',adminpanelrouter);

// app.all('/admin-panel/*', (req, res) => {
//     res.render('admin-panel/404',{
//         base_url:process.env.BASE_URL+"/admin-panel/"
//     });
// });


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});
// crons.schedule(" */5 * * * *", async () => {
//   let newdate=new Date();
//     console.log("event completeion cron job ran at",newdate.getHours()+":"+newdate.getMinutes())
//     checkifappointmentiscomplete();
// })
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
