var admin = require("firebase-admin/messaging");
const notificationmodel=require('../models/Notifications');
var FCM = require('fcm-node');
var serverKey = 'AAAAXwP_ruM:APA91bFscree1K5mBt6haYPOQyPBcpJsdMxbwJtH1Lpe-Vywg9rJJcxXZiiOCnUUln2V8E6PoGYt1sSSpx_3BQdUMDFz_otHk08aACBzhIdzmLBrgQiN-3ufRAuZsAXHLYidEkFVtgz-'; //put your server key here
var fcm = new FCM(serverKey);
const sendpushnotificationtotopic = async (msg, topic,to_id) => {
  const message = {
    data: {
      message: msg,
    },
    android: {
            priority: "high",  // Here goes priority
            ttl: 10 * 60 * 1000, // Time to live
        },
    // token: registrationToken
    topic: topic,
  };

  // Send a message to the device corresponding to the provided
  // registration token.
  admin
    .getMessaging()
    .send(message)
    .then((response) => {
      notificationmodel.create({
        message: msg,
        to_id:topic,
        status: "unread",
        notification_type: "push",
        date: new Date(),
        
      }).then(()=>{
        console.log("Successfully sent message to topic:", response);
      })
    })
    .catch((error) => {
      console.log("Error sending message:", error);
    });
};
const sendpushnotificationtouser = async (msg, user,to_id) => {
  try{
    if(user?.fcm_token){
      const message = {
        notification: {
          title: process.env.MESSAGE_TITLE||"message from swift",
          body: msg
        },
        // data: {
        //   title:msg,
        //   body:msg,
        //   message:msg
        // },
        "android": {
          "direct_boot_ok": true,
        },
        token: user.fcm_token
       
      };
      admin
        .getMessaging()
        .send(message)
        .then((response) => {
          // Response is a message ID string.
          notificationmodel.create({
            message: msg,
            to_id:user._id,
            status: "unread",
            notification_type: "push",
            date: new Date(),
    
          }).then(()=>{
            console.log("Successfully sent message to user:", response);
          })
         
        })
        .catch((error) => {
          console.log("tried sending but failed, fcm token is not there");
        });
    }
    else{
      notificationmodel.create({
        message: msg,
        to_id:user._id,
        status: "unread",
        notification_type: "push",
        date: new Date().toISOString(),

      })
      console.log("user is not logged in==>",user._id);
    }
  }catch(err){
    console.log(err);

  }
};
const sendtestmessage=(to_id)=>{
  require("../config/firebase.config.js")
  const message = {
        data: {
          score: "850",
          time: "2:45",
        },
        token:to_id,
      };
admin
  .getMessaging()
  .send(message)
  .then((response) => {
    // Response is a message ID string.
    
      console.log("Successfully sent message to user:", response);
    })
}

module.exports = { sendpushnotificationtotopic,sendpushnotificationtouser,sendtestmessage };
