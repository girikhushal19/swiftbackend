function handlefilter(){
    var client_name = $("#client_name").val();
    var lawyer_name = $("#lawyer_name").val();
    var start_date = $("#start_date").val();
    var end_date = $("#end_date").val();
    var mode_of_payment = $("#mode_of_payment").val();
    var mode_of_service = $("#mode_of_service").val();
    var url = "<%=base_url%>paymenthistory?client_name="+client_name+"&lawyer_name="+lawyer_name+"&start_date="+start_date+"&end_date="+end_date+"&mode_of_payment="+mode_of_payment+"&mode_of_service="+mode_of_service;
    window.location.href = url;
  }

  <div  class="dataTables_filter">
  <input type="text" id="client_name" class="form-control form-control-sm" placeholder="Nom d'utilisateur" aria-controls="example1">
  <input type="text" id="lawyer_name"class="form-control form-control-sm" placeholder="Nom de l'avocat" aria-controls="example1">
  <input type="text" id="mode_of_payment"class="form-control form-control-sm" placeholder="Mode de paiement" aria-controls="example1">
  <input type="text" id="mode_of_service"class="form-control form-control-sm" placeholder="Mode de service" aria-controls="example1">
  <input type="date" id="start_date" class="form-control form-control-sm" placeholder="date d'entrée en vigueur" aria-controls="example1">
  <input type="date" id="end_date" class="form-control form-control-sm" placeholder="date de fin" aria-controls="example1">
  <input type="button" id="search" class="btn btn-primary"  value="filtre" onclick="handlefilter()">
</div>