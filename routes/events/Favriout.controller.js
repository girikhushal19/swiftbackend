const Favriout = require("../../models/events/Favriout.js");
var mongoose = require('mongoose');

module.exports.savefavriout = async(req, res) => {
    const isexists=await Favriout.findOne({
        user_id: req.body.user_id,
        event_id: req.body.event_id,
    })
   if(isexists){
    return res.send({
        data: null,
        status: false,
        message: "Favriout existe déjà",
        errmessage: "",
    });
   }else{
    const favriout = new Favriout({
        user_id: req.body.user_id,
        event_id: req.body.event_id,
    });
    favriout.save().then((favriout) => {
        return res.send({
            data: favriout,
            status: true,
            message: "Favriout sauvé avec succès",
            errmessage: "",
        });
    }).catch((err) => {
        return res.send({
            status: false,
            message: "",
            error: err,
            errmessage: "Le sauvetage de Favriout a échoué"
        });
    })
   }
}
module.exports.getallfavriouts = (req, res) => {
        Favriout.find({}, (err, favriouts) => {
            if (err) {
                res.json({
                    status: false,
                    message: "",
                    errmessage: err.message,
                    data: null
                });
            } else {
                res.json({
                    status: true,
                    data: favriouts,
                    message: "",
                    errmessage: ""
                });
            }
        });
    }
module.exports.getfavrioutbyclientID = async(req, res) => {
        
        const student_id=req.params.client_id ;
        //console.log("student_id",student_id)
       await Favriout.aggregate([
            {$match: { user_id :student_id } },
            
            {
                $lookup: {
                    from: "events",
                    let: { provider_id: { "$toObjectId": "$event_id" } },
                    pipeline: [{
                        $match: { $expr: { $eq: ["$_id", "$$provider_id"] } },

                    },
                    // { $addFields: { providerIDSTR: { "$toString": "$_id" } } },
                    // {
                    //     $lookup: {
                    //         from: "reviews",
                    //         localField: "providerIDSTR",
                    //         foreignField: "provider_id",
                    //         as: "reviews"
                    //     }
                    // },
                    // {
                    //     $lookup: {
                    //         from: "lawyerservices",
                    //         localField: "providerIDSTR",
                    //         foreignField: "provider_id",
                    //         as: "services"
                    //     }
                    // },
                    // { $addFields: { service: { $arrayElemAt: ["$services", 0] }, provider_idOBJ: { "$toObjectId": "$provider_id" } } },
                    // {
                    //    $lookup:{
                    //     from:"reviews",
                    //     localField:"providerIDSTR",
                    //     foreignField:"provider_id",
                    //     as:"reviews"
                    //   }
                    // },
                    // {
                    //     $addFields: {

                    //         avgRating: { $avg: "$reviews.rating" }
                    //     }
                    // },

                    ],
                    as: "events"
                }
            },
            {$addFields:{provider:{$arrayElemAt:["$events",0]}}},
        
        ]).exec((err, favriouts) => {
            //console.log(favriouts,err)
            if (err) {
                        res.json({
                            status: false,
                            message: "",
                            errmessage: err.message,
                            data: null
                        });
                    } else {
                        res.json({
                            status: true,
                            data: favriouts,
                            message: "",
                            errmessage: ""
                        });
                    }
        })
        // console.log(req.params.student_id)
        // Favriout.find({ student_id: req.params.student_id }, (err, favriouts) => {
        //     if (err) {
        //         res.json({
        //             status: false,
        //             message: "",
        //             errmessage: err.message,
        //             data: null
        //         });
        //     } else {
        //         res.json({
        //             status: true,
        //             data: favriouts,
        //             message: "",
        //             errmessage: ""
        //         });
        //     }
        // });
    }
module.exports.deletefavorite =async (req, res) => {
        await Favriout.findOneAndDelete({  event_id: req.body.event_id,user_id:req.body.user_id }).then(( favriout) => {
            return res.json({
                status: true,
                message: "Favriout a été supprimé avec succès",
                errmessage: "",
                data: favriout
            });
        });
    }
   