const express =require('express');
const CartController = require('./CartController');
const UserCartController = require("./UserCartController");
const router = express.Router();
const multer=require("multer");
const path=require("path");
const PRODUCT_PATH=process.env.PRODUCT_PATH;
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve(PRODUCT_PATH))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });
router.post('/addproductocart',CartController.addproductocart);
router.post('/deleteproductfromcart',CartController.deleteProductfromcart);
router.post('/updatecart',CartController.updatecart);
router.post('/clearcart',CartController.clearcart);
router.post('/getcartbyuserid',CartController.getcartbyuserid);
router.post('/getcartbyuseridCopy',CartController.getcartbyuseridCopy);

router.post('/getcartbyuserid1',CartController.getcartbyuserid1);
router.post('/checkoutcart',CartController.checkoutcart);
router.post('/checkoutbuynow',CartController.checkoutbuynow);
router.get("/checkpromocode/:promo",CartController.checkpromocode);

router.post("/applPromoCode",CartController.applPromoCode);
router.post("/beforecheckoutcart",CartController.beforecheckoutcart);
router.post("/updateCartQuantity",CartController.updateCartQuantity);

router.post("/userAddToCart",UserCartController.userAddToCart);
router.get("/userGetToCart/:id",UserCartController.userGetToCart);
router.post("/increaseCartQuantity",UserCartController.increaseCartQuantity);
router.post("/decreaseCartQuantity",UserCartController.decreaseCartQuantity);
router.post("/deleteItemFromCart",UserCartController.deleteItemFromCart);

router.post("/userGetCartBeforeCheckoutApi",UserCartController.userGetCartBeforeCheckoutApi);
router.post("/withOutParamUserGetCartBeforeCheckoutApi",UserCartController.withOutParamUserGetCartBeforeCheckoutApi);

router.post("/userCheckoutApi",UserCartController.userCheckoutApi);
router.post("/updateVariationChange",UserCartController.updateVariationChange);


module.exports=router