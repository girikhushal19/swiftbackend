const { default: mongoose } = require("mongoose");
const { Product } = require("../../models/products/Product");
const Seller = require("../../models/seller/Seller");
const User= require("../../models/user/User");
const VariationsPrice =require("../../models/products/VariationPrice");
const GlobalSettings=require("../../models/admin/GlobalSettings");
const orders = require("../../models/orders/order");
const CoupensModel=require("../../models/seller/Coupens");
const CommissionModel=require("../../models/admin/Commissions");
const {calculateShippingcostforproducts} =require("../../routes/shipping/ShippingController");
const {createordersfunc}=require("../orders/OrderController");
const {groupBy,groupByfunc}=require("../../modules/Utility");
const stringGen=require("../../modules/randomString");
const {myAddressFunction} =require("../../modules/AddressCalculation")
const SellerTimeSloats = require("../../models/seller/SellerTimeSloats"); 
const SellerBranchesModel = require("../../models/seller/SellerBranchesModel");
const BASE_URL=process.env.BASE_URL;
const fs = require('fs');
var request = require('request');
const GOOGLE_KEY = process.env.GOOGLE_KEY;

const axios = require('axios');


    
module.exports = {
    userAddToCart:async(req,res)=>{
        try{
            console.log("hereeeeee");
            let {quantity,product_id,seller_id,shop_id,variation_id,delivery_type,user_id} = req.body;
            //shipping_charge coupon_code
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            if(!product_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du produit est requise"
                })
            }
            if(!shop_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du magasin est requise"
                })
            }
            if(!seller_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du vendeur est requis"
                })
            }
            if(!quantity)
            {
                quantity = 1;
            }
            if(!delivery_type)
            {
                delivery_type = 1;
            }
            if(variation_id)
            {
                let variation_match = await Product.aggregate([
                    {
                        $match:{
                            _id:mongoose.Types.ObjectId(product_id)
                        }
                    },
                    {
                        $project:{
                            "sales_info_variation_prices":{
                                $filter:{
                                    input:"$sales_info_variation_prices",
                                    as:"variation",
                                    cond:{
                                        $eq:["$$variation._id",mongoose.Types.ObjectId(variation_id) ]
                                    }
                                }
                            }
                        }
                    }
                ]);
                //console.log("variation_match ", JSON.stringify(variation_match));
                if(variation_match.length == 0)
                {
                    return res.send({
                        status:false,
                        message:"L'identifiant du produit n'est pas valide"
                    })
                }else{
                    //console.log(variation_match[0].sales_info_variation_prices);
                    let variation = variation_match[0].sales_info_variation_prices;
                    
                    if(variation.length > 0)
                    {
                        //console.log("dsfdsf " ,variation);return false;
                        if( variation[0].stock <  quantity)
                        {
                            return res.send({
                                status:false,
                                message:"Le produit n'est plus en stock"
                            })
                        }
                    }
                }
                //return false;
            }else{
                let product_rec = await Product.findOne({_id:product_id},{stock:1});
                if(!product_rec)
                {
                    return res.send({
                        status:false,
                        message:"L'identifiant du produit n'est pas valide"
                    })
                }
                if( product_rec.stock <  quantity)
                {
                    return res.send({
                        status:false,
                        message:"Le produit n'est plus en stock"
                    })
                }
            }

            let add_obj = {
                quantity:quantity,
                product_id:product_id,
                seller_id:seller_id,
                shop_id:shop_id,
                variation_id:variation_id,
                delivery_type:delivery_type
            };
            let user_record = await User.findOne({_id:user_id},{cart:1});
            if(!user_record)
            {
                return res.send({
                    status:false,
                    message:"Identifiant d'utilisateur invalide"
                })
            }
            //console.log(user_record);
            let product_allready = false;
            let all_cart_data = user_record.cart;
            if(all_cart_data.length > 0)
            {
                //console.log(all_cart_data[0].seller_id);return false;
                if(all_cart_data[0].seller_id != seller_id)
                {
                    return res.send({
                        status:false,
                        message:"Vous ne pouvez pas acheter des produits de vendeurs différents en même temps"
                    })
                }
            }
            let new_cart_data = all_cart_data.filter((val)=>{
                if(val.product_id == product_id && val.shop_id == shop_id && val.variation_id == variation_id)
                {
                    product_allready = true;
                    val.quantity = quantity;
                }
                return val;
            });
            if(product_allready == true)
            {
                all_cart_data = new_cart_data;
            }else{
                all_cart_data.push(add_obj);
            }
            await User.updateOne({_id:user_id},{cart:all_cart_data}).then((result)=>{
                return res.send({
                    status:true,
                    message:"L'article a été ajouté au panier avec succès"
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    userGetToCart:async(req,res)=>{
        try{
            
            let user_id = req.params.id;
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            let tax_percant = 0; let imidiate_shipping_cost = 0;
            let estimate_time = 0;
            let stng_rec = await GlobalSettings.findOne({});
            if(stng_rec)
            {
                tax_percant = stng_rec.tax;
                imidiate_shipping_cost = stng_rec.imidiate_shipping_cost;
                estimate_time = stng_rec.estimate_time;
            }
            let current_date = new Date();
            let current_time = current_date.getHours()+"h"+current_date.getMinutes();
            current_date.setMinutes(current_date.getMinutes() + estimate_time);
            let final_estimate_time = current_date.getHours()+"h"+current_date.getMinutes();
            console.log("current_time ", current_time);
            console.log("final_estimate_time ", final_estimate_time);
            let estimate_time_string = current_time+" - "+final_estimate_time;
            //console.log("tax_percant ", tax_percant);
            //return false;
            // let product_not_variant = await User.aggregate([
                
            //     {
            //         $unwind:"$cart"
            //     },
            //     {
            //         $match:{
            //             _id:mongoose.Types.ObjectId(user_id)
            //         }
            //     },
            //     {
            //         $match:{
            //             $expr:{
            //                 $eq:["$cart.variation_id",""]
            //             }
            //         }
            //     },
            //     {
            //         $addFields:{
            //             "cart_quan":"$cart.quantity"
            //         }
            //     },
            //     {
            //         $lookup:{
            //             from:"products",
            //             let:{"p_id":{"$toObjectId":  "$cart.product_id" },"cart_quantaty":"$cart_quan" },
            //             pipeline:[
            //                 {
            //                     $match:{
            //                         $expr:{
            //                             $eq:["$_id","$$p_id"]
            //                         }
            //                     }
            //                 },
            //                 {
            //                     $group:{
            //                         _id:"$_id",
            //                         totalAmount:{
            //                             $sum:{
            //                                 $multiply:["$price","$$cart_quantaty"]
            //                             }
            //                         },
            //                         count: { $sum: 1 }
            //                     }
            //                 }
            //             ],
            //             as:"pro_rec"
            //         }
            //     },
            //     {
            //         $project:{
            //             "pro_rec.totalAmount" : 1
            //         }
            //     }
            // ]);
            // let product_with_variant = await User.aggregate([
            //     {
            //         $match:{
            //             _id:mongoose.Types.ObjectId(user_id)
            //         }
            //     },
            //     {
            //         $project:{
            //             "cart":{
            //                 $filter:{
            //                     input:"$cart",
            //                     as:"cart_data",
            //                     cond:{
            //                         $ne:["$$cart_data.variation_id",""]
            //                     }
            //                 }
            //             }
            //         }
            //     },
            // ]);
            //mongoose.set("debug",true);
            await User.aggregate([
                {
                    $unwind:"$cart"
                },
                {
                    $match:{
                        _id:mongoose.Types.ObjectId(user_id)
                    }
                },
                // {
                //     "$addFields": {
                //         "vv_id": {$cond: [ 
                //             { 
                //                   $eq: [ "$cart.variation_id", "" ] 
                //              },
                //             "", 
                //             {"$toObjectId":"$cart.variation_id"}
                //         ] }
                //     }
                // },
                {
                    $lookup:{
                        from:"products",
                        let:{"p_id":{"$toObjectId":  "$cart.product_id" },"vv_id": {
                            $cond: [ 
                                { 
                                  $eq: [ "$cart.variation_id", "" ] 
                                },
                                "", 
                                {"$toObjectId":"$cart.variation_id"}
                            ]}
                        },
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$_id","$$p_id"]
                                    }
                                }
                            },
                            {
                                $addFields:{
                                    "style_id":{
                                        $arrayElemAt:["$style",0]
                                    }
                                }
                            },
                            {
                                $lookup:{
                                    from:"productfavorites",
                                    let:{"prdct_id":{"$toString":"$_id" }},
                                    pipeline:[
                                        {
                                            $match:{
                                                $expr:{
                                                    $and:[
                                                        {$eq:["$user_id",user_id]},
                                                        {$eq:["$product_id","$$prdct_id"]}
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as:"fav_item"
                                }
                            },
                            {
                                $lookup:{
                                    from:"brands",
                                    let:{"brand_id":{"$toObjectId":"$brand_name"}},
                                    pipeline:[
                                        {
                                            $match:{
                                                $expr:{
                                                    $eq:["$_id","$$brand_id"]
                                                }
                                            }
                                        }
                                    ],
                                    as:"brand_record"
                                }
                            },
                            {
                                $lookup:{
                                    from:"styles",
                                    let:{"style_id":{"$toObjectId":"$style_id"}},
                                    pipeline:[
                                        {
                                            $match:{
                                                $expr:{
                                                    $eq:["$_id","$$style_id"]
                                                }
                                            }
                                        }
                                    ],
                                    as:"style_rec"
                                }
                            },
                            {
                                $project:{
                                    "provider_id":1,
                                    "title":1,
                                    "fav_item":1,
                                    "style_id":1,
                                    "style_rec.name":1,
                                    "brand_record.name":1,
                                    "category_id": 1,
                                    "condition": 1,
                                    "price": 1,
                                    "images": 1,
                                    "dangerous_goods": 1,
                                    "spec_attributes": 1,
                                    "branches": 1,
                                    "brand_name": 1,
                                    "style": 1,
                                    "is_active": 1,
                                    "enable_variation": 1,
                                    "view_count": 1,
                                    "seller_will_bear_shipping": 1,
                                    "is_delisted": 1,
                                    "is_deleted": 1,
                                    "sales_info_variation_prices":{
                                        $filter:{
                                            input:"$sales_info_variation_prices",
                                            as:"variation",
                                            cond:{
                                                $eq:["$$variation._id","$$vv_id"]
                                            }
                                        }
                                    }
                                }
                            }
                        ],
                        as:"product_rec"
                    }
                },
                {
                    $lookup:{
                        from:"products",
                        let:{"p_id":{"$toObjectId":  "$cart.product_id" },"vv_id": {
                            $cond: [ 
                                { 
                                  $eq: [ "$cart.variation_id", "" ] 
                                },
                                "", 
                                {"$toObjectId":"$cart.variation_id"}
                            ]}
                        },
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$_id","$$p_id"]
                                    }
                                }
                            },
                            {
                                $addFields:{
                                    "style_id":{
                                        $arrayElemAt:["$style",0]
                                    }
                                }
                            },
                            {
                                $lookup:{
                                    from:"productfavorites",
                                    let:{"prdct_id":{"$toString":"$_id" }},
                                    pipeline:[
                                        {
                                            $match:{
                                                $expr:{
                                                    $and:[
                                                        {$eq:["$user_id",user_id]},
                                                        {$eq:["$product_id","$$prdct_id"]}
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as:"fav_item"
                                }
                            },
                            {
                                $lookup:{
                                    from:"brands",
                                    let:{"brand_id":{"$toObjectId":"$brand_name"}},
                                    pipeline:[
                                        {
                                            $match:{
                                                $expr:{
                                                    $eq:["$_id","$$brand_id"]
                                                }
                                            }
                                        }
                                    ],
                                    as:"brand_record"
                                }
                            },
                            {
                                $lookup:{
                                    from:"styles",
                                    let:{"style_id":{"$toObjectId":"$style_id"}},
                                    pipeline:[
                                        {
                                            $match:{
                                                $expr:{
                                                    $eq:["$_id","$$style_id"]
                                                }
                                            }
                                        }
                                    ],
                                    as:"style_rec"
                                }
                            },
                            {
                                $project:{
                                    "provider_id":1,
                                    "title":1,
                                    "fav_item":1,
                                    "style_id":1,
                                    "style_rec.name":1,
                                    "brand_record.name":1,
                                    "category_id": 1,
                                    "condition": 1,
                                    "price": 1,
                                    "images": 1,
                                    "dangerous_goods": 1,
                                    "spec_attributes": 1,
                                    "branches": 1,
                                    "brand_name": 1,
                                    "style": 1,
                                    "is_active": 1,
                                    "enable_variation": 1,
                                    "view_count": 1,
                                    "seller_will_bear_shipping": 1,
                                    "is_delisted": 1,
                                    "is_deleted": 1,
                                    "sales_info_variation_prices":{
                                        $filter:{
                                            input:"$sales_info_variation_prices",
                                            as:"variation",
                                            cond:{
                                                $gt:["$$variation.stock",0]
                                            }
                                        }
                                    }
                                }
                            }
                        ],
                        as:"product_rec_all_variation"
                    }
                },
                {
                    $project:{
                        "password": 0,
                        "token": 0,
                        "companyName": 0,
                        "userAddress": 0,
                        "forgotPasswordLink": 0,
                        "forgotPasswordTime": 0,
                        "forgotPasswordUsed": 0,
                        "is_deleted": 0,
                        "is_active": 0,
                        "verification_code": 0,
                        "otp_date_time": 0,
                        "otp_used_or_not": 0,
                        "isverfied": 0,
                        "mobile_code": 0,
                        "mobile_otp_date_time": 0,
                        "mobile_otp_used_or_not": 0,
                        "mobileIsverfied": 0,
                        "credit_limit": 0,
                        "extProvider": 0,
                        "loggedin_time": 0,
                        "eventcart":0,
                        "fav_event_cat": 0,
                        "createdAt": 0,
                        "updatedAt":0,
                        "__v": 0,
                    }
                },
                {
                    $sort:{
                        "cart.created_at":-1
                    }
                } 
                
            ]).then((result)=>{
                if(result.length > 0)
                {
                    let product_total_price_not_varint = 0; let product_total_price_with_varint = 0;
                    result.forEach((val)=>{
                        let price = 0;
                        if(val.cart.variation_id == "" )
                        {
                            if(val.product_rec.length > 0)
                            {
                                //price = val.product_rec[0].price;
                                price = val.product_rec[0].price * val.cart.quantity;
                                product_total_price_not_varint += price;
                            }
                        }else{
                             
                            //console.log(val.product_rec[0].sales_info_variation_prices);
                            if(val.product_rec[0].sales_info_variation_prices.length)
                            {
                                console.log("price ", val.product_rec[0].sales_info_variation_prices[0].price);
                                console.log("quantity ", val.cart.quantity);

                                price = val.product_rec[0].sales_info_variation_prices[0].price * val.cart.quantity;
                            }
                            product_total_price_with_varint += price;
                        }
                        //let total = price;
                        //return total;
                    });
                    console.log("product_total_price_not_varint ", product_total_price_not_varint);
                    console.log("product_total_price_with_varint ", product_total_price_with_varint);
                    let product_total = parseFloat(product_total_price_not_varint) +parseFloat(product_total_price_with_varint);
                    product_total.toFixed(2);
                    product_total = parseFloat(product_total);
                    let tax_value = product_total * tax_percant / 100;
                    tax_value = tax_value.toFixed(2);

                    tax_value = parseFloat(tax_value);
                    let cart_total = parseFloat(tax_value) + product_total;
                    cart_total = cart_total.toFixed(2);
                    cart_total = parseFloat(cart_total)
                    let cart_data = {
                        estimate_time:estimate_time_string,
                        imidiate_shipping_cost:imidiate_shipping_cost,
                        product_total:product_total,
                        tax_value:tax_value,
                        cart_total:cart_total
                    }
                    return res.send({
                        status:true,
                        message:"Success",
                        cart_data:cart_data,
                        record:result
                    });
                }else{
                    return res.send({
                        status:false,
                        message:"Aucun article dans le panier",
                        record:[]
                    });
                }                
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message
                });
            })
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    increaseCartQuantity:async(req,res)=>{
        try{
            let {cart_id,user_id} = req.body;
            
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                });
            }
            if(!cart_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du panier est requise"
                });
            }
            //mongoose.set("debug",true);
            let cart_record = await User.aggregate([
                {
                    $match:{
                        _id:mongoose.Types.ObjectId(user_id)
                    }
                }
                //,
                // {
                //     $match:{
                //         "cart":{
                //             $elemMatch:{
                //                 "_id":mongoose.Types.ObjectId(cart_id)
                //             }
                //         }
                //     }
                // }

                
                 
            ]);
            //db.users.find({awards: {$elemMatch: {award:'National Medal', year:1975}}})
            //console.log("cart_record ", cart_record);return false;
            if(cart_record.length == 0)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur n'est pas valide"
                });
            }else{
                //console.log(cart_record);
                let new_array = cart_record[0].cart.map((val)=>{
                    //console.log(val);
                    if(val._id == cart_id)
                    {
                        val.quantity = val.quantity + 1;
                    }
                    return val;
                })
                //console.log(new_array);
                await User.updateOne({_id:user_id},{cart:new_array});
                return res.send({
                    status:true,
                    message:"Quantit a augmenté le succès pleinement"
                });
            }
            
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    decreaseCartQuantity:async(req,res)=>{
        try{
            let {cart_id,user_id} = req.body;
            
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                });
            }
            if(!cart_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du panier est requise"
                });
            }
            //mongoose.set("debug",true);
            let cart_record = await User.aggregate([
                {
                    $match:{
                        _id:mongoose.Types.ObjectId(user_id)
                    }
                }
            ]);
            //console.log("cart_record ", cart_record);return false;
            if(cart_record.length == 0)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur n'est pas valide"
                });
            }else{
                //console.log(cart_record);
                let no_error = 0;
                let new_array = cart_record[0].cart.map((val)=>{
                    //console.log(val);
                    if(val._id == cart_id)
                    {
                        if(val.quantity >1)
                        {
                            val.quantity = val.quantity - 1;
                            no_error = 0;
                        }else{
                            no_error = 1;
                        }
                        
                    }
                    return val;
                });
                //console.log(new_array);
                if(no_error == 0)
                {
                    await User.updateOne({_id:user_id},{cart:new_array});
                    return res.send({
                        status:true,
                        message:"Quantit a augmenté le succès pleinement"
                    });
                }else{
                    return res.send({
                        status:false,
                        message:"La quantité ne peut pas être inférieure à un"
                    });
                }
            }
            
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    deleteItemFromCart:async(req,res)=>{
        try{
            let {cart_id,user_id} = req.body;
            
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                });
            }
            if(!cart_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du panier est requise"
                });
            }
            let cart_data = await User.findOne({_id:user_id},{cart:1});
            if(!cart_data)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur n'est pas valide"
                });
            }else{
                let filter_rec = cart_data.cart.filter((val)=>{
                    if(val._id != cart_id)
                    {
                        return val;
                    }
                });
                console.log("filter_rec ", filter_rec);
                await User.updateOne({_id:user_id},{cart:filter_rec});
                return res.send({
                    status:true,
                    message:"L'article a été retiré du panier"
                });
            }
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    userGetCartBeforeCheckoutApi:async(req,res)=>{
        try{
            /*
            "user_id":"6654777c4fb55f99733594e1",
            "delivery_type":1,  // 1 = Imidiate 2 = future delivery 3=pickup 
            "code":"",
            "pickup_delivery_date":"",
            "pickup_delivery_time":"",
            "delivery_address_id":"666710078ae7ab488bad35ff",
            "payment_type":1 // 1=Stripe 2=Paypal
            */
            let {payment_type,delivery_address_id,pickup_delivery_time,pickup_delivery_date,code,delivery_type,user_id} = req.body;
            let check_deliver_type = 1;
            //shipping_charge coupon_code
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            if(!delivery_type)
            {
                return res.send({
                    status:false,
                    message:"Le type de livraison est requise"
                })
            }
            if(!pickup_delivery_date)
            {
                return res.send({
                    status:false,
                    message:"La date d'enlèvement ou de livraison est requise"
                })
            }
            if(!pickup_delivery_time)
            {
                return res.send({
                    status:false,
                    message:"L'heure d'enlèvement ou de livraison est requise"
                })
            }
            if(!payment_type)
            {
                return res.send({
                    status:false,
                    message:"Le type de paiement est requise"
                })
            }
            if(delivery_type == 1 || delivery_type == 2)
            {
                check_deliver_type = 1;
                if(!delivery_address_id)
                {
                    return res.send({
                        status:false,
                        message:"L'identifiant de l'adresse de livraison est requise"
                    })
                }
            }else{
                check_deliver_type = 2;
            }
            const coupon = await CoupensModel.findOne({ code: code });
            const productsincoupon = coupon?.products || [];
            const coupontype = coupon?.type;
            const couponamount = coupon?.discountamount;
             
            let totalPrice = 0; let discounted_amount = 0;
            let totalcommission = 0; let totalshipping = 0; let totalplusperproduct = 0;
            let totaltax = 0; let totalproductsinorder = 0; let totalproductsinqty = 0;
            let tax_percant = 0;
            let admin_commission = 0; 
            let shipping_cost = 0;
            let admin_shipping_cost = 0; let imidiate_shipping_cost = 0;
            let stng_rec = await GlobalSettings.findOne({});
            if(stng_rec)
            {
                tax_percant = stng_rec.tax;
                admin_commission = stng_rec.commission;
                admin_shipping_cost = stng_rec.shipping_cost;
                imidiate_shipping_cost = stng_rec.imidiate_shipping_cost;
            }

           

            let date = pickup_delivery_date;
            date = date.split("/").reverse().join("-");
            let day = new Date(date).getDay();

            console.log("date ", date);
            console.log("dd ", day);
            let ext_pickup_delivery_time = pickup_delivery_time.split("-");
            let start_time = 0; let end_time = 0;
            if(ext_pickup_delivery_time.length > 1)
            {
                start_time = ext_pickup_delivery_time[0];
                end_time = ext_pickup_delivery_time[1];
                start_time = start_time.substring(0,2);
                end_time = end_time.substring(0,2);
            }
            //console.log("start_time ", start_time);
            //console.log("end_time ", end_time);

            let user_data = await User.findOne({_id:user_id});
            if(!user_data)
            {
                return res.send({
                    status:false,
                    message:"ID utilisateur invalide"
                })
            }
            let cart_data = user_data.cart;
            let address = user_data.address;
            let new_address = address.filter((vall)=>{
                if(vall._id == delivery_address_id)
                {
                    return vall;
                }
            })
            

            //console.log("new_address ", new_address);
            if(cart_data.length == 0)
            {
                return res.send({
                    status:false,
                    message:"Aucun article n'est disponible dans le panier"
                })
            }else{
                //mongoose.set("debug",true);
                let seller_id = cart_data[0].seller_id;
                let seller_availablity = await SellerTimeSloats.find({seller_id:seller_id,start_time:{$lte:start_time},end_time:{$gte:end_time},day_number:day,day_enable:true,delivery_type:check_deliver_type });
                //console.log("seller_availablity ", seller_availablity);
                // if(seller_availablity.length == 0)
                // {
                //     return res.send({
                //         status:false,
                //         message:"Le vendeur n'est pas disponible dans cette période"
                //     })
                // }

                let shop_id = cart_data[0].shop_id;
                let user_latitude = 0; let user_longitude = 0;
                let shop_latitude = 0; let shop_longitude = 0;
                let distance_in_meter = 0;
                if(delivery_address_id)
                {
                    if(new_address.length > 0)
                    {
                        //console.log("new_address ", new_address);
                        let location = new_address[0].location;
                        if(location.coordinates.length > 1)
                        {
                            user_latitude = location.coordinates[0];
                            user_longitude = location.coordinates[1];
                        }

                    }
                    let shop_record = await SellerBranchesModel.findOne({_id:shop_id});
                    if(shop_record)
                    {
                        let location = shop_record.location;
                        if(location.coordinates.length > 1)
                        {
                            shop_latitude = location.coordinates[0];
                            shop_longitude = location.coordinates[1];
                        }
                    }
                    console.log("user_latitude " , user_latitude);
                    console.log("user_longitude " , user_longitude);
                    console.log("shop_latitude " , shop_latitude);
                    console.log("shop_longitude " , shop_longitude);
                    
                    let my_url = `https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${user_latitude},${user_longitude}&destinations=${shop_latitude}%2C${shop_longitude}&key=${GOOGLE_KEY}`;
                    
                    const responseee = await axios.get(my_url);
                    //console.log(responseee);
                    //console.log("response ", responseee.data);
                    let new_body_param = responseee.data;
                    // console.log("body ", body);
                    // let new_body_param = JSON.parse(body);
                    //console.log("new_body_param ", new_body_param);
                    if(new_body_param)
                    {
                        if(new_body_param["rows"])
                        {
                            //console.log("dsfsf");
                            if(new_body_param.rows.length > 0)
                            {
                                if(new_body_param.rows[0].elements)
                                {
                                    if(new_body_param.rows[0].elements.length > 0)
                                    {
                                        if(new_body_param.rows[0].elements[0].status)
                                        {
                                            if(new_body_param.rows[0].elements[0].status == "OK")
                                            {
                                                console.log(new_body_param.rows[0].elements[0].distance)
                                                console.log(new_body_param.rows[0].elements[0].distance.value)
                                                if(new_body_param.rows[0].elements[0].distance)
                                                {
                                                    distance_in_meter = new_body_param.rows[0].elements[0].distance.value;
                                                    //return distance_in_meter;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //console.log("data ", data);
                }
                //console.log("distance_in_meter ", distance_in_meter);
                let distance_in_km = 0;
                if(distance_in_meter > 0)
                {
                    distance_in_km = distance_in_meter / 1000;
                    if(distance_in_km > 0)
                    {
                        distance_in_km = distance_in_km.toFixed(2);
                        admin_shipping_cost = admin_shipping_cost * 10;
                        shipping_cost = distance_in_km * admin_shipping_cost;
                        shipping_cost = parseFloat(shipping_cost); 
                    }
                }
                console.log("distance_in_km ", distance_in_km);
                console.log("shipping_cost ", shipping_cost);
                
                if(delivery_type == 1)
                {
                    shipping_cost = shipping_cost + imidiate_shipping_cost;
                }
                shipping_cost = shipping_cost.toFixed(2);
                shipping_cost = parseFloat(shipping_cost);
                if(delivery_type == 3)
                {
                    shipping_cost = 0;
                }
                //return false;


                let not_available_product = [];
                let products = [];
                for (let i = 0; i < cart_data.length; i++)
                {
                    //console.log(cart_data[i]);
                    if(cart_data[i].variation_id)
                    {
                        console.log("iffffffffffffffffffff 829");
                        //console.log("variation_id " , cart_data[i].variation_id);
                        //mongoose.set("debug",true);
                        await Product.aggregate([
                            {
                                $match:{
                                    _id:mongoose.Types.ObjectId(cart_data[i].product_id)
                                }
                            },
                            {
                                $project:{
                                    "sales_info_variation_prices":{
                                        $filter:{
                                            input:"$sales_info_variation_prices",
                                            as:"variation",
                                            cond:{
                                                    $eq:[ "$$variation._id",mongoose.Types.ObjectId(cart_data[i].variation_id) ]
                                                }
                                        }
                                    },
                                    reported: 1,
                                    dispatch_info: 1,
                                    category_id: 1,
                                    condition: 1,
                                    brand: 1,
                                    style: 1,
                                    price: 1,
                                    images: 1,
                                    dangerous_goods: 1,
                                    spec_attributes: 1,
                                    brand_name: 1,
                                    is_active: 1,
                                    enable_variation: 1,
                                    view_count: 1,
                                    seller_will_bear_shipping: 1,
                                    is_delisted: 1,
                                    is_deleted: 1,
                                    sales_info: 1,
                                    created_at: 1,
                                    sku: 1,
                                    stock: 1,
                                    title: 1,
                                    provider_id: 1,
                                    description: 1,
                                    catagory: 1,
                                    branches: 1,
                                    discover_by: 1
                                }
                            }
                        ]).then((product_rec)=>{
                            if(product_rec.length > 0)
                            {
                                
                                //console.log("iffff product_rec ",product_rec);
                                //console.log("ifffffff 826  ");
                                let new_product_rec = product_rec[0].sales_info_variation_prices.filter((val2)=>{
                                    //console.log("stock ", val2.stock);
                                    
                                    if(val2.stock < cart_data[i].quantity)
                                    {
                                       // console.log("hereeeee 833");
                                        return val2;
                                    }
                                })
                                if(new_product_rec.length > 0)
                                {
                                    not_available_product.push(cart_data[i]);
                                }else{
                                    //let shipping_cost = 0;
                                    //console.log("iffff product_rec 844", JSON.stringify(product_rec[0]));
                                    
                                    let plusperproduct = product_rec[0].sales_info_variation_prices[0].price * cart_data[i].quantity;
                                    let total_product_price_loop = product_rec[0].sales_info_variation_prices[0].price * cart_data[i].quantity;
                                    total_product_price_loop = parseFloat(total_product_price_loop);
                                     
                                    //When coupon applied here put less price in // pricetouser and discountedPrice also will be
                                    
                                    let discountedPrice = 0; let couponApplied = "";
                                    //When coupon applied here put less price in // pricetouser and discountedPrice also will be
                                    if (productsincoupon?.includes(cart_data[i].product_id ))
                                    {
                                        if (coupontype == "a")
                                        {
                                            if(product_rec[0].price >= couponamount)
                                            {
                                                // product["couponApplied"] = coupon.code;
                                                // product["discountedPrice"] = productprice - couponamount;
                                                
                                                // totalpaymentamount = totalpaymentamount + (productprice * qty);


                                                //discountedPrice = total_product_price_loop - couponamount;
                                                discountedPrice = couponamount;
                                                couponApplied = coupon.code;
                                            }
                                        } else if (coupontype == "p") {
                                            // product["couponApplied"] = coupon.code;
                                            // product["discountedPrice"] = productprice - (productprice * couponamount / 100);
                                            
                                            // totalpaymentamount = totalpaymentamount + (productprice * qty);
                                            discountedPrice = total_product_price_loop * couponamount /100;
                                            couponApplied = coupon.code;
                                            
                                        }
                                    }
                                    let after_discount_product_price = total_product_price_loop - discountedPrice;
                                    after_discount_product_price = after_discount_product_price.toFixed(2);
                                    after_discount_product_price = parseFloat(after_discount_product_price);

                                    let commission_price = after_discount_product_price * admin_commission / 100;
                                    commission_price = commission_price.toFixed(2);
                                    commission_price = parseFloat(commission_price);

                                    let sellerprofit = total_product_price_loop - commission_price;
                                    sellerprofit = sellerprofit.toFixed(2);
                                    sellerprofit = parseFloat(sellerprofit);

                                    //let tax = total_product_price_loop * tax_percant /100;
                                    let tax = after_discount_product_price * tax_percant /100;
                                    tax = tax.toFixed(2);
                                    tax = parseFloat(tax);

                                    let finalprice = total_product_price_loop + tax; 
                                    finalprice = finalprice.toFixed(2);
                                    finalprice = parseFloat(finalprice);

                                    let pricetouser = finalprice - discountedPrice;
                                    pricetouser = pricetouser.toFixed(2);
                                    pricetouser = parseFloat(pricetouser);


                                    discounted_amount = discountedPrice + discounted_amount;
                                    discounted_amount = discounted_amount.toFixed(2);
                                    discounted_amount = parseFloat(discounted_amount);

                                    sellerprofit = sellerprofit - discountedPrice;
                                    sellerprofit = sellerprofit.toFixed(2);
                                    sellerprofit = parseFloat(sellerprofit);

                                    totalPrice = totalPrice + finalprice;
                                    totalPrice = totalPrice.toFixed(2);
                                    totalPrice = parseFloat(totalPrice);

                                    totalcommission = totalcommission + commission_price;
                                    totalcommission = totalcommission.toFixed(2);
                                    totalcommission = parseFloat(totalcommission);

                                    //totalshipping = totalshipping + shipping_cost;
                                    totalplusperproduct = totalplusperproduct + plusperproduct;
                                    totalplusperproduct = totalplusperproduct.toFixed(2);
                                    totalplusperproduct = parseFloat(totalplusperproduct);

                                    totaltax = totaltax + tax; 
                                    totaltax = totaltax.toFixed(2);
                                    totaltax = parseFloat(totaltax);

                                    totalproductsinorder = cart_data.length; 
                                    totalproductsinqty = totalproductsinorder + cart_data[i].quantity;
                                    let part_id = parseInt((Math.random())*99999999);
                                    products.push( {
                                        discountedPrice:discountedPrice,
                                        couponApplied:couponApplied,
                                        part_id:part_id,
                                        product: product_rec[0],
                                        provider_id:product_rec[0].provider_id,
                                        product_price: product_rec[0].sales_info_variation_prices[0].price,
                                        quantity: cart_data[i].quantity,
                                        plusperproduct: plusperproduct,
                                        varientPrice: total_product_price_loop,
                                        tax:tax,
                                        finalprice:finalprice,
                                        pricetouser:pricetouser,
                                        commission:commission_price,
                                        sellerprofit:sellerprofit,
                                        shipping_cost:shipping_cost,
                                        variation_id: cart_data[i].variation_id
                                    });
                                }

                            }
                        })
                        // console.log("ifffffff ");
                        // console.log("product_rec ", product_rec);
                    }else{
                        console.log("elseeeeeeeeeeeeeeeeeeeee 980");
                        let product_rec = await Product.aggregate([
                            {
                                $match:{
                                    _id:mongoose.Types.ObjectId(cart_data[i].product_id)
                                }
                            },
                            {
                                $match:{
                                    stock:{$lt:cart_data[i].quantity}
                                }
                            }
                        ])
                        //console.log("product_rec ", product_rec);
                        // console.log("elseeeeeeee ");
                        // console.log("product_rec ", product_rec);
                        if(product_rec.length > 0)
                        {
                            
                            //console.log("hereeeeeee ");
                            not_available_product.push(cart_data[i]);
                        }
                        let product_rec_2 = await Product.aggregate([
                            {
                                $match:{
                                    _id:mongoose.Types.ObjectId(cart_data[i].product_id)
                                }
                            },
                            {
                                $match:{
                                    stock:{$gte:cart_data[i].quantity}
                                }
                            }
                        ])
                        //console.log("elseeeeee product_rec_2 ",product_rec_2);
                        if(product_rec_2.length > 0)
                        {
                            //let shipping_cost = 0;
                            //console.log("iffff product_rec_2 844", JSON.stringify(product_rec_2[0]));
                            let plusperproduct = product_rec_2[0].price * cart_data[i].quantity;
                            let total_product_price_loop = product_rec_2[0].price * cart_data[i].quantity;
                            total_product_price_loop = parseFloat(total_product_price_loop);
                             
                            let discountedPrice = 0; let couponApplied = "";
                            //When coupon applied here put less price in // pricetouser and discountedPrice also will be
                            if (productsincoupon?.includes(cart_data[i].product_id ))
                            {
                                if (coupontype == "a")
                                {
                                    if(product_rec_2[0].price >= couponamount)
                                    {
                                        // product["couponApplied"] = coupon.code;
                                        // product["discountedPrice"] = productprice - couponamount;
                                        // discounted_amount = discounted_amount + (productprice - couponamount) * qty;
                                        // totalpaymentamount = totalpaymentamount + (productprice * qty);


                                        //discountedPrice = total_product_price_loop - couponamount;
                                        discountedPrice = couponamount;
                                        couponApplied = coupon.code;
                                    }
                                } else if (coupontype == "p") {
                                    // product["couponApplied"] = coupon.code;
                                    // product["discountedPrice"] = productprice - (productprice * couponamount / 100);
                                    // discounted_amount = discounted_amount + (productprice - ((productprice * couponamount) / 100)) * qty;
                                    // totalpaymentamount = totalpaymentamount + (productprice * qty);
                                    discountedPrice = total_product_price_loop * couponamount /100;
                                    couponApplied = coupon.code;
                                    
                                }
                            }
                            //  else {
                            //     product["discountedPrice"] = productprice;
                            //     discounted_amount = discounted_amount + (productprice) * qty;
                            //     totalpaymentamount = (totalpaymentamount + productprice) * qty;
                            // }

                            
                            
                            let after_discount_product_price = total_product_price_loop - discountedPrice;
                            after_discount_product_price = after_discount_product_price.toFixed(2);
                            after_discount_product_price = parseFloat(after_discount_product_price);

                            let commission_price = after_discount_product_price * admin_commission / 100;
                            commission_price = commission_price.toFixed(2);
                            commission_price = parseFloat(commission_price);

                            let sellerprofit = total_product_price_loop - commission_price;
                            sellerprofit = sellerprofit.toFixed(2);
                            sellerprofit = parseFloat(sellerprofit);

                            //let tax = total_product_price_loop * tax_percant /100;
                            let tax = after_discount_product_price * tax_percant /100;
                            tax = tax.toFixed(2);
                            tax = parseFloat(tax);

                            let finalprice = total_product_price_loop + tax; 
                            finalprice = finalprice.toFixed(2);
                            finalprice = parseFloat(finalprice);

                            let pricetouser = finalprice - discountedPrice;
                            pricetouser = pricetouser.toFixed(2);
                            pricetouser = parseFloat(pricetouser);


                            discounted_amount = discountedPrice + discounted_amount;
                            discounted_amount = discounted_amount.toFixed(2);
                            discounted_amount = parseFloat(discounted_amount);

                            sellerprofit = sellerprofit - discountedPrice;
                            sellerprofit = sellerprofit.toFixed(2);
                            sellerprofit = parseFloat(sellerprofit);

                            totalPrice = totalPrice + finalprice;
                            totalPrice = totalPrice.toFixed(2);
                            totalPrice = parseFloat(totalPrice);

                            totalcommission = totalcommission + commission_price;
                            totalcommission = totalcommission.toFixed(2);
                            totalcommission = parseFloat(totalcommission);

                            //totalshipping = totalshipping + shipping_cost;
                            totalplusperproduct = totalplusperproduct + plusperproduct;
                            totalplusperproduct = totalplusperproduct.toFixed(2);
                            totalplusperproduct = parseFloat(totalplusperproduct);

                            totaltax = totaltax + tax; 
                            totaltax = totaltax.toFixed(2);
                            totaltax = parseFloat(totaltax);

                            totalproductsinorder = cart_data.length; 
                            totalproductsinqty = totalproductsinorder + cart_data[i].quantity;
                            let part_id = parseInt((Math.random())*99999999);
                            products.push( {
                                discountedPrice:discountedPrice,
                                couponApplied:couponApplied,
                                part_id:part_id,
                                product: product_rec_2[0],
                                provider_id:product_rec_2[0].provider_id,
                                product_price: product_rec_2[0].price,
                                quantity: cart_data[i].quantity,
                                plusperproduct: plusperproduct,
                                varientPrice: total_product_price_loop,
                                tax:tax,
                                finalprice:finalprice,
                                pricetouser:pricetouser,
                                commission:commission_price,
                                sellerprofit:sellerprofit,
                                shipping_cost:shipping_cost,
                                variation_id: cart_data[i].variation_id
                            });
                        }
                    }
                    
                    

                }
                //console.log("not_available_product ", not_available_product);
                if(not_available_product.length > 0)
                {
                    return res.send({
                        status:false,
                        message:"Certains produits ne sont plus en stock",
                        record:not_available_product
                    });
                }
                //console.log("products ");
                //console.log(products);
                // "quantity" : 1,
                // "varientPrice" : 17729.38,
                // "discountedPrice" : 17729.38,
                // "pricetouser" : 17774.38,
                // "couponApplied" : "",
                // "tax" : 1772.938,
                // "shipping_cost" : 45,
                // "commission" : 1241.0566000000001,
                // "plusperproduct" : 0,
                // "finalprice" : 17774.38,
                // "sellerprofit" : 16488.3234,

               
                // "order_id" : 1033,
                // "shippingaddress" : {}, 
                let shippingaddress = {};
                if(new_address.length > 0)
                {
                    shippingaddress = new_address[0];
                }
                
                //console.log("products ", products);
                //return false;
                //
                //shipping_cost = 0;
                totalshipping = parseFloat(shipping_cost);
                totalshipping = totalshipping.toFixed(2);
                totalshipping = parseFloat(totalshipping);

                const combo_id = stringGen(20);
                console.log("totalPrice ", totalPrice);
                console.log("totalcommission ", totalcommission);
                console.log("totalshipping ", totalshipping);
                console.log("totalplusperproduct ", totalplusperproduct);
                console.log("totaltax ", totaltax);
                console.log("totalproductsinorder ", totalproductsinorder);
                console.log("totalproductsinqty ", totalproductsinqty);
                console.log("discounted_amount ", discounted_amount);
                console.log("payment_amount ", totalPrice);

                totalPrice = totalPrice + totalshipping;
                totalPrice = totalPrice.toFixed(2);
                totalPrice = parseFloat(totalPrice);

                let after_promo = totalPrice - discounted_amount;
                after_promo = after_promo.toFixed(2);
                after_promo = parseFloat(after_promo);
                
                let insert_data = {
                    delivery_type:delivery_type,
                    userId:user_id,
                    //"products":products,
                    "combo_id" : combo_id,
                    "totalshipping" : totalshipping,
                    "totalplusperproduct" : totalplusperproduct,
                    "totaltax" : totaltax,
                    "totalproductsinorder" : totalproductsinorder,
                    "totalproductsinqty" : totalproductsinqty,
                    "payment_status" : false,
                    "promo_code" : code,
                    "payment_method" :  payment_type,
                    "transaction_id" : "",
                    "date_of_transaction" : new Date(),
                    "status" : 0,
                    "delivery_status" : 0,
                    "date_of_delivery" : null,
                    "returned_reason" : null,
                    "totalPrice" : after_promo,
                    "totalcommission" : totalcommission,
                    "discounted_amount":discounted_amount,
                    "payment_amount":totalPrice,
                    "shippingaddress":shippingaddress
                }  
                return res.send({
                    status:true,
                    message:"Cliquer sur continuer et faire une commande après le paiement",
                    record:insert_data
                });
                
            }
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    userCheckoutApi:async(req,res)=>{
        try{
            /*
            "user_id":"6654777c4fb55f99733594e1",
            "delivery_type":1,  // 1 = Imidiate 2 = future delivery 3=pickup 
            "code":"",
            "pickup_delivery_date":"",
            "pickup_delivery_time":"",
            "delivery_address_id":"666710078ae7ab488bad35ff",
            "payment_type":1 // 1=Stripe 2=Paypal
            */
            let {payment_type,delivery_address_id,pickup_delivery_time,pickup_delivery_date,code,delivery_type,user_id,payment_submit_web_type} = req.body;
            if(!payment_submit_web_type)
            {
                payment_submit_web_type = 0;
            }
            let check_deliver_type = 1;
            //shipping_charge coupon_code
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            if(!delivery_type)
            {
                return res.send({
                    status:false,
                    message:"Le type de livraison est requise"
                })
            }
            if(!pickup_delivery_date)
            {
                return res.send({
                    status:false,
                    message:"La date d'enlèvement ou de livraison est requise"
                })
            }
            if(!pickup_delivery_time)
            {
                return res.send({
                    status:false,
                    message:"L'heure d'enlèvement ou de livraison est requise"
                })
            }
            if(!payment_type)
            {
                return res.send({
                    status:false,
                    message:"Le type de paiement est requise"
                })
            }
            if(delivery_type == 1 || delivery_type == 2)
            {
                check_deliver_type = 1;
                if(!delivery_address_id)
                {
                    return res.send({
                        status:false,
                        message:"L'identifiant de l'adresse de livraison est requise"
                    })
                }
            }else{
                check_deliver_type = 2;
            }
            const coupon = await CoupensModel.findOne({ code: code });
            const productsincoupon = coupon?.products || [];
            const coupontype = coupon?.type;
            const couponamount = coupon?.discountamount;
             
            let totalPrice = 0; let discounted_amount = 0;
            let totalcommission = 0; let totalshipping = 0; let totalplusperproduct = 0;
            let totaltax = 0; let totalproductsinorder = 0; let totalproductsinqty = 0;
            let tax_percant = 0;
            let shipping_cost = 0;
            let admin_commission = 0; 
            let admin_shipping_cost = 0; let imidiate_shipping_cost = 0;
            let stng_rec = await GlobalSettings.findOne({});
            if(stng_rec)
            {
                tax_percant = stng_rec.tax;
                admin_commission = stng_rec.commission;
                admin_shipping_cost = stng_rec.shipping_cost;
                imidiate_shipping_cost = stng_rec.imidiate_shipping_cost;
            }

           

            let date = pickup_delivery_date;
            date = date.split("/").reverse().join("-");
            let day = new Date(date).getDay();

            console.log("date ", date);
            console.log("dd ", day);
            let ext_pickup_delivery_time = pickup_delivery_time.split("-");
            let start_time = 0; let end_time = 0;
            if(ext_pickup_delivery_time.length > 1)
            {
                start_time = ext_pickup_delivery_time[0];
                end_time = ext_pickup_delivery_time[1];
                start_time = start_time.substring(0,2);
                end_time = end_time.substring(0,2);
            }
            //console.log("start_time ", start_time);
            //console.log("end_time ", end_time);

            let user_data = await User.findOne({_id:user_id});
            if(!user_data)
            {
                return res.send({
                    status:false,
                    message:"ID utilisateur invalide"
                })
            }
            let cart_data = user_data.cart;
            let address = user_data.address;
            let new_address = address.filter((vall)=>{
                if(vall._id == delivery_address_id)
                {
                    return vall;
                }
            })
            

            //console.log("new_address ", new_address);
            if(cart_data.length == 0)
            {
                return res.send({
                    status:false,
                    message:"Aucun article n'est disponible dans le panier"
                })
            }else{
                //mongoose.set("debug",true);
                let seller_id = cart_data[0].seller_id;
                let seller_availablity = await SellerTimeSloats.find({seller_id:seller_id,start_time:{$lte:start_time},end_time:{$gte:end_time},day_number:day,day_enable:true,delivery_type:check_deliver_type });
                //console.log("seller_availablity ", seller_availablity);
                if(seller_availablity.length == 0)
                {
                    return res.send({
                        status:false,
                        message:"Le vendeur n'est pas disponible dans cette période"
                    })
                }

                let shop_id = cart_data[0].shop_id;
                let user_latitude = 0; let user_longitude = 0;
                let shop_latitude = 0; let shop_longitude = 0;
                let distance_in_meter = 0;
                if(delivery_address_id)
                {
                    if(new_address.length > 0)
                    {
                        //console.log("new_address ", new_address);
                        let location = new_address[0].location;
                        if(location.coordinates.length > 1)
                        {
                            user_latitude = location.coordinates[0];
                            user_longitude = location.coordinates[1];
                        }

                    }
                    let shop_record = await SellerBranchesModel.findOne({_id:shop_id});
                    if(shop_record)
                    {
                        let location = shop_record.location;
                        if(location.coordinates.length > 1)
                        {
                            shop_latitude = location.coordinates[0];
                            shop_longitude = location.coordinates[1];
                        }
                    }
                    console.log("user_latitude " , user_latitude);
                    console.log("user_longitude " , user_longitude);
                    console.log("shop_latitude " , shop_latitude);
                    console.log("shop_longitude " , shop_longitude);
                    
                    let my_url = `https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${user_latitude},${user_longitude}&destinations=${shop_latitude}%2C${shop_longitude}&key=${GOOGLE_KEY}`;
                    
                    const responseee = await axios.get(my_url);
                    //console.log(responseee);
                    //console.log("response ", responseee.data);
                    let new_body_param = responseee.data;
                    // console.log("body ", body);
                    // let new_body_param = JSON.parse(body);
                    //console.log("new_body_param ", new_body_param);
                    if(new_body_param)
                    {
                        if(new_body_param["rows"])
                        {
                            //console.log("dsfsf");
                            if(new_body_param.rows.length > 0)
                            {
                                if(new_body_param.rows[0].elements)
                                {
                                    if(new_body_param.rows[0].elements.length > 0)
                                    {
                                        if(new_body_param.rows[0].elements[0].status)
                                        {
                                            if(new_body_param.rows[0].elements[0].status == "OK")
                                            {
                                                console.log(new_body_param.rows[0].elements[0].distance)
                                                console.log(new_body_param.rows[0].elements[0].distance.value)
                                                if(new_body_param.rows[0].elements[0].distance)
                                                {
                                                    distance_in_meter = new_body_param.rows[0].elements[0].distance.value;
                                                    //return distance_in_meter;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //console.log("data ", data);
                }
                //console.log("distance_in_meter ", distance_in_meter);
                let distance_in_km = 0;
                if(distance_in_meter > 0)
                {
                    distance_in_km = distance_in_meter / 1000;
                    if(distance_in_km > 0)
                    {
                        distance_in_km = distance_in_km.toFixed(2);
                        admin_shipping_cost = admin_shipping_cost * 10;
                        shipping_cost = distance_in_km * admin_shipping_cost;
                        shipping_cost = parseFloat(shipping_cost); 
                    }
                }
                console.log("distance_in_km ", distance_in_km);
                console.log("shipping_cost ", shipping_cost);
                
                if(delivery_type == 1)
                {
                    shipping_cost = shipping_cost + imidiate_shipping_cost;
                }
                shipping_cost = shipping_cost.toFixed(2);
                shipping_cost = parseFloat(shipping_cost);
                //return false;
                if(delivery_type == 3)
                {
                    shipping_cost = 0;
                }

                let not_available_product = [];
                let products = [];
                for (let i = 0; i < cart_data.length; i++)
                {
                    //console.log(cart_data[i]);
                    if(cart_data[i].variation_id)
                    {
                        console.log("iffffffffffffffffffff 829");
                        //console.log("variation_id " , cart_data[i].variation_id);
                        //mongoose.set("debug",true);
                        await Product.aggregate([
                            {
                                $match:{
                                    _id:mongoose.Types.ObjectId(cart_data[i].product_id)
                                }
                            },
                            {
                                $project:{
                                    "sales_info_variation_prices":{
                                        $filter:{
                                            input:"$sales_info_variation_prices",
                                            as:"variation",
                                            cond:{
                                                    $eq:[ "$$variation._id",mongoose.Types.ObjectId(cart_data[i].variation_id) ]
                                                }
                                        }
                                    },
                                    reported: 1,
                                    dispatch_info: 1,
                                    category_id: 1,
                                    condition: 1,
                                    brand: 1,
                                    style: 1,
                                    price: 1,
                                    images: 1,
                                    dangerous_goods: 1,
                                    spec_attributes: 1,
                                    brand_name: 1,
                                    is_active: 1,
                                    enable_variation: 1,
                                    view_count: 1,
                                    seller_will_bear_shipping: 1,
                                    is_delisted: 1,
                                    is_deleted: 1,
                                    sales_info: 1,
                                    created_at: 1,
                                    sku: 1,
                                    stock: 1,
                                    title: 1,
                                    provider_id: 1,
                                    description: 1,
                                    catagory: 1,
                                    branches: 1,
                                    discover_by: 1
                                }
                            }
                        ]).then((product_rec)=>{
                            if(product_rec.length > 0)
                            {
                                
                                //console.log("iffff product_rec ",product_rec);
                                //console.log("ifffffff 826  ");
                                let new_product_rec = product_rec[0].sales_info_variation_prices.filter((val2)=>{
                                    //console.log("stock ", val2.stock);
                                    
                                    if(val2.stock < cart_data[i].quantity)
                                    {
                                       // console.log("hereeeee 833");
                                        return val2;
                                    }
                                })
                                if(new_product_rec.length > 0)
                                {
                                    not_available_product.push(cart_data[i]);
                                }else{
                                    //let shipping_cost = 0;
                                    //console.log("iffff product_rec 844", JSON.stringify(product_rec[0]));
                                    
                                    let plusperproduct = product_rec[0].sales_info_variation_prices[0].price * cart_data[i].quantity;
                                    let total_product_price_loop = product_rec[0].sales_info_variation_prices[0].price * cart_data[i].quantity;
                                    total_product_price_loop = parseFloat(total_product_price_loop);
                                     
                                    //When coupon applied here put less price in // pricetouser and discountedPrice also will be
                                    
                                    let discountedPrice = 0; let couponApplied = "";
                                    //When coupon applied here put less price in // pricetouser and discountedPrice also will be
                                    if (productsincoupon?.includes(cart_data[i].product_id ))
                                    {
                                        if (coupontype == "a")
                                        {
                                            if(product_rec[0].price >= couponamount)
                                            {
                                                // product["couponApplied"] = coupon.code;
                                                // product["discountedPrice"] = productprice - couponamount;
                                                
                                                // totalpaymentamount = totalpaymentamount + (productprice * qty);


                                                //discountedPrice = total_product_price_loop - couponamount;
                                                discountedPrice = couponamount;
                                                couponApplied = coupon.code;
                                            }
                                        } else if (coupontype == "p") {
                                            // product["couponApplied"] = coupon.code;
                                            // product["discountedPrice"] = productprice - (productprice * couponamount / 100);
                                            
                                            // totalpaymentamount = totalpaymentamount + (productprice * qty);
                                            discountedPrice = total_product_price_loop * couponamount /100;
                                            couponApplied = coupon.code;
                                            
                                        }
                                    }
                                    let after_discount_product_price = total_product_price_loop - discountedPrice;
                                    after_discount_product_price = after_discount_product_price.toFixed(2);
                                    after_discount_product_price = parseFloat(after_discount_product_price);

                                    let commission_price = after_discount_product_price * admin_commission / 100;
                                    commission_price = commission_price.toFixed(2);
                                    commission_price = parseFloat(commission_price);

                                    let sellerprofit = total_product_price_loop - commission_price;
                                    sellerprofit = sellerprofit.toFixed(2);
                                    sellerprofit = parseFloat(sellerprofit);

                                    //let tax = total_product_price_loop * tax_percant /100;
                                    let tax = after_discount_product_price * tax_percant /100;
                                    tax = tax.toFixed(2);
                                    tax = parseFloat(tax);

                                    let finalprice = total_product_price_loop + tax; 
                                    finalprice = finalprice.toFixed(2);
                                    finalprice = parseFloat(finalprice);

                                    let pricetouser = finalprice - discountedPrice;
                                    pricetouser = pricetouser.toFixed(2);
                                    pricetouser = parseFloat(pricetouser);


                                    discounted_amount = discountedPrice + discounted_amount;
                                    discounted_amount = discounted_amount.toFixed(2);
                                    discounted_amount = parseFloat(discounted_amount);

                                    sellerprofit = sellerprofit - discountedPrice;
                                    sellerprofit = sellerprofit.toFixed(2);
                                    sellerprofit = parseFloat(sellerprofit);

                                    totalPrice = totalPrice + finalprice;
                                    totalPrice = totalPrice.toFixed(2);
                                    totalPrice = parseFloat(totalPrice);

                                    totalcommission = totalcommission + commission_price;
                                    totalcommission = totalcommission.toFixed(2);
                                    totalcommission = parseFloat(totalcommission);

                                    //totalshipping = totalshipping + shipping_cost;
                                    totalplusperproduct = totalplusperproduct + plusperproduct;
                                    totalplusperproduct = totalplusperproduct.toFixed(2);
                                    totalplusperproduct = parseFloat(totalplusperproduct);

                                    totaltax = totaltax + tax; 
                                    totaltax = totaltax.toFixed(2);
                                    totaltax = parseFloat(totaltax);

                                    totalproductsinorder = cart_data.length; 
                                    totalproductsinqty = totalproductsinorder + cart_data[i].quantity;

                                    let part_id = parseInt((Math.random())*99999999);
                                    products.push( {
                                        discountedPrice:discountedPrice,
                                        couponApplied:couponApplied,
                                        part_id:part_id,
                                        product: product_rec[0],
                                        provider_id:product_rec[0].provider_id,
                                        product_price: product_rec[0].sales_info_variation_prices[0].price,
                                        quantity: cart_data[i].quantity,
                                        plusperproduct: plusperproduct,
                                        varientPrice: total_product_price_loop,
                                        tax:tax,
                                        finalprice:finalprice,
                                        pricetouser:pricetouser,
                                        commission:commission_price,
                                        sellerprofit:sellerprofit,
                                        shipping_cost:shipping_cost,
                                        variation_id: cart_data[i].variation_id
                                    });
                                }

                            }
                        })
                        // console.log("ifffffff ");
                        // console.log("product_rec ", product_rec);
                    }else{
                        console.log("elseeeeeeeeeeeeeeeeeeeee 980");
                        let product_rec = await Product.aggregate([
                            {
                                $match:{
                                    _id:mongoose.Types.ObjectId(cart_data[i].product_id)
                                }
                            },
                            {
                                $match:{
                                    stock:{$lt:cart_data[i].quantity}
                                }
                            }
                        ])
                        //console.log("product_rec ", product_rec);
                        // console.log("elseeeeeeee ");
                        // console.log("product_rec ", product_rec);
                        if(product_rec.length > 0)
                        {
                            
                            //console.log("hereeeeeee ");
                            not_available_product.push(cart_data[i]);
                        }
                        let product_rec_2 = await Product.aggregate([
                            {
                                $match:{
                                    _id:mongoose.Types.ObjectId(cart_data[i].product_id)
                                }
                            },
                            {
                                $match:{
                                    stock:{$gte:cart_data[i].quantity}
                                }
                            }
                        ])
                        //console.log("elseeeeee product_rec_2 ",product_rec_2);
                        if(product_rec_2.length > 0)
                        {
                            //let shipping_cost = 0;
                            //console.log("iffff product_rec_2 844", JSON.stringify(product_rec_2[0]));
                            let plusperproduct = product_rec_2[0].price * cart_data[i].quantity;
                            let total_product_price_loop = product_rec_2[0].price * cart_data[i].quantity;
                            total_product_price_loop = parseFloat(total_product_price_loop);
                             
                            let discountedPrice = 0; let couponApplied = "";
                            //When coupon applied here put less price in // pricetouser and discountedPrice also will be
                            if (productsincoupon?.includes(cart_data[i].product_id ))
                            {
                                if (coupontype == "a")
                                {
                                    if(product_rec_2[0].price >= couponamount)
                                    {
                                        // product["couponApplied"] = coupon.code;
                                        // product["discountedPrice"] = productprice - couponamount;
                                        // discounted_amount = discounted_amount + (productprice - couponamount) * qty;
                                        // totalpaymentamount = totalpaymentamount + (productprice * qty);


                                        //discountedPrice = total_product_price_loop - couponamount;
                                        discountedPrice = couponamount;
                                        couponApplied = coupon.code;
                                    }
                                } else if (coupontype == "p") {
                                    // product["couponApplied"] = coupon.code;
                                    // product["discountedPrice"] = productprice - (productprice * couponamount / 100);
                                    // discounted_amount = discounted_amount + (productprice - ((productprice * couponamount) / 100)) * qty;
                                    // totalpaymentamount = totalpaymentamount + (productprice * qty);
                                    discountedPrice = total_product_price_loop * couponamount /100;
                                    couponApplied = coupon.code;
                                    
                                }
                            }
                            //  else {
                            //     product["discountedPrice"] = productprice;
                            //     discounted_amount = discounted_amount + (productprice) * qty;
                            //     totalpaymentamount = (totalpaymentamount + productprice) * qty;
                            // }

                            
                            
                            let after_discount_product_price = total_product_price_loop - discountedPrice;
                            after_discount_product_price = after_discount_product_price.toFixed(2);
                            after_discount_product_price = parseFloat(after_discount_product_price);

                            let commission_price = after_discount_product_price * admin_commission / 100;
                            commission_price = commission_price.toFixed(2);
                            commission_price = parseFloat(commission_price);

                            let sellerprofit = total_product_price_loop - commission_price;
                            sellerprofit = sellerprofit.toFixed(2);
                            sellerprofit = parseFloat(sellerprofit);

                            //let tax = total_product_price_loop * tax_percant /100;
                            let tax = after_discount_product_price * tax_percant /100;
                            tax = tax.toFixed(2);
                            tax = parseFloat(tax);

                            let finalprice = total_product_price_loop + tax; 
                            finalprice = finalprice.toFixed(2);
                            finalprice = parseFloat(finalprice);

                            let pricetouser = finalprice - discountedPrice;
                            pricetouser = pricetouser.toFixed(2);
                            pricetouser = parseFloat(pricetouser);


                            discounted_amount = discountedPrice + discounted_amount;
                            discounted_amount = discounted_amount.toFixed(2);
                            discounted_amount = parseFloat(discounted_amount);

                            sellerprofit = sellerprofit - discountedPrice;
                            sellerprofit = sellerprofit.toFixed(2);
                            sellerprofit = parseFloat(sellerprofit);

                            totalPrice = totalPrice + finalprice;
                            totalPrice = totalPrice.toFixed(2);
                            totalPrice = parseFloat(totalPrice);

                            totalcommission = totalcommission + commission_price;
                            totalcommission = totalcommission.toFixed(2);
                            totalcommission = parseFloat(totalcommission);

                            //totalshipping = totalshipping + shipping_cost;
                            totalplusperproduct = totalplusperproduct + plusperproduct;
                            totalplusperproduct = totalplusperproduct.toFixed(2);
                            totalplusperproduct = parseFloat(totalplusperproduct);

                            totaltax = totaltax + tax; 
                            totaltax = totaltax.toFixed(2);
                            totaltax = parseFloat(totaltax);

                            totalproductsinorder = cart_data.length; 
                            totalproductsinqty = totalproductsinorder + cart_data[i].quantity;

                            let part_id = parseInt((Math.random())*99999999);
                            products.push( {
                                discountedPrice:discountedPrice,
                                couponApplied:couponApplied,
                                part_id:part_id,
                                product: product_rec_2[0],
                                provider_id:product_rec_2[0].provider_id,
                                product_price: product_rec_2[0].price,
                                quantity: cart_data[i].quantity,
                                plusperproduct: plusperproduct,
                                varientPrice: total_product_price_loop,
                                tax:tax,
                                finalprice:finalprice,
                                pricetouser:pricetouser,
                                commission:commission_price,
                                sellerprofit:sellerprofit,
                                shipping_cost:shipping_cost,
                                variation_id: cart_data[i].variation_id
                            });
                        }
                    }
                    
                    

                }
                //console.log("not_available_product ", not_available_product);
                if(not_available_product.length > 0)
                {
                    return res.send({
                        status:false,
                        message:"Certains produits ne sont plus en stock",
                        record:not_available_product
                    });
                }
                //console.log("products ");
                //console.log(products);
                // "quantity" : 1,
                // "varientPrice" : 17729.38,
                // "discountedPrice" : 17729.38,
                // "pricetouser" : 17774.38,
                // "couponApplied" : "",
                // "tax" : 1772.938,
                // "shipping_cost" : 45,
                // "commission" : 1241.0566000000001,
                // "plusperproduct" : 0,
                // "finalprice" : 17774.38,
                // "sellerprofit" : 16488.3234,

               
                // "order_id" : 1033,
                // "shippingaddress" : {}, 
                let shippingaddress = {};
                if(new_address.length > 0)
                {
                    shippingaddress = new_address[0];
                }
                
                //console.log("products ", products);
                //return false;
                //
                //shipping_cost = 0;
                totalshipping = parseFloat(shipping_cost);
                totalshipping = totalshipping.toFixed(2);
                totalshipping = parseFloat(totalshipping);

                const combo_id = stringGen(20);
                console.log("totalPrice ", totalPrice);
                console.log("totalcommission ", totalcommission);
                console.log("totalshipping ", totalshipping);
                console.log("totalplusperproduct ", totalplusperproduct);
                console.log("totaltax ", totaltax);
                console.log("totalproductsinorder ", totalproductsinorder);
                console.log("totalproductsinqty ", totalproductsinqty);
                console.log("discounted_amount ", discounted_amount);
                console.log("payment_amount ", totalPrice);

                totalPrice = totalPrice + totalshipping;
                totalPrice = totalPrice.toFixed(2);
                totalPrice = parseFloat(totalPrice);

                let after_promo = totalPrice - discounted_amount;
                after_promo = after_promo.toFixed(2);
                after_promo = parseFloat(after_promo);
                
                let insert_data = {
                    payment_submit_web_type:payment_submit_web_type,
                    delivery_type:delivery_type,
                    userId:user_id,
                    "products":products,
                    "combo_id" : combo_id,
                    "totalshipping" : totalshipping,
                    "totalplusperproduct" : totalplusperproduct,
                    "totaltax" : totaltax,
                    "totalproductsinorder" : totalproductsinorder,
                    "totalproductsinqty" : totalproductsinqty,
                    "payment_status" : false,
                    "promo_code" : code,
                    "payment_method" :  payment_type,
                    "transaction_id" : "",
                    "date_of_transaction" : new Date(),
                    "status" : 0,
                    "delivery_status" : 0,
                    "date_of_delivery" : null,
                    "returned_reason" : null,
                    "totalPrice" : after_promo,
                    "totalcommission" : totalcommission,
                    "discounted_amount":discounted_amount,
                    "payment_amount":totalPrice,
                    "shippingaddress":shippingaddress,
                    "pickup_delivery_date":date,
                    "pickup_delivery_time":pickup_delivery_time,
                    "pickup_delivery_date_only":date
                }  
                await orders.create(insert_data).then((result)=>{
                    return res.send({
                        status:true,
                        message:"Success",
                        url:BASE_URL+"/api/order/OrderPayment/"+result._id
                    });
                })  .catch((e)=>{
                    return res.send({
                        status:false,
                        message:e.message
                    });
                })
                
            }
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },

    withOutParamUserGetCartBeforeCheckoutApi:async(req,res)=>{
        try{
            /*
            "user_id":"6654777c4fb55f99733594e1",
            "delivery_type":1,  // 1 = Imidiate 2 = future delivery 3=pickup 
            "code":"",
            "pickup_delivery_date":"",
            "pickup_delivery_time":"",
            "delivery_address_id":"666710078ae7ab488bad35ff",
            "payment_type":1 // 1=Stripe 2=Paypal
            */
            let {payment_type,delivery_address_id,pickup_delivery_time,pickup_delivery_date,code,delivery_type,user_id} = req.body;
            let check_deliver_type = 1;
            //shipping_charge coupon_code
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            /*if(!delivery_type)
            {
                return res.send({
                    status:false,
                    message:"Le type de livraison est requise"
                })
            }
            if(!pickup_delivery_date)
            {
                return res.send({
                    status:false,
                    message:"La date d'enlèvement ou de livraison est requise"
                })
            }
            if(!pickup_delivery_time)
            {
                return res.send({
                    status:false,
                    message:"L'heure d'enlèvement ou de livraison est requise"
                })
            }
            if(!payment_type)
            {
                return res.send({
                    status:false,
                    message:"Le type de paiement est requise"
                })
            }
            if(delivery_type == 1 || delivery_type == 2)
            {
                check_deliver_type = 1;
                if(!delivery_address_id)
                {
                    return res.send({
                        status:false,
                        message:"L'identifiant de l'adresse de livraison est requise"
                    })
                }
            }else{
                check_deliver_type = 2;
            } */
            const coupon = await CoupensModel.findOne({ code: code });
            const productsincoupon = coupon?.products || [];
            const coupontype = coupon?.type;
            const couponamount = coupon?.discountamount;
             
            let totalPrice = 0; let discounted_amount = 0;
            let totalcommission = 0; let totalshipping = 0; let totalplusperproduct = 0;
            let totaltax = 0; let totalproductsinorder = 0; let totalproductsinqty = 0;
            let tax_percant = 0;
            let shipping_cost = 0;
            let admin_commission = 0; 
            let admin_shipping_cost = 0; let imidiate_shipping_cost = 0;
            let stng_rec = await GlobalSettings.findOne({});
            if(stng_rec)
            {
                tax_percant = stng_rec.tax;
                admin_commission = stng_rec.commission;
                admin_shipping_cost = stng_rec.shipping_cost;
                imidiate_shipping_cost = stng_rec.imidiate_shipping_cost;
            }
            let start_time = 0; let end_time = 0;let day = 0;
            if(pickup_delivery_date)
            {   
                let date = pickup_delivery_date;
                date = date.split("/").reverse().join("-");
                day = new Date(date).getDay();

                console.log("date ", date);
                console.log("dd ", day);
                let ext_pickup_delivery_time = pickup_delivery_time.split("-");
                
                if(ext_pickup_delivery_time.length > 1)
                {
                    start_time = ext_pickup_delivery_time[0];
                    end_time = ext_pickup_delivery_time[1];
                    start_time = start_time.substring(0,2);
                    end_time = end_time.substring(0,2);
                }
            }
            
            //console.log("start_time ", start_time);
            //console.log("end_time ", end_time);

            let user_data = await User.findOne({_id:user_id});
            if(!user_data)
            {
                return res.send({
                    status:false,
                    message:"ID utilisateur invalide"
                })
            }
            let cart_data = user_data.cart;
            let address = user_data.address;
            let new_address = address.filter((vall)=>{
                if(vall._id == delivery_address_id)
                {
                    return vall;
                }
            })
            

            //console.log("new_address ", new_address);
            if(cart_data.length == 0)
            {
                return res.send({
                    status:false,
                    message:"Aucun article n'est disponible dans le panier"
                })
            }else{
                //mongoose.set("debug",true);
                let seller_id = cart_data[0].seller_id;
                let seller_availablity = await SellerTimeSloats.find({seller_id:seller_id,start_time:{$lte:start_time},end_time:{$gte:end_time},day_number:day,day_enable:true,delivery_type:check_deliver_type });
                //console.log("seller_availablity ", seller_availablity);
                // if(seller_availablity.length == 0)
                // {
                //     return res.send({
                //         status:false,
                //         message:"Le vendeur n'est pas disponible dans cette période"
                //     })
                // }

                let shop_id = cart_data[0].shop_id;
                let user_latitude = 0; let user_longitude = 0;
                let shop_latitude = 0; let shop_longitude = 0;
                let distance_in_meter = 0;
                if(delivery_address_id)
                {
                    if(new_address.length > 0)
                    {
                        //console.log("new_address ", new_address);
                        let location = new_address[0].location;
                        if(location.coordinates.length > 1)
                        {
                            user_latitude = location.coordinates[0];
                            user_longitude = location.coordinates[1];
                        }

                    }
                    let shop_record = await SellerBranchesModel.findOne({_id:shop_id});
                    if(shop_record)
                    {
                        let location = shop_record.location;
                        if(location.coordinates.length > 1)
                        {
                            shop_latitude = location.coordinates[0];
                            shop_longitude = location.coordinates[1];
                        }
                    }
                    console.log("user_latitude " , user_latitude);
                    console.log("user_longitude " , user_longitude);
                    console.log("shop_latitude " , shop_latitude);
                    console.log("shop_longitude " , shop_longitude);
                    
                    let my_url = `https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${user_latitude},${user_longitude}&destinations=${shop_latitude}%2C${shop_longitude}&key=${GOOGLE_KEY}`;
                    
                    const responseee = await axios.get(my_url);
                    //console.log(responseee);
                    //console.log("response ", responseee.data);
                    let new_body_param = responseee.data;
                    // console.log("body ", body);
                    // let new_body_param = JSON.parse(body);
                    //console.log("new_body_param ", new_body_param);
                    if(new_body_param)
                    {
                        if(new_body_param["rows"])
                        {
                            //console.log("dsfsf");
                            if(new_body_param.rows.length > 0)
                            {
                                if(new_body_param.rows[0].elements)
                                {
                                    if(new_body_param.rows[0].elements.length > 0)
                                    {
                                        if(new_body_param.rows[0].elements[0].status)
                                        {
                                            if(new_body_param.rows[0].elements[0].status == "OK")
                                            {
                                                console.log(new_body_param.rows[0].elements[0].distance)
                                                console.log(new_body_param.rows[0].elements[0].distance.value)
                                                if(new_body_param.rows[0].elements[0].distance)
                                                {
                                                    distance_in_meter = new_body_param.rows[0].elements[0].distance.value;
                                                    //return distance_in_meter;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //console.log("data ", data);
                }
                //console.log("distance_in_meter ", distance_in_meter);
                let distance_in_km = 0;
                if(distance_in_meter > 0)
                {
                    distance_in_km = distance_in_meter / 1000;
                    if(distance_in_km > 0)
                    {
                        distance_in_km = distance_in_km.toFixed(2);
                        admin_shipping_cost = admin_shipping_cost * 10;
                        shipping_cost = distance_in_km * admin_shipping_cost;
                        shipping_cost = parseFloat(shipping_cost); 
                    }
                }
                console.log("distance_in_km ", distance_in_km);
                console.log("shipping_cost ", shipping_cost);

                if(delivery_type == 1)
                {
                    shipping_cost = shipping_cost + imidiate_shipping_cost;
                }

                shipping_cost = shipping_cost.toFixed(2);
                shipping_cost = parseFloat(shipping_cost);
                //return false;
                if(delivery_type == 3)
                {
                    shipping_cost = 0;
                }

                let not_available_product = [];
                let products = [];
                for (let i = 0; i < cart_data.length; i++)
                {
                    //console.log(cart_data[i]);
                    if(cart_data[i].variation_id)
                    {
                        console.log("iffffffffffffffffffff 829");
                        //console.log("variation_id " , cart_data[i].variation_id);
                        //mongoose.set("debug",true);
                        await Product.aggregate([
                            {
                                $match:{
                                    _id:mongoose.Types.ObjectId(cart_data[i].product_id)
                                }
                            },
                            {
                                $project:{
                                    "sales_info_variation_prices":{
                                        $filter:{
                                            input:"$sales_info_variation_prices",
                                            as:"variation",
                                            cond:{
                                                    $eq:[ "$$variation._id",mongoose.Types.ObjectId(cart_data[i].variation_id) ]
                                                }
                                        }
                                    },
                                    reported: 1,
                                    dispatch_info: 1,
                                    category_id: 1,
                                    condition: 1,
                                    brand: 1,
                                    style: 1,
                                    price: 1,
                                    images: 1,
                                    dangerous_goods: 1,
                                    spec_attributes: 1,
                                    brand_name: 1,
                                    is_active: 1,
                                    enable_variation: 1,
                                    view_count: 1,
                                    seller_will_bear_shipping: 1,
                                    is_delisted: 1,
                                    is_deleted: 1,
                                    sales_info: 1,
                                    created_at: 1,
                                    sku: 1,
                                    stock: 1,
                                    title: 1,
                                    provider_id: 1,
                                    description: 1,
                                    catagory: 1,
                                    branches: 1,
                                    discover_by: 1
                                }
                            }
                        ]).then((product_rec)=>{
                            if(product_rec.length > 0)
                            {
                                
                                //console.log("iffff product_rec ",product_rec);
                                //console.log("ifffffff 826  ");
                                let new_product_rec = product_rec[0].sales_info_variation_prices.filter((val2)=>{
                                    //console.log("stock ", val2.stock);
                                    
                                    if(val2.stock < cart_data[i].quantity)
                                    {
                                       // console.log("hereeeee 833");
                                        return val2;
                                    }
                                })
                                if(new_product_rec.length > 0)
                                {
                                    not_available_product.push(cart_data[i]);
                                }else{
                                    //let shipping_cost = 0;
                                    //console.log("iffff product_rec 844", JSON.stringify(product_rec[0]));
                                    
                                    let plusperproduct = product_rec[0].sales_info_variation_prices[0].price * cart_data[i].quantity;
                                    let total_product_price_loop = product_rec[0].sales_info_variation_prices[0].price * cart_data[i].quantity;
                                    total_product_price_loop = parseFloat(total_product_price_loop);
                                     
                                    //When coupon applied here put less price in // pricetouser and discountedPrice also will be
                                    
                                    let discountedPrice = 0; let couponApplied = "";
                                    //When coupon applied here put less price in // pricetouser and discountedPrice also will be
                                    if (productsincoupon?.includes(cart_data[i].product_id ))
                                    {
                                        if (coupontype == "a")
                                        {
                                            if(product_rec[0].price >= couponamount)
                                            {
                                                // product["couponApplied"] = coupon.code;
                                                // product["discountedPrice"] = productprice - couponamount;
                                                
                                                // totalpaymentamount = totalpaymentamount + (productprice * qty);


                                                //discountedPrice = total_product_price_loop - couponamount;
                                                discountedPrice = couponamount;
                                                couponApplied = coupon.code;
                                            }
                                        } else if (coupontype == "p") {
                                            // product["couponApplied"] = coupon.code;
                                            // product["discountedPrice"] = productprice - (productprice * couponamount / 100);
                                            
                                            // totalpaymentamount = totalpaymentamount + (productprice * qty);
                                            discountedPrice = total_product_price_loop * couponamount /100;
                                            couponApplied = coupon.code;
                                            
                                        }
                                    }
                                    let after_discount_product_price = total_product_price_loop - discountedPrice;
                                    after_discount_product_price = after_discount_product_price.toFixed(2);
                                    after_discount_product_price = parseFloat(after_discount_product_price);

                                    let commission_price = after_discount_product_price * admin_commission / 100;
                                    commission_price = commission_price.toFixed(2);
                                    commission_price = parseFloat(commission_price);

                                    let sellerprofit = total_product_price_loop - commission_price;
                                    sellerprofit = sellerprofit.toFixed(2);
                                    sellerprofit = parseFloat(sellerprofit);

                                    //let tax = total_product_price_loop * tax_percant /100;
                                    let tax = after_discount_product_price * tax_percant /100;
                                    tax = tax.toFixed(2);
                                    tax = parseFloat(tax);

                                    let finalprice = total_product_price_loop + tax; 
                                    finalprice = finalprice.toFixed(2);
                                    finalprice = parseFloat(finalprice);

                                    let pricetouser = finalprice - discountedPrice;
                                    pricetouser = pricetouser.toFixed(2);
                                    pricetouser = parseFloat(pricetouser);


                                    discounted_amount = discountedPrice + discounted_amount;
                                    discounted_amount = discounted_amount.toFixed(2);
                                    discounted_amount = parseFloat(discounted_amount);

                                    sellerprofit = sellerprofit - discountedPrice;
                                    sellerprofit = sellerprofit.toFixed(2);
                                    sellerprofit = parseFloat(sellerprofit);

                                    totalPrice = totalPrice + finalprice;
                                    totalPrice = totalPrice.toFixed(2);
                                    totalPrice = parseFloat(totalPrice);

                                    totalcommission = totalcommission + commission_price;
                                    totalcommission = totalcommission.toFixed(2);
                                    totalcommission = parseFloat(totalcommission);

                                    //totalshipping = totalshipping + shipping_cost;
                                    totalplusperproduct = totalplusperproduct + plusperproduct;
                                    totalplusperproduct = totalplusperproduct.toFixed(2);
                                    totalplusperproduct = parseFloat(totalplusperproduct);

                                    totaltax = totaltax + tax; 
                                    totaltax = totaltax.toFixed(2);
                                    totaltax = parseFloat(totaltax);

                                    totalproductsinorder = cart_data.length; 
                                    totalproductsinqty = totalproductsinorder + cart_data[i].quantity;

                                    let part_id = parseInt((Math.random())*99999999);
                                    products.push( {
                                        discountedPrice:discountedPrice,
                                        couponApplied:couponApplied,
                                        part_id:part_id,
                                        product: product_rec[0],
                                        provider_id:product_rec[0].provider_id,
                                        product_price: product_rec[0].sales_info_variation_prices[0].price,
                                        quantity: cart_data[i].quantity,
                                        plusperproduct: plusperproduct,
                                        varientPrice: total_product_price_loop,
                                        tax:tax,
                                        finalprice:finalprice,
                                        pricetouser:pricetouser,
                                        commission:commission_price,
                                        sellerprofit:sellerprofit,
                                        shipping_cost:shipping_cost,
                                        variation_id: cart_data[i].variation_id
                                    });
                                }

                            }
                        })
                        // console.log("ifffffff ");
                        // console.log("product_rec ", product_rec);
                    }else{
                        console.log("elseeeeeeeeeeeeeeeeeeeee 980");
                        let product_rec = await Product.aggregate([
                            {
                                $match:{
                                    _id:mongoose.Types.ObjectId(cart_data[i].product_id)
                                }
                            },
                            {
                                $match:{
                                    stock:{$lt:cart_data[i].quantity}
                                }
                            }
                        ])
                        //console.log("product_rec ", product_rec);
                        // console.log("elseeeeeeee ");
                        // console.log("product_rec ", product_rec);
                        if(product_rec.length > 0)
                        {
                            
                            //console.log("hereeeeeee ");
                            not_available_product.push(cart_data[i]);
                        }
                        let product_rec_2 = await Product.aggregate([
                            {
                                $match:{
                                    _id:mongoose.Types.ObjectId(cart_data[i].product_id)
                                }
                            },
                            {
                                $match:{
                                    stock:{$gte:cart_data[i].quantity}
                                }
                            }
                        ])
                        //console.log("elseeeeee product_rec_2 ",product_rec_2);
                        if(product_rec_2.length > 0)
                        {
                            //let shipping_cost = 0;
                            //console.log("iffff product_rec_2 844", JSON.stringify(product_rec_2[0]));
                            let plusperproduct = product_rec_2[0].price * cart_data[i].quantity;
                            let total_product_price_loop = product_rec_2[0].price * cart_data[i].quantity;
                            total_product_price_loop = parseFloat(total_product_price_loop);
                             
                            let discountedPrice = 0; let couponApplied = "";
                            //When coupon applied here put less price in // pricetouser and discountedPrice also will be
                            if (productsincoupon?.includes(cart_data[i].product_id ))
                            {
                                if (coupontype == "a")
                                {
                                    if(product_rec_2[0].price >= couponamount)
                                    {
                                        // product["couponApplied"] = coupon.code;
                                        // product["discountedPrice"] = productprice - couponamount;
                                        // discounted_amount = discounted_amount + (productprice - couponamount) * qty;
                                        // totalpaymentamount = totalpaymentamount + (productprice * qty);


                                        //discountedPrice = total_product_price_loop - couponamount;
                                        discountedPrice = couponamount;
                                        couponApplied = coupon.code;
                                    }
                                } else if (coupontype == "p") {
                                    // product["couponApplied"] = coupon.code;
                                    // product["discountedPrice"] = productprice - (productprice * couponamount / 100);
                                    // discounted_amount = discounted_amount + (productprice - ((productprice * couponamount) / 100)) * qty;
                                    // totalpaymentamount = totalpaymentamount + (productprice * qty);
                                    discountedPrice = total_product_price_loop * couponamount /100;
                                    couponApplied = coupon.code;
                                    
                                }
                            }
                            //  else {
                            //     product["discountedPrice"] = productprice;
                            //     discounted_amount = discounted_amount + (productprice) * qty;
                            //     totalpaymentamount = (totalpaymentamount + productprice) * qty;
                            // }
                            
                            let after_discount_product_price = total_product_price_loop - discountedPrice;
                            after_discount_product_price = after_discount_product_price.toFixed(2);
                            after_discount_product_price = parseFloat(after_discount_product_price);

                            let commission_price = after_discount_product_price * admin_commission / 100;
                            commission_price = commission_price.toFixed(2);
                            commission_price = parseFloat(commission_price);

                            let sellerprofit = total_product_price_loop - commission_price;
                            sellerprofit = sellerprofit.toFixed(2);
                            sellerprofit = parseFloat(sellerprofit);

                            //let tax = total_product_price_loop * tax_percant /100;
                            let tax = after_discount_product_price * tax_percant /100;
                            tax = tax.toFixed(2);
                            tax = parseFloat(tax);

                            let finalprice = total_product_price_loop + tax; 
                            finalprice = finalprice.toFixed(2);
                            finalprice = parseFloat(finalprice);

                            let pricetouser = finalprice - discountedPrice;
                            pricetouser = pricetouser.toFixed(2);
                            pricetouser = parseFloat(pricetouser);


                            discounted_amount = discountedPrice + discounted_amount;
                            discounted_amount = discounted_amount.toFixed(2);
                            discounted_amount = parseFloat(discounted_amount);

                            sellerprofit = sellerprofit - discountedPrice;
                            sellerprofit = sellerprofit.toFixed(2);
                            sellerprofit = parseFloat(sellerprofit);

                            totalPrice = totalPrice + finalprice;
                            totalPrice = totalPrice.toFixed(2);
                            totalPrice = parseFloat(totalPrice);

                            totalcommission = totalcommission + commission_price;
                            totalcommission = totalcommission.toFixed(2);
                            totalcommission = parseFloat(totalcommission);

                            //totalshipping = totalshipping + shipping_cost;
                            totalplusperproduct = totalplusperproduct + plusperproduct;
                            totalplusperproduct = totalplusperproduct.toFixed(2);
                            totalplusperproduct = parseFloat(totalplusperproduct);

                            totaltax = totaltax + tax; 
                            totaltax = totaltax.toFixed(2);
                            totaltax = parseFloat(totaltax);

                            totalproductsinorder = cart_data.length; 
                            totalproductsinqty = totalproductsinorder + cart_data[i].quantity;
                            let part_id = parseInt((Math.random())*99999999);
                            products.push( {
                                discountedPrice:discountedPrice,
                                couponApplied:couponApplied,
                                part_id:part_id,
                                product: product_rec_2[0],
                                provider_id:product_rec_2[0].provider_id,
                                product_price: product_rec_2[0].price,
                                quantity: cart_data[i].quantity,
                                plusperproduct: plusperproduct,
                                varientPrice: total_product_price_loop,
                                tax:tax,
                                finalprice:finalprice,
                                pricetouser:pricetouser,
                                commission:commission_price,
                                sellerprofit:sellerprofit,
                                shipping_cost:shipping_cost,
                                variation_id: cart_data[i].variation_id
                            });
                        }
                    }
                    
                    

                }
                //console.log("not_available_product ", not_available_product);
                if(not_available_product.length > 0)
                {
                    return res.send({
                        status:false,
                        message:"Certains produits ne sont plus en stock",
                        record:not_available_product
                    });
                }
                //console.log("products ");
                //console.log(products);
                // "quantity" : 1,
                // "varientPrice" : 17729.38,
                // "discountedPrice" : 17729.38,
                // "pricetouser" : 17774.38,
                // "couponApplied" : "",
                // "tax" : 1772.938,
                // "shipping_cost" : 45,
                // "commission" : 1241.0566000000001,
                // "plusperproduct" : 0,
                // "finalprice" : 17774.38,
                // "sellerprofit" : 16488.3234,

               
                // "order_id" : 1033,
                // "shippingaddress" : {}, 
                let shippingaddress = {};
                if(new_address.length > 0)
                {
                    shippingaddress = new_address[0];
                }
                
                //console.log("products ", products);
                //return false;
                //
                //shipping_cost = 0;
                totalshipping = parseFloat(shipping_cost);
                totalshipping = totalshipping.toFixed(2);
                totalshipping = parseFloat(totalshipping);

                const combo_id = stringGen(20);
                console.log("totalPrice ", totalPrice);
                console.log("totalcommission ", totalcommission);
                console.log("totalshipping ", totalshipping);
                console.log("totalplusperproduct ", totalplusperproduct);
                console.log("totaltax ", totaltax);
                console.log("totalproductsinorder ", totalproductsinorder);
                console.log("totalproductsinqty ", totalproductsinqty);
                console.log("discounted_amount ", discounted_amount);
                console.log("payment_amount ", totalPrice);

                totalPrice = totalPrice + totalshipping;
                totalPrice = totalPrice.toFixed(2);
                totalPrice = parseFloat(totalPrice);

                let after_promo = totalPrice - discounted_amount;
                after_promo = after_promo.toFixed(2);
                after_promo = parseFloat(after_promo);
                
                let insert_data = {
                    delivery_type:delivery_type,
                    userId:user_id,
                    //"products":products,
                    "combo_id" : combo_id,
                    "totalshipping" : totalshipping,
                    "totalplusperproduct" : totalplusperproduct,
                    "totaltax" : totaltax,
                    "totalproductsinorder" : totalproductsinorder,
                    "totalproductsinqty" : totalproductsinqty,
                    "payment_status" : false,
                    "promo_code" : code,
                    "payment_method" :  payment_type,
                    "transaction_id" : "",
                    "date_of_transaction" : new Date(),
                    "status" : 0,
                    "delivery_status" : 0,
                    "date_of_delivery" : null,
                    "returned_reason" : null,
                    "totalPrice" : after_promo,
                    "totalcommission" : totalcommission,
                    "discounted_amount":discounted_amount,
                    "payment_amount":totalPrice,
                    "shippingaddress":shippingaddress
                }  
                return res.send({
                    status:true,
                    message:"Success",
                    record:insert_data
                });
                
            }
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    updateVariationChange:async(req,res)=>{
        try{
            //console.log("updateVariationChange ", req.body); 
            let {product_id,variation_id,cart_id,user_id} = req.body;
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            if(!product_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du produit est requise"
                })
            }
            if(!variation_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de la variation est requis"
                })
            }
            if(!cart_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du panier est requis"
                })
            }
            let cart_record = await User.aggregate([
                {
                    $match:{
                        _id:mongoose.Types.ObjectId(user_id)
                    }
                }
            ]);
            if(cart_record.length > 0)
            {
                let all_new_cart_filter = cart_record[0].cart.filter((val)=>{
                    if(val._id == cart_id)
                    {
                        val.variation_id = variation_id;
                    }
                    return val;
                });

                await User.updateOne({
                    _id:user_id
                },{
                    cart:all_new_cart_filter
                });
                let cart_rec = await User.findOne({_id:user_id},{cart:1});
                return res.send({
                    status:true,
                    message:"La taille et la couleur de l'article de votre panier ont été modifiées avec succès",
                    record:cart_rec
                })
            }else{
                return res.send({
                    status:false,
                    message:"ID non valide"
                })
            }
            
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    }
}























































