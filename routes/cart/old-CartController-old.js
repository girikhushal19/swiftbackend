const { default: mongoose } = require("mongoose");
const { Product } = require("../../models/products/Product");
const Seller = require("../../models/seller/Seller");
const User= require("../../models/user/User");
const VariationsPrice =require("../../models/products/VariationPrice");
const GlobalSettings=require("../../models/admin/GlobalSettings");
const orders = require("../../models/orders/order");
const CoupensModel=require("../../models/seller/Coupens");
const CommissionModel=require("../../models/admin/Commissions");
const {calculateShippingcostforproducts} =require("../../routes/shipping/ShippingController");
const {createordersfunc}=require("../orders/OrderController");
const {groupBy,groupByfunc}=require("../../modules/Utility");
const stringGen=require("../../modules/randomString");
const BASE_URL=process.env.BASE_URL;
const fs = require('fs');

module.exports = {
    addProducttocart:async(req,res)=>{

         //console.log(req.body);
        // {
        //     userid: '641c3f7deaa2ea2bb402d45b',
        //     productid: '64ae881204cf123c7963111c',
            
        //     variation_id: '64b4040cbc294a44ee48073e',
        //     variations: [],
        //     price: 16000,
        //     quantity: 3
        //   }

        var first_variation_image_name = req.body.first_variation_image_name;
        var firstVariationValue = req.body.firstVariationValue;
        var secondVariationValue = req.body.secondVariationValue;
        var secondVariationName = req.body.secondVariationName;
        
        const userid=req.body.userid;
        const productid=req.body.productid;
        const price=req.body.price;
        var variation_id = req.body.variation_id;
        if(variation_id)
        {
            //console.log("iffff 39");
            const vari_record = await VariationsPrice.findOne({product_id:productid});
            console.log("vari_record ",vari_record);
            let is_first_variation = vari_record.variation_option_1;
            let is_second_variation = vari_record.variation_option_2;

            // console.log("is_first_variation ",is_first_variation);
            // console.log("is_second_variation ",is_second_variation);

            // console.log("======================================");
            // console.log("first_variation_image_name ",first_variation_image_name);
            // console.log("secondVariationValue ",secondVariationValue);

            if(is_first_variation != "" && is_second_variation != "")
            {
                if(first_variation_image_name == "" || first_variation_image_name == null || first_variation_image_name == undefined || secondVariationValue == "" || secondVariationValue == null || secondVariationValue == undefined)
                {
                    //console.log("iffff 41");
                    return res.send({
                        status:false,
                        message:"Veuillez sélectionner une variante"
                    })
                }
            }else if(is_first_variation != "")
            {
                if(first_variation_image_name == "" || first_variation_image_name == null || first_variation_image_name == undefined )
                {
                    //console.log("iffff 41");
                    return res.send({
                        status:false,
                        message:"Veuillez sélectionner une variante"
                    })
                }
            }else if(is_second_variation != "")
            {
                if(secondVariationValue == "" || secondVariationValue == null || secondVariationValue == undefined)
                {
                    //console.log("iffff 41");
                    return res.send({
                        status:false,
                        message:"Veuillez sélectionner une variante"
                    })
                }
            }
            //return false;
            variation_id=mongoose.Types.ObjectId(req.body.variation_id);
            
        }
        //return false;
        // const variations=req.body.variations;
        const quantity=req.body.quantity;
        const product= await Product.findById(productid);
        let variationdata=[];
        // if(variation_id){
        //     variationdata=await VariationsPrice.findById(variation_id)
        // }else{
        //     variationdata=product
        // }
        const cartdata={
            product:product,
            quantity:quantity,
            price:price,
            variation_id:variation_id,
            variation_image:first_variation_image_name,
            variation_color:firstVariationValue,
            variation_value:secondVariationValue,
            variation_name:secondVariationName
        
        }
        //console.log("cartdata",cartdata);return false;
        const cart=(await User.findById(userid))?.cart;
        let isproductthere=cart?.filter(e=>{
            if(e.product)
            {
                let upid=e.product[0]?._id+"";
                let variid=e.variation_id;

                let variation_color=e.variation_color;
                let variation_image=e.variation_image;
                let variation_name=e.variation_name;
                let variation_value=e.variation_value;

                let iscond= (upid==productid)&&(variid==variation_id)
                
                console.log("e.product._id+",productid,e.product[0]._id,e.product._id+""==productid,"e.variation_id==variation_id",e.variation_id,variation_id,e.variation_id==variation_id)
                if(upid==productid &&  variation_image == first_variation_image_name &&  variation_color == firstVariationValue &&  variation_name == secondVariationName &&  variation_value == secondVariationValue)
                {
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
            
        })[0];
        //console.log("product isproductthere",isproductthere)
        let exceptproduct=cart?.filter(e=>e._id+""!=isproductthere?._id+"");
        if(isproductthere){
            cartdata["quantity"]=isproductthere.quantity+quantity;
            // exceptproduct.concat(cartdata);
            exceptproduct.push(cartdata);
            await User.findByIdAndUpdate(userid,{
                "cart":exceptproduct
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"Produit ajouté avec succès"
                })
            })
        }else{
            await User.findByIdAndUpdate(userid,{
                "$push":{"cart":cartdata}
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"Produit ajouté avec succès"
                })
            })
        }
       
        
    },
    deleteProductfromcart:async(req,res)=>{
        
        try{
            const userid=req.body.userid;
            const productid=req.body.productid;
            // const productidOBJ=mongoose.Types.ObjectId(productid);
            const user=await User.findById(userid);

            const cartdata=user?.cart?.filter((e)=>{
                const stringid=e.product[0]?._id+"";
                 console.log("stringid",stringid,"productid",productid,productid==stringid);
                 //return false;
                return stringid!=productid
            });
            //console.log("cartdata",cartdata);return false;

            user.cart=cartdata;
            user.save().then((result)=>{
                return res.send({
                    status:true,
                    message:"Produit retiré avec succès"
                })
            }).catch((error)=>{
                console.log(error);
            })
        }catch(error)
        {
            console.log(error);
        }
        

        
    },
    updatecart:async(req,res)=>{
        const userid=req.body.userid;
        // const productwithquantity=[
        //     {
        //         productid:"",
        //         quantity:""
        //     }
        // ]
        const productwithquantity=req.body.productwithquantity;
        let cartdata=[];
       for(let k=0;k<productwithquantity.length;k++){
            let e=productwithquantity[k];
            let product=await Product.findById(e.productid);
            let quantity=e.quantity;
            let price=e.price;
            let variation_id=e.variation_id;
            cartdata.push({
                product:product,
                quantity:quantity,
                price:price,
                variation_id:variation_id
            })
           
        };
       
        await User.findByIdAndUpdate(userid,{
            "cart":cartdata
        }).then((result)=>{
            return res.send({
                status:true,
                data:result,
                message:"Produit ajouté avec succès"
            })
        })
    },
    updateCartQuantity:async(req,res)=>{
        try{
            var {user_id,id} = req.body;
            var qtt = req.body.quantity;
            if(id == "" || id == null || id == undefined)
            {
              return res.send({
                status: false,
                message: "L'identifiant du panier n'est pas valide"
              })
            }
            if(user_id == "" || user_id == null || user_id == undefined)
            {
              return res.send({
                status: false,
                message: "L'identifiant de l'utilisateur est requis"
              })
            }

            var record = await User.findOne({_id:user_id},{cart:1});
            console.log(record);
            if(!record)
            {
                return res.send({
                    status: false,
                    message: "Identifiant de l'utilisateur invalide"
                  })
            }
            if(record.cart.length == 0)
            {
                return res.send({
                    status: false,
                    message: "L'identifiant du panier n'est pas valide"
                  })
            }
            //console.log("record.cart.length",record.cart.length);
            let cartdata=[];
            record.cart.forEach((val)=>{
                console.log("hereeee ifff 182");
                //console.log(val.product[0]._id);
                    console.log(val._id.toString());
                    var quantity = val.quantity;
                    var product = val.product;
                    var price = val.price;
                    var variation_id = val.variation_id;
                    console.log("val._id.toString() ", val._id.toString() );
                    console.log("id ", id );
                    if(val._id.toString() == id)
                    {
                        console.log("hereeee ifff 189");
                        quantity = qtt;
                    }
                    //console.log("quantity ifff ",quantity);
                    cartdata.push({
                        product:product,
                        quantity:quantity,
                        price:price,
                        variation_id:variation_id,
                        variation_image:val.variation_image ? val.variation_image : '',
                        variation_color:val.variation_color ? val.variation_color :'',
                        variation_value:val.variation_value ? val.variation_value : '',
                        variation_name:val.variation_name ? val.variation_name : '',
                    })
            });
            //console.log(cartdata);
            await User.findByIdAndUpdate(user_id,{
                "cart":cartdata
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"Quantité mise à jour succès complet"
                })
            }).catch((error)=>{
                return res.send({
                    status:false,
                    message:error.message
                })
            });
        }catch(error){
            return res.send({
                status:false,
                message:error.message
            })
        }
    },
    clearcart: async(req,res)=>{
        const userid=req.body.userid;
        const user=await User.findById(userid);
        user.cart=[]
        user.save().then((result)=>{
            return res.send({
                status:true,
                message:"Élément du panier supprimé avec succès"
            })
        })
    },
    getcartbyuserid1: async(req,res)=>{
        const userid=req.body.userid;
        const user=await User.aggregate([
            {
            $match:{_id:mongoose.Types.ObjectId(userid)}
            },
            {
                $unwind:"$cart"
            },

            {
                $lookup:{
                    from:"variationprices",
                    let:{variation_idObj:{
                        $cond:{
                            if:{$ne:["$cart.variation_id",""]},
                            then:{"$toObjectId":"$cart.variation_id"},
                            else:""
                        }
                    }},
                    pipeline:[
                        {$match:{
                            $expr:{
                                $eq:["$_id","$$variation_idObj"]
                               
                            }
                        }}
                    ],
                    as:"variationsobject"
                }
            },
            { "$unwind": "$variationsobject" },
            {
                $addFields:{
                    // singlecart:{$arrayElemAt:["$cart",0]},
                    // singlevariations:{$arrayElemAt:["$variationsobject",0]},
                     singlevariations:"$variationsobject",
                    singleproduct:{$arrayElemAt:["$cart.product",0]}
                 }
            },
            
            {
                $addFields:{
                    phototoshow:{
                        $cond:{
                            if:{$gt:["$singlevariations.images",null]},
                            then:{
                                $cond:{
                                    if:{$gt:[{$size:"$singlevariations.images"},0]},
                                    // then:{$gt:[{$size:"$singlevariations.images"},0]},
                                    // else:{$gt:[{$size:"$singlevariations.images"},0]}
                                    then:"$singlevariations.images",
                                    else:{
                                        $cond:{
                                            if:{$gt:[{$size:"$singleproduct.images"},0]},
                                            // then:{$gt:[{$size:"$singlevariations.images"},0]},
                                            // else:{$gt:[{$size:"$singlevariations.images"},0]}
                                            then:"$singleproduct.images",
                                            else:[]
                                        }
                                    }
                                }
                            },
                            // then:"$singlevariations.images",
                            else:{
                                $cond:{
                                    if:{$gt:[{$size:"$singleproduct.images"},0]},
                                    // then:{$gt:[{$size:"$singlevariations.images"},0]},
                                    // else:{$gt:[{$size:"$singlevariations.images"},0]}
                                    then:"$singleproduct.images",
                                    else:[]
                                }
                            }
                            }
                        }
                    // 
                    // phototoshow:[]
                }
            },
            {
                $addFields:{
                   cartnew:{
                       "$mergeObjects": [
                           "$cart",
                           "$singlevariations",
                           {
                               phototoshow:"$phototoshow" 
                           },
                           {
                            singlevariations:"$singlevariations"
                           }
                         ]
                   }
                }
               },
            // {$unwind:"$cartnew"},
            { "$group": {
                "_id": "$_id",
               
                "cart": { "$push": "$cartnew" },
                // "photostoshow":{$push:"$phototoshow"}
                // "cart.variation":{ "$push": "$variations" }
                // "productObjects": { "$push": "$variatio" }
            }}
    ]);
    console.log("user",user)
        const totalprice=user[0]?.cart?.reduce((acc,e)=>{
            return acc+e.price
        },0)
        // const totalprice=0
        return res.send({
            status:true,
            message:"Panier récupéré avec succès",
            data:user[0],
            totalprice:totalprice||0
        })
    },
    getcartbyuserid: async(req,res)=>{
        const userid=req.body.userid;
        const cart=(await User.findById(userid))?.cart;
        //console.log("cart ",cart);
        const newcartdata=[]
        let exceptproduct = [];
        let all_available = true;
        for(let i=0;i<cart?.length;i++)
        {
            let elem=cart[i];
            //console.log("elem ",elem);
            let varid="";
            let quantity = elem.quantity;
            if(elem.variation_id)
            {
                //varid=mongoose.Types.ObjectId(elem.variation_id);
                let vari_rec = await VariationsPrice.findOne({_id:elem.variation_id});
                //console.log(vari_rec);
                //console.log("elem.product",elem.product);
                // console.log("vari_rec.variation_option_2",vari_rec.variation_option_2);
                // console.log("vari_rec.price",vari_rec.price);
                // console.log("vari_rec.stock",vari_rec.stock);
                //console.log("elem.variation_value",elem.variation_value);
                let priceee = elem.price;
                
                
                let is_available = elem.is_available;
                if(vari_rec.variation_option_2 && vari_rec.price && vari_rec.stock)
                {
                    if(vari_rec.variation_option_2.length > 0 && vari_rec.price.length > 0 && vari_rec.stock.length > 0)
                    {
                        let indexxx = vari_rec.variation_option_2.indexOf(elem.variation_value);
                        //console.log("elem.variation_value  ", elem.variation_value);
                        
                        if(vari_rec.price.length >= indexxx && vari_rec.stock.length >= indexxx)
                        {
                            // console.log("vari_rec.stock  ", vari_rec);
                            // console.log("indexxx  ", indexxx);
                            priceee = vari_rec.price[indexxx];
                            quantity = vari_rec.stock[indexxx];
                            if(vari_rec.stock[indexxx] >= elem.quantity)
                            {
                                // console.log("here 457 ", elem.product);
                                 console.log("vari_rec.stock[indexxx] ", vari_rec.stock[indexxx]);
                                //console.log("elem.quantity ", elem.quantity);
                                is_available = true;
                            }else{
                               // console.log("here 470 elseeeeee ", elem.product);
                                is_available = false;
                                all_available  = false;
                            }
                        }
                    }
                }

                // console.log("here 477 ");
                // console.log("is_available ",is_available);
                // console.log("is_available i", i);
                const cartdata={
                    product:elem.product,
                    quantity:elem.quantity,
                    price:priceee,
                    variation_id:elem.variation_id,
                    variation_image:elem.variation_image ? elem.variation_image : '',
                    variation_color:elem.variation_color ? elem.variation_color :'',
                    variation_value:elem.variation_value ? elem.variation_value : '',
                    variation_name:elem.variation_name ? elem.variation_name : '',
                    is_available:is_available,
                    total_quantity:quantity
                }
                exceptproduct.push(cartdata);

                // variation_option_2: [ '128 GB', '256 GB' ],
                // price: [ 1600, 1620 ],
                // stock: [ 15, 2 ],

                
            }else{
                //console.log("elem product id ",elem.product[0]._id);
                //console.log("elem first_variation_image_name ",elem);
                if(elem.product)
                {
                    if(elem.product.length > 0)
                    {
                        let idd = elem.product[0]._id;
                        let product_rec = await Product.findOne({_id:idd} );
                        //console.log("product_rec ",product_rec);

                        let is_available = true;
                        if(product_rec)
                        {
                            // console.log("elem.quantity ",elem.quantity);
                            // console.log("product_rec.stock ",product_rec.stock);
                            quantity = product_rec.stock;
                            if(product_rec.stock <= elem.quantity)
                            {
                                is_available = false;
                                all_available  = false;
                            }
                        // console.log("is_available ",is_available);
                            let  aaaa = [];
                            aaaa.push(product_rec);
                            const cartdata={
                                product:aaaa,
                                quantity:elem.quantity,
                                price:product_rec.price,
                                variation_id:elem.variation_id,
                                variation_image:elem.variation_image ? elem.variation_image : '',
                                variation_color:elem.variation_color ? elem.variation_color :'',
                                variation_value:elem.variation_value ? elem.variation_value : '',
                                variation_name:elem.variation_name ? elem.variation_name : '',
                                is_available:is_available,
                                total_quantity:quantity
                            }
                            exceptproduct.push(cartdata);
                        }
                    }
                }
                
                


                 
                // exceptproduct.concat(cartdata);
                
                


            }
            //console.log("elem ",elem);
            
        }
        // console.log("exceptproduct ", JSON.stringify(exceptproduct));
        // console.log("cart ", JSON.stringify(cart));
        console.log("response  line 567");
        await User.findByIdAndUpdate(userid,{
                    "cart":exceptproduct
                });
        
        const totalprice=cart?.reduce((acc,e)=>{
            return acc+(e?.price)*(e?.quantity)
        },0)
        const final_cart_value = (await User.findById(userid))?.cart;
        //console.log("final_cart_value 574",final_cart_value);
        console.log("response  line 576");
        return res.send({
            status:true,
            message:"Panier récupéré avec succès",
            data:{cart:final_cart_value},
            all_available:all_available,
            totalprice:totalprice||0
        })
    },
    getcartbyuseridCopy: async(req,res)=>{
        const userid=req.body.userid;
        const cart=(await User.findById(userid))?.cart;
        
        return res.send({
            status:true,
            message:"Panier récupéré avec succès",
            data:{cart:cart}
        })
    },
    checkoutbuynowprev:async(req,res)=>{
        const { 
        product_id,
        qty,
        address,
        variation_id,
        from,
        price,
        shipping_city,
        id,
        code
        }=req.body;
        let userId=id;
        const coupon=await CoupensModel.findOne({code:code});
        const user= await User.findById(id);
        // console.log("req.body",req.body)
        let shippingmethod;
        const shipping_address=user.address.filter(e=>e.is_shipping_address==true)[0]||user.address[0];
        const billing_address=user.address.filter(e=>e.is_billing_address==true)[0]||user.address[0];
        // console.log("shipping_address",shipping_address,"billing_address",billing_address)
        const productsincoupon=coupon?.products||[];
        const coupontype=coupon?.type;
        const couponamount=coupon?.discountamount;
        const nproduct=await Product.findById(product_id);
        const products=[
            {product:nproduct,
            quantity:qty,
            price:price,
            variation_id:variation_id
        }
        ]
        const istheresomethingincart=products.length;
        let totalprice=0;
        let totalcommission=0;
        let totalshippingcost=0;
        let totalproductsinqty=0;
        let totalproductsinorder=0;
        let finalprice=0;
        let totaltax=0;
        let discounted_amount=0;
        let totalpaymentamount=0;
        let totalplusperproduct=0;
        if(istheresomethingincart){
            let totalprice=0
            totalproductsinorder=products.length;
            const shippingcostsArray=[];
            if (products.length) {
                
                for(let i=0;i<products.length;i++){
                    product=products[i]
                    let seller_will_bear_shipping=product.seller_will_bear_shipping;
                    // totalproductsinqty=totalproductsinqty+product.quantity;
                    let productprice=price;
                    // console.log("productprice=====>",productprice,"product.varientPrice",product.varientPrice,"product?.price",product?.price)
                    let data3={
                        productprice:productprice,
                        product_id:product.product._id,
                        product_qty:product.quantity,
                        shipping_city:shipping_city
                    }
                   await calculateShippingcostforproducts(data3).then((result)=>{
                    
                        // console.log("result",result)
                        if(!result.result.length){
                            res.send({
                            status:false,
                            message:"Non expédiable à cet endroit"
                           })
                           
                        product["is_shippable"]=false;
                        product["shipping_cost"]=0;
                        product["plusperproduct"]=0;
                        }else{
                            if(productsincoupon?.includes(product.product._id.toString())){
                                // console.log("yes",productsincoupon)
                                if(coupontype=="a"){
                                    product["couponApplied"]=coupon.code;
                                    product["discountedPrice"]=productprice-couponamount;

                                    discounted_amount=(productprice-couponamount)*qty;
                                    totalpaymentamount=(totalpaymentamount+productprice)*qty;
                                    // console.log("in another if else a","productprice",productprice,'product["discountedPrice"]',product["discountedPrice"])
                                }else if(coupontype=="p"){
                                    product["couponApplied"]=coupon.code;
                                    product["discountedPrice"]=productprice-(productprice*couponamount/100);
                                    discounted_amount=(productprice-((productprice*couponamount)/100))*qty;
                                    totalpaymentamount=(totalpaymentamount+productprice)*qty;
                                    // console.log("in another if else p","productprice",productprice,'product["discountedPrice"]',product["discountedPrice"])
                                }else{
                                    // console.log("in another if else none","productprice",productprice,'product["discountedPrice"]',product["discountedPrice"])
                                }
                            }else{
                                // console.log("in another else","productprice",productprice,'product["discountedPrice"]',product["discountedPrice"])
                                product["discountedPrice"]=productprice;
                                // discounted_amount=0;
                                totalpaymentamount=(totalpaymentamount+productprice)*qty;
                            }
                           
                            totalshippingcost=totalshippingcost+result.result[0].shippingcostforproducts;
                            product["shipping_cost"]=result.result[0].shippingcostforproducts
                        }
                        
                        product["plusperproduct"]= result.plusperproduct;
                        totalplusperproduct+=result.plusperproduct;
                        product["varientPrice"]=productprice
                        product["tax"]=result.producttax
                        totaltax=totaltax+product["tax"];
                        product["commission"]=result.productcommission
                        shippingmethod=result.result[0];
                        product["shipping_details"]=shippingmethod;
                        totalcommission+=result.productcommission
                        finalprice=result.plusperproduct+product["shipping_cost"]+product["discountedPrice"]*qty+result.productcommission+result.producttax;
                        // console.log(result.plusperproduct,"result.plusperproduct","product[shipping_cost]",product["shipping_cost"],"product[discountedPrice]",product["discountedPrice"]*qty,'product["discountedPrice"]*qty',"result.productcommission"+result.producttax)
                        let sellerprofit=0;
                        product["finalprice"]=finalprice
                        
                        let pricetouser=0;
                        if(seller_will_bear_shipping){
                            pricetouser=product["discountedPrice"]*qty;
                            sellerprofit=product["discountedPrice"]*qty-result.plusperproduct-product["shipping_cost"]-result.productcommission-result.producttax;
                        }else{
                            pricetouser=product["discountedPrice"]*qty+product["shipping_cost"]
                            sellerprofit=product["discountedPrice"]*qty-result.plusperproduct-result.productcommission-result.producttax;
                        }
                        product["sellerprofit"]=sellerprofit
                        product["pricetouser"]=pricetouser
                        product["variation_id"]=product.variation_id
                        // console.log("finalprice",finalprice)
                        // data={
                        //     productcommission:result.productcommission,
                        //     producttax:result.producttax,
                        //     shippingcost:result[0].costforproducts
                        // }
                        // shippingcostsArray.push(data);
                    })
                   
                }

                totalprice = products?.reduce((accm, elem) => {
                    // console.log("elem", elem)
                    if ((elem?.finalprice) && elem.quantity) {
                        totalproductsinqty+=elem.quantity;
                        let productprice=elem?.finalprice;
                        return accm + (productprice )
                    }
                    return accm;
                }, 0);
            }
            
          try{
            if (0) {
                // console.log("products=============>",products)
                const datatoupdate = {
                    ...(products.length && { products: products }),
                    ...(address && { address: address }),
                   
                    ...(totalprice && { totalPrice: totalprice }),
                    ...(totalcommission && { totalcommission: totalcommission }),
                    ...(products.length&&{totalproductsinorder:products.length}),
    
                }
                await orders.findByIdAndUpdate(id, datatoupdate)
                    .then((result) => {
                        return res.send({
                            status: true,
                            message: "La commande a été mise à jour avec succès"
                        })
                    })
            } else {
                const newOrder = new orders();
                newOrder.products = products;
                newOrder.address = address;
                newOrder.totalproductsinorder=totalproductsinorder;
                newOrder.totalproductsinqty=totalproductsinqty
                newOrder.userId = userId;
                newOrder.shipping_details=shippingmethod;
                newOrder.totaltax = totaltax
                newOrder.discounted_amount=discounted_amount;
                newOrder.totalPrice = totalprice;
                newOrder.totalplusperproduct=totalplusperproduct;
                 newOrder.totalcommission = totalcommission;
                 newOrder.payment_amount=totalpaymentamount;
                 newOrder.totalshipping = totalshippingcost;
                 newOrder.shippingaddress=shipping_address;
                 newOrder.billingaddress=billing_address;
                // newOrder.totalproductsinorder = products.length;
                newOrder.save().then((result) => {
                    if(result){
                        const url=BASE_URL+"/api/common/getpayment/"+result._id+"/"+result.totalPrice+"/"+result.userId+"/"+from;
                        // console.log("url",url)
                        return res.send({
                            status:true,
                            url:url,
                            message:"success"
                        })
                    }else{
                        return res.send({
                            status:false,
                            message:"failed"
                        })
                    }
                })
            }
          }catch(e){
            
            return res.send({
                status:false,
                message:e.message
            })
          }
           
          
        }else{
            return res.send({
                status:false,
                message:"Le panier est vide"
            })
        }

    },
    checkoutbuynow: async (req, res) => {
    const {
        product_id,
        qty,
        address,
        variation_id,
        variations,
        from,
        price,
        shipping_city,
        id,
        code
    } = req.body;
    let userId = id;
    const coupon = await CoupensModel.findOne({ code: code });
    const user = await User.findById(id);
    const notshippalbeproduct=[]
    // console.log("req.body", req.body)
    let shippingmethod;
    const shipping_address = user.address.filter(e => e.is_shipping_address == true)[0] || user.address[0];
    const billing_address = user.address.filter(e => e.is_billing_address == true)[0] || user.address[0];
    // console.log("shipping_address",shipping_address,"billing_address",billing_address)
    const productsincoupon = coupon?.products || [];
    const coupontype = coupon?.type;
    const couponamount = coupon?.discountamount;
    const nproduct = await Product.findById(product_id);
    //console.log("nproduct "+nproduct);
    const products = [
        {
            product: nproduct,
            provider_id:nproduct.provider_id,
            quantity: qty,
            price: price,
            variation_id: variation_id,
            variations:variations
        }
    ]
    const istheresomethingincart = products.length;
    let totalprice = 0;
    let totalcommission = 0;
    let totalshippingcost = 0;
    let totalproductsinqty = 0;
    let totalproductsinorder = 0;
    let finalprice = 0;
    let totaltax = 0;
    let discounted_amount = 0;
    let totalpaymentamount = 0;
    let totalplusperproduct = 0;
    if (istheresomethingincart) {
        let totalprice = 0
        totalproductsinorder = products.length;
        const shippingcostsArray = [];
        if (products.length) {

            for (let i = 0; i < products.length; i++) {
                product = products[i]
                let seller_will_bear_shipping = product.seller_will_bear_shipping;
                // totalproductsinqty=totalproductsinqty+product.quantity;
                let productprice = price;
                // console.log("productprice=====>", productprice, "product.varientPrice", product.varientPrice, "product?.price", product?.price)
                let data3 = {
                    productprice: productprice,
                    product_id: product.product._id,
                    product_qty: product.quantity,
                    shipping_city: shipping_city
                }
                 const provider=await Seller.findById(product.provider_id)
                    product["provider_name"]=provider?.fullname;
                    product["provider_id"]=provider?._id;
                    product["part_id"]=parseInt((Math.random())*99999999);
                   // console.log("data3 --->>>>> " +JSON.stringify(data3));return false;
                await calculateShippingcostforproducts(data3).then((result) => {

                    //console.log("result", result);return false;
                     if (!result ) {
                    //if (!result.result.length) {
                        notshippalbeproduct.push(product);
                        return
                    //   return  res.send({
                    //         status: false,
                    //         message: "not shippable at this location"
                    //     })

                        product["is_shippable"] = false;
                        product["shipping_cost"] = 0;
                        product["plusperproduct"] = 0;
                    } else {
                        if (productsincoupon?.includes(product.product._id.toString())) {
                            // console.log("yes", productsincoupon)
                            if (coupontype == "a") {
                                product["couponApplied"] = coupon.code;
                                product["discountedPrice"] = productprice - couponamount;

                                discounted_amount = (productprice - couponamount) * qty;
                                totalpaymentamount = (totalpaymentamount + productprice) * qty;
                                // console.log("in another if else a", "productprice", productprice, 'product["discountedPrice"]', product["discountedPrice"])
                            } else if (coupontype == "p") {
                                product["couponApplied"] = coupon.code;
                                product["discountedPrice"] = productprice - (productprice * couponamount / 100);
                                discounted_amount = (productprice - ((productprice * couponamount) / 100)) * qty;
                                totalpaymentamount = (totalpaymentamount + productprice) * qty;
                                // console.log("in another if else p", "productprice", productprice, 'product["discountedPrice"]', product["discountedPrice"])
                            } else {
                                console.log("in another if else none", "productprice", productprice, 'product["discountedPrice"]', product["discountedPrice"])
                            }
                        } else {
                            // console.log("in another else", "productprice", productprice, 'product["discountedPrice"]', product["discountedPrice"])
                            product["discountedPrice"] = productprice;
                            // discounted_amount=0;
                            totalpaymentamount = (totalpaymentamount + productprice) * qty;
                        }
                        if(result)
                        {
                            totalshippingcost = totalshippingcost + result.result[0].shippingcostforproducts;
                            product["shipping_cost"] = result.result[0].shippingcostforproducts
                        }else{
                            product["shipping_cost"] = totalshippingcost
                        }
                        
                    }

                    product["plusperproduct"] = result.plusperproduct;
                    totalplusperproduct += result.plusperproduct;
                    product["varientPrice"] = productprice
                    product["tax"] = result.producttax
                    totaltax = totaltax + product["tax"];
                    product["commission"] = result.productcommission
                    shippingmethod = result.result[0];
                    product["shipping_details"] = shippingmethod;
                    totalcommission += result.productcommission
                    finalprice = result.plusperproduct + product["shipping_cost"] + product["discountedPrice"] * qty + result.productcommission + result.producttax;
                    // console.log(result.plusperproduct,"result.plusperproduct","product[shipping_cost]",product["shipping_cost"],"product[discountedPrice]",product["discountedPrice"]*qty,'product["discountedPrice"]*qty',"result.productcommission"+result.producttax)
                    let sellerprofit = 0;
                    product["finalprice"] = finalprice

                    let pricetouser = 0;
                    if (seller_will_bear_shipping) {
                        pricetouser = product["discountedPrice"] * qty;
                        sellerprofit = product["discountedPrice"] * qty - result.plusperproduct - product["shipping_cost"] - result.productcommission - result.producttax;
                    } else {
                        pricetouser = product["discountedPrice"] * qty + product["shipping_cost"]
                        sellerprofit = product["discountedPrice"] * qty - result.plusperproduct - result.productcommission - result.producttax;
                    }
                    product["sellerprofit"] = sellerprofit
                    product["pricetouser"] = pricetouser
                    product["variation_id"] = product.variation_id
                    product["variations"] = product.variations
                    // console.log("finalprice",finalprice)
                    // data={
                    //     productcommission:result.productcommission,
                    //     producttax:result.producttax,
                    //     shippingcost:result[0].costforproducts
                    // }
                    // shippingcostsArray.push(data);
                })

            }

            totalprice = products?.reduce((accm, elem) => {
                // console.log("elem", elem)
                if ((elem?.finalprice) && elem.quantity) {
                    totalproductsinqty += elem.quantity;
                    let productprice = elem?.finalprice;
                    return accm + (productprice)
                }
                return accm;
            }, 0);
        }
        // return res.send({status:true})
       if(notshippalbeproduct.length==0){
        try {
            const newOrder = new orders();
            newOrder.products = products;
            newOrder.address = address;
            newOrder.totalproductsinorder = totalproductsinorder;
            newOrder.totalproductsinqty = totalproductsinqty
            newOrder.userId = userId;
            newOrder.shipping_details = shippingmethod;
            newOrder.totaltax = totaltax
            newOrder.discounted_amount = discounted_amount;
            newOrder.totalPrice = totalprice;
            newOrder.totalplusperproduct = totalplusperproduct;
            newOrder.totalcommission = totalcommission;
            newOrder.payment_amount = totalpaymentamount;
            newOrder.totalshipping = totalshippingcost;
            newOrder.shippingaddress = shipping_address;
            newOrder.billingaddress = billing_address;
            // newOrder.totalproductsinorder = products.length;
            newOrder.save().then((result) => {
                if (result) {
                    const url = BASE_URL + "/api/common/getpayment/" + result._id + "/" + result.totalPrice + "/" + result.userId + "/" + from;
                    // console.log("url",url)
                    return res.send({
                        status: true,
                        url: url,
                        message: "success"
                    })
                } else {
                    return res.send({
                        status: false,
                        message: "failed"
                    })
                }
            })
        } catch (e) {

            return res.send({
                status: false,
                message: e.message
            })
        }
       }else{
        return res.send({
            status:false,
            message:"product is not shippable",
            notshippalbeproduct:notshippalbeproduct
        })
       }
       


    } else {
        return res.send({
            status: false,
            message: "Le panier est vide"
        })
    }

    },
    checkoutcartbefore:async(req,res)=>{
        const { 
        billingaddress,
        shippingaddress,
        from,
        shipping_city,
        shipping_details,
        code,
        id
        }=req.body;
        let userId=id;
        const coupon=await CoupensModel.findOne({code:code});
        const user= await User.findById(id);
        // console.log("coupon",coupon)
        let shippingmethod;
        const shipping_address=user?.address?.filter(e=>e.is_shipping_address==true)[0]||user?.address[0];
        const billing_address=user?.address?.filter(e=>e.is_billing_address==true)[0]||user?.address[0];
        const productsincoupon=coupon?.products||[];
        const coupontype=coupon?.type;
        const couponamount=coupon?.discountamount;
        let products=(await User.findById(id))?.cart;
        // console.log("shipping_address",shipping_address,"billing_address",billing_address,"user",user)
        const istheresomethingincart=products?.length;
        let totalprice=0;
        let totalcommission=0;
        let totalshippingcost=0;
        let totalproductsinqty=0;
        let totalproductsinorder=0;
        let finalprice=0;
        let totaltax=0;
        let discounted_amount=0;
        let totalpaymentamount=0;
        let totalplusperproduct=0;
        let nonshippableproducts=[];
        let shippingmethods=[];
        if(istheresomethingincart){
            let totalprice=0
            totalproductsinorder=products.length
            const shippingcostsArray=[];
            if (products.length) {
               
                for(let i=0;i<products.length;i++){
                    product=products[i]
                    let seller_will_bear_shipping=product?.seller_will_bear_shipping;
                    let productprice=product.varientPrice||product?.price;
                    let qty=product.quantity;
                    // console.log("productprice=====>",productprice,"product.varientPrice",product.varientPrice,"product?.price",product?.price)
                    let data3={
                        productprice:productprice,product_id:product.product._id,product_qty:qty,shipping_city:shipping_city
                    }
                    
                   await calculateShippingcostforproducts(data3).then((result)=>{
                        // console.log("product",product,"result",result)
                        if(!result.result.length){
                            nonshippableproducts.push(product);
                            
                            product["is_shippable"]=false;
                            product["shipping_cost"]=0;
                            product["discountedPrice"]=productprice;
                            // discounted_amount=discounted_amount+(productprice)*qty;
                            
                        // console.log("discounted price in case of not shippable====>",discounted_amount,"(productprice)*qty",(productprice)*qty)
                        }else{
                            if(productsincoupon?.includes(product.product._id.toString())){
                                if(coupontype=="a"){
                                    product["couponApplied"]=coupon.code;
                                    product["discountedPrice"]=productprice-couponamount;
                                    discounted_amount=discounted_amount+(productprice-couponamount)*qty;
                                    totalpaymentamount=totalpaymentamount+(productprice*qty);
                                }else if(coupontype=="p"){
                                    product["couponApplied"]=coupon.code;
                                    product["discountedPrice"]=productprice-(productprice*couponamount/100);
                                    discounted_amount=discounted_amount+(productprice-((productprice*couponamount)/100))*qty;
                                    totalpaymentamount=totalpaymentamount+(productprice*qty);
                                }
                            }else{
                                product["discountedPrice"]=productprice;
                                discounted_amount=discounted_amount+(productprice)*qty;
                                totalpaymentamount=(totalpaymentamount+productprice)*qty;
                            }
                            totalshippingcost=totalshippingcost+result.result[0].shippingcostforproducts
                            product["is_shippable"]=true;
                            product["shipping_cost"]=result.result[0].shippingcostforproducts
                            product["plusperproduct"]= result.plusperproduct;
                            totalplusperproduct+=result.plusperproduct;
                            product["varientPrice"]=productprice
                            product["tax"]=result.producttax
                            totaltax=totaltax+product["tax"];
                            product["commission"]=result.productcommission;
                            
                            let sellerprofit=0;
                            let pricetouser=0;
                            if(product?.seller_will_bear_shipping){
                                pricetouser=product["discountedPrice"]*qty;
                                sellerprofit=product["discountedPrice"]*qty-result.plusperproduct-product["shipping_cost"]-result.productcommission-result.producttax;
                            }else{
                                pricetouser=product["discountedPrice"]*qty+product["shipping_cost"]
                                sellerprofit=product["discountedPrice"]*qty-result.plusperproduct-result.productcommission-result.producttax;
                            }
                            product["variation_id"]=product.variation_id
                            shippingmethod=result.result[0];
                            // shippingmethods.push(shippingmethod)
                            product["shipping_details"]=shippingmethod;
                            product["shipping_id"]=shippingmethod._id+""
                            totalcommission+=result.productcommission
                            finalprice=result.plusperproduct+product["shipping_cost"]+product["discountedPrice"]*qty+result.productcommission+result.producttax;
                            sellerprofit=sellerprofit;
                            product["finalprice"]=finalprice
                            product["sellerprofit"]=sellerprofit
                            product["pricetouser"]=pricetouser
                            product["variation_id"] = product.variation_id
                            product["variations"] = product.variations
                        }
                        
                        // console.log("productsincoupon===>",productsincoupon,"product.product._id",product.product._id)
                        // data={
                        //     productcommission:result.productcommission,
                        //     producttax:result.producttax,
                        //     shippingcost:result[0].costforproducts
                        // }
                        // shippingcostsArray.push(data);
                    })
                   
                }
                products=products.filter(elem=>elem.is_shippable==true)
                totalprice = products?.reduce((accm, elem) => {
                    // console.log("elem", elem)
                    if ((elem?.finalprice) && elem.quantity) {
                        totalproductsinqty+=elem.quantity;
                        let productprice=elem?.finalprice;
                        return accm + (productprice )
                    }
                    return accm;
                }, 0);
            }
            
            const groupbyshippingmethods=groupByfunc(products,'shipping_id');
            const keys=Object.keys(groupbyshippingmethods)
            // groupbyshippingmethods=null
            // console.log("keys===>",keys)
            const allorders=[];
            keys?.map((key)=>{
                productsobject=groupbyshippingmethods[key]
                productsobject?.map(product=>allorders.push(product))
                allorders.push()
            })
            //  console.log("allorders===>",allorders)
            if(groupbyshippingmethods.length==0){
                return res.send({
                    status:false,
                    message:"les produits ne sont pas expédiables"
                })
            }else if(groupbyshippingmethods.length==1){
                const newOrder = new orders();
                newOrder.products = products;
                // newOrder.address = address;
                newOrder.totalproductsinqty=totalproductsinqty;
                newOrder.totalproductsinorder=totalproductsinorder;
                newOrder.userId = userId;
                newOrder.totaltax = totaltax;
                newOrder.shippingaddress=shipping_address;
                newOrder.billingaddress=billing_address;
                newOrder.discounted_amount=discounted_amount;
                newOrder.totalPrice = totalprice;
                newOrder.totalplusperproduct=totalplusperproduct;
                 newOrder.totalcommission = totalcommission;
                 newOrder.payment_amount=totalpaymentamount;
                 newOrder.totalshipping = totalshippingcost;
                 newOrder.shipping_details=shipping_details;
                // newOrder.totalproductsinorder = products.length;
                newOrder.save().then((result) => {
                    if(result){
                        const url=BASE_URL+"/api/common/getpayment/"+result._id+"/"+result.totalPrice+"/"+result.userId+"/"+from;
                        
                        return res.send({
                            status:true,
                            url:url,
                            nonshippableproducts:nonshippableproducts,
                            message:"success"
                        })
                    }else{
                        return res.send({
                            status:false,
                            message:"failed"
                        })
                    }
                })
            }else {
                const combo_id=stringGen(20);
                const allorders=[];
                keys?.map((key)=>{allorders.push(groupbyshippingmethods[key])})
                const allsavedorders=[]
                for(let j=0;j<groupbyshippingmethods.length;j++){
                    let oneorder=allorders[j];
                    const newOrder = new orders();
                    newOrder.products = oneorder.oneorder;
                    newOrder.combo_id=combo_id;
                    // newOrder.address = address;
                    newOrder.totalproductsinqty=oneorder.totalproductsinqty;
                    newOrder.totalproductsinorder=oneorder.totalproductsinorder;
                    newOrder.userId = userId;
                    newOrder.totaltax = oneorder.totaltax;
                    newOrder.shippingaddress=oneorder.shipping_address;
                    newOrder.billingaddress=oneorder.billing_address;
                    newOrder.discounted_amount=oneorder.discounted_amount;
                    newOrder.totalPrice = oneorder.totalprice;
                    newOrder.totalplusperproduct=oneorder.totalplusperproduct;
                     newOrder.totalcommission = oneorder.totalcommission;
                     newOrder.payment_amount=oneorder.totalpaymentamount;
                     newOrder.totalshipping = oneorder.totalshippingcost;
                     newOrder.shipping_details=oneorder.shipping_details;
                      allsavedorders.push(newOrder)
                    // newOrder.totalproductsinorder = products.length;
                    newOrder.save();
                    
                }
                // console.log("allsavedorders",allsavedorders)
                const totalprice=allorders.reduce((acc,e)=>{
                    acc+=e.pricetouser
                    return acc
                },0)
                // console.log("totalprice",totalprice)
                if(totalprice){
                    const url=BASE_URL+"/api/common/getpaymentforcombo/"+combo_id+"/"+totalprice+"/"+userId+"/"+from;
                    
                    return res.send({
                        status:true,
                        url:url,
                        nonshippableproducts:nonshippableproducts,
                        message:"success"
                    })
                }else{
                    return res.send({
                        status:false,
                        message:"failed"
                    })
                }
            }
            // const totalcommissions=200;
            
           
          
        }else{
            return res.send({
                status:false,
                message:"Le panier est vide"
            })
        }

    },
    checkoutcart: async (req, res) => {
      try{
        const {
            billingaddress,
            shippingaddress,
            from,
            shipping_city,
            shipping_details,
            code,
            id
        } = req.body;
        var  shipping_iddd = req.body.shipping_id;
        console.log("1036");
        //console.log(shipping_iddd);
        if(!shipping_iddd)
        {
            return res.send({
                status:false,
                message: "Veuillez choisir l'option d'expédition de votre choix"
            })
        }

        //console.log("shipping_iddd " , shipping_iddd);
        var filtered = shipping_iddd.filter(function (el) {
            return el !=  "";
          });
          
        //console.log(filtered);

        

        let userId = id;
        const coupon = await CoupensModel.findOne({ code: code });
        const user = await User.findById(id);
        //console.log("user 1029"+user);return false;
        // console.log("coupon", coupon)
        let shippingmethod;
        const shipping_address = user?.address?.filter(e => e.is_shipping_address == true)[0] || user?.address[0];
        const billing_address = user?.address?.filter(e => e.is_billing_address == true)[0] || user?.address[0];
        const productsincoupon = coupon?.products || [];
        const coupontype = coupon?.type;
        const couponamount = coupon?.discountamount;
        let cartproducts = (await User.findById(id))?.cart;

        // console.log("cartproducts " , JSON.stringify(cartproducts));
        // console.log("cartproducts.length " , cartproducts.length);
        // return false;
        if(filtered.length < cartproducts.length)
        {
            return res.send({
                status:false,
                message: "Veuillez choisir l'option d'expédition de votre choix"
            })
        }
        let products = [ ]
        cartproducts?.map((cartproduct)=>{

            //console.log("cartproduct ",cartproduct);
            let filePathCopy = "";
            let img_nn = "";
            if(cartproduct.variation_image)
            {
                //console.log("variation_image ",cartproduct.variation_image);
                const filePath = './views/admin-panel/public/media/'+cartproduct.variation_image;

                img_nn = 'order_'+Date.now()+"_"+Array.from(Array(6), () => Math.floor(Math.random() * 36).toString(36)).join('')+'.png';
                filePathCopy = './views/admin-panel/public/media/'+img_nn;
                    
                fs.copyFile(filePath, filePathCopy, (err) => {
                    // if (err) 
                    // {
                    //     //throw err;
                    //     //console.log('File Copy Successfully.');
                    // }

                });
                //console.log("filePathCopy ",filePathCopy);
            }

            products.push( {
                product: cartproduct?.product[0],
                provider_id:cartproduct?.product[0].provider_id,
                quantity: cartproduct.quantity,
                price: cartproduct.price,
                variation_id: cartproduct.variation_id,
                //variations:cartproduct.variations,
                variation_color: cartproduct.variation_color,
                variation_image: img_nn,
                variation_name: cartproduct.variation_name,
                variation_value: cartproduct.variation_value,
                is_available: cartproduct.is_available
            })
        })
        // console.log("products ", products);
        // return false;
        // console.log("shipping_address", shipping_address, "billing_address", billing_address, "user", user)
        const istheresomethingincart = products?.length;
        let totalprice = 0;
        let totalcommission = 0;
        let totalshippingcost = 0;
        let totalproductsinqty = 0;
        let totalproductsinorder = 0;
        let finalprice = 0;
        let totaltax = 0;
        let discounted_amount = 0;
        let totalpaymentamount = 0;
        let totalplusperproduct = 0;
        let nonshippableproducts = [];
        let shippingmethods = [];
        if (istheresomethingincart) {
            let totalprice = 0
            totalproductsinorder = products.length;
            //console.log("totalproductsinorder ",totalproductsinorder);
            if(totalproductsinorder > shipping_iddd.length)
            {
                return res.send({
                    status:false,
                    message: "Veuillez sélectionner l'option d'expédition pour chaque produit"
                })
            }
            const shippingcostsArray = [];
            if (products.length) {
                // console.log("products===>",products)
                for (let i = 0; i < products.length; i++) {
                    
                    product =products[i];
                    let seller_will_bear_shipping = product?.seller_will_bear_shipping;
                    let productprice = product.varientPrice || product?.price;
                    let qty = product.quantity;
                    // console.log("productprice=====>",productprice,"product.varientPrice",product.varientPrice,"product?.price",product?.price)
                    let data3 = {
                        productprice: productprice, product_id: product.product._id, product_qty: qty, shipping_city: shipping_city,
                        shipping_idd:shipping_iddd[i]
                    }
                    const provider=await Seller.findById(product.provider_id)
                    product["provider_name"]=provider?.fullname;
                    product["provider_id"]=provider?._id;
                    product["part_id"]=parseInt((Math.random())*99999999);
                    
                    await calculateShippingcostforproducts(data3).then((result) => {
                        //console.log("result in cart ",result);return false;
                        //console.log("hereee");return false;
                        if(!result){
                        //if (!result?.result?.length) {
                            nonshippableproducts.push(product);
    
                            product["is_shippable"] = false;
                            product["shipping_cost"] = 0;
                            product["discountedPrice"] = productprice;
                            // discounted_amount=discounted_amount+(productprice)*qty;
    
                            // console.log("discounted price in case of not shippable====>", discounted_amount, "(productprice)*qty", (productprice) * qty)
                        } else {
                            console.log("here 1192");
                            //console.log("result length");
                           //console.log(result.result.length);
                          // console.log(result.result);
                           
                           //return false;


                            if(result.result[0].shippingcostforproducts == undefined || result.result[0].shippingcostforproducts == 'undefined' || result.result[0].shippingcostforproducts == null || result.result[0].shippingcostforproducts == 'null' )
                            {
                                nonshippableproducts.push(product);
                                product["is_shippable"] = false;
                                product["shipping_cost"] = 0;
                                product["discountedPrice"] = productprice;
                            }else{
                                if (productsincoupon?.includes(product.product._id.toString())) {
                                    if (coupontype == "a") {
                                        if(productprice >= couponamount)
                                        {
                                            product["couponApplied"] = coupon.code;
                                            product["discountedPrice"] = productprice - couponamount;
                                            discounted_amount = discounted_amount + (productprice - couponamount) * qty;
                                            totalpaymentamount = totalpaymentamount + (productprice * qty);
                                        }
                                    } else if (coupontype == "p") {
                                        product["couponApplied"] = coupon.code;
                                        product["discountedPrice"] = productprice - (productprice * couponamount / 100);
                                        discounted_amount = discounted_amount + (productprice - ((productprice * couponamount) / 100)) * qty;
                                        totalpaymentamount = totalpaymentamount + (productprice * qty);
                                    }
                                } else {
                                    product["discountedPrice"] = productprice;
                                    discounted_amount = discounted_amount + (productprice) * qty;
                                    totalpaymentamount = (totalpaymentamount + productprice) * qty;
                                }
                                // console.log("shippingcostforproducts");
                                // console.log(result.result[0].shippingcostforproducts);
                                // return false;
                                //totalshippingcost = totalshippingcost + result.result[0].shippingcostforproducts
                                product["is_shippable"] = true;
                                //product["shipping_cost"] = result.result[0].shippingcostforproducts;


                                    //console.log("result ", result);
                                    // console.log("product ", product.product);
                                    console.log("qty ", qty);
                                    const product_weight = product.product.dispatch_info.weight; 
                                    const product_totalweight=product_weight*qty;
                                    const product_totalweight_in_gm = product_totalweight * 1000;
                                    console.log("product_totalweight ",product_totalweight);
                                    const max_weight_in_gm = result.result[0].max_weight * 1000;
                                    const cost_additional_weight = result.result[0].cost_additional_weight * 1000;
                                    const cose_per_unit = result.result[0].cose_per_unit;
                                    let shipping_price = result.result[0].cost_base;
                                    if(product_totalweight_in_gm > max_weight_in_gm)
                                    {
                                       let remaining_weight = product_totalweight_in_gm - max_weight_in_gm;
                                       let one_unit_in_gm =remaining_weight / cost_additional_weight;
                                       console.log("one_unit_in_gm ",one_unit_in_gm);
                                       let total_additional_price = one_unit_in_gm * cose_per_unit;
                                       console.log("=======================> 1487 ");
                                       console.log("total_additional_price ",total_additional_price);
                                       shipping_price = shipping_price + total_additional_price;
                                    }

                                    // console.log("product 1492",product);
                                    // console.log("product seller_will_bear_shipping 1493 ",product.product.seller_will_bear_shipping);
                                    // return false;

                                    // if(product.product.seller_will_bear_shipping == true)
                                    // {
                                    //     shipping_price = 0;
                                    // }

                                    totalshippingcost = totalshippingcost + shipping_price;
                                    product["shipping_cost"] = shipping_price;

                                    console.log("=======================> 1494 ");
                                    console.log("totalshippingcost ", totalshippingcost);




                                product["plusperproduct"] = result.plusperproduct;
                                totalplusperproduct += result.plusperproduct;
                                product["varientPrice"] = productprice
                                product["tax"] = result.producttax
                                totaltax = totaltax + product["tax"];
                                product["commission"] = result.productcommission;
                                
                                // console.log("hereee ");
                                // console.log(coupon);
                                // return false;
                                let sellerprofit = 0;
                                let pricetouser = 0;
                                // console.log("product ",product);
                                // console.log("product seller_will_bear_shipping ",product.product.seller_will_bear_shipping);
                                // return false;
                                if (product.product?.seller_will_bear_shipping) {
                                    pricetouser = product["discountedPrice"] * qty;
                                    //sellerprofit = product["discountedPrice"] * qty - result.plusperproduct - product["shipping_cost"] - result.productcommission - result.producttax;
                                    sellerprofit = product["discountedPrice"] * qty - result.plusperproduct - product["shipping_cost"] - result.productcommission;
                                } else {
                                    pricetouser = product["discountedPrice"] * qty + product["shipping_cost"]
                                    //sellerprofit = product["discountedPrice"] * qty - result.plusperproduct - result.productcommission - result.producttax;
                                    sellerprofit = product["discountedPrice"] * qty - result.plusperproduct - result.productcommission;
                                }
                                product["variation_id"] = product.variation_id
                                shippingmethod = result.result[0];
                                // shippingmethods.push(shippingmethod)
                                product["shipping_details"] = shippingmethod;
                                product["shipping_id"] = shippingmethod._id + ""
                                totalcommission += result.productcommission
                                finalprice = result.plusperproduct + product["shipping_cost"] + product["discountedPrice"] * qty + result.productcommission + result.producttax;
                                sellerprofit = sellerprofit;
                                product["finalprice"] = pricetouser
                                product["sellerprofit"] = sellerprofit
                                product["pricetouser"] = pricetouser
                                product["variation_id"] = product.variation_id
                                //product["variations"] = product.variations
                            }
                            
                        }
                        // console.log("products===> after changes",products)
                        // console.log("productsincoupon===>",productsincoupon,"product.product._id",product.product._id)
                        // data={
                        //     productcommission:result.productcommission,
                        //     producttax:result.producttax,
                        //     shippingcost:result[0].costforproducts
                        // }
                        // shippingcostsArray.push(data);
                    })
    
                }
                // products = products.filter(elem => elem.is_shippable == true)
                totalprice = products?.reduce((accm, elem) => {
                    // console.log("elem", elem)
                    if ((elem?.finalprice) && elem.quantity) {
                        totalproductsinqty += elem.quantity;
                        let productprice = elem?.finalprice;
                        return accm + (productprice)
                    }
                    return accm;
                }, 0);
            }
            // console.log("products====> befor groupby",products)
            const groupbyshippingmethods = groupByfunc(products, 'provider_id');
            const keys = Object.keys(groupbyshippingmethods)
            // groupbyshippingmethods=null
            // console.log("keys===>", keys)
            const allorders = [];
            keys?.map((key) => {
                productsobject = [groupbyshippingmethods[key]]
                productsobject?.map(product => allorders.push(product))
                allorders.push()
            });
            //console.log("allorders ", allorders);return false;
            // console.log("groupbyshippingmethods===>", groupbyshippingmethods)
            if (allorders.length == 0) {
                return res.send({
                    status: false,
                    message: "les produits ne sont pas expédiables",
                    nonshippableproducts: nonshippableproducts,
                })
            } else if (allorders.length == 1) 
            {
                //console.log("line no 1302");return false;
                const newOrder = new orders();
                newOrder.products = products;
                // newOrder.address = address;
                newOrder.totalproductsinqty = totalproductsinqty;
                newOrder.totalproductsinorder = totalproductsinorder;
                newOrder.userId = userId;
                newOrder.totaltax = totaltax;
                newOrder.shippingaddress = shipping_address;
                newOrder.billingaddress = billing_address;
                newOrder.discounted_amount = discounted_amount;
                newOrder.totalPrice = totalprice + totaltax;
                newOrder.totalplusperproduct = totalplusperproduct;
                newOrder.totalcommission = totalcommission;
                newOrder.payment_amount = totalpaymentamount;
                newOrder.totalshipping = totalshippingcost;
                newOrder.shipping_details = shipping_details;
                // newOrder.totalproductsinorder = products.length;
                newOrder.save().then((result) => {
                    if (result) {
                        const url = BASE_URL + "/api/common/getpayment/" + result._id + "/" + result.totalPrice + "/" + result.userId + "/" + from;
    
                        return res.send({
                            status: true,
                            url: url,
                            "order_id":result._id,
                            nonshippableproducts: nonshippableproducts,
                            message: "success",

                        })
                    } else {
                        return res.send({
                            status: false,
                            message: "failed",
                            nonshippableproducts: nonshippableproducts,
                        })
                    }
                })
            } else {
                console.log("line no 1341");
                const combo_id = stringGen(20);
                // const allorders = [];
                // keys?.map((key) => { allorders.push(groupbyshippingmethods[key]) })
                //console.log("allorders",allorders)
                const allsavedorders = [];
                let totalprice=0;
                for (let j = 0; j < keys.length; j++) {

                    console.log("hereeeeeeeeeeeeeeeeeeeeee 1624");
                    let key=keys[j]
                    let oneorderlot = groupbyshippingmethods[key];
                     
                    //console.log("oneorderlot",oneorderlot);return false;

                    let onelotproducts=[]
                    let totalproductsinqtylot=0;
                    let totalproductsinorderlot=0;
                    let discounted_amountlot=0;
                    let totalpricelot=0;
                    let totaltaxlot=0;
                    let totalplusperproductlot=0;
                    let totalcommissionlot=0;
                    let totalpaymentamountlot=0;
                    let totalshippingcostlot=0;
                    //console.log("oneorderlot "+oneorderlot);return false;
                    for(let k=0;k<oneorderlot?.length;k++)
                    {
                        let singleorder=oneorderlot[k];
                        // console.log("singleorder 1248",singleorder)
                        onelotproducts.push(singleorder)
                        totalprice+=singleorder.pricetouser + singleorder.tax;
                        // console.log("singleorder",singleorder,"singleorder.pricetouser",singleorder.pricetouser)
                        totalproductsinqtylot+=singleorder.quantity;
                        totaltaxlot+=singleorder.tax
                        totalproductsinorderlot=oneorderlot?.length;
                        discounted_amountlot+= singleorder.discountedPrice
                        totalpricelot+=singleorder.finalprice
                        totalplusperproductlot+=singleorder.plusperproduct
                        totalcommissionlot+=singleorder.commission
                        totalpaymentamountlot=totalpaymentamountlot+(singleorder.price*singleorder.quantity)
                        totalshippingcostlot+=singleorder.shipping_cost
                        totalpricelot+=singleorder.tax
                    }
                    console.log("====================================>")
                    console.log("====================================>")
                    console.log("====================================>")
                    console.log("====================================>")

                     //console.log("onelotproducts khushal ",onelotproducts);

                    console.log("====================================>")
                    console.log("====================================>")
                    console.log("====================================>")
                    console.log("====================================>")
                    const newOrder = new orders();
                    newOrder.products = onelotproducts;
                    newOrder.combo_id = combo_id;
                    // newOrder.address = address;
                    newOrder.totalproductsinqty = totalproductsinqtylot;
                    newOrder.totalproductsinorder = totalproductsinorderlot;
                    newOrder.userId = userId;
                    newOrder.totaltax = totaltaxlot;
                    newOrder.shippingaddress = shipping_address;
                    newOrder.billingaddress = billing_address;
                    newOrder.discounted_amount = discounted_amountlot;
                    newOrder.totalPrice = totalpricelot;
                    newOrder.totalplusperproduct = totalplusperproductlot;
                    newOrder.totalcommission = totalcommissionlot;
                    newOrder.payment_amount = totalpaymentamountlot;
                    newOrder.totalshipping = totalshippingcostlot;
                    newOrder.shipping_details = oneorderlot[0].shipping_details;
                   
                    newOrder.totalproductsinorder = oneorderlot.length;
                    newOrder.save();
    
                }
                
                console.log("1691 total price ",totalprice);

                if (totalprice) {
                    const url = BASE_URL + "/api/common/getpaymentforcombo/" + combo_id + "/" + totalprice + "/" + userId + "/" + from;
    
                    return res.send({
                        status: true,
                        url: url,
                        combo_id:combo_id,
                        nonshippableproducts: nonshippableproducts,
                        message: "success",
                        
                    })
                } else {
                    return res.send({
                        status: false,
                        nonshippableproducts: nonshippableproducts,
                        message: "failed",
                        groupbyshippingmethods:groupbyshippingmethods
                    })
                }

                // return res.send({
                //             status: false,
                            
                //             message: "failed",
                //             groupbyshippingmethods:groupbyshippingmethods,
                //             allorders:allorders,
                //             products:products
                //         })
            }
                // const totalcommissions=200;
    
    
    
        } else {
            return res.send({
                status: false,
                message: "Le panier est vide"
            })
        }
     }catch(error)
     {
        console.log(error);
        return res.send({
            status: false,
            message: error.message
        })
     }
    }
    ,
    checkpromocode:async(req,res)=>
    {

        var d_date = new Date();
        const promo=req.params.promo;
        if(!promo)
        {
            return res.send({
                status:false,
                errmessage:"La promotion est requise",
                data:null
            })
        }
        const cdate=new Date();
        cdate.setHours(0,0,0,0);
        const cdatestr=cdate.toISOString()
        //console.log("cdate",cdate,"promo",promo,cdatestr)
    
        const ispromovalid=await CoupensModel.aggregate([
        { $match:{
                code:promo,
            status:true,
            //  start_date:{$lte:cdate},
            //  end_date:{$gte:cdate}
                end_date:{$gt:d_date}
            }},
            {$addFields:{"islimitexahusted":{$subtract:["$limit","$used"]}}},
            {$match:{"islimitexahusted":{$gt:0}}}

        ])
        // console.log(ispromovalid)
        if(ispromovalid.length>0){
            return res.send({
                status:true,
                message:"Code appliqué avec succès",
                data:ispromovalid
            })
        }else{
            return res.send({
                status:false,
                errmessage:"Code Coupen non valide ",
                data:null
            })
        }
    },
    beforecheckoutcart: async (req, res) => {
        const {
            billingaddress,
            shippingaddress,
            from,
            shipping_city,
            shipping_details,
            code,
            id
        } = req.body;
        let userId = id;


        var  shipping_iddd = req.body.shipping_id;
       // console.log("1544 shipping_iddd ",shipping_iddd);
        
        const coupon = await CoupensModel.findOne({ code: code });
        const user = await User.findById(id);
        // console.log("coupon", coupon)
        let shippingmethod;
        const shipping_address = user?.address?.filter(e => e.is_shipping_address == true)[0] || user?.address[0];
        const billing_address = user?.address?.filter(e => e.is_billing_address == true)[0] || user?.address[0];
        const productsincoupon = coupon?.products || [];
        const coupontype = coupon?.type;
        const couponamount = coupon?.discountamount;
        let cartproducts = (await User.findById(id))?.cart;
        let products = [ ]
        cartproducts?.map((cartproduct)=>{
            products.push( {
                product: cartproduct?.product[0],
                quantity: cartproduct.quantity,
                price: cartproduct.price,
                variation_id: cartproduct.variation_id,
                variation_color: cartproduct.variation_color,
                variation_image: cartproduct.variation_image,
                variation_name: cartproduct.variation_name,
                variation_value: cartproduct.variation_value,
                is_available: cartproduct.is_available,
                total_quantity: cartproduct.total_quantity,

                // variations:cartproduct.variations
            })
        })
        // console.log("shipping_address", shipping_address, "billing_address", billing_address, "user", user)
        const istheresomethingincart = products?.length;
        let promo_price_greater_product_price = 0;
        let totalprice = 0;
        let totalcommission = 0;
        let totalshippingcost = 0;
        let totalproductsinqty = 0;
        let totalproductsinorder = 0;
        let finalprice = 0;
        let totaltax = 0;
        let discounted_amount = 0;
        let totalpaymentamount = 0;
        let totalplusperproduct = 0;
        let nonshippableproducts = [];
        let shippingmethods = [];
        if (istheresomethingincart) {
            let totalprice = 0
            totalproductsinorder = products.length
            const shippingcostsArray = [];
            if (products.length) {
                // console.log("products===>",products)
                for (let i = 0; i < products.length; i++) {
                    //console.log("i ",i);
                    product =products[i];
                    let seller_will_bear_shipping = product?.seller_will_bear_shipping;
                    let productprice = product.varientPrice || product?.price;
                    let qty = product.quantity;
                    // console.log("productprice=====>",productprice,"product.varientPrice",product.varientPrice,"product?.price",product?.price)
                    let let_shipping_iddd = "";
                    // if(shipping_iddd)
                    // {
                    //     // console.log("first iff");
                    //     // console.log("i ",i);
                    //     // console.log("shipping_iddd.length ",shipping_iddd.length);
                    //     if(i < shipping_iddd.length)
                    //     {
                    //        // console.log("second iff");
                    //         let_shipping_iddd = shipping_iddd[i];
                    //     }
                    // }
                    if(shipping_iddd)
                    {
                        // console.log("first iff");
                        // console.log("i ",i);
                        // console.log("shipping_iddd.length ",shipping_iddd.length);
                        if(i < shipping_iddd.length)
                        {
                           // console.log("second iff");
                            let_shipping_iddd = shipping_iddd[i];
                             
                        }
                        if(shipping_iddd[i] != "")
                        {
                            product["shipping_selected"] = 1;
                        }else{
                            product["shipping_selected"] = 0;
                        }
                    }else{

                        product["shipping_selected"] = 0;
                    }
                    let data3 = {
                        productprice: productprice, product_id: product.product._id, product_qty: qty, shipping_city: shipping_city,
                        "shipping_idd":let_shipping_iddd
                    }
    
                    await calculateShippingcostforproducts(data3).then((result) => {
                        //console.log( "result",result);
                        //console.log("hereeeeeee ",result.result[0]);
                        if (!result?.result?.length) {
                            nonshippableproducts.push(product);
    
                            product["is_shippable"] = false;
                            product["shipping_cost"] = 0;
                            product["discountedPrice"] = productprice;
                            // discounted_amount=discounted_amount+(productprice)*qty;
    
                            // console.log("discounted price in case of not shippable====>", discounted_amount, "(productprice)*qty", (productprice) * qty)
                        } else {
                           
                            // console.log("here");
                            // console.log(result);return false;
                            if(result.result[0].shippingcostforproducts == undefined || result.result[0].shippingcostforproducts == 'undefined' || result.result[0].shippingcostforproducts == null || result.result[0].shippingcostforproducts == 'null' )
                            {
                                nonshippableproducts.push(product);
                                product["is_shippable"] = false;
                                product["shipping_cost"] = 0;
                                product["discountedPrice"] = productprice;
                            }else{
                                console.log("here 1585");
                                if (productsincoupon?.includes(product.product._id.toString())) {
                                    if (coupontype == "a")
                                    {
                                        //console.log("1589 coupon ",coupon);
                                        //console.log("productprice ",productprice);
                                        if(productprice >= couponamount)
                                        {
                                            product["couponApplied"] = coupon.code;
                                            product["discountedPrice"] = productprice - couponamount;
                                            discounted_amount = discounted_amount + (productprice - couponamount) * qty;
                                            totalpaymentamount = totalpaymentamount + (productprice * qty);
                                        }
                                        // else{
                                        //     promo_price_greater_product_price = 1;
                                        // }                                        
                                    } else if (coupontype == "p") {
                                        product["couponApplied"] = coupon.code;
                                        product["discountedPrice"] = productprice - (productprice * couponamount / 100);
                                        discounted_amount = discounted_amount + (productprice - ((productprice * couponamount) / 100)) * qty;
                                        totalpaymentamount = totalpaymentamount + (productprice * qty);
                                    }
                                } else {
                                    product["discountedPrice"] = productprice;
                                    discounted_amount = discounted_amount + (productprice) * qty;
                                    totalpaymentamount = (totalpaymentamount + productprice) * qty;
                                }

                                console.log("iiiiiiiiiiiii 1683 iiiii  ",[i]);
                                console.log(shipping_iddd[i]);
                                //return false;
                                if(shipping_iddd[i] != "")
                                {
                                     //console.log("result ", result);
                                    // console.log("product ", product.product);
                                    console.log("qty ", qty);

                                    const product_weight = product.product.dispatch_info.weight; 
                                    const product_totalweight=product_weight*qty;
                                    const product_totalweight_in_gm = product_totalweight * 1000;
                                    console.log("product_totalweight ",product_totalweight);
                                    const max_weight_in_gm = result.result[0].max_weight * 1000;
                                    const cost_additional_weight = result.result[0].cost_additional_weight * 1000;
                                    const cose_per_unit = result.result[0].cose_per_unit;
                                    let shipping_price = result.result[0].cost_base;
                                    
                                    if(product_totalweight_in_gm > max_weight_in_gm)
                                    {
                                       let remaining_weight = product_totalweight_in_gm - max_weight_in_gm;
                                       let one_unit_in_gm =remaining_weight / cost_additional_weight;
                                       console.log("one_unit_in_gm ",one_unit_in_gm);
                                       
                                       let total_additional_price = one_unit_in_gm * cose_per_unit;

                                       console.log("=======================> 1945 ");

                                       console.log("total_additional_price ",total_additional_price);
                                       
                                       shipping_price = shipping_price + total_additional_price;
                                    }
                                    // console.log("product ",product);
                                    // console.log("product seller_will_bear_shipping ",product.product.seller_will_bear_shipping);
                                    // return false;
                                    if(product.product.seller_will_bear_shipping == true)
                                    {
                                        shipping_price = 0;
                                    }
                                    totalshippingcost = totalshippingcost + shipping_price;
                                    product["shipping_cost"] = shipping_price;
                                }else{
                                    totalshippingcost = totalshippingcost + 0;
                                    product["shipping_cost"] = 0;
                                }

                                
                                product["is_shippable"] = true;
                                
                                product["plusperproduct"] = result.plusperproduct;
                                totalplusperproduct += result.plusperproduct;
                                product["varientPrice"] = productprice
                                product["tax"] = result.producttax
                                totaltax = totaltax + product["tax"];
                                product["commission"] = result.productcommission;
        
                                let sellerprofit = 0;
                                let pricetouser = 0;
                                if (product?.seller_will_bear_shipping) {
                                    pricetouser = product["discountedPrice"] * qty;
                                    //sellerprofit = product["discountedPrice"] * qty - result.plusperproduct - product["shipping_cost"] - result.productcommission - result.producttax;

                                    sellerprofit = product["discountedPrice"] * qty - result.plusperproduct - product["shipping_cost"] - result.productcommission;
                                } else {
                                    pricetouser = product["discountedPrice"] * qty + product["shipping_cost"]
                                    //sellerprofit = product["discountedPrice"] * qty - result.plusperproduct - result.productcommission - result.producttax;
                                    sellerprofit = product["discountedPrice"] * qty - result.plusperproduct - result.productcommission;
                                }
                                product["variation_id"] = product.variation_id
                                shippingmethod = result.result;
                                // shippingmethods.push(shippingmethod)
                                let vf = result.data_1_more.map((val)=>{
                                    //console.log("1717   val",val);
                                    if(shipping_iddd[i] != "")
                                    {
                                        if(val._id.toString() == shipping_iddd[i])
                                        {
                                            val["is_selected"] = true;
                                        }else{
                                            val["is_selected"] = false;
                                        }
                                    }else{
                                        val["is_selected"] = false;
                                    }
                                    return val;
                                });
                                product["shipping_details"] = vf;
                                product["shipping_id"] = shippingmethod._id + ""
                                totalcommission += result.productcommission
                                finalprice = result.plusperproduct + product["shipping_cost"] + product["discountedPrice"] * qty + result.productcommission + result.producttax;
                                sellerprofit = sellerprofit;
                                product["finalprice"] = pricetouser
                                product["sellerprofit"] = sellerprofit
                                product["pricetouser"] = pricetouser
                                product["variation_id"] = product.variation_id
                                product["variations"] = product.variations
                            }
                        }
                        // console.log("products===> after changes",products)
                        // console.log("productsincoupon===>",productsincoupon,"product.product._id",product.product._id)
                        // data={
                        //     productcommission:result.productcommission,
                        //     producttax:result.producttax,
                        //     shippingcost:result[0].costforproducts
                        // }
                        // shippingcostsArray.push(data);
                    })
    
                }
                products = products.filter(elem => elem.is_shippable == true)
                totalprice = products?.reduce((accm, elem) => {
                    // console.log("elem", elem)
                    if ((elem?.finalprice) && elem.quantity) {
                        totalproductsinqty += elem.quantity;
                        let productprice = elem?.finalprice;
                        return accm + (productprice)
                    }
                    return accm;
                }, 0);
            }
            // console.log("products====> befor groupby",products)
            const groupbyshippingmethods = groupByfunc(products, 'shipping_id');
            const keys = Object.keys(groupbyshippingmethods)
            // groupbyshippingmethods=null
            // console.log("keys===>", keys)
            const allorders = [];
            keys?.map((key) => {
                productsobject = groupbyshippingmethods[key]
                productsobject?.map(product => allorders.push(product))
                // allorders.push(productsobject)
            })
            // console.log("groupbyshippingmethods===>", groupbyshippingmethods)
            let totalpricetoshow=0;
            let totalpricebeforedistoshow=0;
            let totalshippingtoshow=0;
            let totaldiscountedpricetoshow=0;
            let pricetousertotaltoshow=0;
            if (allorders.length == 0) {
                return res.send({
                    status: false,
                    message: "les produits ne sont pas expédiables",
                    nonshippableproducts: nonshippableproducts,
                })
            } else if (allorders.length == 1) {
                console.log("line 1583");
                let onlyorderingroup=groupbyshippingmethods[keys[0]]
                //  console.log("onlyorderingroup");
                //  console.log(onlyorderingroup);
                //  console.log(onlyorderingroup[0].tax);
                //  return false;
                if(onlyorderingroup.length > 0)
                {
                    totalpricetoshow += onlyorderingroup[0].price;
                    totalshippingtoshow+=onlyorderingroup[0].shipping_cost;
                    totalpricebeforedistoshow+=onlyorderingroup[0].price*onlyorderingroup[0].quantity;
                    totaldiscountedpricetoshow+=onlyorderingroup[0].discountedPrice;
                    pricetousertotaltoshow += onlyorderingroup[0].pricetouser;
                    pricetousertotaltoshow += onlyorderingroup[0].tax;
                }
                

                
                //  console.log("totalpricetoshow");
                //  console.log(onlyorderingroup.price);
                //  return false;
                console.log("1701 line no totaldiscountedpricetoshow ",totaldiscountedpricetoshow);
                console.log("1701 line no totaltax ",totaltax);
                discountpercent = (((totalpricebeforedistoshow-totaldiscountedpricetoshow)/totalpricebeforedistoshow)*100)
                totaldiscounttoshow = totalpricebeforedistoshow-totaldiscountedpricetoshow
                return res.send({
                    status: true,
                    data: allorders,
                    nonshippableproducts: nonshippableproducts,
                    message: "success",
                    totalprice:totalpricetoshow,
                    totalshipping:totalshippingtoshow,
                    totalpricebeforedis:totalpricebeforedistoshow,
                    totaldiscountedprice:totaldiscountedpricetoshow,
                    pricetousertotal:pricetousertotaltoshow,
                    totaldiscounttoshow:totaldiscounttoshow,
                    discountpercent:discountpercent,
                    totaltax:totaltax
                })
            } else {
                console.log("line 1606");
                const combo_id = stringGen(20);
                // const allorders = [];
                // keys?.map((key) => { allorders.push(groupbyshippingmethods[key]) })
                // console.log("allorders",allorders)
                const allsavedorders = [];
                
                for (let j = 0; j < keys.length; j++) {
                    let key=keys[j]
                    let oneorderlot = groupbyshippingmethods[key];
                    // console.log("oneorderlot",oneorderlot)
                    let onelotproducts=[]
                    let totalproductsinqtylot=0;
                    let totalproductsinorderlot=0;
                    let discounted_amountlot=0;
                    let totalpricelot=0;
                    let totaltaxlot=0;
                    let totalplusperproductlot=0;
                    let totalcommissionlot=0;
                    let totalpaymentamountlot=0;
                    let totalshippingcostlot=0;
                    for(let k=0;k<oneorderlot?.length;k++){
                        let singleorder=oneorderlot[k];
                         //console.log("singleorder",singleorder)
                        onelotproducts.push(singleorder)
                        totalpricetoshow+=singleorder.price;
                        totalshippingtoshow+=singleorder.shipping_cost;
                        totalpricebeforedistoshow+=singleorder.price*singleorder.quantity
                        totaldiscountedpricetoshow+=singleorder.discountedPrice;
                        pricetousertotaltoshow+=singleorder.pricetouser
                        // console.log("singleorder",singleorder,"singleorder.pricetouser",singleorder.pricetouser)
                        totalproductsinqtylot+=singleorder.quantity;
                        totaltaxlot+=singleorder.tax
                        totalproductsinorderlot=oneorderlot?.length;
                        discounted_amountlot+= singleorder.discountedPrice
                        totalpricelot+=singleorder.finalprice
                        totalplusperproductlot+=singleorder.plusperproduct
                        totalcommissionlot+=singleorder.commission
                        totalpaymentamountlot=totalpaymentamountlot+(singleorder.price*singleorder.quantity)
                        totalshippingcostlot+=singleorder.shipping_cost
                        pricetousertotaltoshow+=singleorder.tax
                    }
                    console.log("====================================>")
                    console.log("====================================>")
                    console.log("====================================>")
                    console.log("====================================>")
                    //console.log("onelotproducts",onelotproducts)
                    console.log("====================================>")
                    console.log("====================================>")
                    console.log("====================================>")
                    console.log("====================================>")
                   
    
                }
            //    console.log("totalprice",totalprice)
                if (totalprice) {
                    console.log("1789 line no totaltax ",totaltax);
                    console.log("here 1790 ");
                    // const url = BASE_URL + "/api/common/getpaymentforcombo/" + combo_id + "/" + totalprice + "/" + userId + "/" + from;
                    discountpercent= (((totalpricebeforedistoshow-totaldiscountedpricetoshow)/totalpricebeforedistoshow)*100)
                    totaldiscounttoshow=totalpricebeforedistoshow-totaldiscountedpricetoshow
                    return res.send({
                        status: true,
                        data: allorders,
                        nonshippableproducts: nonshippableproducts,
                        message: "success",
                        totalprice:totalpricetoshow,
                        totalshipping:totalshippingtoshow,
                        totalpricebeforedis:totalpricebeforedistoshow,
                        totaldiscountedprice:totaldiscountedpricetoshow,
                        pricetousertotal:pricetousertotaltoshow,
                        discountpercent:discountpercent,
                        totaldiscounttoshow:totaldiscounttoshow,
                        totaltax:totaltax
                    })
                } else {
                    return res.send({
                        status: false,
                        nonshippableproducts: nonshippableproducts,
                        message: "failed"
                    })
                }
            }
            // const totalcommissions=200;
    
    
    
        } else {
            return res.send({
                status: false,
                message: "Le panier est vide"
            })
        }
    
    }
}