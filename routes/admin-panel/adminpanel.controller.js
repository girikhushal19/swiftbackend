const orders=require("../../models/orders/order");
const sales=require("../../models/sales/Sales");
const sellers=require("../../models/seller/Seller");
const ordersModel=require("../../models/orders/order");
const pageViewsModel=require("../../models/products/pageViews");
const {Product}=require("../../models/products/Product");
const VariationsPrice =require("../../models/products/VariationPrice");
const Ratingnreviews = require("../../models/actions/Ratingnreviews");
const usersmodel= require("../../models/user/User");
const base_url = process.env.BASE_URL + "/admin-panel/";
const root_url = process.env.BASE_URL;
const mongoose=require("mongoose");
const {getdateinadvance,getDays,getDate} =require("../../modules/dates.js")
const moment=require("moment")
module.exports={
    renderhomepage : async (req, res) => {
        let date = new Date(), y = date.getFullYear(), m = date.getMonth();
        let currDate = new Date(y, m, 1);
        let daydateto30 = new Date(y, m + 1, 0);
        // let currDate=getdateinadvance(1);
        // let daydateto30=getdateinadvance(30);
        //  console.log("currDate",currDate,"daydateto30",daydateto30)
        let numberofdays=getDays(currDate.getFullYear(),currDate.getMonth()+1);
        let numberofdaysarray=[];
        let totalsalesarray=[];
        let totalordersarray=[];
        let totalcommissionarray=[];
        /*
            string,number,int,bigint,boolean,object,undefined,null
            array
            Text,string,boolean,list,tupple,set,frozenset,
        */
        // console.log("numberofdaysarray",numberofdaysarray)
        
        const monthrange={$and:[
            {createdAt:{
            $gt:currDate
            }},
            {createdAt:{
                $lte:daydateto30
            }}
        ]};
        console.log("monthrange",monthrange,"currDate",currDate,"daydateto30",daydateto30)
        const totalreviews= (await Ratingnreviews.find(monthrange))?.length;
        const salesthismonth=await sales.find(monthrange);
        const sellerthismonth=await sellers.find(monthrange);
        const totalsellerthismonth=sellerthismonth.length;
        const sellerthismonthnotapproved=sellerthismonth.filter(
            (seller) => seller.approved ==false
          ).length;
        const ordersthismonth= await orders.find({});
        const proccessingorders= (await orders.find({status:1})).length;
        const completedorders= (await orders.find({status:2})).length;
        const cancelledorders= (await orders.find({status:3})).length;
        const refundedorders= (await orders.find({status:4})).length;
        // console.log("ordersthismonth",ordersthismonth)
        const totalearnings=ordersthismonth.reduce((accum,order)=>{
            return accum+order.totalPrice
        },0);
        const totalcommissionearned=ordersthismonth.reduce((accum,order)=>{
            return accum+order.totalcommission
        },0);
       
        const totalproductsthismonth=(await Product.find(monthrange)).length;
        // console.log(reservations);
        const totoalnumberoforders = ordersthismonth.length;
        const totalwithdrawls=0;


        for(let i=1;i<=numberofdays;i++){
            let datetocompare=getDate(currDate.getFullYear(),currDate.getMonth()+1,i);
            numberofdaysarray.push(i);
            let totalsalesonthisdate=salesthismonth.filter((e)=>{
                // console.log("e.createdAt==datetocompare",e.createdAt,datetocompare)
                return e.createdAt.toISOString().split("T")[0]==datetocompare.toISOString().split("T")[0]
            });
            if(totalsalesonthisdate.length){
                let totalsalesonthisdateqty=totalsalesonthisdate.reduce((accum,e)=>accum+e.quantity,0);
                totalsalesarray.push(totalsalesonthisdateqty)
            }else{
                totalsalesarray.push(0)
            }
            //total orders
            let totalordersonthisdate=ordersthismonth.filter((e)=>{
                // console.log("e.createdAt.toString().split('T')[0]==datetocompare.toString().split('T')[0]",e.createdAt.toString().split("T")[0],datetocompare.toString().split("T")[0])
                return e.createdAt.toISOString().split("T")[0]==datetocompare.toISOString().split("T")[0]
            });
            if(totalordersonthisdate.length){
                let totalordersonthisdateqty=totalordersonthisdate.reduce((accum,e)=>accum+e.totalproductsinorder,0);
                totalordersarray.push(totalordersonthisdateqty)
            }else{
                totalordersarray.push(0)
            }

            //total commissions
            let totalcommissionsonthisdate=ordersthismonth.filter((e)=>{
                // console.log("e.createdAt==datetocompare",e.createdAt,datetocompare)
                return e.createdAt.toISOString().split("T")[0]==datetocompare.toISOString().split("T")[0]
            });
            if(totalcommissionsonthisdate.length){
                let totalcommissionsonthisdateqty=totalcommissionsonthisdate.reduce((accum,e)=>accum+e.totalcommission,0);
                totalcommissionarray.push(totalcommissionsonthisdateqty)
            }else{
                totalcommissionarray.push(0)
            }
            // console.log("datetocompare",datetocompare,"currDate.getFullYear(),currDate.getMonth()+1,i",currDate.getFullYear(),currDate.getMonth()+1,i)
        }

        const orderspie=[
            ordersthismonth.length,
            completedorders,
            proccessingorders,
            cancelledorders,
            refundedorders
        ]

        const deliverpie=[
           17,
            5,
            4,
            2,
           6
        ]
        // 'Total',
        // 'En cours',
        // 'Expédié',
        // 'Terminé',
        // 'Annulé',
        /*{$addFields: {$subtract: [
            {$subtract: ["$value1","$value2"]}, //will serve as the first input for the outer $subtract
            "$value3"
            totaltax: 19.43,
            discounted_amount: 48.56,

        ]}*/
        let totalgains=totalearnings-totalcommissionearned;
        let all_earning = await orders.aggregate([
            {
                $group:{
                    _id:null,
                    total_amount:{$sum:"$payment_amount"},
                    total_commission:{$sum:"$totalcommission"},
                    total_shipping:{$sum:"$totalshipping"},
                    total_tax:{$sum:"$totaltax"},
                    discounted_amounttt:{$sum:"$discounted_amount"},
                    totalEarning:{
                        $sum:{
                            $subtract:[{$subtract:["$payment_amount","$totalcommission"]},"$totalshipping" ]
                        },
                    } 
                     
                }
            }
        ])
        // orders-number-done
        // sales -earnings-done
        // totalpageviews-number
        // gains-earning

        // totalsalesthismongth
        // total ordersthis mongth

        // orders-
        //     total-done
        //     completed-done
        //     ongoing-done
        //     cancelled-done
        //     refunded -done

        // products
        //     -total
        //     -in stock
        //     -out of stock
        // reviews
                return res.send({
                    status:true,
                    data:{
                        all_earning,
                        totalgains,
                        totalreviews,
                        // salesthismonth,
                        ordersthismonth,
                        totalsellerthismonth,
                        sellerthismonthnotapproved,
                        totoalnumberoforders,
                        totalearnings,
                        totalcommissionearned,
                        totalproductsthismonth,
                        base_url,
                        numberofdaysarray,
                        totalsalesarray,
                        totalordersarray,
                        totalcommissionarray,
                        moment,
                        totalwithdrawls,
                        reservations:ordersthismonth,
                        orderspie,
                        deliverpie
                    
                    }
                })
                // res.render("admin-panel/index.ejs", {
                //   salesthismonth,
                //   totalsellerthismonth,
                //   sellerthismonthnotapproved,
                //   totoalnumberoforders,
                //   totalearnings,
                //   totalcommissionearned,
                //   totalproductsthismonth,
                //   base_url,
                //   numberofdaysarray,
                //     totalsalesarray,
                //     totalordersarray,
                //     totalcommissionarray,
                //   moment,
                //   totalwithdrawls,
                //   reservations:ordersthismonth,
                //   orderspie
                
                // });
            },
            
    renderhomepageSeller : async (req, res) => {
            
        // totalpageview-done
        // totalorder-done
        // total order/sales
        // .total-done
        // .processing-done
        // .cancelled-done
        // .completed-done
        // .returned-done

        // daily earnings

        // totalgains=done
        const id=req.params.id;
        let date = new Date(), y = date.getFullYear(), m = date.getMonth();
        let currDate = new Date(y, m, 1);
        let daydateto30 = new Date(y, m + 1, 0);

        let numberofdays = getDays(currDate.getFullYear(), currDate.getMonth() + 1);
        let numberofdaysarray = [];
        let totalearningsarray = [];


        const monthrange = {
        $and: [
            {
                createdAt: {
                    $gt: currDate
                }
            },
            {
                createdAt: {
                    $lte: daydateto30
                }
            }
        ]
        };

        const totalpageViews=(await pageViewsModel.findOne({provider_id:id}))?.count||0
        const totalorders = await orders.find({});
        const totalordersthismonth = await orders.find({monthrange});

        const proccessingorders = (await orders.find({ status: 0 })).length;
        const completedorders = (await orders.find({ status: 1 })).length;
        const cancelledorders = (await orders.find({ status: 2 })).length;
        const refundedorders = (await orders.find({ status: 4 })).length;//something fishey here
        const sellerobj=await sellers.findById(id);
        const totalearnings = sellerobj?.totalpendingamount+sellerobj.totalpaidamount;

        // const totalcommissionearned = ordersthismonth.reduce((accum, order) => {
        //     return accum + order.totalcommission
        // }, 0);


        // console.log(reservations);
        const totoalnumberoforders = totalorders.length;
        const totalwithdrawls = 0;


        for (let i = 1; i <= numberofdays; i++) {
        let datetocompare = getDate(currDate.getFullYear(), currDate.getMonth() + 1, i);
        numberofdaysarray.push(i);



        //totalearning
        let totalearningonthisdate = totalordersthismonth.filter((e) => {
            // console.log("e.createdAt.toString().split('T')[0]==datetocompare.toString().split('T')[0]",e.createdAt.toString().split("T")[0],datetocompare.toString().split("T")[0])
            return e.createdAt.toISOString().split("T")[0] == datetocompare.toISOString().split("T")[0]
        });
        console.log("totalearningonthisdate ",totalearningonthisdate);
        if (totalearningonthisdate.length) {
            let totalearningonthisdateprice = totalearningonthisdate.reduce((accum, e) => accum + e.totalPrice, 0);
            totalearningsarray.push(totalearningonthisdateprice)
        } else {
            totalearningsarray.push(0)
        }


        }

        const orderspie = [
        totoalnumberoforders,
        completedorders,
        proccessingorders,
        cancelledorders,
        refundedorders
        ]



        return res.send({
        status: true,
        data: {
            totalpageViews,
            totoalnumberoforders,
            totalearnings,
            totalearningsarray,
            totalgains:0,
            base_url,
            numberofdaysarray,


            moment,

            orderspie

        }
        })
    },
    
    renderhomepageSeller2 : async (req, res) => {
        try{
        // totalpageview-done
        // totalorder-done
        // total order/sales
        // .total-done
        // .processing-done
        // .cancelled-done
        // .completed-done
        // .returned-done

        // daily earnings

        // totalgains=done
        const id=req.body.id;
        var p_view_startdate = req.body.p_view_startdate;
        var p_view_enddate = req.body.p_view_enddate;

        const start_date=req.body.start_date;
        const end_date=req.body.end_date;

        let date = new Date(), y = date.getFullYear(), m = date.getMonth();
        var currDate;
        var daydateto30;
        if(start_date&&end_date){
            currDate= new Date(start_date);
            daydateto30= new Date(end_date);

             
            
            // To calculate the time difference of two dates
            var Difference_In_Time = daydateto30.getTime() - currDate.getTime();
            
            // To calculate the no. of days between two dates
            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
            console.log("Difference_In_Days"+Difference_In_Days);
        }else{
            currDate = new Date(y, m, 1);
            daydateto30 = new Date(y, m + 1, 0);
            var Difference_In_Days = getDays(currDate.getFullYear(), currDate.getMonth() + 1);
        }

        
        

        let numberofdays = parseInt(Difference_In_Days);
        // console.log("numberofdays"+numberofdays);
        // return false;
        let numberofdaysarray = [];
        let totalearningsarray = [];


        const monthrange = {
        $and: [
            {
                createdAt: {
                    $gt: currDate
                }
            },
            {
                createdAt: {
                    $lte: daydateto30
                }
            }
        ]
        };
        var p_query = {};
        var p_p_query = {};
        p_query['provider_id'] = id;
        if(p_view_startdate && p_view_enddate)
        {
            p_view_startdate = new Date(p_view_startdate);
            p_view_enddate = new Date(p_view_enddate);
            p_query["$and"] = [
                {created_at:{'$gte':p_view_startdate}},
                {created_at:{'$lte':p_view_enddate}}
            ]
        }else if(p_view_startdate)
        {
            p_query['created_at'] = {'$gte':p_view_startdate};
        }else if(p_view_enddate)
        {
            p_query['created_at'] = {'$lte':p_view_enddate};
        }
        //mongoose.set("debug",true);

        const totalpageViews=(await pageViewsModel.findOne(
            {provider_id:id}
            ))?.count||0
            const affichage_des_pages=(await pageViewsModel.findOne(
                p_query
                ))?.count||0
        const totalorders = await orders.find({});
        const totalordersthismonth = await orders.find({monthrange});
        const proccessingorders = (await orders.find({ status: 0 })).length;
        const completedorders = (await orders.find({ status: 1 })).length;
        const cancelledorders = (await orders.find({ status: 2 })).length;
        const refundedorders = (await orders.find({ status: 4 })).length;//something fishey here
        const sellerobj=await sellers.findById(id);
        const totalearnings = sellerobj?.totalpendingamount+sellerobj.totalpaidamount;

        // const totalcommissionearned = ordersthismonth.reduce((accum, order) => {
        //     return accum + order.totalcommission
        // }, 0);


        // console.log(reservations);
        const totoalnumberoforders = totalorders.length;
        const totalwithdrawls = 0;

        //console.log("totalordersthismonth"+totalordersthismonth);return false;
        for (let i = 1; i <= numberofdays; i++) {
        let datetocompare = getDate(currDate.getFullYear(), currDate.getMonth() + 1, i);
        numberofdaysarray.push(i);

        //console.log("datetocompare"+datetocompare);return false;


        //totalearning
        let totalearningonthisdate = totalordersthismonth.filter((e) => {
            // console.log("e.createdAt.toString().split('T')[0]==datetocompare.toString().split('T')[0]",e.createdAt.toString().split("T")[0],datetocompare.toString().split("T")[0])
            return e.createdAt.toISOString().split("T")[0] == datetocompare.toISOString().split("T")[0]
        });
        if (totalearningonthisdate.length) {
            let totalearningonthisdateprice = totalearningonthisdate.reduce((accum, e) => accum + e.totalPrice, 0);
            totalearningsarray.push(totalearningonthisdateprice)
        } else {
            totalearningsarray.push(0)
        }


        }

        const orderspie = [
        totoalnumberoforders,
        completedorders,
        proccessingorders,
        cancelledorders,
        refundedorders
        ]



        return res.send({
        status: true,
        data: {
            affichage_des_pages,
            totalpageViews,
            totoalnumberoforders,
            totalearnings,
            totalearningsarray,
            totalgains:0,
            base_url,
            numberofdaysarray,


            moment,

            orderspie

        }
        })
      }catch(error)
      {
        return res.send({
            status:false,
            message:error.message

        });
      }
    },

    usersFilter: async (req, res) => {
        try{
            // console.log(req.body);
            // return false;
            // {
            //     firstName: 'aaaaaaaaaa',
            //     lastName: 'zzzzzzzzz',
            //     email: 'az@email.com',
            //     mobileNumber: 56465487998,
            //     status: '1'
            //   }
            const {firstName,lastName,email,mobileNumber,status} = req.body;
            const limit=10;
            
            const pageno=0;
            const skip=limit*pageno;
            //console.log("pageno",pageno)
            
            let query = {"$match":{is_deleted:false} };
            if(firstName)
            {
                //query = { "$match":{"first_name": {'$regex' : '^'+firstName, '$options' : 'i'}  } }
                query["$match"]["first_name"] = {'$regex' : '^'+firstName, '$options' : 'i'};
            }
            if(lastName)
            {
                //query = { "$match":{"last_name": {'$regex' : '^'+lastName, '$options' : 'i'}  } }
                query["$match"]["last_name"] = {'$regex' : '^'+lastName, '$options' : 'i'};
            }
            if(email)
            {
                //query = { "$match":{"email": {'$regex' : '^'+email, '$options' : 'i'}  } };
                query["$match"]["email"] = {'$regex' : '^'+email, '$options' : 'i'};
            }
            if(mobileNumber)
            {
               // query = { "$match":{"phone": {'$regex' : '^'+mobileNumber, '$options' : 'i'}  } };
                query["$match"]["phone"] = {'$regex' : '^'+mobileNumber, '$options' : 'i'};
            }
            if(status == 1)
            {
                //query = { "$match":{is_active:true} };
                query["$match"]["is_active"] = true;
            }else if(status == 2)
            {
                //query = { "$match":{is_active:false} };
                query["$match"]["is_active"] = false;
            }

            //mongoose.set("debug",true);
            const users=await usersmodel.aggregate([
                query,
                {$addFields:{userIDSTR:{"$toString":"$_id"}}},
                {
                    $lookup:{
                        from:"orders",
                        localField:"userIDSTR",
                        foreignField:"userId",
                        as:"orders"
                    }
                },
                {
                    $addFields:{
                        "orderLength":{$size:"$orders"}
                    }
                },
                {
                    $project:{"orders":0}
                },
                {$skip:skip},
                {$limit:limit},
                {
                    $sort:{
                        createdAt:-1
                    }
                }
            ]);
            let cc= users.length;
            const pages=Math.ceil( cc / 10)
                 
                   

            return res.send({
                status:true,
                data:users,
                pages:pages,
                totalCounnt:cc
            })
            // return res.render("admin-panel/users.ejs", { users, base_url,moment });
        }catch(error){
            return res.send({
                status:false,
                data:[],
                errorMessage:error.message
            })
        }
        
      },
      users: async (req, res) => {
        try{
            console.log(req.query)
            const limit=10;
            
            const pageno=req.params.pageno;
            const skip=limit*pageno;
            console.log("pageno",pageno)
            if(pageno==null||pageno=="null"){
                // const count=(await usersmodel.aggregate([
                //     {$match:{is_deleted:false}},
                //     {$addFields:{userIDSTR:{"$toString":"$_id"}}},
                //     {
                //         $lookup:{
                //             from:"orders",
                //             localField:"userIDSTR",
                //             foreignField:"userId",
                //             as:"orders"
                //         }
                //     },
                //     // {$skip:skip},
                //     // {$limit:limit}
                // ]))?.length
                const count = await usersmodel.count({is_deleted:false});
                const pages=Math.ceil(count/limit)
                return res.send({
                    status:true,
                    pages:pages,
                    totalCounnt:count
                })
            }
            // const first_name=req.query.first_name?{first_name:req.query.first_name}:{};
            // const last_name=req.query.last_name?{last_name:req.query.last_name}:{};
            // const email=req.query.email?{email:req.query.email}:{};
            // const phone=req.query.phone_number?{phone:req.query.phone_number}:{};
            // const start_date=req.query.start_date?{date_of_registration:{$gte:new Date(req.query.start_date)}}:{};
            // const end_date=req.query.end_date?{date_of_registration:{$lte:new Date(req.query.end_date)}}:{};
            //mongoose.set("debug",true);
            const users=await usersmodel.aggregate([
                {$match:{is_deleted:false}},
                {$addFields:{userIDSTR:{"$toString":"$_id"}}},
                {
                    $lookup:{
                        from:"orders",
                        localField:"userIDSTR",
                        foreignField:"userId",
                        as:"orders"
                    }
                },
                {
                    $addFields:{
                        "orderLength":{$size:"$orders"}
                    }
                },
                {
                    $project:{"orders":0}
                },
                {$skip:skip},
                {$limit:limit},
                {
                    $sort:{
                        createdAt:-1
                    }
                }
            ]);
            return res.send({
                status:true,
                data:users
            })
            // return res.render("admin-panel/users.ejs", { users, base_url,moment });
        }catch(error){
            return res.send({
                status:false,
                data:[],
                errorMessage:error.message
            })
        }
        
      },
      allActiveUsersAdmin: async (req, res) => {
        try{
            console.log(req.query)
            const limit=10;
            
            const pageno=req.params.pageno;
            const skip=limit*pageno;
            console.log("pageno",pageno)
            if(pageno==null||pageno=="null"){
                 
                const count = await usersmodel.count({is_deleted:false,is_active: true});
                const pages=Math.ceil(count/limit)
                return res.send({
                    status:true,
                    pages:pages,
                    totalCounnt:count
                })
            }
             
            //mongoose.set("debug",true);
            const users=await usersmodel.aggregate([
                {$match:{is_deleted:false}},
                {
                    $match:{
                        is_active: true
                    }
                },
                {$addFields:{userIDSTR:{"$toString":"$_id"}}},
                {
                    $lookup:{
                        from:"orders",
                        localField:"userIDSTR",
                        foreignField:"userId",
                        as:"orders"
                    }
                },
                {
                    $addFields:{
                        "orderLength":{$size:"$orders"}
                    }
                },
                {
                    $project:{"orders":0}
                },
                {$skip:skip},
                {$limit:limit},
                {
                    $sort:{
                        createdAt:-1
                    }
                }
            ]);
            return res.send({
                status:true,
                data:users
            })
            // return res.render("admin-panel/users.ejs", { users, base_url,moment });
        }catch(error){
            return res.send({
                status:false,
                data:[],
                errorMessage:error.message
            })
        }
        
      },
      allInActiveUsersAdmin: async (req, res) => {
        try{
            console.log(req.query)
            const limit=10;
            
            const pageno=req.params.pageno;
            const skip=limit*pageno;
            console.log("pageno",pageno)
            if(pageno==null||pageno=="null"){
                 
                const count = await usersmodel.count({is_deleted:false,is_active: false});
                const pages=Math.ceil(count/limit)
                return res.send({
                    status:true,
                    pages:pages,
                    totalCounnt:count
                })
            }
             
            //mongoose.set("debug",true);
            const users=await usersmodel.aggregate([
                {$match:{is_deleted:false}},
                {
                    $match:{
                        is_active: false
                    }
                },
                {$addFields:{userIDSTR:{"$toString":"$_id"}}},
                {
                    $lookup:{
                        from:"orders",
                        localField:"userIDSTR",
                        foreignField:"userId",
                        as:"orders"
                    }
                },
                {
                    $addFields:{
                        "orderLength":{$size:"$orders"}
                    }
                },
                {
                    $project:{"orders":0}
                },
                {$skip:skip},
                {$limit:limit},
                {
                    $sort:{
                        createdAt:-1
                    }
                }
            ]);
            return res.send({
                status:true,
                data:users
            })
            // return res.render("admin-panel/users.ejs", { users, base_url,moment });
        }catch(error){
            return res.send({
                status:false,
                data:[],
                errorMessage:error.message
            })
        }
        
      },
      allDeletedUsersAdmin: async (req, res) => {
        try{
            console.log(req.query)
            const limit=10;
            
            const pageno=req.params.pageno;
            const skip=limit*pageno;
            console.log("pageno",pageno)
            if(pageno==null||pageno=="null"){
                 
                const count = await usersmodel.count({is_deleted:true});
                const pages=Math.ceil(count/limit)
                return res.send({
                    status:true,
                    pages:pages,
                    totalCounnt:count
                })
            }
             
            //mongoose.set("debug",true);
            const users=await usersmodel.aggregate([
                {$match:{is_deleted:true}},
                {$addFields:{userIDSTR:{"$toString":"$_id"}}},
                {
                    $lookup:{
                        from:"orders",
                        localField:"userIDSTR",
                        foreignField:"userId",
                        as:"orders"
                    }
                },
                {
                    $addFields:{
                        "orderLength":{$size:"$orders"}
                    }
                },
                {
                    $project:{"orders":0}
                },
                {$skip:skip},
                {$limit:limit},
                {
                    $sort:{
                        createdAt:-1
                    }
                }
            ]);
            return res.send({
                status:true,
                data:users
            })
            // return res.render("admin-panel/users.ejs", { users, base_url,moment });
        }catch(error){
            return res.send({
                status:false,
                data:[],
                errorMessage:error.message
            })
        }
        
      },
      getSingleUsers : async (req,res) =>{
        console.log("getSingleUsers ->>> "+JSON.stringify(req.params));
        //mongoose.set("debug",true);
        //mongoose.set('debug', true);
        var user_id = req.params.user_id;
        console.log("user_id ->>> "+ user_id);
        const users=await usersmodel.aggregate([
            {$match:{_id:mongoose.Types.ObjectId(user_id)}},
            {$limit:1}
        ]);

        return res.send({
            status:true,
            data:users
        })
      },
      reports : async (req, res) => {

        //mongoose.set('debug', true);
        const byday=req.body.byday;
        const byyear=req.body.byyear;
        const byvendor=req.body.byvendor;
        const start_date=req.body.start_date;
        const end_date=req.body.end_date;
        let date = new Date(), y = date.getFullYear(), m = date.getMonth();
        let currDate;
        let daydateto30;
        if(start_date&&end_date){
            currDate= new Date(start_date);
            daydateto30= new Date(end_date);
        }else if(byday){
            currDate= new Date();
            daydateto30= new Date();
        }else if(byyear){
            currDate= new Date(y, m, 1);
            daydateto30= new Date(y, m + 12, 0);
        }else{
            currDate= new Date(y, m, 1);
            daydateto30= new Date(y, m + 1, 0);
        }
        
        // let currDate=getdateinadvance(1);
        // let daydateto30=getdateinadvance(30);
        //  console.log("currDate",currDate,"daydateto30",daydateto30)
        let numberofdays=getDays(currDate.getFullYear(),currDate.getMonth()+1);
        let numberofdaysarray=[];
        let totalsalesarray=[];
        let totalordersarray=[];
        let totalcommissionarray=[];
       
        // console.log("numberofdaysarray",numberofdaysarray)
        
        const monthrange={
            
            $and:[
            {createdAt:{
            $gt:currDate
            }},
            {createdAt:{
                $lte:daydateto30
            }}
        ]};
        console.log("monthrange",monthrange,"currDate",currDate,"daydateto30",daydateto30)
        const totalreviews= (await Ratingnreviews.find(monthrange))?.length;
        const salesthismonth=await ordersModel.find(monthrange);
        const sellerthismonth=await sellers.find(monthrange);
        const totalsellerthismonth=sellerthismonth.length;
        const sellerthismonthnotapproved=sellerthismonth.filter(
            (seller) => seller.approved ==false
          ).length;
        const ordersthismonth= await orders.find(monthrange);
        const proccessingorders= (await orders.find({status:1})).length;
        const completedorders= (await orders.find({status:2})).length;
        const cancelledorders= (await orders.find({status:3})).length;
        const refundedorders= (await orders.find({status:4})).length;
        // console.log("ordersthismonth",ordersthismonth)
        const totalearnings=ordersthismonth.reduce((accum,order)=>{
            return accum+order.totalPrice
        },0);
        const totalcommissionearned=ordersthismonth.reduce((accum,order)=>{
            return accum+order.totalcommission
        },0);
       
        const totalproductsthismonth=(await Product.find(monthrange)).length;
        // console.log(reservations);
        const totoalnumberoforders = ordersthismonth.length;
        const totalwithdrawls=0;


        for(let i=1;i<=numberofdays;i++){
            let datetocompare=getDate(currDate.getFullYear(),currDate.getMonth()+1,i);
            numberofdaysarray.push(i);
            let totalsalesonthisdate=salesthismonth.filter((e)=>{
                // console.log("e.createdAt==datetocompare",e.createdAt,datetocompare)
                return e.createdAt.toISOString().split("T")[0]==datetocompare.toISOString().split("T")[0]
            });
            if(totalsalesonthisdate.length){
                let totalsalesonthisdateqty=totalsalesonthisdate.reduce((accum,e)=>accum+e.quantity,0);
                totalsalesarray.push(totalsalesonthisdateqty)
            }else{
                totalsalesarray.push(0)
            }
            //total orders
            let totalordersonthisdate=ordersthismonth.filter((e)=>{
                // console.log("e.createdAt.toString().split('T')[0]==datetocompare.toString().split('T')[0]",e.createdAt.toString().split("T")[0],datetocompare.toString().split("T")[0])
                return e.createdAt.toISOString().split("T")[0]==datetocompare.toISOString().split("T")[0]
            });
            if(totalordersonthisdate.length){
                let totalordersonthisdateqty=totalordersonthisdate.reduce((accum,e)=>accum+e.totalproductsinorder,0);
                totalordersarray.push(totalordersonthisdateqty)
            }else{
                totalordersarray.push(0)
            }

            //total commissions
            let totalcommissionsonthisdate=ordersthismonth.filter((e)=>{
                // console.log("e.createdAt==datetocompare",e.createdAt,datetocompare)
                return e.createdAt.toISOString().split("T")[0]==datetocompare.toISOString().split("T")[0]
            });
            if(totalcommissionsonthisdate.length){
                let totalcommissionsonthisdateqty=totalcommissionsonthisdate.reduce((accum,e)=>accum+e.totalcommission,0);
                totalcommissionarray.push(totalcommissionsonthisdateqty)
            }else{
                totalcommissionarray.push(0)
            }
            // console.log("datetocompare",datetocompare,"currDate.getFullYear(),currDate.getMonth()+1,i",currDate.getFullYear(),currDate.getMonth()+1,i)
        }
        let totaldelivery=ordersthismonth.length;
        let completeddelivery=ordersthismonth.filter((e)=>e.delivery_status==3)
        let somethinginbetween=0
        let intransistdelivery=ordersthismonth.filter((e)=>e.delivery_status==1||e.delivery_status==2)
        const orderspie=[
            ordersthismonth.length,
            completedorders,
            proccessingorders,
            cancelledorders,
            refundedorders
        ]

        const deliverypie=[
            totaldelivery,
            completeddelivery,
            somethinginbetween,
            intransistdelivery
    ]

let totalgains=totalearnings-totalcommissionearned;

// orders-number-done
// sales -earnings-done
// totalpageviews-number
// gains         -earning

// totalsalesthismongth
// total ordersthis mongth

// orders-
//     total-done
//     completed-done
//     ongoing-done
//     cancelled-done
//     refunded -done

// products
//     -total
//     -in stock
//     -out of stock
// reviews
        return res.send({
            status:true,
            data:{
                totalgains,
                totalreviews,
                salesthismonth:salesthismonth?.length,
                totalsellerthismonth,
                sellerthismonthnotapproved,
                totoalnumberoforders,
                totalearnings,
                totalcommissionearned,
                totalproductsthismonth,
                base_url,
                numberofdaysarray,
                //   totalsalesarray,
                  totalordersarray,
                //   totalcommissionarray,
                moment,
                // totalpageViews,
                totalwithdrawls,
                // reservations:ordersthismonth,
                // orderspie,
                // deliverypie
               
              }
        })
        // res.render("admin-panel/index.ejs", {
        //   salesthismonth,
        //   totalsellerthismonth,
        //   sellerthismonthnotapproved,
        //   totoalnumberoforders,
        //   totalearnings,
        //   totalcommissionearned,
        //   totalproductsthismonth,
        //   base_url,
        //   numberofdaysarray,
        //     totalsalesarray,
        //     totalordersarray,
        //     totalcommissionarray,
        //   moment,
        //   totalwithdrawls,
        //   reservations:ordersthismonth,
        //   orderspie
         
        // });
      },
      alllogs:async(req,res)=>{
       const start_date=req.body.start_date;
        const end_date=req.body.end_date;
        const seller_id=req.body.seller_id;
        const order_id=req.body.order_id;
        const status=req.body.status
        const query={payment_status:true}
        if(seller_id){
            query["products"]={$elemMatch:{"product.provider_id":seller_id}}
        }
        if(order_id){
            query["order_id"]=order_id
        }
        if(status?.toString()){
            query["status"]=parseInt(status)
        }
        let date = new Date(), y = date.getFullYear(), m = date.getMonth();
        let currDate;
        let daydateto30;
        if(start_date&&end_date){
            currDate= new Date(start_date);
            daydateto30= new Date(end_date);
            query["$and"]=[
            {createdAt:{
            $gt:currDate
            }},
            {createdAt:{
                $lte:daydateto30
            }}
        ]
        }else if(start_date&&!end_date){
            currDate= new Date(start_date);
           
            query["createdAt"]={
                $gt:currDate
                }
            
        }else if(!start_date&&end_date){
            console.log("end_date",end_date)
            daydateto30= new Date(end_date);
            console.log("end_date",daydateto30)
            daydateto30=new Date(daydateto30)
            console.log("end_date",daydateto30)
            query["createdAt"]={
                $lte:daydateto30
                }
        }
        else{
            currDate= new Date(y, m, 1);
            daydateto30= new Date(y, m + 1, 0);
            query["$and"]=[
                {createdAt:{
                $gt:currDate
                }},
                {createdAt:{
                    $lte:daydateto30
                }}
            ]
        }
        
        // const monthrange={$and:[
        //     {createdAt:{
        //     $gt:currDate
        //     }},
        //     {createdAt:{
        //         $lte:daydateto30
        //     }}
        // ]};
        const pageno=req.body.pageno
        const limit=10;
        
       
        const skip=limit*pageno;
            if(pageno==null||pageno=="null"){
            const count=(await ordersModel.aggregate([
                {$match:query},
                {$addFields:{
                    "sellerearning":{
                        $subtract:["$totalPrice",{$add:["$totalcommission",{$add:["$totaltax",{$add:["$totalplusperproduct","$totalshipping"]}]}]}]
                    },
                    "singleprovider":{
                        arrayElemAt:["$provider",0]
                    }
                }},
                {
                    $addFields:{
                        sellerinfo:{
                            $map:
                            {
                              input: "$products",
                              as: "product",
                              in: { 
                                "isreadytopickup":{
                                    $cond:{
                                        if:{$gt:["$$product.readytopickup",null]},
                                        then:"$$product.readytopickup",
                                        else:false
                                    }
                                },
                                "price":"$$product.pricetouser",
                                "sellername":{
                                    $cond:{
                                        if:{$gt:["$$product.product.provider_name",null]},
                                        then:"$$product.product.provider_name",
                                        else:""
                                    }
                                },
                                "tax":"$$product.tax",
                                "shipping_cost":"$$product.shipping_cost",
                                "commission":"$$product.commission",
                                "sellerprofit":"$$product.sellerprofit",
                                // "is_paid":"$$product.is_paid",
                                "readytopickup":"$$product.readytopickup"
                               }
                            }
                        }
                    }
                },
                {$project:{
                    "order_id":1,
                    "sellerinfo":1,
                    "totalPrice":1,
                    "sellerearning":1,
                    "totalcommission":1,
                    "totalshipping":1,
                    "totaltax":1,
                    "status":1,
                    "createdAt":1,
                    "status":1
                }}
            ]))?.length
    
            const pages=Math.ceil(count/limit)
            return res.send({
                status:true,
                pages:pages
            })
        }
        ///console.log("hereeeeeeeee 1203");
        //mongoose.set("debug",true);
        const orders=await ordersModel.aggregate([
            {$match:query},
            {$addFields:{
                "sellerearning":{
                    $subtract:["$totalPrice",{$add:["$totalcommission",{$add:["$totaltax",{$add:["$totalplusperproduct","$totalshipping"]}]}]}]
                },
                "singleprovider":{
                    arrayElemAt:["$provider",0]
                }
            }},
            {
                $addFields:{
                    sellerinfo:{
                        $map:
                        {
                          input: "$products",
                          as: "product",
                          in: { 
                            "isreadytopickup":{
                                $cond:{
                                    if:{$gt:["$$product.readytopickup",null]},
                                    then:"$$product.readytopickup",
                                    else:false
                                }
                            },
                            "price":"$$product.pricetouser",
                            "sellername":{
                                $cond:{
                                    if:{$gt:["$$product.product.provider_name",null]},
                                    then:"$$product.product.provider_name",
                                    else:""
                                }
                            },
                            "tax":"$$product.tax",
                            "shipping_cost":"$$product.shipping_cost",
                            "commission":"$$product.commission",
                            "sellerprofit":"$$product.sellerprofit",
                            // "is_paid":"$$product.is_paid",
                            "readytopickup":"$$product.readytopickup"
                           }
                        }
                    }
                }
            },
            {$skip:skip},
            {$limit:limit},
            {$project:{
                "order_id":1,
                "sellerinfo":1,
                "totalPrice":1,
                "sellerearning":1,
                "totalcommission":1,
                "totalshipping":1,
                "totaltax":1,
                "status":1,
                "createdAt":1,
                "status":1
            }}
        ]);
        return res.send({
            status:true,
            data:orders
        })
      },
     termsandcondition:(req,res)=>{
        return res.render("misc/termsandcondition.ejs")
      },
     privacypolicy:(req,res)=>{
        return res.render("misc/privacypolicy.ejs")
      },
    aboutoblack:(req,res)=>{
    return res.render("misc/aboutoblack.ejs")
    },
    getorderdetailbyorderid:async(req,res)=>{
        const id=req.params.id
        let objectid;
        if(id){
            objectid=mongoose.Types.ObjectId(id)
        }
        const orders=await ordersModel.aggregate([
            {$match:{_id:objectid}},
            {
                $addFields:{
                    userObjID:{"$toObjectId":"$userId"}
                }
            },
            {
                $lookup:{
                    from:"users",
                    foreignField:"_id",
                    localField:"userObjID",
                    as:"user"
                }
            },
           

            {$addFields:{
                "userobject":{$arrayElemAt:["$user",0]},
                "sellerearning":{
                    $subtract:["$totalPrice",{$add:["$totalcommission",{$add:["$totaltax",{$add:["$totalplusperproduct","$totalshipping"]}]}]}]
                },
                "singleprovider":{
                    arrayElemAt:["$provider",0]
                },
                "stringorderid":{"$toString":"$order_id"}
            }},
            {
                $addFields:{
                    // allproducts:"$products",
                    sellerinfo:{
                        $map:
                        {
                          input: "$products",
                          as: "product",
                          in: { 
                            "isreadytopickup":{
                                $cond:{
                                    if:{$gt:["$$product.readytopickup",null]},
                                    then:"$$product.readytopickup",
                                    else:false
                                }
                            },
                            // "part_id":{$concat: ["$stringorderid", {$concat:["-",{"$toString":{$add:[{$indexOfArray:["$products","$$product"]},1]}}]}]},
                            "price":"$$product.pricetouser",
                            "part_id":"$$product.part_id",
                            "delivery_status":"$$product.delivery_status",
                            "sellername":"$$product.provider_name",
                            "tax":"$$product.tax",
                            "shipping_cost":"$$product.shipping_cost",
                            "commission":"$$product.commission",
                            "sellerprofit":"$$product.sellerprofit",
                            "variation_id":"$$product.variation_id",
                            "readytopickup":"$$product.readytopickup",
                            "userfirstname":"$userobject.first_name",
                            "userlastname":"$userobject.last_name",
                            "product":"$$product",
                            "deliveryaddress":"$shippingaddress"
                           }
                        }
                    }
                }
            },
            {$project:{
                "order_id":1,
                "sellerinfo":1,
                "totalPrice":1,
                "sellerearning":1,
                "totalcommission":1,
                "totalshipping":1,
                "totaltax":1,
                "status":1,
                "createdAt":1,
                "status":1
            }}
        ]);
        const neworderdata=[]
        const sellerinfo=orders[0].sellerinfo;
       for(let i=0;i<sellerinfo.length;i++){
            e=sellerinfo[i];
            //console.log("eeee "+ JSON.stringify(e));return false;
            
            let varid= e.variation_id ? mongoose.Types.ObjectId(e.variation_id) : "";
            //console.log("e",e)
            const datatoadd=await VariationsPrice.aggregate([
                {$match:{_id:varid}},
                
                {
                            $addFields:{
                                phototoshow:{
                                    $cond:{
                                        if:{$gt:["$images",null]},
                                        then:{
                                            $cond:{
                                                if:{$gt:[{$size:"$images"},0]},
                                                // then:{$gt:[{$size:"$singlevariations.images"},0]},
                                                // else:{$gt:[{$size:"$singlevariations.images"},0]}
                                                then:"$images",
                                                else:e?.product.product?.images
                                            }
                                        },
                                        // then:"$singlevariations.images",
                                        else:e?.product.product?.images
                                        }
                                    }
                                // 
                                // phototoshow:[]
                            }
                        },
            ])
            const seller=await sellers.findById(e.product.product?.provider_id)
            const address= seller?.pickupaddress;
            //console.log("address",address)
            //console.log("seller",seller)
            let phototoshowarray;
            let photo_array = [];
            if(datatoadd.length>0)
            {
                phototoshowarray=datatoadd[0]?.phototoshow
                // console.log("here 894 "+typeof(datatoadd[0]?.phototoshow));
                // console.log("here 894 "+JSON.stringify(datatoadd[0]?.phototoshow));
                if(datatoadd[0]?.phototoshow != "")
                {
                    // console.log("ifff value");
                    // console.log("here 894 "+datatoadd[0]?.phototoshow[0]);
                    photo_array = datatoadd[0]?.phototoshow[0];
                }
                
            }else{
                phototoshowarray=e?.product.product?.images
            }
            
            // let newobjectopush={e,phototoshow:phototoshowarray}

           //console.log("phototoshowarray "+ e?.product.product?.images);
            e["phototoshow"]=phototoshowarray;
            e["photo_array"] = photo_array;
            //e["photoToShowArray"]=phototoshowarray.split(",");
            e["address"]=address[0]
            // neworderdata.push(e)
        }
        orders["sellerinfo"]=neworderdata
        return res.send({
            status:true,
            data:orders
        })
    }
}