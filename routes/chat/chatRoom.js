const express =require('express');
// controllers
const chatRoom = require('./chatRoom.controller');
const ChatController = require('./ChatController');

const router = express.Router();
const multer = require("multer");
const path=require("path")
const admin_lawyer_profile_path=process.env.admin_lawyer_profile_path;
const admin_user_profile_path=process.env.admin_user_profile_path;
const admin_images_path=process.env.admin_images_path;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(admin_user_profile_path))
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
})

const upload2 = multer({ storage: storage });
//{{local}}/api/chat
router
  .get('/:userId', chatRoom.getRecentConversation)
  .get('/newroom/:roomId', chatRoom.getConversationByRoomId)
  .post('/initiate', chatRoom.initiate)
  .post('/:roomId/message', chatRoom.postMessage)
  .post('/:roomId/mark-read', chatRoom.markConversationReadByRoomId)
  .get('/hidechatforuser/:id',chatRoom.hidechatforuser)
  .get('/hidechatforprovider/:id',chatRoom.hidechatforprovider)
  .get('/getchatsbyroomidforuser/:id',chatRoom.getchatsbyroomidforuser)
  .get('/getchatsbyroomidforprovider/:id',chatRoom.getchatsbyroomidforprovider)
  .get('/getallchatsbyclientid/:id',chatRoom.getallchatsbyclientid)
  .get('/getallchatsbyproviderid/:id',chatRoom.getallchatsbyproviderid)
  .get('/saveimages',upload2.fields([
    { 
      name: 'images', 
      maxCount: 10
    }
    
  
  ]
  ),chatRoom.saveimages)  
.post("/new_initiate_chat",ChatController.new_initiate_chat)
.get("/get_all_user_chat/:id",ChatController.get_all_user_chat)
.get("/get_all_seller_chat/:id",ChatController.get_all_seller_chat)
.get("/get_all_message_room_seller/:id",ChatController.get_all_message_room_seller)
.get("/get_all_message_room_user/:id",ChatController.get_all_message_room_user)
.get("/delete_chat_user_by_room_id/:id",ChatController.delete_chat_user_by_room_id)
.get("/delete_chat_seller_by_room_id/:id",ChatController.delete_chat_seller_by_room_id)
.post("/report_chat_by_seller",ChatController.report_chat_by_seller)
.post("/report_chat_by_user",ChatController.report_chat_by_user)
.post("/check_cht_available_or_not",ChatController.check_cht_available_or_not)
.post("/get_all_message_by_product_user_id",ChatController.get_all_message_by_product_user_id)
module.exports=router;