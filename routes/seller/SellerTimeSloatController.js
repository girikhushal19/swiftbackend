const Seller = require("../../models/seller/Seller");
const orders = require("../../models/orders/order");
const PaymentRequestmodel=require("../../models/admin/PaymentRequest");
const SubscriptionModel=require("../../models/subscriptions/Subscription");
const pageViewsModel=require("../../models/products/pageViews");
const bycrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const mongoose=require("mongoose");
const CoupensModel=require("../../models/seller/Coupens");
const SellerContactModel=require("../../models/seller/SellerContactModel");
const SellerQueryModel=require("../../models/seller/SellerQueryModel");
const SellerTimeSloats=require("../../models/seller/SellerTimeSloats");

// const PaymentRequestmodel=require("../../models/admin/PaymentRequest");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const ejs = require('ejs');
const {getdateinadvance,getDays,getDate} =require("../../modules/dates.js")
const customConstant = require('../../helpers/customConstant');
const SellertermsconditionModel = require("../../models/admin/SellertermsconditionModel");
const { Product } = require("../../models/products/Product");
const fs = require("fs");
const {
    sendmail
} = require("../../modules/sendmail");
const apptext_cmsmodel = require("../../models/admin/Apptext");
const transport = nodemailer.createTransport({
    name: "Swift",
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    auth: {
      user: process.env.MAILER_EMAIL_ID,
      pass: process.env.MAILER_PASSWORD,
    }
});

module.exports = {
    addTimeSloatSeller:async(req,res)=>
    {
        try{
            //console.log("req.body " , JSON.stringify(req.body));
            let {seller_id,deliveryType,monday,tuesday,wednesday,thrusday,friday,saturday,sunday,quantities,quantitiesTuesday,quantitieswednesday,quantitiesthrusday,quantitiesfriday,quantitiessaturday,quantitiessunday} = req.body.formdata;
            //console.log("seller_id ", seller_id);
            
            if(quantities)
            {
                if(quantities.length > 0)
                {
                    for(let x=0; x<quantities.length; x++)
                    {
                        //console.log("mondayStartTime " , quantities[x].mondayStartTime);
                        //console.log("mondayEndTime " , quantities[x].mondayEndTime);
                        if(quantities[x].mondayStartTime > 0 || quantities[x].mondayEndTime > 0)
                        {
                            let arr = {
                                "seller_id":seller_id,
                                "delivery_type":deliveryType,
                                "day_enable":monday,
                                "day_name":"monday",
                                "day_number":1,
                                "start_time":quantities[x].mondayStartTime,
                                "end_time":quantities[x].mondayEndTime
                            }
                            let check = await SellerTimeSloats.findOne(arr);
                            if(!check)
                            {
                                await SellerTimeSloats.create(arr);
                                //console.log("hereeee iffffff ");
                            }
                            //console.log("hereeee 67 line No. ");
                        }
                    }
                }
            }
            if(quantitiesTuesday)
            {
                if(quantitiesTuesday.length > 0)
                {
                    for(let x=0; x<quantitiesTuesday.length; x++)
                    {
                        //console.log("tuesdayStartTime " , quantitiesTuesday[x].tuesdayStartTime);
                        //console.log("tuesdayEndTime " , quantitiesTuesday[x].tuesdayEndTime);
                        if(quantitiesTuesday[x].tuesdayStartTime > 0 || quantitiesTuesday[x].tuesdayEndTime > 0)
                        {
                            let arr = {
                                "seller_id":seller_id,
                                "delivery_type":deliveryType,
                                "day_enable":tuesday,
                                "day_name":"tuesday",
                                "day_number":2,
                                "start_time":quantitiesTuesday[x].tuesdayStartTime,
                                "end_time":quantitiesTuesday[x].tuesdayEndTime
                            }
                            let check = await SellerTimeSloats.findOne(arr);
                            if(!check)
                            {
                                await SellerTimeSloats.create(arr);
                                //console.log("hereeee iffffff ");
                            }
                            //console.log("hereeee 67 line No. ");
                        }
                    }
                }
            }

            if(quantitieswednesday)
            {
                if(quantitieswednesday.length > 0)
                {
                    for(let x=0; x<quantitieswednesday.length; x++)
                    {
                        //console.log("wednesdayStartTime " , quantitieswednesday[x].wednesdayStartTime);
                        //console.log("wednesdayEndTime " , quantitieswednesday[x].wednesdayEndTime);
                        if(quantitieswednesday[x].wednesdayStartTime > 0 || quantitieswednesday[x].wednesdayEndTime > 0)
                        {
                            let arr = {
                                "seller_id":seller_id,
                                "delivery_type":deliveryType,
                                "day_enable":wednesday,
                                "day_name":"wednesday",
                                "day_number":3,
                                "start_time":quantitieswednesday[x].wednesdayStartTime,
                                "end_time":quantitieswednesday[x].wednesdayEndTime
                            }
                            let check = await SellerTimeSloats.findOne(arr);
                            if(!check)
                            {
                                await SellerTimeSloats.create(arr);
                                //console.log("hereeee iffffff ");
                            }
                            //console.log("hereeee 67 line No. ");
                        }
                    }
                }
            }
            if(quantitiesthrusday)
            {
                if(quantitiesthrusday.length > 0)
                {
                    for(let x=0; x<quantitiesthrusday.length; x++)
                    {
                        //console.log("thrusdayStartTime " , quantitiesthrusday[x].thrusdayStartTime);
                        //console.log("thrusdayEndTime " , quantitiesthrusday[x].thrusdayEndTime);
                        if(quantitiesthrusday[x].thrusdayStartTime > 0 || quantitiesthrusday[x].thrusdayEndTime > 0)
                        {
                            let arr = {
                                "seller_id":seller_id,
                                "delivery_type":deliveryType,
                                "day_enable":thrusday,
                                "day_name":"thrusday",
                                "day_number":4,
                                "start_time":quantitiesthrusday[x].thrusdayStartTime,
                                "end_time":quantitiesthrusday[x].thrusdayEndTime
                            }
                            let check = await SellerTimeSloats.findOne(arr);
                            if(!check)
                            {
                                await SellerTimeSloats.create(arr);
                                //console.log("hereeee iffffff ");
                            }
                            //console.log("hereeee 67 line No. ");
                        }
                    }
                }
            }
            if(quantitiesfriday)
            {
                if(quantitiesfriday.length > 0)
                {
                    for(let x=0; x<quantitiesfriday.length; x++)
                    {
                        //console.log("fridayStartTime " , quantitiesfriday[x].fridayStartTime);
                        //console.log("fridayEndTime " , quantitiesfriday[x].fridayEndTime);
                        if(quantitiesfriday[x].fridayStartTime > 0 || quantitiesfriday[x].fridayEndTime > 0)
                        {
                            let arr = {
                                "seller_id":seller_id,
                                "delivery_type":deliveryType,
                                "day_enable":friday,
                                "day_name":"friday",
                                "day_number":5,
                                "start_time":quantitiesfriday[x].fridayStartTime,
                                "end_time":quantitiesfriday[x].fridayEndTime
                            }
                            let check = await SellerTimeSloats.findOne(arr);
                            if(!check)
                            {
                                await SellerTimeSloats.create(arr);
                                //console.log("hereeee iffffff ");
                            }
                            //console.log("hereeee 67 line No. ");
                        }
                    }
                }
            }
            if(quantitiessaturday)
            {
                if(quantitiessaturday.length > 0)
                {
                    for(let x=0; x<quantitiessaturday.length; x++)
                    {
                        //console.log("saturdayStartTime " , quantitiessaturday[x].saturdayStartTime);
                        //console.log("saturdayEndTime " , quantitiessaturday[x].saturdayEndTime);
                        if(quantitiessaturday[x].saturdayStartTime > 0 || quantitiessaturday[x].saturdayEndTime > 0)
                        {
                            let arr = {
                                "seller_id":seller_id,
                                "delivery_type":deliveryType,
                                "day_enable":saturday,
                                "day_name":"saturday",
                                "day_number":6,
                                "start_time":quantitiessaturday[x].saturdayStartTime,
                                "end_time":quantitiessaturday[x].saturdayEndTime
                            }
                            let check = await SellerTimeSloats.findOne(arr);
                            if(!check)
                            {
                                await SellerTimeSloats.create(arr);
                                //console.log("hereeee iffffff ");
                            }
                            //console.log("hereeee 67 line No. ");
                        }
                    }
                }
            }
            if(quantitiessunday)
            {
                if(quantitiessunday.length > 0)
                {
                    for(let x=0; x<quantitiessunday.length; x++)
                    {
                        //console.log("sundayStartTime " , quantitiessunday[x].sundayStartTime);
                        //console.log("sundayEndTime " , quantitiessunday[x].sundayEndTime);
                        if(quantitiessunday[x].sundayStartTime > 0 || quantitiessunday[x].sundayEndTime > 0)
                        {
                            let arr = {
                                "seller_id":seller_id,
                                "delivery_type":deliveryType,
                                "day_enable":sunday,
                                "day_name":"sunday",
                                "day_number":0,
                                "start_time":quantitiessunday[x].sundayStartTime,
                                "end_time":quantitiessunday[x].sundayEndTime
                            }
                            let check = await SellerTimeSloats.findOne(arr);
                            if(!check)
                            {
                                await SellerTimeSloats.create(arr);
                                //console.log("hereeee iffffff ");
                            }
                            //console.log("hereeee 67 line No. ");
                        }
                    }
                }
            }
            //console.log("hereeee 72 line No. ");
            return res.send({
                status:true,
                message:"Le temps de latence a pleinement contribué au succès de l'opération"
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    getTimeSloatSeller:async(req,res)=>{
        try{
            let id = req.params.id;
            let record = await SellerTimeSloats.aggregate([
                {
                    $match:{
                        seller_id:id
                    }
                }
            ]).sort({
                day_number:1
            });
            return res.send({
                status:true,
                message:"Success",
                record:record
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    deleteTimeSloatSeller:async(req,res)=>{
        try{
            let id = req.params.id;
            //mongoose.set("debug",true);
            let record = await SellerTimeSloats.findOne({ _id:id });
            //console.log("record ", record);
            if(!record)
            {
                return res.send({
                    status:false,
                    message:"ID invalide"
                });
            }
            await SellerTimeSloats.deleteOne({ _id:id });
            return res.send({
                status:true,
                message:"L'enregistrement a été supprimé avec succès"
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    enableDisableTimeSloat:async(req,res)=>{
        try{
            let id = req.params.id;
            console.log("id -- ", id);
            let record = await SellerTimeSloats.findOne({ _id:id });
            //console.log("record ", record);
            if(!record)
            {
                return res.send({
                    status:false,
                    message:"ID invalide"
                });
            }
            record.day_enable = !record.day_enable;
            record.save();
            return res.send({
                status:true,
                message:"Enregistrement complet des succès actualisés"
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    }
}