const Seller = require("../../models/seller/Seller");
const orders = require("../../models/orders/order");
const PaymentRequestmodel=require("../../models/admin/PaymentRequest");
const SubscriptionModel=require("../../models/subscriptions/Subscription");
const pageViewsModel=require("../../models/products/pageViews");
const bycrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const mongoose=require("mongoose")
const CoupensModel=require("../../models/seller/Coupens");
const SellerContactModel=require("../../models/seller/SellerContactModel");
const SellerQueryModel=require("../../models/seller/SellerQueryModel");
const SellerBranchesModel = require("../../models/seller/SellerBranchesModel");

// const PaymentRequestmodel=require("../../models/admin/PaymentRequest");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const ejs = require('ejs');
const {getdateinadvance,getDays,getDate} =require("../../modules/dates.js")
const customConstant = require('../../helpers/customConstant');
const SellertermsconditionModel = require("../../models/admin/SellertermsconditionModel");
const { Product } = require("../../models/products/Product");
const fs = require("fs");
const {
    sendmail
} = require("../../modules/sendmail");
const apptext_cmsmodel = require("../../models/admin/Apptext");
const transport = nodemailer.createTransport({
    name: "Swift",
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    auth: {
      user: process.env.MAILER_EMAIL_ID,
      pass: process.env.MAILER_PASSWORD,
    }
});

module.exports = {

  getAllBranch:async(req,res)=>{
    try{
      let id = req.params.id;
      if(!id)
      {
        return res.send({
          status:false,
          message:"L'identifiant est requis"
        })
      }
      //.sort({color_name:1})
      let limit = 100000;
      let skip = 0 * limit;
      let seller_rec = await SellerBranchesModel.find({seller_id:id} ).skip(skip).limit(limit);
      if(seller_rec.length == 0)
      {
        return res.send({
          status:false,
          message:"Aucune donnée n'a été trouvée pour cet identifiant"
        })
      }else{
        return res.send({
          status:true,
          message:"Succès",
          record:seller_rec
        })
      }
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      })
    }
  },
  getSingleBranch:async(req,res)=>{
    try{
        let id = req.params.id;
        let matched_branch = await SellerBranchesModel.findOne({_id:id});
        if(!matched_branch)
        {
          return res.send({
            status:false,
            message:"Aucune branche trouvée pour cet identifiant"
          });
        }else{
          return res.send({
            status:true,
            message:"Success",
            record:matched_branch
          });
        }
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      })
    }
  },
  updateBranch:async(req,res)=>{
    try{
      // console.log("req.files " , req.files);
      // console.log("req.file " , req.file);
      // console.log("req.body " , req.body);
      let {id,shopname,shopdesc,address,shopLatitude,shopLongitude,seller_id} = req.body;
      if(!seller_id)
      {
        return res.send({
          status:false,
          message:"L'identifiant de l'utilisateur est invalide"
        });
      }
      if (!shopLatitude || !shopLongitude || shopLongitude == undefined  || shopLatitude == undefined )
      {
          return res.send({
              status: false,
              message: "L'adresse du magasin doit être une adresse Google"
          })
      }
      let seller_rec = await Seller.findOne({_id:seller_id},{branches:1});
      if(!seller_rec)
      {
        return res.send({
          status:false,
          message:"L'identifiant de l'utilisateur est invalide"
        });
      }
      // let matched_branch = seller_rec.branches.filter((val)=>{
      //   if(val._id == id)
      //   {
      //     return val;
      //   }
      // });
      //console.log("matched_branch ", matched_branch);
      let matched_branch = await SellerBranchesModel.findOne({_id:id});
      if(!matched_branch)
      {
        return res.send({
          status:false,
          message:"Aucune branche trouvée pour cet identifiant"
        });
      }else{
        shopLatitude = parseFloat(shopLatitude);
        shopLongitude = parseFloat(shopLongitude);
        let shopphoto = matched_branch.shopphoto;
        if(req.files)
        {
          if(req.files?.shopphoto)
          {
            //console.log("second ifff ifff");
            let directory = "./views/admin-panel/public/";
            let fileWithPath = directory+shopphoto;
            //console.log("fileWithPath " ,fileWithPath);
            if(fs.existsSync(fileWithPath))
            {
              console.log("in side ifff fileWithPath " ,fileWithPath);
              fs.unlink(fileWithPath,(err)=>{
                console.log("unlink file error "+err);
              })
            }
            shopphoto = "user/"+req.files.shopphoto[0].filename;
          }
        }
        let location = {
          type:"Point",
          coordinates:[shopLatitude,shopLongitude]
        }
        let new_data = {
          shopname:req.body.shopname ? req.body.shopname : matched_branch.shopname,
          shopdesc:req.body.shopdesc ? req.body.shopdesc : matched_branch.shopdesc,
          address:req.body.address ? req.body.address : matched_branch.address,
          location:location,
          shopphoto:shopphoto,
          status:matched_branch.status,
          main_branch:matched_branch.main_branch
        };
        //console.log("new_data ", new_data);
        
        //console.log("all_branch ", all_branch);
        await SellerBranchesModel.updateOne({_id:id},new_data).then((result)=>{
          return res.send({
            status:true,
            message:"L'enregistrement de la branche a été mis à jour avec succès."
          })
        }).catch((e)=>{
          return res.send({
            status:false,
            message:e.message
          })
        });
      }
    }catch(e)
    {
      return res.send({
        status:false,
        message:e.message
      })
    }
  },
  addBranch:async(req,res)=>{
    try{
      // console.log("req.body ", req.body);
      // console.log("req.body ", req.files);
      // return false;
      let{seller_id,shopname,shopdesc,address,shopLatitude,shopLongitude} = req.body;
      //shopphoto
      if(!seller_id)
      {
        return res.send({
          status:false,
          message:"L'identifiant de l'utilisateur est invalide"
        });
      }
      if (!shopLatitude || !shopLongitude || shopLongitude == undefined  || shopLatitude == undefined )
      {
          return res.send({
              status: false,
              message: "L'adresse du magasin doit être une adresse Google"
          })
      }
      let seller_rec = await Seller.findOne({_id:seller_id},{branches:1});
      if(!seller_rec)
      {
        return res.send({
          status:false,
          message:"L'identifiant de l'utilisateur est invalide"
        });
      }
      shopLatitude = parseFloat(shopLatitude);
      shopLongitude = parseFloat(shopLongitude);
      let location = {
        type:"Point",
        coordinates:[shopLatitude,shopLongitude]
      };
      let shopphoto = "";
      if(req.files)
      {
        if(req.files.shopphoto)
        {
          shopphoto = "user/"+req.files.shopphoto[0].filename;
        }
      }
      let new_data = {
        seller_id:seller_id,
        shopname:shopname,
        shopdesc:shopdesc,
        address:address,
        shopLatitude:shopLatitude,
        shopLongitude:shopLongitude,
        shopphoto:shopphoto,
        location:location
      };
      let cad = await SellerBranchesModel.count(new_data);
      if(cad > 0)
      {
        return res.send({
          status:false,
          message:"Avec le même magasin de détail déjà disponible"
        })
      }
      await SellerBranchesModel.create(new_data).then((result)=>{
        return res.send({
          status:true,
          message:"L'enregistrement de la branche a été ajouté avec succès."
        })
      }).catch((e)=>{
        return res.send({
          status:false,
          message:e.message
        })
      })
      // let all_branch = seller_rec.branches;
      // all_branch.push(new_data);
      // //console.log("all_branch ", all_branch);
      // //return false;
      // await Seller.updateOne({_id:seller_id},{branches:all_branch}).then((result)=>{
      //   return res.send({
      //     status:true,
      //     message:"L'enregistrement de la branche a été ajouté avec succès."
      //   })
      // }).catch((e)=>{
      //   return res.send({
      //     status:false,
      //     message:e.message
      //   })
      // })
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      })
    }
  },
  activeInactiveBranch:async(req,res)=>{
    try{
      let {seller_id,branch_id,status} = req.body;
      if(!seller_id)
      {
        return res.send({
          status:false,
          message:"L'identifiant de l'utilisateur est invalide"
        });
      }
      if (!branch_id )
      {
          return res.send({
              status: false,
              message: "L'identifiant de la branche est requis"
          })
      }
      let seller_rec = await Seller.findOne({_id:seller_id},{branches:1});
      if(!seller_rec)
      {
        return res.send({
          status:false,
          message:"L'identifiant de l'utilisateur est invalide"
        });
      }
      if(seller_rec.branches.length == 0)
      {
        return res.send({
          status:false,
          message:"L'identifiant de l'utilisateur est invalide"
        });
      }
      let all_branch = seller_rec.branches.map((val)=>{
        if(val._id == branch_id)
        {
          val.status = status;
          
        }
        return val;
      });
      //console.log("all_branch ", all_branch);
      await Seller.updateOne({_id:seller_id},{branches:all_branch}).then((result)=>{
        return res.send({
          status:true,
          message:"L'enregistrement de la branche a été mis à jour avec succès."
        })
      }).catch((e)=>{
        return res.send({
          status:false,
          message:e.message
        })
      });
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      })
    }
  }

};