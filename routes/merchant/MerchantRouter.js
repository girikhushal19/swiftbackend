const express =require('express');
const MerchantController = require('./MerchantController');
const router = express.Router();
const multer=require("multer");
const path=require("path");
const user_path=process.env.USER_PATH;

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/uploads/driver');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
});
const uploadDriverProfile = multer({ storage: storage });

const storage1 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/uploads/merchant')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
});
const uploadTo = multer({ storage: storage1 });
router.post('/editMerchantProfileSubmitApi',uploadTo.fields([
  { 
    name: 'file', 
    maxCount: 1
  }
]
),MerchantController.editMerchantProfileSubmitApi);
router.post('/merchantRegistration',MerchantController.merchantRegistration);
router.post('/merchantLogin',MerchantController.merchantLogin);
router.post('/merchantForgotPassword',MerchantController.merchantForgotPassword);
router.post('/resetPasswordSubmit',MerchantController.resetPasswordSubmit);
router.post('/getMerchantProfile',MerchantController.getMerchantProfile);
router.post('/getMerchantLimit',MerchantController.getMerchantLimit);

router.post('/allMerchantPushNotification',MerchantController.allMerchantPushNotification);
router.post('/allMerchantPushNotificationCount',MerchantController.allMerchantPushNotificationCount);

router.post('/getHomePageChartData',MerchantController.getHomePageChartData);

module.exports=router;
