const express =require('express');
const AngularAdminController = require('./AngularAdminController');
const RoutePlanController = require('./RoutePlanController');
const AdminMerchantController = require('./AdminMerchantController');
const AdminShipmentController = require('./AdminShipmentController');
const AdminCommonController = require('./AdminCommonController');
const AdminAttribute = require("./AdminAttribute");
const router = express.Router();
const multer=require("multer");
const path=require("path");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/uploads/routePlan');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
});
const uploadTo = multer({ storage: storage });

const storageAdminImage = multer.diskStorage({

  destination: function(req, file, cb) {
      cb(null, './public/uploads/adminProfile');
  },

  filename: function(req, file, cb) {
      var dynamicFileName = Math.floor(Math.random() * Date.now());
    cb(null, 'u_p_'+ dynamicFileName + '-' + file.originalname);
  }
});
var uploadAdminImage = multer({ 
storage: storageAdminImage
});



router.post('/adminLogin',AngularAdminController.adminLogin);
router.post('/getAdminProfile',AngularAdminController.getAdminProfile);
router.post('/addDriverSubmit',AngularAdminController.addDriverSubmit);
router.post('/allDriversCount',AngularAdminController.allDriversCount);
router.post('/allDrivers',AngularAdminController.allDrivers);
router.post('/updateDriverStatusApi',AngularAdminController.updateDriverStatusApi);
router.post('/deleteDriverACApi',AngularAdminController.deleteDriverACApi);
router.post('/getSingleDriverApi',AngularAdminController.getSingleDriverApi);
router.post('/editDriverSubmit',AngularAdminController.editDriverSubmit);

router.post("/editAdminProfileSubmitApi",uploadAdminImage.array('file',1),(AngularAdminController.editAdminProfileSubmitApi ));

router.post('/addRouteSubmit',RoutePlanController.addRouteSubmit);
router.post('/addRouteFileSubmit',uploadTo.fields([
  { 
    name: 'file', 
    maxCount: 10
  }
]
),RoutePlanController.addRouteFileSubmit);
router.post('/editRouteSubmit',RoutePlanController.editRouteSubmit);
router.post('/editRouteFileSubmit',uploadTo.fields([
  { 
    name: 'file', 
    maxCount: 10
  }
]
),RoutePlanController.editRouteFileSubmit);
router.post('/allRoutePlanCount',RoutePlanController.allRoutePlanCount);
router.post('/allRoutePlan',RoutePlanController.allRoutePlan);
router.post('/getStopListWithDriver',RoutePlanController.getStopListWithDriver);
router.post('/getAllRouteStop',RoutePlanController.getAllRouteStop);
router.post('/getSingleStopApi',RoutePlanController.getSingleStopApi);
router.post('/deleteStopApi',RoutePlanController.deleteStopApi);
router.post('/updatePositionRoute',RoutePlanController.updatePositionRoute);
router.post('/updatePositionRouteLast',RoutePlanController.updatePositionRouteLast);
router.post('/updateTotalTimeDistance',RoutePlanController.updateTotalTimeDistance);
router.post('/getRouteAssignCheck',RoutePlanController.getRouteAssignCheck);

router.post('/finalRouteAssignApi',RoutePlanController.finalRouteAssignApi);
router.post('/finalRouteAssignApi2',RoutePlanController.finalRouteAssignApi2);
router.post('/assignToDriverApi',RoutePlanController.assignToDriverApi);

router.post('/allDriversForAssignCount',AngularAdminController.allDriversForAssignCount);
router.post('/allDriversForAssign',AngularAdminController.allDriversForAssign);
router.post('/getSingleDriverSupportPage',AngularAdminController.getSingleDriverSupportPage);
router.get('/getSingleSellerTermsConditionPage',AngularAdminController.getSingleSellerTermsConditionPage);

router.post('/editSellerTermsCondition',AngularAdminController.editSellerTermsCondition);
router.post('/editDriverSupportSubmit',AngularAdminController.editDriverSupportSubmit);
router.get('/getAllSlugWebPage',AngularAdminController.getAllSlugWebPage);
router.get('/getWebPageById/:id',AngularAdminController.getWebPageById);
router.get('/getWebPageBySlug/:url_slug',AngularAdminController.getWebPageBySlug);


router.post('/userWebContactUs',AngularAdminController.userWebContactUs);
router.post('/updateWebPageSubmit',AngularAdminController.updateWebPageSubmit);


router.post('/allMerchant',AdminMerchantController.allMerchant);
router.post('/allMerchantCount',AdminMerchantController.allMerchantCount);
router.post('/getAllActiveMerchantAdminApi',AdminMerchantController.getAllActiveMerchantAdminApi);
router.post('/sendMerchantPushNotificationSubmit',AdminMerchantController.sendMerchantPushNotificationSubmit);
router.post('/getMerchantNotification',AdminMerchantController.getMerchantNotification);
router.post('/allAdminPushNotification',AdminMerchantController.allAdminPushNotification);
router.post('/allAdminPushNotificationCount',AdminMerchantController.allAdminPushNotificationCount);
router.post('/deletePushNotificationApi',AdminMerchantController.deletePushNotificationApi);
router.post('/allMerchantForInvoice',AdminMerchantController.allMerchantForInvoice);

router.post('/allCompletedShipment',AdminShipmentController.allCompletedShipment);
router.post('/generateInvoiceApi',AdminShipmentController.generateInvoiceApi);
router.post('/allAdminInvoice',AdminShipmentController.allAdminInvoice);
router.post('/allAdminInvoiceCount',AdminShipmentController.allAdminInvoiceCount);


router.post('/updateMerchantStatusApi',AdminMerchantController.updateMerchantStatusApi);
router.post('/allAdminClientMerchant',AdminMerchantController.allAdminClientMerchant);
router.post('/allAdminClientMerchantCount',AdminMerchantController.allAdminClientMerchantCount);
router.post('/getAdminClientDetail',AdminMerchantController.getAdminClientDetail);
router.post('/allAdminShipment',AdminShipmentController.allAdminShipment);
router.post('/allAdminShipmentCount',AdminShipmentController.allAdminShipmentCount);

router.post("/allLatestShipmentCount",AdminShipmentController.allLatestShipmentCount);
router.post("/allLatestShipment",AdminShipmentController.allLatestShipment);

router.post('/allAdminInvoiceShipment',AdminShipmentController.allAdminInvoiceShipment);
router.post('/allAdminInvoiceShipmentCount',AdminShipmentController.allAdminInvoiceShipmentCount);


router.post('/getAdminShipmentDetail',AdminShipmentController.getAdminShipmentDetail);
router.post('/getAdminAllShipmentId',AdminShipmentController.getAdminAllShipmentId);
router.post('/getAdminShipmentAddress',AdminShipmentController.getAdminShipmentAddress);

router.post('/settingSubmit',AdminCommonController.settingSubmit);
router.post('/getSetting',AdminCommonController.getSetting);
router.post('/updateCreditLimit',AngularAdminController.updateCreditLimit);
router.get('/allRouteStop/:id',RoutePlanController.allRouteStop);

router.post("/addColorsSubmit",AdminAttribute.addColorsSubmit);
router.get("/allColorCount",AdminAttribute.allColorCount );
router.post("/allColor",AdminAttribute.allColor );
router.post("/updateColorStatusApi",AdminAttribute.updateColorStatusApi );
router.post("/getSingleColor",AdminAttribute.getSingleColor );
router.post("/editColorsSubmit",AdminAttribute.editColorsSubmit );

router.post("/addBrandSubmit",AdminAttribute.addBrandSubmit);
router.get("/allBrandCount",AdminAttribute.allBrandCount );
router.post("/allBrand",AdminAttribute.allBrand );
router.post("/updateBrandStatusApi",AdminAttribute.updateBrandStatusApi );
router.post("/getSingleBrand",AdminAttribute.getSingleBrand );
router.post("/editBrandSubmit",AdminAttribute.editBrandSubmit );

router.post("/addSizeSubmit",AdminAttribute.addSizeSubmit);
router.get("/allSizeCount",AdminAttribute.allSizeCount );
router.post("/allSize",AdminAttribute.allSize );
router.post("/updateSizeStatusApi",AdminAttribute.updateSizeStatusApi );
router.post("/getSingleSize",AdminAttribute.getSingleSize );
router.post("/editSizeSubmit",AdminAttribute.editSizeSubmit );

router.post("/addStyleSubmit",AdminAttribute.addStyleSubmit);
router.get("/allStyleCount",AdminAttribute.allStyleCount );
router.post("/allStyle",AdminAttribute.allStyle );
router.post("/updateStyleStatusApi",AdminAttribute.updateStyleStatusApi );
router.post("/getSingleStyle",AdminAttribute.getSingleStyle );
router.post("/editStyleSubmit",AdminAttribute.editStyleSubmit );
//  
module.exports=router