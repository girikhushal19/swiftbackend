const Seller = require("../../models/seller/Seller");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const AdminModel = require("../../models/admin/AdminModel");
const DriversModel = require("../../models/admin/DriversModel");
const RoutePlansModel = require("../../models/admin/RoutePlansModel");
const StopsModel = require("../../models/admin/StopsModel");
const DriverSupportModel = require("../../models/admin/DriverSupportModel");
const SettingModel = require("../../models/admin/SettingModel");
const GlobalSettings=require("../../models/admin/GlobalSettings");

const fs = require('fs');
const mongoose = require("mongoose");
//var bcrypt = require('bcryptjs');
//const jwt = require("jsonwebtoken");

module.exports = {
  settingSubmit:async function(req,res,next)
  {
    try{
      console.log("req.body  hereeeeeeeeee");
      //console.log(req.body);return false;
      /*{
          tax: 10,
          commission: 0.5,
          return_max_day: 5,
          returnprice: 50,
          shipping_cost: 0.5,
          imidiate_shipping_cost: 5.25,
          estimate_time: 20
        }
      */
      let {tax,commission,return_max_day,returnprice,shipping_cost,imidiate_shipping_cost,estimate_time} = req.body;
      //  ...(catagory&&{title:catagory}),
      let arr_obj = {
        ...(tax && {tax:tax}),
        ...(commission && {commission:commission}),
        ...(return_max_day && {return_max_day:return_max_day}),
        ...(returnprice && {returnprice:returnprice}),
        ...(shipping_cost && {shipping_cost:shipping_cost}),
        ...(imidiate_shipping_cost && {imidiate_shipping_cost:imidiate_shipping_cost}),
        ...(estimate_time && {estimate_time:estimate_time})
      };
      //console.log("arr_obj ", arr_obj);
      //mongoose.set("debug",true);
      await GlobalSettings.updateOne({},arr_obj).then((result)=>{
        let return_response = {"error":false,success: true,errorMessage:"Enregistrement complet des succès mis à jour"};
        return res.status(200).send(return_response);
      }).catch((e)=>{
        let return_response = {"error":true,success: false,errorMessage:e.message};
        return res.status(200).send(return_response);
      });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },

  getSetting:async function(req,res,next)
  {
    SettingModel.find({  }, {}).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        var return_response = {"error":false,success: true,errorMessage:"success","result":result};
              res.status(200)
              .send(return_response);
      }
    });
  },





};