const Seller = require("../../models/seller/Seller");
const Brands = require("../../models/products/Brands");
const SizeModel = require("../../models/products/SizeModel");
const StyleModel = require("../../models/products/StyleModel");

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const AdminModel = require("../../models/admin/AdminModel");
const DriversModel = require("../../models/admin/DriversModel");
const RoutePlansModel = require("../../models/admin/RoutePlansModel");
const StopsModel = require("../../models/admin/StopsModel");
const DriverSupportModel = require("../../models/admin/DriverSupportModel");
const SellertermsconditionModel = require("../../models/admin/SellertermsconditionModel");
const WebapppagesModel = require("../../models/admin/WebapppagesModel");
const ContactenquiryModel = require("../../models/admin/ContactenquiryModel");
const Color = require("../../models/admin/Color");
const UsersModel = require("../../models/user/User");
const fs = require('fs');
const mongoose = require("mongoose");


module.exports = {
  addColorsSubmit:async(req,res)=>{
    try{
      let {color_name,color_code} = req.body;
      await Color.create({
        color_name:color_name,
        color_code:color_code,
        status:1
      }).then((result)=>{
        return res.send({
          status: true,
          errorMessage: "Ajouter un succès de couleur pleinement "
        });
      }).catch((e)=>{
        return res.send({
          status: false,
          errorMessage: e.message
        });
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  allColorCount:async(req,res)=>{
    try{
      let total= await Color.count({});
      let limit = 10;
      let page = total / limit;
      let totalPageNumber = Math.ceil(page);
      return res.send({
        status:true,
        totalPageNumber:totalPageNumber
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  allColor:async(req,res)=>{
    try{
      
      let limit = 10;
      let skip = req.body.numofpage * limit;
      let total= await Color.find({}).skip(skip).limit(limit).sort({color_name:1});
      return res.send({
        status:true,
        record:total
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  updateColorStatusApi:async(req,res)=>{
    try{
      let {id,status} = req.body;
      if(!id)
      {
        return res.send({
          status: false,
          errorMessage: "L'identifiant est requis"
        });
      }
      await Color.updateOne({_id:id},{status:status}).then((result)=>{
        return res.send({
          status: true,
          errorMessage: "Statut mis à jour succès complet"
        });
      }).catch((e)=>{
        return res.send({
          status: false,
          errorMessage: e.message
        });
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  getSingleColor:async(req,res)=>{
    try{
      let id = req.body.id;
      
      let total= await Color.find({_id:id});
      if(total.length > 0)
      {
        return res.send({
          status:true,
          record:total
        })
      }else{
        return res.send({
          status:false,
          record:[]
        })
      }
      
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  editColorsSubmit:async(req,res)=>{
    try{
      let {color_name,color_code,edit_id} = req.body;
      await Color.updateOne({_id:edit_id},{
        color_name:color_name,
        color_code:color_code
      }).then((result)=>{
        return res.send({
          status: true,
          errorMessage: "Votre dossier mis à jour avec succès"
        });
      }).catch((e)=>{
        return res.send({
          status: false,
          errorMessage: e.message
        });
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  addBrandSubmit:async(req,res)=>{
    try{
      let {name} = req.body;
      await Brands.create({
        name:name
      }).then((result)=>{
        return res.send({
          status: true,
          errorMessage: "Attribut ajouté avec succès"
        });
      }).catch((e)=>{
        return res.send({
          status: false,
          errorMessage: e.message
        });
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  allBrandCount:async(req,res)=>{
    try{
      let total= await Brands.count({});
      let limit = 10;
      let page = total / limit;
      let totalPageNumber = Math.ceil(page);
      return res.send({
        status:true,
        totalPageNumber:totalPageNumber
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  allBrand:async(req,res)=>{
    try{
      
      let limit = 10;
      let skip = req.body.numofpage * limit;
      let total= await Brands.find({}).skip(skip).limit(limit).sort({color_name:1});
      return res.send({
        status:true,
        record:total
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  updateBrandStatusApi:async(req,res)=>{
    try{
      let {id,status} = req.body;
      if(!id)
      {
        return res.send({
          status: false,
          errorMessage: "L'identifiant est requis"
        });
      }
      await Brands.updateOne({_id:id},{status:status}).then((result)=>{
        return res.send({
          status: true,
          errorMessage: "Statut mis à jour succès complet"
        });
      }).catch((e)=>{
        return res.send({
          status: false,
          errorMessage: e.message
        });
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  getSingleBrand:async(req,res)=>{
    try{
      let id = req.body.id;
      
      let total= await Brands.find({_id:id});
      if(total.length > 0)
      {
        return res.send({
          status:true,
          record:total
        })
      }else{
        return res.send({
          status:false,
          record:[]
        })
      }
      
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  editBrandSubmit:async(req,res)=>{
    try{
      let {name,edit_id} = req.body;
      await Brands.updateOne({_id:edit_id},{
        name:name
      }).then((result)=>{
        return res.send({
          status: true,
          errorMessage: "Votre dossier mis à jour avec succès"
        });
      }).catch((e)=>{
        return res.send({
          status: false,
          errorMessage: e.message
        });
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  addSizeSubmit:async(req,res)=>{
    try{
      let {name} = req.body;
      await SizeModel.create({
        name:name
      }).then((result)=>{
        return res.send({
          status: true,
          errorMessage: "Attribut ajouté avec succès"
        });
      }).catch((e)=>{
        return res.send({
          status: false,
          errorMessage: e.message
        });
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  allSizeCount:async(req,res)=>{
    try{
      let total= await SizeModel.count({});
      let limit = 10;
      let page = total / limit;
      let totalPageNumber = Math.ceil(page);
      return res.send({
        status:true,
        totalPageNumber:totalPageNumber
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  allSize:async(req,res)=>{
    try{
      
      let limit = 10;
      let skip = req.body.numofpage * limit;
      let total= await SizeModel.find({}).skip(skip).limit(limit).sort({color_name:1});
      return res.send({
        status:true,
        record:total
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  updateSizeStatusApi:async(req,res)=>{
    try{
      let {id,status} = req.body;
      if(!id)
      {
        return res.send({
          status: false,
          errorMessage: "L'identifiant est requis"
        });
      }
      await SizeModel.updateOne({_id:id},{status:status}).then((result)=>{
        return res.send({
          status: true,
          errorMessage: "Statut mis à jour succès complet"
        });
      }).catch((e)=>{
        return res.send({
          status: false,
          errorMessage: e.message
        });
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  getSingleSize:async(req,res)=>{
    try{
      let id = req.body.id;
      
      let total= await SizeModel.find({_id:id});
      if(total.length > 0)
      {
        return res.send({
          status:true,
          record:total
        })
      }else{
        return res.send({
          status:false,
          record:[]
        })
      }
      
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  editSizeSubmit:async(req,res)=>{
    try{
      let {name,edit_id} = req.body;
      await SizeModel.updateOne({_id:edit_id},{
        name:name
      }).then((result)=>{
        return res.send({
          status: true,
          errorMessage: "Votre dossier mis à jour avec succès"
        });
      }).catch((e)=>{
        return res.send({
          status: false,
          errorMessage: e.message
        });
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  addStyleSubmit:async(req,res)=>{
    try{
      let {name} = req.body;
      await StyleModel.create({
        name:name
      }).then((result)=>{
        return res.send({
          status: true,
          errorMessage: "Attribut ajouté avec succès"
        });
      }).catch((e)=>{
        return res.send({
          status: false,
          errorMessage: e.message
        });
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  allStyleCount:async(req,res)=>{
    try{
      let total= await StyleModel.count({});
      let limit = 10;
      let page = total / limit;
      let totalPageNumber = Math.ceil(page);
      return res.send({
        status:true,
        totalPageNumber:totalPageNumber
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  allStyle:async(req,res)=>{
    try{
      
      let limit = 10;
      let skip = req.body.numofpage * limit;
      let total= await StyleModel.find({}).skip(skip).limit(limit).sort({color_name:1});
      return res.send({
        status:true,
        record:total
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  updateStyleStatusApi:async(req,res)=>{
    try{
      let {id,status} = req.body;
      if(!id)
      {
        return res.send({
          status: false,
          errorMessage: "L'identifiant est requis"
        });
      }
      await StyleModel.updateOne({_id:id},{status:status}).then((result)=>{
        return res.send({
          status: true,
          errorMessage: "Statut mis à jour succès complet"
        });
      }).catch((e)=>{
        return res.send({
          status: false,
          errorMessage: e.message
        });
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  getSingleStyle:async(req,res)=>{
    try{
      let id = req.body.id;
      
      let total= await StyleModel.find({_id:id});
      if(total.length > 0)
      {
        return res.send({
          status:true,
          record:total
        })
      }else{
        return res.send({
          status:false,
          record:[]
        })
      }
      
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
  editStyleSubmit:async(req,res)=>{
    try{
      let {name,edit_id} = req.body;
      await StyleModel.updateOne({_id:edit_id},{
        name:name
      }).then((result)=>{
        return res.send({
          status: true,
          errorMessage: "Votre dossier mis à jour avec succès"
        });
      }).catch((e)=>{
        return res.send({
          status: false,
          errorMessage: e.message
        });
      })
    }catch(e){
      return res.send({
        status: false,
        errorMessage: e.message
      });
    }
  },
};