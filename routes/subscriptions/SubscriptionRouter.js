const express =require('express');
const SubscriptionPackagesController = require('./SubscriptionPackages.controller');
const SubscriptionsController = require("./Subscriptions.controller")
const router = express.Router();
const multer=require("multer");
const path=require("path");
const user_path=process.env.USER_PATH;
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve(user_path))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });
router.post('/createoreditsubpackage',uploadTo.fields([
    { 
      name: 'photo', 
      maxCount: 10
    }
  ]
  ),SubscriptionPackagesController.createSubPackage);
  router.get('/getAllSubscriptions',SubscriptionPackagesController.getAllSubscriptions);
  router.get('/getSubscriptionById/:id',SubscriptionPackagesController.getSubscriptionById);
  router.get('/deleteSubscription/:id',SubscriptionPackagesController.deleteSubscription);
  router.post("/createSubscription", SubscriptionsController.createSubscription);
  router.get("/getSubscription/:subscription_id", SubscriptionsController.getSubscriptionById);
  router.get("/getSubscriptionbylawyerID/:lawyer_id", SubscriptionsController.getSubscriptionbylawyerID);
  router.post("/updateSubscription", SubscriptionsController.updateSubscription);
  router.get("/deleteSubscription/:subscription_id", SubscriptionsController.deleteSubscription);
  router.get("/getallsubscriptions", SubscriptionsController.getAllSubscriptions);

  router.post("/getSubscriptionProviderId", SubscriptionsController.getSubscriptionProviderId);

module.exports=router

