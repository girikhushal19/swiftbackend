const { Product } = require("../../models/products/Product");
const UserWebHistoryModel = require("../../models/products/UserWebHistoryModel");

const OrderModel = require("../../models/orders/order");
const ProductstempimagesModel = require("../../models/products/ProductstempimagesModel");
const MatCatagory=require("../../models/products/MatCatagory");
const Variations =require("../../models/products/Variations");
const VariationsPrice =require("../../models/products/VariationPrice");
const pageViewsModel=require("../../models/products/pageViews");
const mongoose=require("mongoose");
const catagorymodel = require("../../models/products/Catagory");
const ProductfavoriteModel = require("../../models/products/ProductfavoriteModel");
const CoupensModel=require("../../models/seller/Coupens");
const Ratingnreviews=require("../../models/actions/Ratingnreviews");
const SettingModel = require("../../models/admin/SettingModel");
const Subscriptions=require("../../models/subscriptions/Subscription");
const ShippingModel = require("../../models/shipping/Shipping");
const GlobalSettings=require("../../models/admin/GlobalSettings");
const SellerModel=require("../../models/seller/Seller");

const fs = require("fs");
const { forEach } = require("async");
module.exports = {

  listProductsTotalCount:async(req,res)=>{
    try{
      //console.log("hereeeeee");
      //mongoose.set("debug",true);
      
      var {category_id,pagno} = req.body;
      var query_front_side = req.body.query;
      //String_qr["ad_name"] = {'$regex' : '^'+search_var, '$options' : 'i'}
      const opquery={is_deleted:false};
            const ipquery={};
            const rpquery={};
            const catagory=req.body.catagory;
            // const attributes=req.body.attributes;
            // const sales_info=req.body.sales_info;
            const sort=req.body.sort||1;
            const pricerangeLow= parseFloat(req.body.pricerangeLow);
            const pricerangeHigh=parseFloat(req.body.pricerangeHigh);
            const color=req.body.color;
            const countryofmfg=req.body.countryofmfg;
            const condition=req.body.condition;
            const availibility=req.body.availibility;
            const rating=parseInt(req.body.rating);

            opquery['is_active']=true;


            if(condition){
              if(condition == "new")
              {
                opquery['condition']=true;
              }else{
                opquery['condition']=false;
              }
                
            }
            if(rating){
                rpquery['rating']= parseInt(rating);
            }
            console.log("pricerangeLow ",pricerangeLow);
            if(pricerangeLow >=0 && pricerangeHigh){
              opquery['$and']=[
                {
                    price:{$gte:pricerangeLow}
                },
                {
                    price:{$lte:pricerangeHigh}
                }
            ]
              
            }

      var category_record = {};
      var sub_category_record = [];
      if(category_id)
      {
        opquery["category_id"] = category_id;
        //category_record = await catagorymodel.findOne({_id:category_id});
        //sub_category_record = await catagorymodel.find({mastercatagory:category_id});
      }
      if(query_front_side)
      {
        opquery["title"] = {'$regex' : '^'+query_front_side, '$options' : 'i'}
      }
      console.log('opquery')
      console.log(opquery);
      //console.log("rpquery ",rpquery);
      //mongoose.set("debug",true);
      pagno = pagno ? pagno : 0;
      var skip = pagno;
      var limit = 10;
      var user_id = req.body.user_id;
      //{ $match: { FieldCollege: { $ne: [] } } },
      if(rating)
      {
        //mongoose.set("debug",true);
        await Product.aggregate([
          {
            $match:opquery
          },
          {
            $sort:{"title":1}
          },
          {
            $lookup:{
                from:"ratingnreviews",
                let:{"prod_id":{"$toString":"$_id"}},
                //let:{productidstring:{"$toString":"$_id"}},
                pipeline:[
                    // {$match:{
                    //     $expr:{
                    //         $eq:["$product_id","$$productidstring"]
                            
                    //     }
                    // }}
                    {
                        $match: rpquery 
                    },
                    {
                        $match:{
                            $expr:{
                                $eq:["$product_id","$$prod_id"]
                            }
                        }
                    }
                    //{$match:{$expr:{"product_id":"$$product_id"}}},
                    // {$addFields:{"userIDOBJ":{"$toObjectId":"$user_id"}}},
                    // {
                    //     $lookup:{
                    //         from:"users",
                    //         localField:"userIDOBJ",
                    //         foreignField:"_id",
                    //         as:"userwhorated"
                    //     }
                    // }
                ],
                as:"reviews"
            },
            
          },
          {
            $addFields:{
              "totalRating":{$size:"$reviews"}
            }
          },
          { $match:{ "reviews":{$ne:[] } } },
          { $addFields: {
                              
              avgRating:{
                  $avg: "$reviews.rating"
              },
              provideridObj:{
                  "$toObjectId":"$provider_id"
              }
          }},
          
          {
            $project:{
              "title":1
            }
          },
          // {$skip:skip},
          // {$limit:limit},
           
        ]).then((result)=>{
          if(result.length > 0)
          {
            var pages =  Math.ceil(result.length / limit);
          }else{
            var pages = 0;
          }
          
          return res.send({
            status:true,
            message:"Success",
            category_record:category_record,
            sub_category_record:sub_category_record,
            data:pages,
          })
        }).catch((error)=>{
          return res.send({
            status:false,
            message:error.message
          })
        });
      }else{
        await Product.aggregate([
          {
            $match:opquery
          },
          {
            $sort:{"title":1}
          },
           
           
          
          // {$skip:skip},
          // {$limit:limit},
          {
            $project:{
              "title":1
            }
          },
        ]).then((result)=>{
          if(result.length > 0)
          {
            var pages =  Math.ceil(result.length / limit);
          }else{
            var pages = 0;
          }
          
          return res.send({
            status:true,
            message:"Success",
            category_record:category_record,
            sub_category_record:sub_category_record,
            data:pages,
          })
        }).catch((error)=>{
          return res.send({
            status:false,
            message:error.message
          })
        });
      }
      
    }catch(error)
    {
      return res.send({
        status:false,
        message:error.message
      })
    }
  },
  listProducts:async(req,res)=>{
    try{
      //console.log("hereeeeee");
      //mongoose.set("debug",true);
      
      var {category_id,pagno} = req.body;
      var query_front_side = req.body.query;
      //String_qr["ad_name"] = {'$regex' : '^'+search_var, '$options' : 'i'}
      const opquery={is_deleted:false};
            const ipquery={};
            const rpquery={};
            const catagory=req.body.catagory;
            // const attributes=req.body.attributes;
            // const sales_info=req.body.sales_info;
            let sort=req.body.sort||1;
            sort = parseInt(sort);
            console.log("sort ", sort);
            const pricerangeLow= parseFloat(req.body.pricerangeLow);
            const pricerangeHigh=parseFloat(req.body.pricerangeHigh);
            const color=req.body.color;
            const countryofmfg=req.body.countryofmfg;
            const condition=req.body.condition;
            const availibility=req.body.availibility;
            const rating=parseInt(req.body.rating);

            opquery['is_active']=true;


            if(condition){
              if(condition == "new")
              {
                opquery['condition']=true;
              }else{
                opquery['condition']=false;
              }
                
            }
            if(rating){
                rpquery['rating']= parseInt(rating);
            }
            console.log("pricerangeLow ",pricerangeLow);
            if(pricerangeLow >=0 && pricerangeHigh){
              opquery['$and']=[
                {
                    price:{$gte:pricerangeLow}
                },
                {
                    price:{$lte:pricerangeHigh}
                }
            ]
              
            }

      var category_record = {};
      var sub_category_record = [];
      if(category_id)
      {
        opquery["category_id"] = category_id;
        //category_record = await catagorymodel.findOne({_id:category_id});
        //sub_category_record = await catagorymodel.find({mastercatagory:category_id});
      }
      if(query_front_side)
      {
        //opquery["title"] = {'$regex' : '^'+query_front_side, '$options' : 'i'}
        opquery["title"] = {'$regex' :  query_front_side, '$options' : 'i'}
      }
      // console.log('opquery')
      // console.log(opquery);
      //console.log("rpquery ",rpquery);
      //mongoose.set("debug",true);
      pagno = pagno ? pagno : 0;
      var limit = 10;
      var skip = pagno*limit;
      var user_id = req.body.user_id;
      //{ $match: { FieldCollege: { $ne: [] } } },
      if(rating)
      {
        //mongoose.set("debug",true);
        await Product.aggregate([
          {
            $match:opquery
          },
          {
            $sort:{"title":1}
          },
          {
            $lookup:{
                from:"ratingnreviews",
                let:{"prod_id":{"$toString":"$_id"}},
                //let:{productidstring:{"$toString":"$_id"}},
                pipeline:[
                    // {$match:{
                    //     $expr:{
                    //         $eq:["$product_id","$$productidstring"]
                            
                    //     }
                    // }}
                    {
                        $match: rpquery 
                    },
                    {
                        $match:{
                            $expr:{
                                $eq:["$product_id","$$prod_id"]
                            }
                        }
                    }
                    //{$match:{$expr:{"product_id":"$$product_id"}}},
                    // {$addFields:{"userIDOBJ":{"$toObjectId":"$user_id"}}},
                    // {
                    //     $lookup:{
                    //         from:"users",
                    //         localField:"userIDOBJ",
                    //         foreignField:"_id",
                    //         as:"userwhorated"
                    //     }
                    // }
                ],
                as:"reviews"
            },
            
          },
          {
            $addFields:{
              "totalRating":{$size:"$reviews"}
            }
          },
          { $match:{ "reviews":{$ne:[] } } },
          { $addFields: {
                              
              avgRating:{
                  $avg: "$reviews.rating"
              },
              provideridObj:{
                  "$toObjectId":"$provider_id"
              }
          }},
          
          // {
          //     $lookup:{
          //         from:"sellers",
          //         localField:"provideridObj",
          //         foreignField:"_id",
          //         as:"seller"
          //     }
          // },
          {
              $lookup:{
                  from:"subscriptions",
                  let:{provider_id:"$provider_id"},
                  pipeline:[
                      {$match:{
                          $expr:{
                              $and:[
                                  {
                                      $eq:["$provider_id","$$provider_id"]
                                      
                                  },{
                                      $lte:[new Date(),"$end_date"]
                                  }
                              ]
                          }
                      }}
                  ],
                  as:"subs"
              }
          },
          {
              $lookup:{
                  from:"variationprices",
                  let:{productidstring:{"$toString":"$_id"}},
                  pipeline:[
                      {$match:{
                          $expr:{
                              $eq:["$product_id","$$productidstring"]
                              // $and:[
                              //     {
                              //         $eq:["$provider_id","$$provider_id"]
                                      
                              //     },{
                              //         $lte:[new Date(),"$end_date"]
                              //     }
                              // ]
                          }
                      }}
                  ],
                  as:"variations"
              }
          },
          {
              $addFields:{
                  pricerangeMin:{
                      $cond:{
                          if:{$gt:[{$size:"$variations"},0]},
                          then:{
                              $min:"$variations.price"
                        
                        
                        },else:"$price"
                  }},
                  pricerangeMax:{
                      $cond:{
                          if:{$gt:[{$size:"$variations"},0]},
                          then:{
                              $max:"$variations.price"
                        
                        
                        },else:"$price"
                  }},
                
              }
            },
            {
              $lookup:{
                  from:"productfavorites",
                  let:{"pro_id":{"$toString":"$_id"}},
                  pipeline:[
                      {
                      $match:{
                          $expr:{
                              $and:[
                                  {$eq:["$user_id",user_id]},
                                  {$eq:["$product_id","$$pro_id"]}
                              ]
                          }
                      }
                  }
              ],
              as:"favProduct"
              }
          },{
              $addFields:{
                  "isFav":{$size:"$favProduct"}
              }
          },
          {$sort:{"price":sort}},
          
          {$skip:skip},
          {$limit:limit},
          {$project:{
              "variations":0
          }}
        ]).then((result)=>{
          return res.send({
            status:true,
            message:"Success",
            category_record:category_record,
            sub_category_record:sub_category_record,
            data:result,
          })
        }).catch((error)=>{
          return res.send({
            status:false,
            message:error.message
          })
        });
      }else{
        await Product.aggregate([
          {
            $match:opquery
          },
          // {
          //   $sort:{"title":1}
          // },
          {
            $lookup:{
                from:"ratingnreviews",
                let:{"prod_id":{"$toString":"$_id"}},
                //let:{productidstring:{"$toString":"$_id"}},
                pipeline:[
                    // {$match:{
                    //     $expr:{
                    //         $eq:["$product_id","$$productidstring"]
                            
                    //     }
                    // }}
                    {
                        $match: rpquery 
                    },
                    {
                        $match:{
                            $expr:{
                                $eq:["$product_id","$$prod_id"]
                            }
                        }
                    }
                    //{$match:{$expr:{"product_id":"$$product_id"}}},
                    // {$addFields:{"userIDOBJ":{"$toObjectId":"$user_id"}}},
                    // {
                    //     $lookup:{
                    //         from:"users",
                    //         localField:"userIDOBJ",
                    //         foreignField:"_id",
                    //         as:"userwhorated"
                    //     }
                    // }
                ],
                as:"reviews"
            },
            
          },
          {
            $addFields:{
              "totalRating":{$size:"$reviews"}
            }
          },
          { $addFields: {
                              
              avgRating:{
                  $avg: "$reviews.rating"
              },
              provideridObj:{
                  "$toObjectId":"$provider_id"
              }
          }},
          
          // {
          //     $lookup:{
          //         from:"sellers",
          //         localField:"provideridObj",
          //         foreignField:"_id",
          //         as:"seller"
          //     }
          // },
          {
              $lookup:{
                  from:"subscriptions",
                  let:{provider_id:"$provider_id"},
                  pipeline:[
                      {$match:{
                          $expr:{
                              $and:[
                                  {
                                      $eq:["$provider_id","$$provider_id"]
                                      
                                  },{
                                      $lte:[new Date(),"$end_date"]
                                  }
                              ]
                          }
                      }}
                  ],
                  as:"subs"
              }
          },
          {
              $lookup:{
                  from:"variationprices",
                  let:{productidstring:{"$toString":"$_id"}},
                  pipeline:[
                      {$match:{
                          $expr:{
                              $eq:["$product_id","$$productidstring"]
                              // $and:[
                              //     {
                              //         $eq:["$provider_id","$$provider_id"]
                                      
                              //     },{
                              //         $lte:[new Date(),"$end_date"]
                              //     }
                              // ]
                          }
                      }}
                  ],
                  as:"variations"
              }
          },
          {
              $addFields:{
                  pricerangeMin:{
                      $cond:{
                          if:{$gt:[{$size:"$variations"},0]},
                          then:{
                              $min:"$variations.price"
                        
                        
                        },else:"$price"
                  }},
                  pricerangeMax:{
                      $cond:{
                          if:{$gt:[{$size:"$variations"},0]},
                          then:{
                              $max:"$variations.price"
                        
                        
                        },else:"$price"
                  }},
                
              }
            },
            {
              $lookup:{
                  from:"productfavorites",
                  let:{"pro_id":{"$toString":"$_id"}},
                  pipeline:[
                      {
                      $match:{
                          $expr:{
                              $and:[
                                  {$eq:["$user_id",user_id]},
                                  {$eq:["$product_id","$$pro_id"]}
                              ]
                          }
                      }
                  }
              ],
              as:"favProduct"
              }
          },{
              $addFields:{
                  "isFav":{$size:"$favProduct"}
              }
          },
          {$sort:{"price":sort}},
          
          {$skip:skip},
          {$limit:limit},
          {$project:{
              "variations":0
          }}
        ]).then((result)=>{
          return res.send({
            status:true,
            message:"Success",
            category_record:category_record,
            sub_category_record:sub_category_record,
            data:result,
          })
        }).catch((error)=>{
          return res.send({
            status:false,
            message:error.message
          })
        });
      }
      
    }catch(error)
    {
      return res.send({
        status:false,
        message:error.message
      })
    }
  },
  autoSuggestionProductList:async(req,res)=>{
    try{
      //console.log(req.body);
      //mongoose.set("debug",true);
      var {category_id,product_name} = req.body;
       
      if(product_name == "" || product_name == null || product_name == undefined)
      {
        return res.send({
          status: false,
          message: "Le nom du produit est obligatoire",
          data: []
        })
      }
      //db.recipes.find({ $text: { $search: "spiced espresso" } });

       

      //var query = { "$match":{"title": {'$regex' : '^'+product_name, '$options' : 'i'}  } };
      var query = { "$match":{"title": {'$regex' : product_name, '$options' : 'i'}  } };
      //db.employee.find({position:{$regex:"software",$options:"i"}}).pretty()

      if(category_id)
      {
        query["$match"]["category_id"] = category_id;
        //query = { "$match":{ "category_id":category_id } }
      }
      
      //mongoose.set("debug",true);
      console.log("eeererer");
      await Product.aggregate([
        query,
        {
          "$group": {
          "_id": "$title",
          "title": { "$first": "$title" },
          "category_id":{"$first":"$category_id"},
          "product_id":{"$first":"$_id"}
          }, 
        },{
          $project:{
            "title":1,
            "category_id":1
          }
        },
        {
          $sort:{"title":1}
        }
      ]).then((result)=>{
        console.log(result.length);
        return res.send({
          status: true,
          message: "Succès",
          data: result
        })
      }).catch((error)=>{
        return res.send({
          status: false,
          message: error.message,
          data: []
        })
      })
    }catch(error)
    {
      return res.send({
        status: false,
        message: error.message,
        data: []
      })
    }
  },
  addFavProduct:async(req,res)=>{
    try{
      var {user_id,product_id} = req.body;
      if(product_id == "" || product_id == null || product_id == undefined)
      {
        return res.send({
          status: false,
          message: "L'identifiant du produit est requis"
        })
      }
      if(user_id == "" || user_id == null || user_id == undefined)
      {
        return res.send({
          status: false,
          message: "L'identifiant de l'utilisateur est requis"
        })
      }
      var checkOwnPro = await Product.count({_id:product_id,provider_id:user_id});
      if(checkOwnPro)
      {
        return res.send({
          status: false,
          message: "C'est votre propre produit"
        })
      }else{
        //mongoose.set("debug",true);
        var allreadyItem = await ProductfavoriteModel.findOne({
          user_id:user_id,
          product_id:product_id,
        });
        //console.log("allreadyItem ", allreadyItem);
        //return false;
        if(allreadyItem)
        {
          console.log("in delete conditon ifff")
          //mongoose.set("debug",true);
          await ProductfavoriteModel.deleteOne({
            user_id:user_id,
            product_id:product_id,
          });

          return res.send({
            status: true,
            message: "Elément supprimé de la liste des favoris"
          })
        }else{
          console.log("in create conditon elseeee")
          await ProductfavoriteModel.create({
            user_id:user_id,
            product_id:product_id,
          }).then((result)=>{
            return res.send({
              status: true,
              message: "Article ajouté à votre liste de favoris"
            })
          }).catch((error)=>{
            return res.send({
              status: false,
              message: error.message
            })
          })
        }
        
      }
    }catch(error){
      return res.send({
        status: false,
        message: error.message
      })
    }
  },
  getFavProduct:async(req,res)=>{
    try{
      var user_id = req.params.id;
      //console.log(user_id);
      await ProductfavoriteModel.aggregate([
        {
          $match:{
            "user_id":user_id
          }
        },
        {
          $addFields:{
            "pro_id":"$product_id"
          }
        },
        {
          $lookup:{
            from:"products",
            let:{"p_id":{"$toObjectId":"$pro_id"}},
            pipeline:[
              {
                $match:{
                  $expr:{
                    $eq:["$_id","$$p_id"]
                  }
                }
 
              }
            ],
            as:"allProduct"
          }
        }
      ]).then((result)=>{
        return res.send({
          status: true,
          message: "Article ajouté à votre liste de favoris",
          record:result
        })
      }).catch((error)=>{
        return res.send({
          status: false,
          message: error.message
        })
      })
    }catch(error){
      return res.send({
        status: false,
        message: error.message
      })
    }
  },
  deleteFavProduct:async(req,res)=>{
    try{
      var id = req.params.id;
      //console.log(user_id);
      var result = await ProductfavoriteModel.findOne({_id:id},{_id:1});
      if(result)
      {
        await ProductfavoriteModel.deleteOne({_id:id}).then((result)=>{
          return res.send({
            status: true,
            message: "Succès de la suppression du poste"
          })
        }).catch((error)=>{
          return res.send({
            status: false,
            message: error.message
          })
        });
      }else{
        return res.send({
          status: false,
          message: "Identité non valide"
        })
      }
    }catch(error){
      return res.send({
        status: false,
        message: error.message
      })
    }
  },
  getAllPromoCode:async(req,res)=>{
    try{
      var d_date = new Date();
      await CoupensModel.aggregate([
        { 
          $match:
          {
            status:true,
          }
        },
        {
          $match:{
            end_date:{$gt:d_date}
          }
        },
        {$addFields:{"islimitexahusted":{$subtract:["$limit","$used"]}}},
        {$match:{"islimitexahusted":{$gt:0}}}
      ]).then((result)=>{
        return res.send({
          status: true,
          message: "Succès",
          data:result
        });
      }).catch((error)=>{
        return res.send({
          status: false,
          message: error.message
        });
      });
    }catch(error){
      return res.send({
        status: false,
        message: error.message
      })
    }
  },
  saveUserViewProduct:async(req,res)=>{
    try{
      //console.log(req.body);
      var {user_id,product_id} = req.body;
      if(!user_id)
      {
        return res.send({
          status:false,
          message:"L'identifiant de l'utilisateur est requis"
        });
      }
      if(!product_id)
      {
        return res.send({
          status:false,
          message:"L'identifiant du produit est requis"
        });
      }

      var match_product = [];
      let own_product = await Product.findOne({_id:product_id,provider_id:user_id},{provider_id:1});
      //console.log("own_product ",own_product);
      if(!own_product)
      {
        let all_old_order = await OrderModel.find({userId:user_id,payment_status: true},{products:1});
        //console.log("all_old_order ", JSON.stringify(all_old_order));
        all_old_order.forEach(element => 
        {
          //console.log(element);
          // console.log(element.products[0].product);
          // console.log(element.products[1].product);
          match_product = element.products.filter((val)=>{
            //console.log(val.product._id);
            let p_id = val.product._id;
            //console.log(p_id.toString());
            if(p_id.toString() == product_id)
            {
              return val;
            }
          });
        });

        //console.log("match_product ", match_product);
        if(match_product.length == 0)
        {
          let check_count = await UserWebHistoryModel.count({user_id:user_id,product_id:product_id});
          console.log("check_count ",check_count);
          if(check_count == 0)
          {
            await UserWebHistoryModel.create({
              user_id:user_id,
              product_id:product_id
            }).then((result)=>{
              return res.send({
                status: true,
                message: "Succès"
              });
            }).catch((error)=>{
              return res.send({
                status: false,
                message: error.message
              });
            });
          }else{
            //console.log("elseeee");
            return res.send({
              status: false,
              message: "Tout est prêt dans l'histoire "
            });
          }
        }else{
          //console.log("elseeee");
          return res.send({
            status: false,
            message: "Article déjà acheté"
          });
        }
      }
    }catch(error)
    {
      return res.send({
        status: false,
        message: error.message
      })
    }
  },
  getUserHistoryProduct:async(req,res)=>{
    try{
      var {user_id} = req.params;
      if(!user_id)
      {
        return res.send({
          status:false,
          message:"L'identifiant de l'utilisateur est requis"
        });
      }
      await UserWebHistoryModel.aggregate([
        {
          $match:{
            user_id:user_id
          }
        },
        {
          $lookup:{
            from:"products",
            let:{"p_id":{"$toObjectId":"$product_id"}},
            pipeline:[
              {
                $match:{
                $expr:{
                    $eq:["$_id","$$p_id"]
                  }
                }
              }
            ],
            as:"product"
          }
        }
      ]).then((result)=>{
        var record = [];
        if(result)
        {
          if(result.length >0)
          {
            record = result.filter((val)=>{
              //console.log("val",val.product[0]);
              if(val != null)
              {
                if(val.product)
                {
                  //console.log("val",val.product[0]);
                  let abc = val.product[0];
                  //console.log("abc",abc);
                  return abc;
                }
              }
               
              
            });
          }
        }
        
        return res.send({
          status: true,
          message: "Succès",
          date:record
        })
      }).catch((error)=>{
        return res.send({
          status: false,
          message: error.message
        })
      });
    }catch(error){
      return res.send({
        status: false,
        message: error.message
      })
    }
  },
  getSingleVariation:async(req,res)=>{
    try{
      let id = req.params.id;
      if(!id)
      {
        return res.send({
          status: false,
          message: "L'identifiant est requis",
          data: []
        })
      }
      // var record_old = await VariationsPrice.findOne({product_id:id});
      // if(!record_old)
      // {
      //   return res.send({
      //     status: false,
      //     message: "Un identifiant invalide est requis",
      //     data: []
      //   })
      // }else{
      //   return res.send({
      //     status: true,
      //     message: "Succès",
      //     data: record_old
      //   })
      // }
      var record_old = await Product.findOne({_id:id},{sales_info_variation_prices:1});
      if(!record_old)
      {
        return res.send({
          status: false,
          message: "Un identifiant invalide est requis",
          data: []
        })
      }else{
        return res.send({
          status: true,
          message: "Succès",
          data: record_old.sales_info_variation_prices
        })
      }
    }catch(error){
      return res.send({
        status: false,
        message: error.message,
        data: []
      })
    } 
  },
  deleteAttributeFromId:async(req,res)=>{
    try{
      //console.log(req.body);
      let {product_id,indexx} = req.body;
      let variation_id = indexx;
      if(!product_id)
      {
        return res.send({
          status: false,
          message: "L'identifiant du produit est requise"
        })
      }
      if(!variation_id)
      {
        return res.send({
          status: false,
          message: "L'identifiant de la variation est requise"
        })
      }
      let product_rec = await Product.findOne({_id:product_id},{sales_info_variation_prices:1});
      if(!product_rec)
      {
        return res.send({
          status: false,
          message: "Identifiant de produit invalide"
        })
      }else{
        let remaining_array = product_rec.sales_info_variation_prices.filter((val)=>{
          if(val._id != variation_id)
          {
            return val;
          }
        });
        let removed_array = product_rec.sales_info_variation_prices.filter((val)=>{
          if(val._id == variation_id)
          {
            return val;
          }
        });
        //console.log("remaining_array ", remaining_array);
        //console.log("removed_array ", removed_array);
        if(removed_array.length > 0)
        {
          let all_img = removed_array[0].image;
          for(let g=0; g<all_img.length; g++)
          {
            //console.log(all_img[g]);
            let val = all_img[g];
            let uploadDir = './views/admin-panel/public/media/';
            let fileNameWithPath = uploadDir + val;
            //console.log("fileNameWithPath ",fileNameWithPath);
            if (fs.existsSync(fileNameWithPath))
            {
                fs.unlink(fileNameWithPath, (err) => 
                {
                  console.log("unlink file error "+err);
                });
            }
          }
        }

        //console.log("hereee");
        await Product.updateOne({_id:product_id},{sales_info_variation_prices:remaining_array});
        return res.send({
          status: true,
          message: "Variation Item supprimé complètement du produit"
        })
      }
      
    }catch(error){
      return res.send({
        status: false,
        message: error.message
      })
    } 
  },
  deleteAttributeIndex:async(req,res)=>{
    try{
      //console.log(req.body);
      let product_id = req.body.product_id;
      let indexx = req.body.indexx;

      var record_old = await VariationsPrice.findOne({product_id:product_id});
      if(!record_old)
      {
        return res.send({
          status: false,
          message: "Un identifiant invalide est requis",
          data: []
        })
      }else{
        //console.log("record_old "+record_old);

        let variation_option_2 = record_old.variation_option_2;
        let price = record_old.price;
        let stock = record_old.stock;
        let sku = record_old.sku;
        if(variation_option_2.length > indexx)
        {
          variation_option_2.splice(indexx, 1);
        }
        if(price.length > indexx)
        {
          price.splice(indexx, 1);
        }
        if(stock.length > indexx)
        {
          stock.splice(indexx, 1);
        }
        if(sku.length > indexx)
        {
          sku.splice(indexx, 1);
        }

        // console.log(variation_option_2);
        // console.log(price);
        // console.log(stock);
        // console.log(sku);
        await VariationsPrice.updateOne({product_id:product_id},{
          variation_option_2: variation_option_2,
          price: price,
          stock: stock,  
          sku: sku,
        }).then((result)=>{
          return res.send({
            status: true,
            message: "Enregistrer les succès supprimés dans leur intégralité",
            data: []
          })
        }).catch((e)=>{
          return res.send({
            status: false,
            message: e.message,
            data: []
          })
        });
      }
    }catch(e){
      return res.send({
        status: false,
        message: e.message,
        data: []
      })
    }
  },
  deleteAttributeIndexImage:async(req,res)=>{
    try{
      //console.log(req.body);
      let product_id = req.body.product_id;
      let indexx = req.body.indexx;

      var record_old = await VariationsPrice.findOne({product_id:product_id});
      if(!record_old)
      {
        return res.send({
          status: false,
          message: "Un identifiant invalide est requis",
          data: []
        })
      }else{
        //console.log("record_old "+record_old);

        let variation_option_1 = record_old.variation_option_1;
        let images = record_old.images;
        if(variation_option_1.length > indexx)
        {
          variation_option_1.splice(indexx, 1);
        }
        if(images.length > indexx)
        {
          //
          let all_img = images[indexx];
          //console.log("all_img ", all_img);
          for(let g=0; g<all_img.length; g++)
          {
            //console.log(all_img[g]);
            let val = all_img[g];
            let uploadDir = './views/admin-panel/public/media/';
            let fileNameWithPath = uploadDir + val;
            //console.log("fileNameWithPath ",fileNameWithPath);
            if (fs.existsSync(fileNameWithPath))
            {
                fs.unlink(fileNameWithPath, (err) => 
                {
                  console.log("unlink file error "+err);
                });
            }
          }
          images.splice(indexx, 1);
        }

        //  console.log(variation_option_1);
        //  console.log(images);
        // console.log(price);
        // console.log(stock);
        // console.log(sku);
        //return false;
        await VariationsPrice.updateOne({product_id:product_id},{
          variation_option_1: variation_option_1,
          images: images,
        }).then((result)=>{
          return res.send({
            status: true,
            message: "Enregistrer les succès supprimés dans leur intégralité",
            data: []
          })
        }).catch((e)=>{
          return res.send({
            status: false,
            message: e.message,
            data: []
          })
        });
      }
    }catch(e){
      return res.send({
        status: false,
        message: e.message,
        data: []
      })
    }
  },
  webSubmitAttribute_old:async(req,res)=>
  {
    try{
      //  console.log(req.body);
      //  console.log(req.files);
      //  return false;

      let formValue = req.body.formValue;
      //console.log(formValue);
      formValue = JSON.parse(formValue);
      

      //console.log("formValue " , formValue);

      let shipping_condition_check = false;
      let quantities = formValue.quantities;
      let quantitiesMonday = formValue.quantitiesMonday;
      let quantitiesTuesday = formValue.quantitiesTuesday;
      let quantitiesWednesday = formValue.quantitiesWednesday;
      

      let firstMainVariation = formValue.firstMainVariation;
      let secondMainVariation = formValue.secondMainVariation;
      var id = req.body.id;
      //console.log("quantities " , quantities);
      if(!id)
      {
        return res.send({
          status:false,
          message:"L'identifiant du produit est requis"
        })
      }
      let product_record = await Product.findOne({_id:id});
      if(!product_record)
      {
        return res.send({
          status:false,
          message:"L'identifiant du produit est requis"
        })
      }
      // console.log("product_record ", product_record);
      // return false;
      if(product_record.seller_will_bear_shipping == true)
      {


        for(let y=0; y<quantitiesMonday.length; y++)
        {
          let price = formValue.quantitiesMonday[y].secondSubVariationPrice;

          const gloabalsettings=await GlobalSettings.findOne({});
          const provider= await SellerModel.findById(product_record.provider_id);
          let plusperproduct = 0;
          let productcommission = 0;
          if(provider)
          {
              if(provider?.sub_id)
              {
                  //sub_id = mongoose.Types.ObjectId(provider.sub_id)
                  let sub_id = provider.sub_id
                  const subscription = await Subscriptions.findOne({_id:sub_id,payment_status:true}).sort({"created_at":-1});
                  //console.log("subscription ",subscription);
                  
                  let product_qty = 1;
                  if(subscription)
                  {
                      plusperproduct = subscription.plusperproduct * product_qty;
                      productcommission = ((subscription.extracommission*price)/100)*product_qty;
                  }else{
                      plusperproduct = gloabalsettings?.plusperproduct*product_qty||0;
                      productcommission = ((gloabalsettings?.commission*price)/100)*product_qty||0;
                  }
              }
              // console.log("plusperproduct  ", plusperproduct);
              // console.log("productcommission  ", productcommission);
          }
          let pro_totla_comision_price = plusperproduct + productcommission;
          let product_final_price_after_commission = price - pro_totla_comision_price;

          //return false;

          let dispatch_info_kk;
          dispatch_info_kk = product_record.dispatch_info
          const product_weight = dispatch_info_kk.weight; 
          // console.log("dispatch_info_kk ",dispatch_info_kk);
          // console.log("dispatch_info_kk.weight ",dispatch_info_kk.weight);
          const product_totalweight = dispatch_info_kk.weight;
          const product_totalweight_in_gm = product_totalweight * 1000;
          //console.log("product_totalweight_in_gm ",product_totalweight_in_gm);
          
          var shipping_record = await ShippingModel.find({},{max_weight:1,cost_base:1,cose_per_unit:1,number_of_days:1,cost_additional_weight:1});
          //console.log("shipping_record ", shipping_record);
          for(let x=0; x<shipping_record.length; x++)
          {
              const max_weight_in_gm = shipping_record[x].max_weight * 1000;
              const cost_additional_weight = shipping_record[x].cost_additional_weight * 1000;
              const cose_per_unit = shipping_record[x].cose_per_unit;
              let shipping_price = shipping_record[x].cost_base;

              // console.log("max_weight_in_gm ",max_weight_in_gm);

              //console.log("394 no. line  shipping_price ",shipping_price);

              if(product_totalweight_in_gm > max_weight_in_gm)
              {
                  let remaining_weight = product_totalweight_in_gm - max_weight_in_gm;
                  let one_unit_in_gm =remaining_weight / cost_additional_weight;

                  //console.log("remaining_weight ",remaining_weight);
                  //console.log("cost_additional_weight ",cost_additional_weight);

                  //console.log("one_unit_in_gm ",one_unit_in_gm);
                  
                  let total_additional_price = one_unit_in_gm * cose_per_unit;

                  

                  //console.log("total_additional_price ",total_additional_price);
                  
                  shipping_price = shipping_price + total_additional_price;
                  if(shipping_price >= product_final_price_after_commission)
                  {
                      shipping_condition_check = true;
                  }
              }
              
              console.log("411 no. line  product_final_price_after_commission ", product_final_price_after_commission);
              console.log("411 no. line  shipping_price ",shipping_price);
              console.log("====================================> for loop end ");
          }
        }
      }
      //console.log("shipping_condition_check ", shipping_condition_check);return false;
      if(shipping_condition_check == true)
      {
          return res.send({
              status:false,
              message:"Vous ne pouvez pas payer les frais d'expédition pour ce produit. Veuillez désactiver la bascule."
          })
      }
      // console.log("quantities ", quantities);
      // console.log("quantitiesMonday ", quantitiesMonday);
      
      //return false;

      var arr_attribute = [];
      let images = [];
      // if(quantities != "" && quantitiesMonday != "")
      // {
      //   // console.log("quantities ", quantities.length);
      //   // console.log("quantitiesMonday ", quantitiesMonday.length);
      //   // console.log("here 663")
      //   // return false;
        
      // }
      if(req.files)
      {
        if(req.files.image0)
        {
          if(req.files.image0.length > 0)
          {
            let image = [];
            image = req.files.image0.map((val)=>{
              return val.filename;
            });
            images.push(image);
          }
        }
        if(req.files.image1)
        {
          if(req.files.image1.length > 0)
          {
            let image = [];
            image = req.files.image1.map((val)=>{
              return val.filename;
            });
            images.push(image);
          }
        }
      }
      let variation_1 = "";
      let variation_option_1 = [];
      let variation_2 = "";
      console.log("iffff  842 ");
      let aa = {};
      if(quantitiesWednesday)
      {
        if(quantitiesWednesday.length > 0)
        {
          for(let x=0; x<quantitiesWednesday.length; x++)
          {
            variation_option_1.push(quantitiesWednesday[x].old_variation_option_11);
          }
        }
      }
      

      for(let x=0; x<quantities.length; x++)
      {
        variation_1 = firstMainVariation;
        variation_option_1.push(quantities[x].firstSubVariation);
        variation_2 = secondMainVariation;
      }

      

      let variation_option_2 = [];
      let price = [];
      let stock = [];
      let sku = [];
      if(quantitiesTuesday)
      {
        if(quantitiesTuesday.length > 0)
        {
          for(let y=0; y<quantitiesTuesday.length; y++)
          {
            variation_option_2.push(quantitiesTuesday[y].old_secondSubVariation);
            price.push(formValue.quantitiesTuesday[y].old_secondSubVariationPrice);
            stock.push(formValue.quantitiesTuesday[y].old_secondSubVariationStock);
            sku.push(formValue.quantitiesTuesday[y].old_secondSubVariationSKU);
          }
        }
      }
      if(quantitiesMonday)
      {
        if(quantitiesMonday.length > 0)
        {
          for(let y=0; y<quantitiesMonday.length; y++)
          {
            variation_option_2.push(quantitiesMonday[y].secondSubVariation);
            price.push(formValue.quantitiesMonday[y].secondSubVariationPrice);
            stock.push(formValue.quantitiesMonday[y].secondSubVariationStock);
            sku.push(formValue.quantitiesMonday[y].secondSubVariationSKU);
          }
        }
      }
      
       
      // aa["variation_1"] = variation_1;
      // aa["variation_option_1"] = variation_option_1;
      // aa["variation_2"] = variation_2;
      // aa["variation_option_2"] = variation_option_2;
      // aa["price"] = price;
      // aa["stock"] = stock;
      // aa["sku"] = sku;
      // aa["images"] = images;
      // arr_attribute.push(aa);
      //attr_sku_4_5
      //  console.log("images ", images);
      //console.log("arr_attribute ", arr_attribute);
      //  return false;

      if(quantities.length == 0 && quantitiesMonday.length == 0)
      {
        return res.send({
            status:false,
            message:"Rien à ajouter"
        })
      }else{
        var record_old = await VariationsPrice.findOne({product_id:id},{_id:1,images:1});
        if(record_old)
        {
          record_old.images.forEach((val)=>{
            let uploadDir = './views/admin-panel/public/media/';
            let fileNameWithPath = uploadDir + val;
            console.log("fileNameWithPath ",fileNameWithPath);
            if (fs.existsSync(fileNameWithPath))
            {
                fs.unlink(fileNameWithPath, (err) => 
                {
                  console.log("unlink file error "+err);
                });
            }
          });
          
          if(images)
          {
            if(images.length > 0)
            {
              if(record_old.images)
              {
                if(record_old.images.length > 0)
                {
                  for(let x=0; x<images.length; x++)
                  {
                    record_old.images.push(images[x]);
                  }
                }
              }
            }else{
              if(record_old.images)
                {
                  if(record_old.images.length > 0)
                  {
                    for(let x=0; x<images.length; x++)
                    {
                      record_old.images.push(images[x]);
                    }
                  }
                }
            }
          }else{
            if(record_old.images)
              {
                if(record_old.images.length > 0)
                {
                  for(let x=0; x<images.length; x++)
                  {
                    record_old.images.push(images[x]);
                  }
                }
              }
          }

          //console.log("record_old.images " , record_old.images );
          variation_1 = formValue.firstMainVariation;
          variation_2 = formValue.secondMainVariation;
          images = record_old.images;
          // console.log("hereeeee");
          // console.log("req.body ", req.body );
          // console.log("variation_1 ", variation_1 );
          // console.log("variation_option_1 ", variation_option_1 );
          // console.log("variation_2 ", variation_2 );
          // console.log("variation_option_2 ", variation_option_2 );
          // console.log("price ", price );
          // console.log("stock ", stock );
          // console.log("sku ", sku );
          // console.log("images ", images );
          // return false;

          const datatoupdate={
            ...(variation_1 && { variation_1: variation_1 }),
            ...(variation_option_1 && { variation_option_1: variation_option_1 }),
            ...(variation_2 && { variation_2: variation_2 }),
            ...(variation_option_2 && { variation_option_2: variation_option_2 }),
            ...(price && { price: price }),
            ...(stock && { stock: stock }),
            ...(sku && { sku: sku }),
            ...(images && { images: images }),

          };
          VariationsPrice.updateOne({_id:record_old._id},datatoupdate,function(err,result){
            if(err)
            {
              return res.send({
                status:false,
                message: err.message
              });
            }else{
              return res.send({
                  status:true,
                  message:"Attribut ajouté succès complet"
              });
            }
          });
        }else{
          // console.log("hereeeee");
          // console.log("variation_1 ", variation_1 );
          // console.log("variation_option_1 ", variation_option_1 );
          // console.log("variation_2 ", variation_2 );
          // console.log("variation_option_2 ", variation_option_2 );
          // console.log("price ", price );
          // console.log("stock ", stock );
          // console.log("sku ", sku );
          // console.log("images ", images );
          // return false;
          VariationsPrice.create({
            variation_1 : variation_1,
            variation_option_1 : variation_option_1,
            variation_2 : variation_2,
            variation_option_2 : variation_option_2,
            price : price,
            stock : stock,
            sku : sku,
            images : images,
            product_id: id
          },function(err,result){
            if(err)
            {
              return res.send({
                status:false,
                message: err.message
              });
            }else{
              return res.send({
                  status:true,
                  message:"Attribut ajouté succès complet"
              });
            }
          });
        }
      }
    }catch(error){
      return res.send({
        status: false,
        message: error.message
      })
    }
  },


  webSubmitAttribute:async(req,res)=>
  {
    try{
      
      // console.log(req.body);
      // console.log(req.files);
      // return false;
      let id = req.body.id;
      let formValue = req.body.formValue;
      formValue = JSON.parse(formValue);
      // console.log("formValue ", formValue);
      // console.log("formValue ", formValue.product_id);
      // console.log("formValue ", formValue.quantities);

      if(!id)
      {
        return res.send({
          status:false,
          message:"L'identifiant du produit est requis"
        })
      }
      let product_record = await Product.findOne({_id:id});
      if(!product_record)
      {
        return res.send({
          status:false,
          message:"L'identifiant du produit est requis"
        })
      }


      let image0 = [];  let image1 = []; let image2 = [];
      let image3 = [];  let image4 = []; let image5 = [];
      let image6 = [];  let image7 = []; let image8 = [];
      let image9 = [];  let image10 = []; let image11 = [];
      let image12 = []; let image13 = [];  let image14 = []; 
      let image15 = []; let image16 = [];  let image17 = [];
      let image18 = []; let image19 = [];
      let quantities = formValue.quantities;
      let m =0;
      if(req.files.image0)
      {
        req.files.image0.map((val)=>{
          image0[m] = val["filename"];
          m++;
        });
      }
      
      if(req.files.image1)
      {
        let n=0;
        req.files.image1.map((val)=>{
          image1[n] = val["filename"];
          n++;
        });
      }
      
      if(req.files.image2)
      {
        let o = 0;
        req.files.image2.map((val)=>{
          image2[o] = val["filename"];
          o++;
        });
      }
      
      if(req.files.image3)
      {
        let p = 0;
        req.files.image3.map((val)=>{
          image3[p] = val["filename"];
          p++;
        });
      }
      
      if(req.files.image4)
      {
        let q = 0;
        req.files.image4.map((val)=>{
          image4[q] = val["filename"];
          q++;
        });
      }
      
      if(req.files.image5)
      {
        let r = 0;
        req.files.image5.map((val)=>{
          image5[r] = val["filename"];
          r++;
        });
      }
      
      if(req.files.image6)
      {
        let s = 0;
        req.files.image6.map((val)=>{
          image6[s] = val["filename"];
          s++;
        });
      }
      
      if(req.files.image7)
      {
        let t = 0;
        req.files.image7.map((val)=>{
          image7[t] = val["filename"];
          t++;
        });
      }
      
      if(req.files.image8)
      {
        let u = 0;
        req.files.image8.map((val)=>{
          image8[u] = val["filename"];
          u++;
        });
      }
      
      if(req.files.image9)
      {
        let v = 0;
        req.files.image9.map((val)=>{
          image9[v] = val["filename"];
          v++;
        });
      }
      
      if(req.files.image10)
      {
        let mm =0;
        req.files.image10.map((val)=>{
          image10[mm] = val["filename"];
          mm++;
        });
      }
      
      if(req.files.image11)
      {
        
        let nn=0;
        req.files.image11.map((val)=>{
          image11[nn] = val["filename"];
          nn++;
        });
      }
      
      if(req.files.image12)
      {
        let oo = 0;
        req.files.image12.map((val)=>{
          image12[oo] = val["filename"];
          oo++;
        });
      }
      
      if(req.files.image13)
      {

        let pp = 0;
        req.files.image13.map((val)=>{
          image13[pp] = val["filename"];
          pp++;
        });
      }
      
      if(req.files.image14)
      {
        let qq = 0;
        req.files.image14.map((val)=>{
          image14[qq] = val["filename"];
          qq++;
        });
      }
      
      if(req.files.image15)
      {
        let rr = 0;
        req.files.image15.map((val)=>{
          image15[rr] = val["filename"];
          rr++;
        });
      }
      
      if(req.files.image16)
      {
        let ss = 0;
        req.files.image16.map((val)=>{
          image16[ss] = val["filename"];
          ss++;
        });
      }
      
      if(req.files.image17)
      {
        let tt = 0;
        req.files.image17.map((val)=>{
          image17[tt] = val["filename"];
          tt++;
        });
      }
      
      if(req.files.image18)
      {
        let uu = 0;
        req.files.image18.map((val)=>{
          image18[uu] = val["filename"];
          uu++;
        });
      }
      
      if(req.files.image19)
      {
        let vv = 0;
        req.files.image19.map((val)=>{
          image19[vv] = val["filename"];
          vv++;
        });
      }
      
        
      // console.log("image0 --- ", image0);
      // console.log("image1 --- ", image1);
      // console.log("image2 --- ", image2);
      console.log("hereeeeeeeee ");
      //console.log("quantities ", quantities);
      let push_array = [];
      let a_inc = 0;
      quantities.map(async (value)=>{
          //let ccc = image+a_inc;
        let arr = {
          product_id:id,
          color: value.color,
          price: value.price,
          stock: value.stock,
          sku: value.sku,
          size: value.size
        };
        if(a_inc == 0)
        {
          arr["image"] = image0;
        }
        if(a_inc == 1)
        {
          arr["image"] = image1;
        }
        if(a_inc == 2)
        {
          arr["image"] = image2;
        }
        if(a_inc == 3)
        {
          arr["image"] = image3;
        }
        if(a_inc == 4)
        {
          arr["image"] = image4;
        }
        if(a_inc == 5)
        {
          arr["image"] = image5;
        }
        if(a_inc == 6)
        {
          arr["image"] = image6;
        }
        if(a_inc == 7)
        {
          arr["image"] = image7;
        }
        if(a_inc == 8)
        {
          arr["image"] = image8;
        }
        if(a_inc == 9)
        {
          arr["image"] = image9;
        }
        if(a_inc == 10)
        {
          arr["image"] = image10;
        }
        if(a_inc == 11)
        {
          arr["image"] = image11;
        }
        if(a_inc == 12)
        {
          arr["image"] = image12;
        }
        if(a_inc == 13)
        {
          arr["image"] = image13;
        }
        if(a_inc == 14)
        {
          arr["image"] = image14;
        }
        if(a_inc == 15)
        {
          arr["image"] = image15;
        }
        if(a_inc == 16)
        {
          arr["image"] = image16;
        }
        if(a_inc == 17)
        {
          arr["image"] = image17;
        }
        if(a_inc == 18)
        {
          arr["image"] = image18;
        }
        if(a_inc == 19)
        {
          arr["image"] = image19;
        } 
        push_array[a_inc] = arr;
        a_inc++;
      });
      //console.log("push_array " , push_array);
      //console.log("product_record " , product_record);
      //return false;
      let vv = [] ;
      if(product_record.sales_info_variation_prices.length > 0) 
      {
        vv = product_record.sales_info_variation_prices;
        for(let t=0; t<push_array.length; t++)
        {
          vv.push(push_array[t]);
        }
        await Product.updateOne({_id:id},{sales_info_variation_prices:vv});
      }else{
        await Product.updateOne({_id:id},{sales_info_variation_prices:push_array});
      }
      
      return res.send({
        status: true,
        message: "Variante ajoutée succès complet"
      })
      //let formValue = req.body.formValue;
      //console.log(formValue);
      
    }catch(error){
      return res.send({
        status: false,
        message: error.message
      })
    }
  },

  webUpdateSubmitAttribute:async(req,res)=>
    {
      try{
        
        // console.log(req.body);
        // console.log(req.files);
        // return false;
        let id = req.body.id;
        let formValue = req.body.formValue;
        formValue = JSON.parse(formValue);
        //console.log("formValue ", formValue);
        let quantitiesTuesday = formValue.quantitiesTuesday;
        //console.log("quantitiesTuesday -------- ", quantitiesTuesday);
        //return false;
        // console.log("formValue ", formValue);
        // console.log("formValue ", formValue.product_id);
        // console.log("formValue ", formValue.quantities);
  
        if(!id)
        {
          return res.send({
            status:false,
            message:"L'identifiant du produit est requis"
          })
        }
        let product_record = await Product.findOne({_id:id});
        if(!product_record)
        {
          return res.send({
            status:false,
            message:"L'identifiant du produit est requis"
          })
        }
  
  
        let image0 = [];  let image1 = []; let image2 = [];
        let image3 = [];  let image4 = []; let image5 = [];
        let image6 = [];  let image7 = []; let image8 = [];
        let image9 = [];  let image10 = []; let image11 = [];
        let image12 = []; let image13 = [];  let image14 = []; 
        let image15 = []; let image16 = [];  let image17 = [];
        let image18 = []; let image19 = [];
        let quantities = formValue.quantities;
        let m =0;
        if(req.files.image0)
        {
          req.files.image0.map((val)=>{
            image0[m] = val["filename"];
            m++;
          });
        }
        
        if(req.files.image1)
        {
          let n=0;
          req.files.image1.map((val)=>{
            image1[n] = val["filename"];
            n++;
          });
        }
        
        if(req.files.image2)
        {
          let o = 0;
          req.files.image2.map((val)=>{
            image2[o] = val["filename"];
            o++;
          });
        }
        
        if(req.files.image3)
        {
          let p = 0;
          req.files.image3.map((val)=>{
            image3[p] = val["filename"];
            p++;
          });
        }
        
        if(req.files.image4)
        {
          let q = 0;
          req.files.image4.map((val)=>{
            image4[q] = val["filename"];
            q++;
          });
        }
        
        if(req.files.image5)
        {
          let r = 0;
          req.files.image5.map((val)=>{
            image5[r] = val["filename"];
            r++;
          });
        }
        
        if(req.files.image6)
        {
          let s = 0;
          req.files.image6.map((val)=>{
            image6[s] = val["filename"];
            s++;
          });
        }
        
        if(req.files.image7)
        {
          let t = 0;
          req.files.image7.map((val)=>{
            image7[t] = val["filename"];
            t++;
          });
        }
        
        if(req.files.image8)
        {
          let u = 0;
          req.files.image8.map((val)=>{
            image8[u] = val["filename"];
            u++;
          });
        }
        
        if(req.files.image9)
        {
          let v = 0;
          req.files.image9.map((val)=>{
            image9[v] = val["filename"];
            v++;
          });
        }
        
        if(req.files.image10)
        {
          let mm =0;
          req.files.image10.map((val)=>{
            image10[mm] = val["filename"];
            mm++;
          });
        }
        
        if(req.files.image11)
        {
          
          let nn=0;
          req.files.image11.map((val)=>{
            image11[nn] = val["filename"];
            nn++;
          });
        }
        
        if(req.files.image12)
        {
          let oo = 0;
          req.files.image12.map((val)=>{
            image12[oo] = val["filename"];
            oo++;
          });
        }
        
        if(req.files.image13)
        {
  
          let pp = 0;
          req.files.image13.map((val)=>{
            image13[pp] = val["filename"];
            pp++;
          });
        }
        
        if(req.files.image14)
        {
          let qq = 0;
          req.files.image14.map((val)=>{
            image14[qq] = val["filename"];
            qq++;
          });
        }
        
        if(req.files.image15)
        {
          let rr = 0;
          req.files.image15.map((val)=>{
            image15[rr] = val["filename"];
            rr++;
          });
        }
        
        if(req.files.image16)
        {
          let ss = 0;
          req.files.image16.map((val)=>{
            image16[ss] = val["filename"];
            ss++;
          });
        }
        
        if(req.files.image17)
        {
          let tt = 0;
          req.files.image17.map((val)=>{
            image17[tt] = val["filename"];
            tt++;
          });
        }
        
        if(req.files.image18)
        {
          let uu = 0;
          req.files.image18.map((val)=>{
            image18[uu] = val["filename"];
            uu++;
          });
        }
        
        if(req.files.image19)
        {
          let vv = 0;
          req.files.image19.map((val)=>{
            image19[vv] = val["filename"];
            vv++;
          });
        }
        
          
        // console.log("image0 --- ", image0);
        // console.log("image1 --- ", image1);
        // console.log("image2 --- ", image2);
        console.log("hereeeeeeeee ");
        //console.log("quantities ", quantities);
        let push_array = [];
        let a_inc = 0;
        quantities.map(async (value)=>{
            //let ccc = image+a_inc;
          if(value.color != "" && value.price)
          {
            let arr = {
              product_id:id,
              color: value.color,
              price: value.price,
              stock: value.stock,
              sku: value.sku,
              size: value.size
            };
            if(a_inc == 0)
            {
              arr["image"] = image0;
            }
            if(a_inc == 1)
            {
              arr["image"] = image1;
            }
            if(a_inc == 2)
            {
              arr["image"] = image2;
            }
            if(a_inc == 3)
            {
              arr["image"] = image3;
            }
            if(a_inc == 4)
            {
              arr["image"] = image4;
            }
            if(a_inc == 5)
            {
              arr["image"] = image5;
            }
            if(a_inc == 6)
            {
              arr["image"] = image6;
            }
            if(a_inc == 7)
            {
              arr["image"] = image7;
            }
            if(a_inc == 8)
            {
              arr["image"] = image8;
            }
            if(a_inc == 9)
            {
              arr["image"] = image9;
            }
            if(a_inc == 10)
            {
              arr["image"] = image10;
            }
            if(a_inc == 11)
            {
              arr["image"] = image11;
            }
            if(a_inc == 12)
            {
              arr["image"] = image12;
            }
            if(a_inc == 13)
            {
              arr["image"] = image13;
            }
            if(a_inc == 14)
            {
              arr["image"] = image14;
            }
            if(a_inc == 15)
            {
              arr["image"] = image15;
            }
            if(a_inc == 16)
            {
              arr["image"] = image16;
            }
            if(a_inc == 17)
            {
              arr["image"] = image17;
            }
            if(a_inc == 18)
            {
              arr["image"] = image18;
            }
            if(a_inc == 19)
            {
              arr["image"] = image19;
            } 
            push_array[a_inc] = arr;
            a_inc++;
          }
        });
        //console.log("push_array " , push_array);
        //console.log("quantitiesTuesday " , quantitiesTuesday);
        let new_arr = quantitiesTuesday.map((value)=>{
          let obj = {
            _id: mongoose.Types.ObjectId(value.old_variation_id),
            image:value.old_images,
            color:value.old_color,
            size:value.old_size,
            price:value.old_price,
            stock:value.old_stock,
            sku:value.old_sku,
            product_id:id
          };
          return obj;
        });
        //console.log("hereeeeeeeeeeeeeeeeeeeeeee ", new_arr);
        //console.log("new_arr ", new_arr);
        //return false;
        let vv = new_arr ;
         
        if(product_record.sales_info_variation_prices.length > 0) 
        {
          // vv = product_record.sales_info_variation_prices;
          // for(let t=0; t<push_array.length; t++)
          // {
          //   vv.push(push_array[t]);
          // }
          if(push_array.length > 0)
          {
            vv = new_arr.concat(push_array);
          }
          //console.log("vv", vv);return false;
          await Product.updateOne({_id:id},{sales_info_variation_prices:vv});
        }else{
          await Product.updateOne({_id:id},{sales_info_variation_prices:push_array});
        }
        
        return res.send({
          status: true,
          message: "Variante ajoutée succès complet"
        })
        //let formValue = req.body.formValue;
        //console.log(formValue);
        
      }catch(error){
        return res.send({
          status: false,
          message: error.message
        })
      }
    },
  deleteProImg:async(req,res)=>{
    try{
      //console.log(req.body);
      var id = req.body.id;
      var name = req.body.name;
      var products_record = await Product.findById(id,{images:1,video:1});
      if(!products_record)
      {
        return res.send({
          status: false,
          message: "error"
        })
      }
      if(products_record.images.length == 0)
      {
        return res.send({
          status: false,
          message: "error"
        })
      }
      let new_img = products_record.images.filter((val)=>{
        //console.log(val);
        if(val != name)
        {
          return val
        }
      });
      //console.log("new_img ",new_img);
      var uploadDir = './views/admin-panel/public';
      let fileNameWithPath = uploadDir + name;
      //console.log(fileNameWithPath);
      
      if (fs.existsSync(fileNameWithPath))
      {
          fs.unlink(fileNameWithPath, (err) => 
          {
              //console.log("unlink file error "+err);
          });
      }
      await Product.findByIdAndUpdate(id, {images:new_img}).then((result) => {
        console.log("result",result)
        return res.send({
            status: true,
            data:result,
            message: "Produit mis à jour avec succès"
        })
      })
    }catch(error){
      return res.send({
        status: false,
        message: error.message
      })
    }
  },
  getVariationPriceStock:async(req,res)=>{
    try{
      console.log(req.body);
      var record = await VariationsPrice.findOne({variation_option_1:req.body.firstVariationValue,variation_option_2:req.body.secondVariationValue,product_id:req.body.product_id});
      if(record)
      {
        return res.send({
          status: true,
          message: "Success",
          record:record
        })
      }else{
        return res.send({
          status: false,
          message: "No data",
          record:{}
        })
      }
    }catch(error){
      return res.send({
        status: false,
        message: error.message
      })
    }
  },
  groupRating:async(req,res)=>{
    try{
      //mongoose.set("debug",true);
      var product_id = req.body.product_id;
      await Ratingnreviews.aggregate([
        {
          $match:{
            "product_id":product_id
          }
        },
        {
          $group:{
            "_id":"$rating",
            count:{$sum:1
            }
          }
        }
      ]).then((result)=>{
        return res.send({
          status: true,
          message: "Success",
          data: result
      })
      }).catch((error)=>{
        return res.send({
          status: false,
          message: error.message,
          data: []
      })
      });
    }catch(error)
    {
        return res.send({
            status: false,
            message: error.message,
            data: []
        })
    }
},
  getMaximumReturnDay:async(req,res)=>{
    try{
      let aa = await SettingModel.find({ attribute_key:"return_max_day"  });
      if(aa)
      {
        return res.send({
          status: true,
          message: "Success",
          data: aa
        })
      }else{
        return res.send({
          status: false,
          message: "Success",
          data: []
        })
      }
      
    }catch(error){
      return res.send({
        status: false,
        message: error.message,
        data: []
      })
    }
  }
};