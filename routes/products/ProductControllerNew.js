const { Product } = require("../../models/products/Product");
const UserWebHistoryModel = require("../../models/products/UserWebHistoryModel");

const OrderModel = require("../../models/orders/order");
const ProductstempimagesModel = require("../../models/products/ProductstempimagesModel");
const MatCatagory=require("../../models/products/MatCatagory");
const Variations =require("../../models/products/Variations");
const VariationsPrice =require("../../models/products/VariationPrice");
const pageViewsModel=require("../../models/products/pageViews");
const mongoose=require("mongoose");
const catagorymodel = require("../../models/products/Catagory");
const ProductfavoriteModel = require("../../models/products/ProductfavoriteModel");
const CoupensModel=require("../../models/seller/Coupens");
const Ratingnreviews=require("../../models/actions/Ratingnreviews");
const SettingModel = require("../../models/admin/SettingModel");
const Subscriptions=require("../../models/subscriptions/Subscription");
const ShippingModel = require("../../models/shipping/Shipping");
const GlobalSettings=require("../../models/admin/GlobalSettings");
const SellerModel=require("../../models/seller/Seller");

const fs = require("fs");
const { forEach } = require("async");

module.exports = {
  filterProducts:async(req,res)=>{
    try{
      //console.log("hereeeeee");
      //mongoose.set("debug",true);
      
      var {category_id,pagno} = req.body;
      var query_front_side = req.body.query;
      //String_qr["ad_name"] = {'$regex' : '^'+search_var, '$options' : 'i'}
      const opquery={is_deleted:false};
            const ipquery={};
            const rpquery={};
            const catagory=req.body.catagory;
            // const attributes=req.body.attributes;
            // const sales_info=req.body.sales_info;
            let sort=req.body.sort||1;
            sort = parseInt(sort);
            console.log("sort ", sort);
            const pricerangeLow= parseFloat(req.body.pricerangeLow);
            const pricerangeHigh=parseFloat(req.body.pricerangeHigh);
            const color=req.body.color;
            const countryofmfg=req.body.countryofmfg;
            const condition=req.body.condition;
            const availibility=req.body.availibility;
            const rating=parseInt(req.body.rating);
            var user_id = req.body.user_id;

            opquery['is_active']=true;


            if(condition){
              if(condition == "new")
              {
                opquery['condition']=true;
              }else{
                opquery['condition']=false;
              }
                
            }
            if(rating){
                rpquery['rating']= parseInt(rating);
            }
            console.log("pricerangeLow ",pricerangeLow);
            if(pricerangeLow >=0 && pricerangeHigh){
              opquery['$and']=[
                {
                    price:{$gte:pricerangeLow}
                },
                {
                    price:{$lte:pricerangeHigh}
                }
            ]
              
            }

      var category_record = {};
      var sub_category_record = [];
      if(category_id)
      {
        opquery["category_id"] = category_id;
        //category_record = await catagorymodel.findOne({_id:category_id});
        //sub_category_record = await catagorymodel.find({mastercatagory:category_id});
      }
      if(query_front_side)
      {
        opquery["title"] = {'$regex' : '^'+query_front_side, '$options' : 'i'}
      }
      // console.log('opquery')
      // console.log(opquery);
      //console.log("rpquery ",rpquery);
      //mongoose.set("debug",true);
      pagno = pagno ? pagno : 0;
      var limit = 10;
      var skip = pagno*limit;
      //{ $match: { FieldCollege: { $ne: [] } } },
       
        await Product.aggregate([
          {
            $match:opquery
          },
          {
            $lookup:{
                from:"ratingnreviews",
                let:{"prod_id":{"$toString":"$_id"}},
                //let:{productidstring:{"$toString":"$_id"}},
                pipeline:[
                    {
                        $match: rpquery 
                    },
                    {
                        $match:{
                            $expr:{
                                $eq:["$product_id","$$prod_id"]
                            }
                        }
                    }
                ],
                as:"reviews"
            },
            
          },
          {
            $addFields:{
              "totalRating":{$size:"$reviews"}
            }
          },
          { $addFields: {
                              
              avgRating:{
                  $avg: "$reviews.rating"
              },
              provideridObj:{
                  "$toObjectId":"$provider_id"
              }
          }},
          
          {
              $lookup:{
                  from:"variationprices",
                  let:{productidstring:{"$toString":"$_id"}},
                  pipeline:[
                      {$match:{
                          $expr:{
                              $eq:["$product_id","$$productidstring"]
                          }
                      }}
                  ],
                  as:"variations"
              }
          },
          {
              $addFields:{
                  pricerangeMin:{
                      $cond:{
                          if:{$gt:[{$size:"$variations"},0]},
                          then:{
                              $min:"$variations.price"
                        
                        
                        },else:"$price"
                  }},
                  pricerangeMax:{
                      $cond:{
                          if:{$gt:[{$size:"$variations"},0]},
                          then:{
                              $max:"$variations.price"
                        
                        
                        },else:"$price"
                  }},
                
              }
            },
            {
              $lookup:{
                  from:"productfavorites",
                  let:{"pro_id":{"$toString":"$_id"}},
                  pipeline:[
                      {
                      $match:{
                          $expr:{
                              $and:[
                                  {$eq:["$user_id",user_id]},
                                  {$eq:["$product_id","$$pro_id"]}
                              ]
                          }
                      }
                  }
              ],
              as:"favProduct"
              }
          },{
              $addFields:{
                  "isFav":{$size:"$favProduct"}
              }
          },
          {$sort:{"price":sort}},
          
          {$skip:skip},
          {$limit:limit},
          {$project:{
              "variations":0,
              "spec_attributes":0,
              "reported":0,
              "dispatch_info":0,
              "shipping_details":0,
              "category_id":0,
              "__v":0,
              "spec_attributes":0,
              "pricerangeMin":0,
              "pricerangeMax":0,
              "reviews":0

          }}
        ]).then((result)=>{
          return res.send({
            status:true,
            message:"Success",
            category_record:category_record,
            sub_category_record:sub_category_record,
            data:result,
          })
        }).catch((error)=>{
          return res.send({
            status:false,
            message:error.message
          })
        });
      
      
    }catch(error)
    {
      return res.send({
        status:false,
        message:error.message
      })
    }
  },
  getNearByShop:async(req,res)=>{
    try{
      let {latitude,longitude,radius} = req.body;
      if(latitude == "" || latitude == null || longitude == "" || longitude == null )
      {
        return res.send({
          status:false,
          message:"L'adresse doit être une adresse google uniquement"
        })
      }
      latitude = parseFloat(latitude);
      longitude = parseFloat(longitude);
      //mongoose.set("debug",true);
      let newradius=radius*1000;
      await SellerModel.aggregate([
        {
          $geoNear:{
            near:{
              type:"Point",coordinates:[ longitude,latitude ]
            },
            distanceMultiplier: 0.001,
            "distanceField": "dist.calculated",
            includeLocs: "dist.location",
            spherical: true,
            "maxDistance":newradius,
          }
        },
        {
          $match:{
            $and:[
              {is_deleted:false},
              {is_approved:true},
              {is_active:true}
            ]
          }
        }
      ]).then((result)=>{
        return res.send({
          status:true,
          message:"Succès",
          data:result
        })
      }).catch((e)=>{
        return res.send({
          status:false,
          message:error.message
        })
      });
    }catch(error)
    {
      return res.send({
        status:false,
        message:error.message
      })
    }
  },
  getProductBySellerId:async(req,res)=>{
    try{
      let {seller_id,page_no} = req.body;
      page_no = page_no?page_no:0;
      let limit = 10;
      let skip = page_no * limit;
      //mongoose.set("debug",true); 
      await Product.aggregate([
        {
          $match:{
            $and:[
              {provider_id:seller_id},
              {is_active:true},
              {is_deleted:false}
            ]
          }
        },
        {
          $skip:skip
        },
        {
          $limit:limit
        }
      ]).then((result)=>{
        return res.send({
          status:true,
          message:"Succès",
          data:result
        })
      }).catch((e)=>{
        return res.send({
          status:false,
          message:error.message
        })
      });
    }catch(error)
    {
      return res.send({
        status:false,
        message:error.message
      })
    }
  }

}