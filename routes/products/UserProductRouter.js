const express = require("express");
const ProductController = require("./ProductController");
const ProductSubController = require("./ProductSubController");
const ProductControllerNew = require("./ProductControllerNew");
const ProductSecondController = require("./ProductSecondController");
const UserProductController = require("./UserProductController");
const router = express.Router();
const multer = require("multer");
const path = require("path");
const SellerController = require('../seller/SellerController');
const PRODUCT_PATH=process.env.PHOTOS_PATH;
const storage = multer.diskStorage({
  
    destination: function (req, file, cb) {
      cb(null, path.resolve(PRODUCT_PATH))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now()+"_"+Array.from(Array(6), () => Math.floor(Math.random() * 36).toString(36)).join('')+ path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });


router.post("/getHomePageProduct",UserProductController.getHomePageProduct);
router.post("/getFilterProducts",UserProductController.getFilterProducts);
router.post("/getFilterProductsCount",UserProductController.getFilterProductsCount);

router.post("/getSingleNewProducts",UserProductController.getSingleNewProducts);
router.get("/getColorWithProductCount",UserProductController.getColorWithProductCount);
router.get("/getCategoryWithProductCount",UserProductController.getCategoryWithProductCount);
router.get("/getSizeWithProductCount",UserProductController.getSizeWithProductCount);
router.get("/getStyleWithProductCount",UserProductController.getStyleWithProductCount);
router.get("/getBrandWithProductCount",UserProductController.getBrandWithProductCount);
router.get("/getDateProductCount",UserProductController.getDateProductCount);

router.post("/bookTryOnProgram",UserProductController.bookTryOnProgram);
router.get("/getAllSellerShopUserSide/:id",UserProductController.getAllSellerShopUserSide);
router.get("/getAllBookedTryOnProgram/:id",UserProductController.getAllBookedTryOnProgram);
router.get("/getSingleBookedTryOnProgram/:id",UserProductController.getSingleBookedTryOnProgram);
router.get("/markUserCancledTryOnProgram/:id",UserProductController.markUserCancledTryOnProgram);
router.get("/markSellerUpdateTryOnProgram/:id/:status",UserProductController.markSellerUpdateTryOnProgram);

router.get("/getSellerTimeSloatTryOn/:id",UserProductController.getSellerTimeSloatTryOn);
router.get("/getSellerTimeSloatPickup/:id",UserProductController.getSellerTimeSloatPickup);
router.get("/getSellerTimeSloatDelivery/:id",UserProductController.getSellerTimeSloatDelivery);

router.post("/getNearByShopBySellerId",UserProductController.getNearByShopBySellerId);

router.get("/forApplyUpdate",UserProductController.forApplyUpdate);
router.get("/deleteOldProduct",UserProductController.deleteOldProduct);
router.post("/getProductDetailForChat",UserProductController.getProductDetailForChat);

module.exports = router;
