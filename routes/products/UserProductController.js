const { Product } = require("../../models/products/Product");
const  ProductModel  = require("../../models/products/Product");
const ProductstempimagesModel = require("../../models/products/ProductstempimagesModel");
const MatCatagory=require("../../models/products/MatCatagory");
const Variations =require("../../models/products/Variations");
const VariationsPrice =require("../../models/products/VariationPrice");
const pageViewsModel=require("../../models/products/pageViews");
const mongoose=require("mongoose");
const catagorymodel = require("../../models/products/Catagory");
const Catagorymodel = require("../../models/products/Catagory");
const ProductfavoriteModel = require("../../models/products/ProductfavoriteModel");
const Subscriptions=require("../../models/subscriptions/Subscription");
const ShippingModel = require("../../models/shipping/Shipping");
const GlobalSettings=require("../../models/admin/GlobalSettings");
const SellerModel=require("../../models/seller/Seller");
const Color = require("../../models/admin/Color");
const SizeModel = require("../../models/products/SizeModel");
const StyleModel = require("../../models/products/StyleModel");
const Brands = require("../../models/products/Brands");
const SellerTimeSloats = require("../../models/seller/SellerTimeSloats");
const UserTryOnProgramModel = require("../../models/seller/UserTryOnProgramModel");
const SellerBranchesModel = require("../../models/seller/SellerBranchesModel");

const fs = require("fs");

module.exports = {

    getHomePageProduct:async(req,res)=>
    {
        try{
            //console.log("hereeeeeeeeee line No. 22 ");
            let category_id = req.body.category_id;
            let user_id = req.body.user_id;

            let query = {
                is_deleted: false
            };
            query["is_active"] = true;
            if(category_id)
            {
                query["category_id"]= {
                        $in:[category_id]
                    }
            }
            //mongoose.set("debug",true);
            // products.aggregate([ { '$match': { is_deleted: false, is_active: true } }, { '$match': { '$expr': { '$in': [ '64e885384fcbe7c068e5da89', '$category_id' ] } } }, { '$project': { category_id: 1, price: 1, images: 1, title: 1, description: 1, catagory: 1, provider_id: 1 } }, { '$sample': { size: 20 } }], {})



            let product_record = await Product.aggregate([
                {
                    $match:query
                },
                {
                    $lookup:{
                        from:"productfavorites",
                        let:{"p_id":{"$toString":"$_id"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $and:[
                                            {$eq:["$product_id","$$p_id"]},
                                            {$eq:["$user_id",user_id]}
                                        ]
                                    }
                                }
                            }
                        ],
                        as:"favProduct"
                    }
                },
                {
                    $addFields:{
                        "isFav":{$size:"$favProduct"}
                    }
                },
                {
                    $project:{
                        category_id:1,
                        price:1,
                        images:1,
                        title:1,
                        description:1,
                        catagory:1,
                        provider_id:1,
                        isFav:1
                    }
                },
                {
                    $sample:{
                        size:20
                    }
                }
            ]);
            return res.send({
                status:true,
                message:"Success",
                products:product_record
            })
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    getFilterProducts:async(req,res)=>{
        try{
            let {category_id,brand_name,condition,style,size,color,minPrice,maxPrice,day_condition,sort,user_id,discover_by,page_no} = req.body;
            //console.log("day_condition ", day_condition);
            if(!page_no)
            {
                page_no = 0;
            }
            let limitt = parseInt(12);
            let skipp = page_no * limitt;
            skipp = parseInt(skipp);
            let query = {is_deleted: false};
            query["is_active"] = true;
            query["stock"] = {$gt:0};
            if(category_id)
            {
                query["category_id"] = {$in:[category_id]}
            }
            if(brand_name)
            {
                if(brand_name.length > 0)
                {
                    query["brand_name"] = {$in:brand_name};
                }
            }
            if(style)
            {
                if(style.length > 0)
                {
                    query["style"] = {$in:style};
                }
                
            }
            //console.log("size ", size);
            //console.log("size.length ", size.length);
            if(size)
            {
                if(size.length > 0)
                {
                    console.log("size  hereeeee 107");
                    query["$and"] = [
                        {
                            "sales_info_variation_prices":{
                                $elemMatch:{size : { $in:size } } 
                            }
                        }
                    ]
                    // {
                    //      $elemMatch:{size : { $in:size } } 
                    // }
                }
            }
            //console.log("query ", query);
            if(color)
            {
                if(color.length > 0)
                {
                    // query["sales_info_variation_prices"] = {
                    //     $elemMatch:{
                    //         color:{$in:color}
                    //     }
                    // }
                    query["$and"] = [
                        {
                            "sales_info_variation_prices":{
                                $elemMatch:{color : { $in:color } } 
                            }
                        }
                    ]

                }
            }
            if(condition)
            {
                if(condition == "new")
                {
                    query["condition"] = true;
                }else if(condition != null && condition != undefined)
                {
                    query["condition"] = false;
                }
            }
            if(minPrice >= 0 && maxPrice)
            {
                query["price"] = {$gte:minPrice,$lte:maxPrice};
                // query["sales_info_variation_prices"] = {
                //         $elemMatch:{
                //             price:{
                //                 $gte:minPrice,
                //                 $lte:maxPrice
                //             }
                //         }
                // }
                query["$and"] = [
                    {
                        "sales_info_variation_prices":{
                            $elemMatch:{price : { $gte:minPrice, $lte:maxPrice } } 
                        }
                    }
                ]
            }else if(minPrice >= 0)
            {
                query["price"] = { $gte:minPrice };
                // query["sales_info_variation_prices"] = {
                //     $elemMatch:{
                //         price:{ $gte:minPrice }
                //     }
                // };
                query["$and"] = [
                    {
                        "sales_info_variation_prices":{
                            $elemMatch:{price : { $gte:minPrice } } 
                        }
                    }
                ]
            }else if(maxPrice)
            {
                query["price"] = { $lte:maxPrice };
                // query["sales_info_variation_prices"] = {
                //     $elemMatch:{
                //         price:{
                //             $lte:maxPrice
                //         }
                //     }
                // };
                query["$and"] = [
                    {
                        "sales_info_variation_prices":{
                            $elemMatch:{price : { $lte:maxPrice } } 
                        }
                    }
                ]
            }
            if(day_condition == 'today' )
            {
                let start_date = new Date();
                start_date.setHours(0,0,0,0);
                let end_date = new Date();
                end_date.setHours(23,59,59,999);
                query["created_at"] = {$gte:start_date,$lte:end_date};
            }else if(day_condition == 'yesterday')
            {
                let yesterday = new Date();
                yesterday.setDate(yesterday.getDate() - 1 );

                let start_date = yesterday.setHours(0,0,0,0);
                let end_date = yesterday.setHours(23,59,59,999);
                query["created_at"] = {$gte:start_date,$lte:end_date};
            }else if(day_condition == 'this_week')
            {
                let curr = new Date; // get current date
                let first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
                let last = first + 6; // last day is the first day + 6

                let firstday = new Date(curr.setDate(first));
                let lastday = new Date(curr.setDate(last));
                //firstday = firstday.setHours(0,0,0,0);
                //lastday = lastday.setHours(0,0,0,0);


                let start_date = firstday.setHours(0,0,0,0);
                let end_date = lastday.setHours(23,59,59,999);
                start_date = new Date(start_date);
                end_date = new Date(end_date);
                query["created_at"] = {$gte:start_date,$lte:end_date};
            }
            let sortt = {"created_at":-1};
            if(sort == "latest")
            {
                sortt = {"created_at":-1};
            }else if(sort == "price_asc")
            {
                sortt = {"price":1};
            }else if(sort == "price_desc")
            {
                sortt = {"price": -1};
            }else{
                sortt = {"created_at":-1};
            }
            if(discover_by == true || discover_by =='true')
            {
                query["discover_by"] = true;
            }
            //console.log("query ", JSON.stringify(query));
            //mongoose.set("debug",true);
            await Product.aggregate([
                {
                    $match:query
                },
                {
                    $lookup:{
                        from:"productfavorites",
                        let:{"p_id":{"$toString":"$_id"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $and:[
                                            {$eq:["$product_id","$$p_id"]},
                                            {$eq:["$user_id",user_id]}
                                        ]
                                    }
                                }
                            }
                        ],
                        as:"favProduct"
                    }
                },
                {
                    $addFields:{
                        "isFav":{$size:"$favProduct"}
                    }
                },
                {
                    $project:{
                        "reported":0,
                        "dispatch_info":0,
                        "spec_attributes":0,
                        "seller_will_bear_shipping":0,
                        "is_delisted": 0,
                        "is_deleted": 0,
                        "sales_info": 0,
                        "__v":0,
                        "favProduct":0
                    }
                },
                {
                    $sort:sortt
                },
                {
                    $skip:skipp
                },
                {
                    $limit:limitt
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    products:result
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message,
                    products:[]
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message,
                products:[]
            });
        }
    },
    getFilterProductsCount:async(req,res)=>{
        try{
            let {category_id,brand_name,condition,style,size,color,minPrice,maxPrice,day_condition,sort,user_id,discover_by} = req.body;
            //console.log("day_condition ", day_condition);
            let query = {is_deleted: false};
            query["is_active"] = true;
            query["stock"] = {$gt:0};
            if(category_id)
            {
                query["category_id"] = {$in:[category_id]}
            }
            if(brand_name)
            {
                if(brand_name.length > 0)
                {
                    query["brand_name"] = {$in:brand_name};
                }
            }
            if(style)
            {
                if(style.length > 0)
                {
                    query["style"] = {$in:style};
                }
                
            }
            //console.log("size ", size);
            //console.log("size.length ", size.length);
            if(size)
            {
                if(size.length > 0)
                {
                    console.log("size  hereeeee 107");
                    query["$and"] = [
                        {
                            "sales_info_variation_prices":{
                                $elemMatch:{size : { $in:size } } 
                            }
                        }
                    ]
                    // {
                    //      $elemMatch:{size : { $in:size } } 
                    // }
                }
            }
            //console.log("query ", query);
            if(color)
            {
                if(color.length > 0)
                {
                    // query["sales_info_variation_prices"] = {
                    //     $elemMatch:{
                    //         color:{$in:color}
                    //     }
                    // }
                    query["$and"] = [
                        {
                            "sales_info_variation_prices":{
                                $elemMatch:{color : { $in:color } } 
                            }
                        }
                    ]

                }
            }
            if(condition)
            {
                if(condition == "new")
                {
                    query["condition"] = true;
                }else if(condition != null && condition != undefined)
                {
                    query["condition"] = false;
                }
            }
            if(minPrice >= 0 && maxPrice)
            {
                query["price"] = {$gte:minPrice,$lte:maxPrice};
                // query["sales_info_variation_prices"] = {
                //         $elemMatch:{
                //             price:{
                //                 $gte:minPrice,
                //                 $lte:maxPrice
                //             }
                //         }
                // }
                query["$and"] = [
                    {
                        "sales_info_variation_prices":{
                            $elemMatch:{price : { $gte:minPrice, $lte:maxPrice } } 
                        }
                    }
                ]
            }else if(minPrice >= 0)
            {
                query["price"] = { $gte:minPrice };
                // query["sales_info_variation_prices"] = {
                //     $elemMatch:{
                //         price:{ $gte:minPrice }
                //     }
                // };
                query["$and"] = [
                    {
                        "sales_info_variation_prices":{
                            $elemMatch:{price : { $gte:minPrice } } 
                        }
                    }
                ]
            }else if(maxPrice)
            {
                query["price"] = { $lte:maxPrice };
                // query["sales_info_variation_prices"] = {
                //     $elemMatch:{
                //         price:{
                //             $lte:maxPrice
                //         }
                //     }
                // };
                query["$and"] = [
                    {
                        "sales_info_variation_prices":{
                            $elemMatch:{price : { $lte:maxPrice } } 
                        }
                    }
                ]
            }
            if(day_condition == 'today' )
            {
                let start_date = new Date();
                start_date.setHours(0,0,0,0);
                let end_date = new Date();
                end_date.setHours(23,59,59,999);
                query["created_at"] = {$gte:start_date,$lte:end_date};
            }else if(day_condition == 'yesterday')
            {
                let yesterday = new Date();
                yesterday.setDate(yesterday.getDate() - 1 );

                let start_date = yesterday.setHours(0,0,0,0);
                let end_date = yesterday.setHours(23,59,59,999);
                query["created_at"] = {$gte:start_date,$lte:end_date};
            }else if(day_condition == 'this_week')
            {
                let curr = new Date; // get current date
                let first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
                let last = first + 6; // last day is the first day + 6

                let firstday = new Date(curr.setDate(first));
                let lastday = new Date(curr.setDate(last));
                //firstday = firstday.setHours(0,0,0,0);
                //lastday = lastday.setHours(0,0,0,0);


                let start_date = firstday.setHours(0,0,0,0);
                let end_date = lastday.setHours(23,59,59,999);
                start_date = new Date(start_date);
                end_date = new Date(end_date);
                query["created_at"] = {$gte:start_date,$lte:end_date};
            }
            let sortt = {"created_at":-1};
            if(sort == "latest")
            {
                sortt = {"created_at":-1};
            }else if(sort == "price_asc")
            {
                sortt = {"price":1};
            }else if(sort == "price_desc")
            {
                sortt = {"price": -1};
            }else{
                sortt = {"created_at":-1};
            }
            if(discover_by == true || discover_by =='true')
            {
                query["discover_by"] = true;
            }
            //console.log("query ", JSON.stringify(query));
            //mongoose.set("debug",true);
            await Product.aggregate([
                {
                    $match:query
                },
                {
                    $lookup:{
                        from:"productfavorites",
                        let:{"p_id":{"$toString":"$_id"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $and:[
                                            {$eq:["$product_id","$$p_id"]},
                                            {$eq:["$user_id",user_id]}
                                        ]
                                    }
                                }
                            }
                        ],
                        as:"favProduct"
                    }
                },
                {
                    $addFields:{
                        "isFav":{$size:"$favProduct"}
                    }
                },
                {
                    $project:{
                        "_id":1
                    }
                },
                {
                    $sort:sortt
                }
            ]).then((result)=>{
                let totalPageNumber =  Math.ceil(result.length/12);
                return res.send({
                    status:true,
                    message:"Success",
                    products:result.length,
                    totalPageNumber:totalPageNumber
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message,
                    products:0,
                    totalPageNumber:0
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message,
                products:0,
                totalPageNumber:0
            });
        }
    },
    getSingleNewProducts:async(req,res)=>{
        try{
            let {id,user_id} = req.body;
            //query["category_id"] = {$in:[category_id]}
            //mongoose.set("debug",true);
            await Product.aggregate([
                {
                    $match:{
                        _id:mongoose.Types.ObjectId(id)
                    }
                },
                {
                    $lookup:{
                        from:"productfavorites",
                        let:{"p_id":{"$toString":"$_id"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $and:[
                                            {$eq:["$product_id","$$p_id"]},
                                            {$eq:["$user_id",user_id]}
                                        ]
                                    }
                                }
                            }
                        ],
                        as:"favProduct"
                    }
                },
                {
                    $addFields:{
                        "isFav":{$size:"$favProduct"}
                    }
                },
                {
                    $lookup:{
                        from:"brands",
                        let:{"b_id":{"$toObjectId":"$brand_name"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$_id","$$b_id"]
                                    }
                                }
                            }
                        ],
                        as:"brands"
                    }
                },
                {
                    $lookup:{
                        from:"products",
                        let:{"sprvdr_id":{"$toString": {$arrayElemAt:["$category_id",0]} } },
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $in:["$$sprvdr_id","$category_id"]
                                    }
                                }
                            },
                            {
                                $match:{
                                    "_id":{$ne:id}
                                }
                            },
                            {
                                $sample:{
                                    size:20
                                }
                            },
                            {
                                $lookup:{
                                    from:"productfavorites",
                                    let:{"p_id":{"$toString":"$_id"}},
                                    pipeline:[
                                        {
                                            $match:{
                                                $expr:{
                                                    $and:[
                                                        {$eq:["$product_id","$$p_id"]},
                                                        {$eq:["$user_id",user_id]}
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as:"similr_favProduct"
                                }
                            },
                            {
                                $addFields:{
                                    "similr_isFav":{$size:"$similr_favProduct"}
                                }
                            },
                            // {
                            //     $project:{
                            //         category_id:1
                            //     }
                            // }
                        ],
                        as:"simlr_pro"
                    }
                }
            ]).then(async(result)=>{

                //query["style"] = {$in:style};
                //console.log("result ", result);
                if(result.length > 0)
                {
                    if(result[0].sales_info_variation_prices.length > 0)
                    {
                        //console.log(result[0].sales_info_variation_prices)
                        for(let x=0; x<result[0].sales_info_variation_prices.length; x++)
                        {
                            let new_image = [];
                            //console.log(result[0].sales_info_variation_prices[x].image);
                            if(result[0].sales_info_variation_prices[x].image.length > 0)
                            {
                                //console.log(result[0].sales_info_variation_prices[x].image[0]);
                                for(let y=0; y<result[0].sales_info_variation_prices[x].image.length; y++)
                                {
                                    console.log(result[0].sales_info_variation_prices[x].image[y]);
                                    let c = "/media/"+result[0].sales_info_variation_prices[x].image[y];
                                    new_image.push(c);
                                }
                            }
                            result[0].sales_info_variation_prices[x].image = new_image;
                        }
                        
                    }
                    let all_available_branch = result[0].branches;
                    let b_rec = await SellerBranchesModel.find( { _id: { $in: all_available_branch } }  );
                    return res.send({
                        status:true,
                        message:"Success",
                        products:result[0],
                        available_branches:b_rec,
                        //similar_product:similar_product
                    });
                }else{
                    return res.send({
                        status:false,
                        message:"Identifiant non valide",
                        products:{}
                    });
                }                
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message,
                    products:[]
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message,
                products:[]
            });
        }
    },
    getColorWithProductCount:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            //colors.aggregate([ { '$match': { status: 1 } }, { '$lookup': { from: 'products', let: { cat_id: { '$toString': '$color_name' } }, pipeline: [ { '$match': { sales_info_variation_prices: { '$elemMatch': { color: { '$in': [ 'Rouge' ] } } } } } ], as: 'cat_product' } }, { '$addFields': { catProductCount: { '$size': '$cat_product' } } }, { '$project': { created_at: 0, updated_at: 0, status: 0, __v: 0 } }], {})
            // query["$and"] = [
            //     {
            //         "sales_info_variation_prices":{
            //             $elemMatch:{color : { $in:color } } 
            //         }
            //     }
            // ]
            await Color.aggregate([
                {
                    $match:{
                        status: 1
                    }
                },
                {
                    $lookup:{
                        from:"products",
                        //let:{"cat_id":{"$toString":"$color_name"}},
                        let: { obj: "$color_name" },
                        pipeline: [
                            { $match: { $expr: { $in: ["$$obj", "$sales_info_variation_prices.color"] } } },
                            {
                                $match:{
                                    $and:[
                                        {
                                            is_deleted: false
                                        },
                                        {
                                            is_active: true
                                        }
                                    ]
                                }
                            }
                        ],
                        as:"cat_product"
                    }
                },
                {
                    $addFields:{
                        "catProductCount":{
                            $size:"$cat_product"
                        }
                    }
                },
                {
                    $project:{
                        "created_at": 0,
                        "updated_at": 0,
                        "status": 0,
                        "__v":0,
                        "cat_product": 0
                    }
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    record:result
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message,
                    record:[]
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message,
                record:[]
            });
        }
    },
    getCategoryWithProductCount:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            await Catagorymodel.aggregate([
                {
                    $match:{
                        is_active: true
                    }
                },
                {
                    $lookup:{
                        from:"products",
                        let:{"cat_id":{"$toString":"$_id"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $in:["$$cat_id","$category_id"]
                                    }
                                }
                            },
                            {
                                $match:{
                                    $and:[
                                        {
                                            is_deleted: false
                                        },
                                        {
                                            is_active: true
                                        }
                                    ]
                                }
                            }
                        ],
                        as:"cat_product"
                    }
                },
                {
                    $addFields:{
                        "catProductCount":{
                            $size:"$cat_product"
                        }
                    }
                },
                {
                    $project:{
                        "is_featured": 0,
                        "is_active": 0,
                        "mastercatagory": 0,
                        "__v": 0,
                        "cat_product": 0
                    }
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    record:result
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message,
                    record:[]
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message,
                record:[]
            });
        }
    },
    getSizeWithProductCount:async(req,res)=>{
        try{
            await SizeModel.aggregate([
                {
                    $match:{
                        status:1
                    }
                },
                {
                    $lookup:{
                        from:"products",
                        let:{"size_name":"$name"},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $in:["$$size_name","$sales_info_variation_prices.size"]
                                    }
                                }
                            },
                            {
                                $match:{
                                    $and:[
                                        {
                                            is_deleted: false
                                        },
                                        {
                                            is_active: true
                                        }
                                    ]
                                }
                            }
                        ],
                        as:"cat_product"
                    }
                },
                {
                    $addFields:{
                        "catProductCount":{
                            $size:"$cat_product"
                        }
                    }
                },
                {
                    $project:{
                        "status": 0,
                        "created_at": 0,
                        "__v": 0,
                        "cat_product":0
                    }
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    record:result
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message,
                    record:[]
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message,
                record:[]
            });
        }
    },
    getStyleWithProductCount:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            await StyleModel.aggregate([
                {
                    $match:{
                        status: 1
                    }
                },
                {
                    $lookup:{
                        from:"products",
                        let:{"stl_id":{"$toString":"$_id"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $in:["$$stl_id","$style"]
                                    }
                                }
                            },
                            {
                                $match:{
                                    $and:[
                                        {
                                            is_deleted: false
                                        },
                                        {
                                            is_active: true
                                        }
                                    ]
                                }
                            }
                        ],
                        as:"cat_product"
                    }
                },
                {
                    $addFields:{
                        "catProductCount":{$size:"$cat_product"}
                    }
                },
                {
                    $project:{
                        status: 0,
                        created_at: 0,
                        __v: 0,
                        cat_product:0
                    }
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    record:result
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message,
                    record:[]
                });
            });
            
        }catch(e){
            return res.send({
                status:false,
                message:e.message,
                record:[]
            });
        }
    },
    getBrandWithProductCount:async(req,res)=>{
        try{
            await Brands.aggregate([
                {
                    $match:{
                        status: 1
                    }
                },
                {
                    $lookup:{
                        from:"products",
                        let:{"b_id":{"$toString":"$_id"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$brand_name","$$b_id"]
                                    }
                                }
                            },
                            {
                                $match:{
                                    $and:[
                                        {
                                            is_deleted: false
                                        },
                                        {
                                            is_active: true
                                        }
                                    ]
                                }
                            }
                        ],
                        as:"cat_product"
                    }
                },
                {
                    $addFields:{
                        "catProductCount":{$size:"$cat_product"}
                    }
                },
                {
                    $project:{
                        status: 0,
                        created_at: 0,
                        __v: 0,
                        cat_product:0
                    }
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    record:result
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message,
                    record:[]
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message,
                record:[]
            });
        }
    },
    getDateProductCount:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            let start_date = new Date();
            start_date.setHours(0,0,0,0);
            let end_date = new Date();
            end_date.setHours(23,59,59,999);
            //mongoose.set("debug",true);
            let today_product = await Product.count({
                is_deleted: false,
                is_active: true,
                created_at:{$gte:start_date,$lte:end_date}
            });

            
            let yesterday = new Date();
            yesterday.setDate(yesterday.getDate() - 1 );

            let yesterday_start_date = yesterday.setHours(0,0,0,0);
            let yesterday_end_date = yesterday.setHours(23,59,59,999);
 
            let yesterday_product = await Product.count({
                is_deleted: false,
                is_active: true,
                created_at:{$gte:yesterday_start_date,$lte:yesterday_end_date}
            });


            let curr = new Date; // get current date
            let first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
            let last = first + 6; // last day is the first day + 6

            let firstday = new Date(curr.setDate(first));
            let lastday = new Date(curr.setDate(last));
            firstday = firstday.setHours(0,0,0,0);
            lastday = lastday.setHours(0,0,0,0);

            //let week_start_date = firstday.setHours(0,0,0,0);
            //let week_end_date = lastday.setHours(23,59,59,999);
            //week_start_date = new Date(week_start_date);
            //week_end_date = new Date(week_end_date);

            let week_product = await Product.count({
                is_deleted: false,
                is_active: true,
                created_at:{$gte:firstday,$lte:lastday}
            });    
            return res.send({
                status:true,
                message:"Success",
                today_product:today_product,
                yesterday_product:yesterday_product,
                week_product:week_product
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message,
                record:[]
            });
        }
    },
    getSellerTimeSloatTryOn:async(req,res)=>{
        try{
            let seller_id = req.params.id;
            if(!seller_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du vendeur est requis"
                });
            }
            let all_rec = await SellerTimeSloats.aggregate([
                {
                    $match:{
                        seller_id:seller_id
                    }
                },
                {
                    $match:{
                        $and:[
                            {"delivery_type":3},
                            {"day_enable": true}
                        ]
                        
                    }
                },
                {
                    $group:{
                        _id:null,
                        day_number:{
                            $addToSet:"$day_number"
                        }
                    }
                }
            ])
            await SellerTimeSloats.aggregate([
                {
                    $match:{
                        seller_id:seller_id
                    }
                },
                {
                    $match:{
                        $and:[
                            {"delivery_type":3},
                            {"day_enable": true}
                        ]
                        
                    }
                },
                {
                    $project:{
                        "created_at": 0,
                        "updated_at": 0,
                        "__v": 0
                    }
                },
                {
                    $sort:{
                        day_number:1
                    }
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    all_rec:all_rec,
                    record:result
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message,
                    record:[]
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message,
                record:[]
            });
        }
    },
    getSellerTimeSloatDelivery:async(req,res)=>{
        try{
            let seller_id = req.params.id;
            if(!seller_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du vendeur est requis"
                });
            }
            let all_rec = await SellerTimeSloats.aggregate([
                {
                    $match:{
                        seller_id:seller_id
                    }
                },
                {
                    $match:{
                        $and:[
                            {"delivery_type":1},
                            {"day_enable": true}
                        ]
                        
                    }
                },
                {
                    $group:{
                        _id:null,
                        day_number:{
                            $addToSet:"$day_number"
                        }
                    }
                }
            ])

            await SellerTimeSloats.aggregate([
                {
                    $match:{
                        seller_id:seller_id
                    }
                },
                {
                    $match:{
                        $and:[
                            {
                                day_enable: true,
                                delivery_type:1
                            }
                        ]
                    }
                },
                {
                    $project:{
                        "created_at": 0,
                        "updated_at": 0,
                        "__v": 0
                    }
                },
                {
                    $sort:{
                        day_number:1
                    }
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    all_rec:all_rec,
                    record:result
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    getSellerTimeSloatPickup:async(req,res)=>{
        try{
            let seller_id = req.params.id;
            if(!seller_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du vendeur est requis"
                });
            }
            let all_rec = await SellerTimeSloats.aggregate([
                {
                    $match:{
                        seller_id:seller_id
                    }
                },
                {
                    $match:{
                        $and:[
                            {"delivery_type":2},
                            {"day_enable": true}
                        ]
                        
                    }
                },
                {
                    $group:{
                        _id:null,
                        day_number:{
                            $addToSet:"$day_number"
                        }
                    }
                }
            ])
            await SellerTimeSloats.aggregate([
                {
                    $match:{
                        seller_id:seller_id
                    }
                },
                {
                    $match:{
                        $and:[
                            {
                                day_enable: true,
                                delivery_type:2
                            }
                        ]
                    }
                },
                {
                    $project:{
                        "created_at": 0,
                        "updated_at": 0,
                        "__v": 0
                    }
                },
                {
                    $sort:{
                        day_number:1
                    }
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    all_rec:all_rec,
                    record:result
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    getAllSellerShopUserSide:async(req,res)=>{
        try{
            let id = req.params.id;
            let record = await SellerBranchesModel.find({seller_id:id});            
            if(record.length == 0)
            {
                return res.send({
                    status:false,
                    message:"Aucun magasin n'est disponible pour ce vendeur"
                });
            }else{
                return res.send({
                    status:true,
                    message:"Success",
                    record:record
                });
            }
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    bookTryOnProgram:async(req,res)=>{
        try{
            //console.log(req.body);return false;
            let{seller_id,date,time,shop_id,user_id,product_id} = req.body;
            if(!seller_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du vendeur est requise"
                });
            }
            if(!date)
            {
                return res.send({
                    status:false,
                    message:"La date est requise"
                });
            }
            if(!time)
            {
                return res.send({
                    status:false,
                    message:"L'heure est requise"
                });
            }
            if(!shop_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de la boutique est requise"
                });
            }
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'utilisateur est requise"
                });
            }
            if(!product_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du produit est requise"
                });
            }
            let split_time = time.split("-");
            console.log("split_time ", split_time);
            let first_time = 0;
            let second_time = 0;
            if(split_time.length > 1)
            {
                first_time = split_time[0];
                second_time = split_time[1];
            }
            first_time = first_time.substring(0, 2);
            second_time = second_time.substring(0, 2);
            console.log("first_time ", first_time);
            console.log("second_time ", second_time);
            console.log("date ", date);

            const d = new Date(date);
            let day = d.getDay();
            console.log("day ", day);
            //mongoose.set("debug",true);
            let day_record = await SellerTimeSloats.findOne({
                seller_id:seller_id,
                day_enable:true,
                delivery_type:3,
                day_number:day,
                start_time:{$lte:first_time},
                end_time:{$gte:second_time}
            });
            if(day_record)
            {
                let json_var = {
                    seller_id:seller_id,
                    product_id:product_id,
                    date:date,
                    time:time,
                    shop_id:shop_id,
                    user_id:user_id,
                    status:1
                }
                //console.log("json_var ", json_var);return false;
                let check = await UserTryOnProgramModel.count(json_var);
                if(check)
                {
                    return res.send({
                        status:false,
                        message:"Vous êtes tous prêts à réserver ce magasin pour cette date et cette heure"
                    });
                }
                //console.log("json_var ", json_var);return false;
                UserTryOnProgramModel.create(json_var).then((result)=>{
                    return res.send({
                        status:true,
                        message:"Votre essai de réservation sur la boutique a été entièrement réussi"
                    });
                }).catch((e)=>{
                    return res.send({
                        status:false,
                        message:e.message
                    });
                });
            }else{
                return res.send({
                    status:false,
                    message:"Le magasin n'est pas disponible pour cette date et cette heure"
                });
            }            
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    getAllBookedTryOnProgram:async(req,res)=>{
        try{
            let id = req.params.id;
            UserTryOnProgramModel.aggregate([
                {
                    $match:{
                        user_id:id
                    }
                    
                },
                {
                    $addFields:{
                        "shp_id":{"$toObjectId":"$shop_id"}
                    }
                },
                {
                    $lookup:{
                        from:"products",
                        let:{"p_id":{"$toObjectId":"$product_id"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$_id","$$p_id"]
                                    }
                                }
                            },
                            {
                                $project:{
                                    "condition": 1,
                                    "brand": 1,
                                    "price": 1,
                                    "images": 1,
                                    "dangerous_goods": 1,
                                    "is_active": 1,
                                    "view_count": 1,
                                    "created_at": 1,
                                    "sku": 1,
                                    "stock": 1,
                                    "title": 1,
                                    "description": 1
                                }
                            }
                        ],
                        as:"product_record"
                    }
                },
                {
                    $lookup:{
                        from:"sellers",
                        let:{"slr_id":{"$toObjectId":"$seller_id"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$_id","$$slr_id"]
                                    }
                                }
                            },
                            {
                                $project:{
                                    "companyname":1,
                                    "vendortype":1,
                                    "fullname":1,
                                    "shopname":1,
                                    "shopdesc":1,
                                    "shopphoto":1,
                                    "phone":1 
                                }
                            }
                           
                        ],
                        as:"slr_record"
                    }
                },
                {
                    $sort:{
                        created_at:-1
                    }
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    record:result
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    getSingleBookedTryOnProgram:async(req,res)=>{
        try{
            let id = req.params.id;
            //mongoose.set("debug",true);
            UserTryOnProgramModel.aggregate([
                {
                    $match:{
                        _id:mongoose.Types.ObjectId(id)
                    }
                },
                {
                    $addFields:{
                        "shp_id":{"$toString":"$shop_id"}
                    }
                },
                {
                    $lookup:{
                        from:"products",
                        let:{"p_id":{"$toObjectId":"$product_id"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$_id","$$p_id"]
                                    }
                                }
                            },
                            {
                                $lookup:{
                                    from:"brands",
                                    let:{"brand_id":{"$toObjectId":"$brand_name"}},
                                    pipeline:[
                                        {
                                            $match:{
                                                $expr:{
                                                    $eq:["$_id","$$brand_id"]
                                                }
                                            }
                                        },
                                        {
                                            $project:{
                                                "name":1
                                            }
                                        }
                                    ],
                                    as:"brand_rec"
                                }
                            },
                            {
                                $addFields:{
                                    "style_id":{
                                        $arrayElemAt:["$style",0]
                                    }
                                }
                            },
                            {
                                $lookup:{
                                    from:"styles",
                                    let:{"stl_ID":{"$toObjectId":"$style_id"}},
                                    pipeline:[
                                        {
                                            $match:{
                                                $expr:{
                                                    $eq:["$_id","$$stl_ID"]
                                                }
                                            }
                                        },
                                        {
                                            $project:{
                                                "name":1
                                            }
                                        }
                                    ],
                                    as:"style_rec"
                                }
                            }
                            // {
                            //     $project:{
                            //         "condition": 1,
                            //         "brand": 1,
                            //         "price": 1,
                            //         "images": 1,
                            //         "dangerous_goods": 1,
                            //         "is_active": 1,
                            //         "view_count": 1,
                            //         "created_at": 1,
                            //         "sku": 1,
                            //         "stock": 1,
                            //         "title": 1,
                            //         "description": 1
                            //     }
                            // }
                        ],
                        as:"product_record"
                    }
                },
                {
                    $lookup:{
                        from:"sellers",
                        let:{"slr_id":{"$toObjectId":"$seller_id"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$_id","$$slr_id"]
                                    }
                                }
                            },
                            {
                                $project:{
                                    "companyname":1,
                                    "vendortype":1,
                                    "fullname":1,
                                    "shopname":1,
                                    "shopdesc":1,
                                    "shopphoto":1,
                                    "phone":1 
                                }
                            }
                           
                        ],
                        as:"slr_record"
                    }
                },
                {
                    $sort:{
                        created_at:-1
                    }
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    record:result
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    markUserCancledTryOnProgram:async(req,res)=>{
        try{
            let id = req.params.id;
            let record = await UserTryOnProgramModel.findOne({_id:id});
            if(!record)
            {
                return res.send({
                    status:false,
                    message:"Identifiant invalide"
                });
            }else{
                if(record.status == 1)
                {
                    await UserTryOnProgramModel.updateOne({
                        _id:id
                    },{
                        status:3
                    }).then((result)=>{
                        return res.send({
                            status:true,
                            message:"Ce programme a été entièrement réalisé avec succès"
                        });
                    }).catch((e)=>{
                        return res.send({
                            status:false,
                            message:e.message
                        });
                    });
                }else if(record.status == 2){
                    return res.send({
                        status:false,
                        message:"Ce programme est déjà terminé"
                    });
                }else if(record.status == 3){
                    return res.send({
                        status:false,
                        message:"Ce programme est tout à fait prêt de vous"
                    });
                }else if(record.status == 4){
                    return res.send({
                        status:false,
                        message:"Ce programme est déjà disponible auprès du vendeur"
                    });
                }
            }
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    markSellerUpdateTryOnProgram:async(req,res)=>{
        try{
            let id = req.params.id;
            let status = req.params.status;
            let record = await UserTryOnProgramModel.findOne({_id:id});
            if(!record)
            {
                return res.send({
                    status:false,
                    message:"Identifiant invalide"
                });
            }else{
                if(record.status == 1)
                {
                    await UserTryOnProgramModel.updateOne({
                        _id:id
                    },{
                        status:status
                    }).then((result)=>{
                        return res.send({
                            status:true,
                            message:"Ce programme a été entièrement réalisé avec succès"
                        });
                    }).catch((e)=>{
                        return res.send({
                            status:false,
                            message:e.message
                        });
                    });
                }else if(record.status == 2){
                    return res.send({
                        status:false,
                        message:"Ce programme est déjà terminé"
                    });
                }else if(record.status == 3){
                    return res.send({
                        status:false,
                        message:"Ce programme est tout à fait prêt de vous"
                    });
                }else if(record.status == 4){
                    return res.send({
                        status:false,
                        message:"Ce programme est déjà disponible auprès du vendeur"
                    });
                }
            }
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    getNearByShopBySellerId:async(req,res)=>{
        try{
            let {seller_id,address,latitude,longitude,product_id} = req.body;
            if(!seller_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du vendeur est requise"
                });
            }
            if(!product_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant du vendeur est requise"
                });
            }
            if(!latitude || !longitude)
            {
                return res.send({
                    status:false,
                    message:"L'adresse doit être celle de Google"
                });
            }
            latitude = parseFloat(latitude);
            longitude = parseFloat(longitude);
            //mongoose.set("debug",true);
            await SellerBranchesModel.aggregate([
                {
                    $geoNear:{
                        near:{
                            type:"Point",
                            coordinates:[latitude,longitude]
                        },
                        distanceMultiplier: 0.001,
                        distanceField: "dist",
                        query:{seller_id:seller_id},
                        spherical:true,
                    }
                },
                {
                    $lookup:{
                        from:"products",
                        let:{"cat_id":{"$toString":"$_id"},"p_id":{"$toObjectId":product_id} },
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $in:["$$cat_id","$branches"]
                                    }
                                }
                            },
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$_id","$$p_id"]
                                    }
                                }
                            }
                        ],
                        as:"product_rec"
                    }
                },
                {
                    $match:{
                        "product_rec":{$ne:[]}
                    }
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    record:result
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message
                });
            })
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    forApplyUpdate:async(req,res)=>{
        try{
            let result = await Product.find({});
            let record = result.filter(async(val)=>{
                if(val.branches.length > 1)
                {
                    console.log("ifffffff");
                }else{
                    await Product.updateOne({_id:val._id},{branches:[]});
                }
            })
            return res.send({
                status:true,
                message:"Success",
                record:record
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    deleteOldProduct:async(req,res)=>{
        try{
            let record = await Product.aggregate([
                {
                    $match:{
                        sales_info_variation_prices:{$ne:[]}
                    }
                    
                },
                {
                    $skip:0
                },
                {
                    $limit:30
                }
            ]);
            // for(let x=0; x<record.length; x++)
            // {
            //     let new_arr = [];
            //     for(let y=0; y<record[x]["sales_info_variation_prices"].length; y++)
            //     {
            //         let new_obj = {
            //             product_id: record[x]["sales_info_variation_prices"][y].product_id,
            //             color: record[x]["sales_info_variation_prices"][y].color,
            //             sku: record[x]["sales_info_variation_prices"][y].sku,
            //             size: record[x]["sales_info_variation_prices"][y].size,
            //             price: record[x]["sales_info_variation_prices"][y].price,
            //             stock: record[x]["sales_info_variation_prices"][y].stock,
            //             image: record[x]["sales_info_variation_prices"][y].image
                        
            //         }
            //         new_arr.push(new_obj);
            //         console.log(record[x]["sales_info_variation_prices"][y]);
            //     }
            //     await Product.updateOne({_id:record[x]._id},{
            //         sales_info_variation_prices:new_arr
            //     })
            // }
            // record.map(async(val)=>{
            //     console.log(val._id);
            //     //await Product.deleteOne({_id:val._id})
            // })
            return res.send({
                status:true,
                message:"Success",
                record:record
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    getProductDetailForChat:async(req,res)=>{
        try{
            console.log(req.body);
            let {product_id,user_id} = req.body;
            await Product.aggregate([
                {
                    $match:{
                        _id:mongoose.Types.ObjectId(product_id)
                    }
                },
                {
                    $lookup:{
                        from:"sellers",
                        let:{"slr_id":{"$toObjectId":"$provider_id"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$_id","$$slr_id"]
                                    }
                                }
                            }
                        ],
                        as:"slr_rec"
                    }
                },
                {
                    $project:{
                        "title": 1,
                        "provider_id": 1,
                        "images": 1,
                        "price": 1, 
                        "slr_rec.fullname": 1,
                        "slr_rec.companyname":1,
                    }
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    record:result
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    }








































    








}