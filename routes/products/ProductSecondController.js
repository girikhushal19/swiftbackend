const { Product } = require("../../models/products/Product");
const  ProductModel  = require("../../models/products/Product");
const ProductstempimagesModel = require("../../models/products/ProductstempimagesModel");
const MatCatagory=require("../../models/products/MatCatagory");
const Variations =require("../../models/products/Variations");
const VariationsPrice =require("../../models/products/VariationPrice");
const pageViewsModel=require("../../models/products/pageViews");
const mongoose=require("mongoose");
const catagorymodel = require("../../models/products/Catagory");
const ProductfavoriteModel = require("../../models/products/ProductfavoriteModel");
const Subscriptions=require("../../models/subscriptions/Subscription");
const ShippingModel = require("../../models/shipping/Shipping");
const GlobalSettings=require("../../models/admin/GlobalSettings");
const SellerModel=require("../../models/seller/Seller");
const Brands = require("../../models/products/Brands");
const StyleModel = require("../../models/products/StyleModel");
const ColorModel = require("../../models/admin/Color");
const SizeModel = require("../../models/products/SizeModel");
const bannerImageModel=require("../../models/admin/Banners");

const fs = require("fs");

module.exports = {
  addProduct:async(req,res)=>{
    try{
      //photo video
      let {seller_id,title,description,category_id,condition,sku,enable_variation,price,stock,spec_attributes,brand,style} = req.body;
      category_id = JSON.parse(category_id);
      //console.log("category_id ", category_id);
      //console.log("category_id ", category_id[0]);
      //return false;
      if(style)
      {
        try{
          style = JSON.parse(style);
        }catch(ee){
          style = JSON.parse(style);
        }
      }
      if(spec_attributes)
      {
        try{
          spec_attributes = JSON.parse(spec_attributes);
        }catch(ee){
          spec_attributes = JSON.parse(spec_attributes);
        }
      }
      console.log("spec_attributes ", spec_attributes);
      if(!seller_id)
      {
        return res.send({
          status:false,
          message:"L'identifiant du vendeur est requis"
        });
      }
      if(!title)
      {
        return res.send({
          status:false,
          message:"Titre est requis"
        });
      }
      if(!price)
      {
        return res.send({
          status:false,
          message:"Le prix est requis"
        });
      }
      if(!stock)
      {
        return res.send({
          status:false,
          message:"Stock est requis"
        });
      }
      if(!category_id)
      {
        return res.send({
          status:false,
          message:"Catégorie est requis"
        });
      }
      let catagory = "";
      if(category_id)
      {
        if(category_id.length > 0)
        {
          let aa = category_id.length -1;
          //console.log(category_id[aa]);
          let cat_record = await catagorymodel.findOne({_id:category_id[aa]},{title:1});
          //console.log("cat_record ",cat_record);
          //return false;
          if(cat_record)
          {
            catagory = cat_record.title;
          }
        }
      }
      let images = [];
      if(req.files)
      {
        if(req.files?.photo)
        {
          req.files.photo.map((val)=>{
            let filename = process.env.PHOTOS_PATH_PERFIX+"/" + val.filename;
            images.push(filename);
          })
          
        }
      }
      let video = "";
      if(req.files)
      {
        if(req.files?.video)
        {
          video = process.env.PHOTOS_PATH_PERFIX+"/" + req.files.video[0].filename;
        }
      }
      if(enable_variation == 'true')
      {
        enable_variation = true;
      }else{
        enable_variation = false;
      }
      if(condition == 'true')
      {
        condition = true;
      }else{
        condition = false;
      }
      //{seller_id,title,description,category_id,condition,sku,enable_variation,price,stock,spec_attributes}
      // let create_data = {
      //   ...(seller_id && {provider_id:seller_id}),
      //   ...(title && {title:title}),
      //   ...(description && {description:description}),
      //   ...(category_id && {category_id:category_id}),
      //   ...(catagory && {catagory:catagory}),
      //   ...(condition && {condition:condition}),
      //   ...(sku && {sku:sku}),
      //   ...(enable_variation && {enable_variation:enable_variation}),
      //   ...(price && {price:price}),
      //   ...(stock && {stock:stock}),
      //   ...(spec_attributes && {spec_attributes:spec_attributes}),
      //   ...(images && {images:images}),
      //   ...(video && {video:video})
      // };
      //console.log("create_data ", create_data);
      const product = new Product();
      product.provider_id = seller_id;
      product.title = title;
      product.description = description;
      product.category_id = category_id;
      product.catagory = catagory;
      product.condition = condition;
      product.sku = sku;
      product.enable_variation = enable_variation;
      product.price = price;
      product.stock = stock;
      product.spec_attributes = spec_attributes;
      product.images = images;
      product.video = video;
      product.brand = brand;
      product.style = style;
      product.save().then((result)=>{
        return res.send({
          status:true,
          message:"L'enregistrement du produit a été ajouté avec succès."
        });
      }).catch((e)=>{
        return res.send({
          status:false,
          message:e.message
        });
      });
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      });
    }
  },
  sellerProductCount:async(req,res)=>
  {
    try{
      let id = req.params.id;
      if(!id)
      {
        return res.send({
          status:false,
          message:"L'identifiant est requis"
        });
      }
      let total_record = await Product.count({provider_id:id,is_deleted:false});
      //console.log("total_record ", total_record);
      let page_nu = total_record / 10;
      page_nu = Math.ceil(page_nu);
      return res.send({
        status:true,
        message:"Succès",
        total_page:page_nu
      })
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      });
    }
  },
  sellerProduct:async(req,res)=>
  {
    try{
      let {id,page_nu} = req.body;
      if(!id)
      {
        return res.send({
          status:false,
          message:"L'identifiant est requis"
        });
      }
      page_nu = page_nu ? page_nu : 0;
      page_nu = parseFloat(page_nu);
      let skip_rec = page_nu * 10;
      //mongoose.set("debug",true);
      await Product.aggregate([
        {
          $match:{
            provider_id:id
          }
        },
        {
          $skip:skip_rec
        },
        {
          $limit:10
        }
      ]).then((result)=>{
        return res.send({
          status:true,
          message:"Succès",
          record:result
        });
      }).catch((e)=>{
        return res.send({
          status:false,
          message:e.message
        });
      });
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      });
    }
  },
  editProduct: async (req, res) => {
    try{ 
    let {
        sku,
        title,
        stock,
        description,
        price,
        weight,
        id,
        parent_sku,
        catagory,
        provider_id,
        random_product_id,
        dangerous_goods,
        condition,brand,style,category_id,spec_attributes
    } = req.body;
    if(style)
    {
      try{
        style = JSON.parse(style);
      }catch(ee){
        style = JSON.parse(style);
      }
    }
    if(spec_attributes)
    {
      try{
        spec_attributes = JSON.parse(spec_attributes);
      }catch(ee){
        spec_attributes = JSON.parse(spec_attributes);
      }
    }
    console.log("spec_attributes ", spec_attributes);
    if(!id)
    {
      return res.send({
        status:false,
        message:"L'identifiant est requis"
      });
    }
     
     
    var enable_variation = req.body.enable_variation;
     
    if(enable_variation == "false")
    {
        enable_variation = false;
    }else{
        enable_variation = true;
    }
    
    let products_record = await Product.findById(id,{images:1,video:1});
    //return false;
    //console.log(seller_will_bear_shipping ," seller_will_bear_shipping");return false;
    if(category_id)
    {
        category_id = JSON.parse(category_id);
        //console.log(category_id);

        let valuesArray = Object.values(category_id);
        
       
        // console.log(valuesArray);
        category_id = valuesArray;
        //console.log("category_id");
        
        //console.log(category_id);
        if(category_id.length > 0)
        {
            let aa = category_id.length -1;
            
            //console.log(category_id[aa]);
            let cat_record = await catagorymodel.findOne({_id:category_id[aa]},{title:1});
            //console.log("cat_record ",cat_record);
            //return false;
            if(cat_record)
            {
                catagory = cat_record.title;
            }
        }
        //return false;
    }else{
        category_id = [];
    }
    let images = [];
    let video;
    var newCat_id = [];
    if(random_product_id)
    {
        var productRec = await ProductstempimagesModel.findOne({random_product_id});
        if(productRec)
        {
            //console.log(productRec);
            images = productRec.images;
        }
        
    }else{
        if (req.files?.photo)
        {
            if(products_record)
            {
                if(products_record.images.length > 0)
                {
                    products_record.images.map((val)=>{
                        images.push(val);
                    });
                }
            }
            
            //images.push(products_record.images);
            req.files.photo?.map((image) => {
               // console.log("photo",image)
                let filename = process.env.PHOTOS_PATH_PERFIX+"/" + image.filename;
                images.push(filename);
            })
        }
    }
    
    if (req.files?.video) {
        //console.log("in req.files.video")
        req.files.video?.map((e) => {
          //  console.log("in req.files.video video",e)
            let filename =  process.env.PHOTOS_PATH_PERFIX+"/" + e.filename;
            video=filename;
        });

        if(products_record)
        {
            if(products_record.video)
            {
                var uploadDir = './views/admin-panel/public';
                let fileNameWithPath = uploadDir + products_record.video;
                //console.log(fileNameWithPath);
                
                if (fs.existsSync(fileNameWithPath))
                {
                    fs.unlink(fileNameWithPath, (err) => 
                    {
                        //console.log("unlink file error "+err);
                    });
                }
            }
        }

    }
    // console.log("images ",images);
    // return false;
    // console.log("req.files,video",video)
    // const attributes=req.body.attributes?JSON.parse(req.body.attributes):[]
    let attributes;
    let dispatch_info;
     
    let size;
    let other_details;
    
    try{
        attributes=JSON.parse(req.body.attributes);
    }catch{
        attributes=req.body.attributes;
    }
    try{
        dispatch_info=JSON.parse(req.body.dispatch_info);
    }catch{
        dispatch_info=req.body.dispatch_info;
    }
 
    try{
        size=JSON.parse(req.body.size);
    }catch{
        size=req.body.size;
    }
    try{
        other_details=JSON.parse(req.body.other_details);
    }catch{
        other_details=req.body.other_details;
    }
     
    
    if(condition)
    {
        if(condition == 'new')
        {
            condition = true
        }else{
            condition = false;
        }
    }
      
    const datatoupdate={
        ...(condition && { condition: condition }),
        ...(dangerous_goods && { dangerous_goods: dangerous_goods }),
        ...(sku && { sku: sku }),
        ...(stock && { stock: stock }),
        ...(provider_id && { provider_id: provider_id }),
        ...(parent_sku&&{parent_sku:parent_sku}),
        ...(title && { title: title }),
        ...(description && { description: description }),
        ...(price && { price: price }),
        ...(brand && { brand: brand }),
        ...(style && { style: style }),
        ...(catagory && { catagory: catagory }),
        ...(category_id && { category_id: category_id }),
        ...(images.length && { images: images }),
        ...(video && { video: video }),
        ...(enable_variation?.toString() && { enable_variation: enable_variation }),
        //...(seller_will_bear_shipping?.toString() && { seller_will_bear_shipping: seller_will_bear_shipping}),
        ...(spec_attributes && { spec_attributes: spec_attributes }),
        //...(attributes && { spec_attributes: attributes }),
        
        // ...(sales_info?.length && { sales_info: variations }),
        // ...(sales_info_variation_prices.length && { sales_info_variation_prices: sales_info_variation_prices }),
        //...(dispatch_info && { dispatch_info: dispatch_info }),
        //...(other_details && { other_details: other_details }),
    };
    // console.log("req.body",size,weight,datatoupdate);
    // return false;
      await Product.findByIdAndUpdate(id, datatoupdate).then((result) => {
          //console.log("result",result)
          return res.send({
              status: true,
              data:result,
              message: "Produit mis à jour avec succès"
          })
      }).catch((e)=>{
        return res.send({
          status:false,
          message:e.message
        });
      })
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      });
    }
  },
  getProductBrand:async(req,res)=>{
    try{
      //let allBrand = new Brands;
      let data = await Brands.find({status:1},{name:1});
      return res.send({
        status:true,
        message:"Success",
        data:data
      })
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      })
    }
  },
  getProductStyle:async(req,res)=>{
    try{
      let data = await StyleModel.find({status:1},{name:1});
      return res.send({
        status:true,
        message:"Success",
        data:data
      });
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      })
    }
  },
  getAllProductColor:async(req,res)=>{
    try{
      let data = await ColorModel.find({status:1},{color_name:1,color_code:1});
      return res.send({
        status:true,
        message:"Success",
        data:data
      });
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      })
    }
  },
  getAllProductSize:async(req,res)=>{
    try{
      let data = await SizeModel.find({status:1},{name:1});
      return res.send({
        status:true,
        message:"Success",
        data:data
      });
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      })
    }
  },
  getHomePageBannerImg:async(req,res)=>{
    try{
      let data = await bannerImageModel.findOne({});
      return res.send({
        status:true,
        message:"Success",
        data:data
      });
    }catch(e){
      return res.send({
        status:false,
        message:e.message
      })
    }
  }
};