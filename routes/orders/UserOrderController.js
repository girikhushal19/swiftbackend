const orders = require("../../models/orders/order");
const ordersModel = require("../../models/orders/order");
const Ratingnreviews = require("../../models/actions/Ratingnreviews");
const User= require("../../models/user/User");
const seller=require("../../models/seller/Seller");
const mongoose=require("mongoose");
const VariationsPrice =require("../../models/products/VariationPrice");
const {calculateShippingcostforproducts} =require("../../routes/shipping/ShippingController");
const sellerModel=require("../../models/seller/Seller");
const AdminModel = require("../../models/admin/AdminModel");
const Notifications = require("../../models/Notifications");
const path = require("path");

const sales=require("../../models/sales/Sales");
const sellers=require("../../models/seller/Seller");
const pageViewsModel=require("../../models/products/pageViews");
const {Product}=require("../../models/products/Product");
const usersmodel= require("../../models/user/User");
const StripeModel = require("../../models/admin/StripeModel");
const base_url = process.env.BASE_URL + "/admin-panel/";
const root_url = process.env.BASE_URL;
const {getdateinadvance,getDays,getDate} =require("../../modules/dates.js")
//const moment=require("moment")
const customConstant = require('../../helpers/customConstant');
const pdf = require('html-pdf');
const ejs = require("ejs"); 
const fs = require('fs');
const nodemailer = require("nodemailer");
const BASE_URL = process.env.BASE_URL;


const transport = nodemailer.createTransport({
    name: "Swift",
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    auth: {
      user: process.env.MAILER_EMAIL_ID,
      pass: process.env.MAILER_PASSWORD,
    }
});


module.exports = {
    
    OrderPayment:async(req,res)=>{
        try{
            let id = req.params.id;
            let order_record = await ordersModel.findOne({_id:id});
            // console.log("order_record ");
            // console.log(order_record);
            if(!order_record)
            {
                return res.send({
                    status:false,
                    message:"ID de commande invalide"
                });
            }else{
                let amount = order_record.totalPrice;
                let final_amount = amount * 100;
                final_amount = final_amount.toFixed(2);
                final_amount = parseFloat(final_amount);
                console.log("final_amount ", final_amount);
                //return false;
                let stripe_record = await StripeModel.findOne({});
                //console.log("stripe_record ", stripe_record);
                //return false;
                //
                const stripe = require('stripe')(stripe_record.stripe_secret);
                // let paymentIntent = await stripe.paymentIntents.create({
                //     amount: amount * 100,
                //     currency: 'eur',
                //     //payment_method: '{{PAYMENT_METHOD_ID}}',
                //     description: 'Swift',
                //     confirm: true,
                //     capture_method: 'manual',
                //     return_url:BASE_URL+"/api/order/orderPaymentSuccess"
                //   });

                //console.log("amount ", amount );
                //return false;
                const session = await stripe.checkout.sessions.create({
                    line_items: [{
                      //name: 'Swift',
                      //description: 'Swift',
                      //images: ['https://example.com/t-shirt.png'],
                      //amount: amount * 100,
                      //currency: 'eur',
                      price_data: {
                        currency: 'eur',
                        unit_amount: final_amount,
                        product_data: {
                          name: 'Swift',
                          description: 'Swift',
                          //images: ['https://example.com/t-shirt.png'],
                        },
                      },
                      quantity: 1,
                    }],
                    mode: 'payment',
                    success_url: BASE_URL+"/api/order/orderPaymentSuccess?session_id={CHECKOUT_SESSION_ID}",
                    cancel_url: BASE_URL+"/api/order/orderPaymentCancel?session_id={CHECKOUT_SESSION_ID}",
                  });
                   
                // console.log(session);
                // return false;

                order_record.payment_status = false;
                order_record.transaction_id = session.id;
                order_record.payment_intent = session.payment_intent
                order_record.date_of_transaction = new Date();
                order_record.payment_method = "Stripe";
                order_record.status = 0;
                order_record.save();
                res.redirect(303, session.url);
            }
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    orderPaymentSuccess:async(req,res)=>{
        try{
            let session_id = req.query.session_id;
            //console.log(session_id);
            let order_record = await ordersModel.findOne({transaction_id:session_id});
            // console.log("order_record ");
            // console.log(order_record);
            if(!order_record)
            {
                return res.send({
                    status:false,
                    message:"ID de commande invalide"
                });
            }else{
                //console.log(order_record);
                
                let payment_submit_web_type = order_record.payment_submit_web_type;

                // console.log("payment_submit_web_type ", payment_submit_web_type);
                // return false;
                for(let i=0; i<order_record.products.length; i++)
                {
                    
                    let product = order_record.products[i].product;
                    let quantity = order_record.products[i].quantity;
                    let variation_id =  order_record.products[i].variation_id;
                    // console.log("quantity ", quantity);
                    // console.log("variation_id ", variation_id);
                    // console.log("product ", product);
                    let new_poduct = await Product.findOne({_id:product._id});
                    
                    if(variation_id)
                    {
                        if(new_poduct)
                        {
                            let new_filter = new_poduct.sales_info_variation_prices.filter((val)=>{
                                if(val._id == variation_id)
                                {
                                    val.stock = val.stock - quantity;
                                }
                                return val;
                            })
                            //console.log("new_filter ", new_filter);
                            new_poduct.sales_info_variation_prices = new_filter;
                            new_poduct.save();
                        }
                    }else{
                        if(new_poduct)
                        {
                            let remaining_q = new_poduct.stock - quantity;
                            new_poduct.stock = remaining_q;
                            new_poduct.save();
                        }
                    }
                }
                let user_id = order_record.userId ;
                await User.updateOne({_id:user_id},{cart:[]});
                //return false;
                order_record.payment_status = true;
                order_record.date_of_transaction = new Date();
                //order_record.status = 1;
                order_record.save();

                var base_url_server = customConstant.base_url;
                var imagUrl = base_url_server+"public/uploads/logo.png";
                let order_array = []; let seller_array = [];
                order_array.push(order_record);
                let userId = order_array[0].userId;
               let result_user = await User.findOne({_id:userId},{first_name:1,last_name:1,email:1, country_code:1, phone:1 } );

               let provider_id = order_array[0].products[0].provider_id;
                let result_seller = await sellerModel.findOne({_id:provider_id},{fullname:1,shopname:1,email:1, countrycode:1, phone:1,pickupaddress:1} );
                seller_array.push(result_seller);

                let admin_record = await AdminModel.findOne({});
                let part_id = order_record.products[0].part_id;
                let message = "Nouvelle commande passée par un utilisateur dont l'ID de commande est - "+part_id;
                let data_save = {
                    to_id:provider_id,
                    user_type:"seller",
                    title:"Nouvelle commande",
                    message:message,
                    status: 'unread',
                    notification_type:"new_order"
                }
                await Notifications.create(data_save);
                if(admin_record)
                {
                    let admin_id = admin_record._id.toString();
                    let data_save_1 = {
                        to_id:admin_id,
                        user_type:"admin",
                        title:"Nouvelle commande",
                        message:message,
                        status: 'unread',
                        notification_type:"new_order"
                    }
                    await Notifications.create(data_save_1);

                }



               //console.log("result_seller ", result_seller);
               // const frenchdate=frenchDayDate(order_array[0].date_of_transaction);
               const today = new Date(order_array[0].date_of_transaction);
               const yyyy = today.getFullYear();
               let mm = today.getMonth() + 1; // Months start at 0!
               let dd = today.getDate();
               if (dd < 10) dd = '0' + dd;
               if (mm < 10) mm = '0' + mm;
               let formattedToday = dd + '.' + mm + '.' + yyyy;
               let hourss = today.getHours();
               let minutess = today.getMinutes();
               // console.log("hourss ",hourss);
               // console.log("minutess ",minutess);
               formattedToday = formattedToday +" "+hourss+"h"+minutess;
               // // console.log("formattedToday ",formattedToday);
               // // return false;
               // invoice=JSON.parse(JSON.stringify(invoice));
               order_array[0]["frenchdate"]=formattedToday;
               //console.log(order_array);return false;
               const filepath=path.join(__dirname, "../../views/admin-panel/paymentinvoice_3.ejs");
               //console.log(filepath)
               let less_val = order_array.length - 1;
               const html = await ejs.renderFile(filepath,{order_array:order_array,result_user:result_user,seller_array:seller_array,imagUrl:imagUrl,less_val:less_val });
               let subject = "Succès de la commande";
               var mainOptions = {
                   from:process.env.MAILER_FROM_EMAIL_ID ,
                   to: result_user.email,
                   subject:subject ,
                   html: html
               };
               // console.log("html data ======================>", mainOptions);
               console.log("hereeeeeeee 746");

               //smtpTransport.sendMail(mainOptions, function (err, info) {
               transport.sendMail(mainOptions, function(err, info) {
                   if (err)
                    {
                        //payment_submit_web_type
                        if(payment_submit_web_type == 1)
                        {
                            let urll = customConstant.base_url_web_site+"paymentSuccess";
                            res.redirect(303, urll);
                        }
                        

                       console.log(err);
                       //return true
                       return res.send({
                        status:true,
                        message:"Succès de paiement intégral"
                    });
                   } else {
                        if(payment_submit_web_type == 1)
                        {
                            let urll = customConstant.base_url_web_site+"paymentSuccess";
                            res.redirect(303, urll);
                        }

                       console.log('Message sent: ' + info);
                       //return true
                       return res.send({
                        status:true,
                        message:"Succès de paiement intégral"
                    });
                   }
               });

                
            }
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    orderPaymentCancel:async(req,res)=>{
        try{
            return res.send({
                status:true,
                message:"Paiement annulé"
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    getOrderMonthly:async(req,res)=>{
        try{
            let record = await ordersModel.aggregate([
                {
                    $group:{
                        _id:{$month:"$date_of_transaction"},
                        "totalPayment":{
                            $sum:"$payment_amount"
                        },
                        "totalRecord":{$count:{} },
                        "monthlyAvgAmount":{$avg: "$payment_amount"}
                        
                    }
                }
            ]);
            return res.send({
                status:true,
                message:"Succès",
                record:record
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    }
}