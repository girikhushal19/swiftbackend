const abuseReportModel=require("../../models/admin/AbuseReports");
const reasonModel=require("../../models/admin/Reason");
const SellerModel=require("../../models/seller/Seller");
const { Product } = require("../../models/products/Product");
const User = require("../../models/user/User");

module.exports={
    addOrupdate:async(req,res)=>{
        //console.log(req.body);
        var {
            reason,
            product,
            productId,
            vendor,
            vendorId,
            content,
            reportedbyName,
            reportedbyId,
            id
        }=req.body;
        if(!reportedbyId)
        {
            return res.send({
                status:false,
                message:"L'identifiant de l'utilisateur est requis"
            })
        }
        if(!reason)
        {
            return res.send({
                status:false,
                message:"Le motif est requis"
            })
        }
        let product_rec= await Product.findOne({ _id:productId},{provider_id:1,title:1});
        if(!product_rec)
        {
            return res.send({
                status:false,
                message:"L'identifiant du produit n'est pas valide"
            })
        }
        let seller_rec = await SellerModel.findOne({ _id:product_rec.provider_id},{fullname:1,shopname:1});
        if(!seller_rec)
        {
            return res.send({
                status:false,
                message:"L'identifiant du vendeur n'est pas valide"
            })
        }
        let user_rec = await User.findOne({ _id:reportedbyId},{first_name:1,last_name:1});
        if(!user_rec)
        {
            return res.send({
                status:false,
                message:"L'identifiant de l'utilisateur n'est pas valide"
            })
        }
        product = product_rec.title;
            
        vendor = seller_rec.fullname +" - "+seller_rec.shopname
        vendorId = seller_rec._id;
        reportedbyName = user_rec.first_name+" "+user_rec.last_name;
        //console.log("product_rec ",product_rec);
        //return false;
        let mediaarray=[]
        if(req.files)
        {
            if(req.files.photo?.length)
            {
                req.files.photo.map((file)=>{
                    newfile=process.env.PHOTOS_PATH_PERFIX+"/"+file.filename;
                    mediaarray.push(newfile);
                })
            
            }
        }
        const datatoupdate={
            ...(reason&&{reason:reason}),
            ...(product&&{product:product}),
            ...(productId&&{productId:productId}),
            ...(vendor&&{vendor:vendor}),
            ...(vendorId&&{vendorId:vendorId}),
            ...(content&&{content:content}),
            ...(reportedbyName&&{reportedbyName:reportedbyName}),
            ...(reportedbyId&&{reportedbyId:reportedbyId}),
            ...(mediaarray.length&&{media:mediaarray}),
        }
       if(id){
        await abuseReportModel.findByIdAndUpdate(id,datatoupdate).then((result)=>{
            return res.send({
                status:true,
                message:"Rapport d'abus soumis succès complet"
            })
        })
       }else{
        await abuseReportModel.create(datatoupdate).then((result)=>{
            return res.send({
                status:true,
                message:"Rapport d'abus soumis succès complet "
            })
        })
       }
    },
    delete:async(req,res)=>{
     const id=req.params.id;
     await abuseReportModel.findByIdAndDelete(id)
     .then((result)=>{
        return res.send({
            status:true,
            message:"deleted"
        })
     })
    },
    addOrupdatereason:async(req,res)=>{
        const {
            reason,
            
            id
        }=req.body;
       
        const datatoupdate={
            ...(reason&&{reason:reason}),
           
        }
       if(id){
        await reasonModel.findByIdAndUpdate(id,datatoupdate).then((result)=>{
            return res.send({
                status:true,
                message:"updated"
            })
        })
       }else{
        await reasonModel.create(datatoupdate).then((result)=>{
            return res.send({
                status:true,
                message:"updated"
            })
        })
       }
    },
    getallreasons:async(req,res)=>{
        await reasonModel.find({}).then((result)=>{
            return res.send({
                status:true,
                data:result,
                message:"fetched"
            })
        })
    },
    markAsresolved:async(req,res)=>{
        const id=req.params.id;
        await abuseReportModel.findByIdAndUpdate(id,{is_resolved:true})
        .then((result)=>{
           return res.send({
               status:true,
               message:"updated"
           })
        })
    },
    markAsUnresolved:async(req,res)=>{
        const id=req.params.id;
        await abuseReportModel.findByIdAndUpdate(id,{is_resolved:false})
        .then((result)=>{
           return res.send({
               status:true,
               message:"updated"
           })
        })
    },
    getallreports:async(req,res)=>{
        const {
            product_id,
            vendor_id,
            reason,
            pageno
        }=req.body;

       
    const limit=10;
        
        
        const skip=limit*pageno;
   
        const query={_id:{$ne:null}}
        if(product_id){
            query["productId"]=product_id
        }
        if(vendor_id){
            query["vendorId"]=vendor_id
        }
        if(reason){
            query["reason"]=reason
        }
        if(pageno==null||pageno=="null"){
            const count=(await abuseReportModel.aggregate([
                {$match:query},
               
            ]))?.length
    
            const pages=Math.ceil(count/limit)
            return res.send({
                status:true,
                pages:pages
            })
        }
        const reports=await abuseReportModel.aggregate([
            {$match:query},
            {$skip:skip},
            {$limit:limit}
        ]);
        return res.send({
            status:true,
            data:reports
        })

    }
}