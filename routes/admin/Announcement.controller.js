const announcementModel = require("../../models/admin/Announcements");
const { sendmail,sendmailusingtemplatestring } = require("../../modules/sendmail");// pathtofile,idata,subject,to_email
const usermodel = require("../../models/user/User");
const sellermodel = require("../../models/seller/Seller");
const { sendpushnotificationtotopic,sendpushnotificationtouser } = require("../../modules/Fcm");
const notificationmodel = require("../../models/admin/Announcements");
const path = require("path");
module.exports = {
    newannouncementwithpermtemplate: async (req, res) => {
        try {
            const { notificationtype, usertype, selecteduser, message, title } = req.body;
            console.log(notificationtype, usertype, selecteduser, message);
            console.log("req.body", req.body)
            let models;
            const pathtofile = path.resolve("./views/notifications/newannouncement.ejs")
            if (usertype == "user") {
                models = usermodel;
            } else if (usertype == "seller") {
                models = sellermodel;
            }
            let topic;
            if (usertype == "user") {
                topic = process.env.TOUSERTOPIC || "user";
            } else if (usertype == "seller") {
                topic = process.env.TOSELLERTOPIC || "seller";
            }
            if (selecteduser[0] == "all") {
                const all = await models.find({ is_deleted: false });
                if (notificationtype == "email") {
                    all?.map(async (usr) => {
                        const name = usertype == "user" ? usr.first_name + " " + usr.last_name : usr.fullname;
                        await sendmail(pathtofile, { name: name, content: message }, "new announcment from admin", usr.email)
                        notificationmodel.create({
                            title: title,
                            notificationtype: notificationtype,
                            usertype: usertype,
                            content: message,
                            sentTo: [usr._id]

                        });
                    })


                } else {
                    sendpushnotificationtotopic(message, topic);
                    all?.map(async(usr) => {
                        topic = "listentoadmin";
                        console.log("in app folder")
                        console.log("usr",usr)
                        if (usr.fcm_token) {

                          await  sendpushnotificationtouser(message, usr, usr._id).then((result) => {
                                notificationmodel.create({
                                    title: title,
                                    notificationtype: notificationtype,
                                    usertype: usertype,
                                    content: message,
                                    sentTo: [usr._id]

                                });
                                console.log(result);
                            }).catch((err) => {
                                console.log(err);
                            })
                        }
                    })

                }
            } else {
                selecteduser?.map(async (user) => {
                    await models.findById(user).then(async (usr) => {
                      if(usr){
                        if (notificationtype == "email") {
                            const name = usertype == "user" ? usr.first_name + " " + usr.last_name : usr?.fullname?usr.first_name+" "+usr.last_name:"";
                            await sendmail(pathtofile, { name: name, content: message }, "new announcment from admin", usr.email)
                            notificationmodel.create({
                                title: title,
                                notificationtype: notificationtype,
                                usertype: usertype,
                                content: message,
                                sentTo: [user]

                            });

                        } else if (notificationtype == "app") {
                            topic = "listentoadmin";
                            console.log("in app folder")
                            if (usr.fcm_token) {

                              await  sendpushnotificationtouser(message, usr, usr._id).then((result) => {
                                    notificationmodel.create({
                                        title: title,
                                        notificationtype: notificationtype,
                                        usertype: usertype,
                                        content: message,
                                        sentTo: [user]

                                    });
                                    console.log(result);
                                }).catch((err) => {
                                    console.log(err);
                                })
                            }
                        }
                      }
                    });
                });
            }


            return res.send({
                status: true,
                message: "Notification envoyée avec succès",
            });
        } catch (err) {
            console.log(err)
        }
    },
    newannouncement: async (req, res) => {
        try {
            const { notificationtype, usertype, selecteduser, message, title } = req.body;
            console.log(notificationtype, usertype, selecteduser, message);
            console.log("req.body", req.body)
            let models;
            const pathtofile = path.resolve("./views/notifications/newannouncement.ejs")
            if (usertype == "user") {
                models = usermodel;
            } else if (usertype == "seller") {
                models = sellermodel;
            }
            let topic;
            if (usertype == "user") {
                topic = process.env.TOUSERTOPIC || "user";
            } else if (usertype == "seller") {
                topic = process.env.TOSELLERTOPIC || "seller";
            }
            if (selecteduser[0] == "all") {
                const all = await models.find({ is_deleted: false });
                if (notificationtype == "email") {
                    all?.map(async (usr) => {
                        const name = usertype == "user" ? usr.first_name + " " + usr.last_name : usr.fullname;
                        await sendmailusingtemplatestring(message, "new announcment from admin", usr.email)
                        notificationmodel.create({
                            title: title,
                            notificationtype: notificationtype,
                            usertype: usertype,
                            content: message,
                            sentTo: [usr._id]

                        });
                    })


                } else {
                    sendpushnotificationtotopic(message, topic);
                    all?.map(async(usr) => {
                        topic = "listentoadmin";
                        console.log("in app folder")
                        console.log("usr",usr)
                        if (usr.fcm_token) {

                          await  sendpushnotificationtouser(message, usr, usr._id).then((result) => {
                                notificationmodel.create({
                                    title: title,
                                    notificationtype: notificationtype,
                                    usertype: usertype,
                                    content: message,
                                    sentTo: [usr._id]

                                });
                                console.log(result);
                            }).catch((err) => {
                                console.log(err);
                            })
                        }
                    })

                }
            } else {
                selecteduser?.map(async (user) => {
                    await models.findById(user).then(async (usr) => {
                      if(usr){
                        if (notificationtype == "email") {
                            const name = usertype == "user" ? usr.first_name + " " + usr.last_name : usr?.fullname?usr.first_name+" "+usr.last_name:"";
                            await sendmailusingtemplatestring( message, "new announcment from admin", usr.email)
                            notificationmodel.create({
                                title: title,
                                notificationtype: notificationtype,
                                usertype: usertype,
                                content: message,
                                sentTo: [user]

                            });

                        } else if (notificationtype == "app") {
                            topic = "listentoadmin";
                            console.log("in app folder")
                            if (usr.fcm_token) {

                              await  sendpushnotificationtouser(message, usr, usr._id).then((result) => {
                                    notificationmodel.create({
                                        title: title,
                                        notificationtype: notificationtype,
                                        usertype: usertype,
                                        content: message,
                                        sentTo: [user]

                                    });
                                    console.log(result);
                                }).catch((err) => {
                                    console.log(err);
                                })
                            }
                        }
                      }
                    });
                });
            }


            return res.send({
                status: true,
                message: "Notification envoyée avec succès",
            });
        } catch (err) {
            console.log(err)
        }
    },
    getallannouncements:async(req,res)=>{
        const pageno=req.params.pageno
        const limit=10;
        
        
        const skip=limit*pageno;
 if(pageno==null||pageno=="null"){
            const count=( await notificationmodel.aggregate([
               
                {$sort:{createdAt:-1}}
            ]))?.length
    
            const pages=Math.ceil(count/limit)
            return res.send({
                status:true,
                pages:pages
            })
        }
        await notificationmodel.aggregate([
            // {$lookup:{
            //     from:"users",
            //     let:{sendTo:"$sentTo"},
            //     pipeline:[
            //         {$match:{_id:{$in:"$$sendTo"}}}
            //     ],
            //     as:"users"
            // }},
            // {$lookup:{
            //     from:"sellers",
            //     let:{sendTo:"$sentTo"},
            //     pipeline:[
            //         {$match:{_id:{$in:"$$sendTo"}}}
            //     ],
            //     as:"sellers"
            // }},
            {$skip:skip},
            {$limit:limit},
            {$sort:{createdAt:-1}}
        ]).then((result)=>{
            return res.send({
                status: true,
                data:result,
            });  
        })
    }


}