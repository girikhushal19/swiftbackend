const Seller = require("../../models/seller/Seller");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const fs = require('fs');
const DriversModel = require("../../models/admin/DriversModel");
const customConstant = require('../../helpers/customConstant');
const DriverSupportModel = require("../../models/admin/DriverSupportModel");
const DriverNotificationModel = require("../../models/admin/DriverNotificationModel");
const mongoose = require("mongoose");
//var bcrypt = require('bcryptjs');
//const jwt = require("jsonwebtoken");

// var FCM = require('fcm-node');
// var serverKey = process.env.FCM_SERVER_KEY; //put your server key here
// var fcm = new FCM(serverKey); 
const transport = nodemailer.createTransport({
  name: "Swift",
  host: process.env.MAILER_HOST,
  port: process.env.MAILER_PORT,
  auth: {
    user: process.env.MAILER_EMAIL_ID,
    pass: process.env.MAILER_PASSWORD,
  }
});
module.exports = {

  driverLogin:async function(req,res,next)
  {
      //console.log("login function");
    try{
      // Get user input
      const { email, password,deviceToken } = req.body;
      // Validate user input
      // Validate if user exist in our database
      var user = "";
      user = await DriversModel.findOne({ email,deleted_at:0 });
      //console.log("user record"+user);
      if (user && (await bcrypt.compare(password, user.password)))
      {
        // Create token
        const token = jwt.sign(
          { user_id: user._id, email },
          "Swift-Driver",
          /*process.env.TOKEN_KEY,
          {
            expiresIn: "2h",
          }*/
        );
        // save user token

        if(user.status != 1)
        {
          res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Votre compte est en cours de révision",
          });
        }else{
          user.token = token;
          DriversModel.updateOne({ "email":email,deleted_at:0 }, 
          {token:token,deviceToken:deviceToken,loggedIn:1,onLineOffLine:1}, function (uerr, docs) {
          if (uerr)
          {
              console.log(uerr);
              res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: uerr,
                    //userRecord:user
                });
          }
          else{
              res.status(200)
              .send({
                  error: false,
                  success: true,
                  errorMessage: "Connexion réussie",
                  userRecord:user
              });
            }
          });
        }
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Détails de connexion non valides"
          });
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: error
      });
    }
  },
  driverLogout:async function(req,res,next)
  {
      //console.log("login function");
    try{
      // Get user input
      const { email } = req.body;
      // Validate user input
      // Validate if user exist in our database
      var user = "";
      user = await DriversModel.findOne({ email,deleted_at:0 });
      //console.log("user record"+user);
      if (user)
      {
        
          DriversModel.updateOne({ "email":email  }, 
          {token:"",deviceToken:"",loggedIn:0,onLineOffLine:0}, function (uerr, docs) {
          if (uerr)
          {
              console.log(uerr);
              res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: uerr,
                    //userRecord:user
                });
          }
          else{
              res.status(200)
              .send({
                  error: false,
                  success: true,
                  errorMessage: "Le conducteur s'est déconnecté avec succès"
              });
            }
          });
        
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Détails de connexion non valides"
          });
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: error
      });
    }
  },
  forgotPasswordDriver:async function(req,res,next)
  {
    try{
      var randomNumber = Array.from(Array(8), () => Math.floor(Math.random() * 36).toString(36)).join('');
      var encryptedPassword = await bcrypt.hash(randomNumber, 10);

      var email = req.body.email;
      DriversModel.find({ email:email, status:1 ,"deleted_at": 0 }, {}).lean(true).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          if(result.length > 0)
          {
            //console.log("here");
            //console.log(result);

            
            DriversModel.updateOne({ "_id":result[0]._id }, 
              {password:encryptedPassword}, function (uerr, docs) {
              if (uerr){
                //console.log(uerr);
                res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: uerr
                  });
            }else{

                    const message = {
                      from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
                      to: result[0].email,         // recipients
                      subject: "Mot de passe oublié", // Subject line
                      html: "<b>"+randomNumber+"</b>" +" Ceci est votre nouveau mot de passe, vous pouvez vous connecter avec ce mot de passe dans notre application et vous pouvez changer le mot de passe après la connexion." // Plain text body
                  };
                  transport.sendMail(message, function(err, info) {
                      if (err) {
                        console.log(err);
                        res.status(200)
                          .send({
                              error: false,
                              success: true,
                              errorMessage: "Votre mot de passe a été modifié avec succès, vous pouvez vérifier votre e-mail"
                          });
                      } else {
                        res.status(200)
                        .send({
                            error: false,
                            success: true,
                            errorMessage: "Votre mot de passe a été modifié avec succès, vous pouvez vérifier votre e-mail"
                        });
                      }
                  });

                
                }
            });

          }else{
            var return_response = {"error":true,success: false,errorMessage:"Compte e-mail non valide ou compte inactif"};
            res.status(200)
            .send(return_response);
          }
        }
      });
    }catch (err) {
      console.log(err);
      var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
    }
  },
  editDriverProfile:async function(req,res,next)
  {
    // console.log("update profile");
    // console.log(req.body);
    // console.log(req.files);
    // return false;
    try{
      var user_id = req.body.user_id;
      var email = req.body.email;
      var mobileNumber = parseInt(req.body.mobileNumber);
      var fullName = req.body.fullName;
      //var lastName = req.body.lastName;

      //console.log(user_id);
      var userRecord = await DriversModel.findOne({ "_id":user_id }); 
      //console.log("userRecord"+userRecord);
      
      // console.log("userImage");
      // console.log(userImage);
      // return false;
      if(userRecord)
      {
        if(req.files)
        {
          if(req.files.length > 0)
          {
            //console.log("ifffffff hereeee");
            var userImage = req.files[0].filename;
            var oldFileName = userRecord.userImage;
            var uploadDir = './public/uploads/driver/';
            let fileNameWithPath = uploadDir + oldFileName;
            //console.log(fileNameWithPath);
            //console.log(userImage);
            if (fs.existsSync(fileNameWithPath))
            {
              fs.unlink(uploadDir + oldFileName, (err) => 
              {
                console.log("unlink file error "+err);
              });
            }
          }else{
            if(userRecord.userImage)
            {
              var userImage = userRecord.userImage;
            }else{
              var userImage = null;
            }
          }            
        }else{
          if(userRecord.userImage)
          {
            var userImage = userRecord.userImage;
          }else{
            var userImage = null;
          }
        }

        var userRecordEmailCheck = await DriversModel.findOne({ "_id":{$ne:user_id},"email":email,"deleted_at":0 }); 
        //console.log("userRecordEmailCheck"+userRecordEmailCheck);
        if(userRecordEmailCheck)
        {
          res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: "Cet email existe déjà chez un autre utilisateur"
          });
        }else{
          
          var userRecordMobileCheck = await DriversModel.findOne({ "_id":{$ne:user_id},"mobileNumber":mobileNumber,"deleted_at":0  }); 
          //console.log("userRecordMobileCheck"+userRecordMobileCheck);
          if(userRecordMobileCheck)
          {
            res.status(200)
              .send({
                error: true,
                success: false,
                errorMessage: "Ce numéro de téléphone mobile existe déjà pour un autre utilisateur"
            });
          }else{
            DriversModel.findByIdAndUpdate({ _id: user_id },{ 
              email: email.toLowerCase(),
              mobileNumber:mobileNumber,
              fullName:fullName,
              userImage:userImage  }, function (err, user) {
              if(err){
                console.log(err);
                res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: "Un problème est survenu",
                      errorOrignalMessage: err
                  });
              }else{
                //console.log("L'enregistrement a été mis à jour avec succès");
                  res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "L'enregistrement a été mis à jour avec succès"
                  });
              }
              })
          }
        }
      }else{
        //console.log("here");
        res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Détails de l'utilisateur non valides"
              });
      }
    }catch(err){
      console.log(err);
    }
  },
  getDriverProfile:async function(req,res,next)
  {
    //console.log("get profile");
    try{
      var base_url = customConstant.base_url;
      var imagUrl = base_url+"public/uploads/driver/";
        
      //console.log("my IP is  "+req.ip);
      var user_id = req.body.user_id;
      //console.log(user_id);
      var userRecord = await DriversModel.findOne({ "_id":user_id }); 
      //console.log("userRecord"+userRecord);
      if(userRecord)
      {
        if(userRecord.length === 0)
        {
          res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Détail de l'utilisateur non valide"
              });
        }else{
          res.status(200)
              .send({
                  error: false,
                  success: true,
                  errorMessage: "Détail de l'utilisateur",
                  imagUrl:imagUrl,
                  userRecord:userRecord,
              });
        }
      }else{
        //console.log("here");
        res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Détail de l'utilisateur non valide"
              });
      }
      
    }catch(err){
      console.log(err);
    }
  },
  checkFcmToken:async function(req,res,next)
  {
    // try{
    //   var base_url = customConstant.base_url;
    //   var imagUrl = base_url+"public/uploads/driver/";
        
    //   //console.log("my IP is  "+req.ip);
    //   var user_id = req.body.user_id;
    //   //console.log(user_id);
    //   var userRecord = await DriversModel.findOne({ "_id":user_id }); 
    //   //console.log("userRecord"+userRecord);
    //   if(userRecord)
    //   {
    //     if(userRecord.length === 0)
    //     {
    //       res.status(200)
    //           .send({
    //               error: true,
    //               success: false,
    //               errorMessage: "Détail de l'utilisateur non valide"
    //           });
    //     }else{
    //       console.log("hereeee 351");
    //       var deviceToken = userRecord.deviceToken;
    //       console.log("deviceToken --->>>>> "+deviceToken);
    //       if(deviceToken && deviceToken != null)
    //         {
    //           var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
    //             to: deviceToken, 
    //             collapse_key: 'your_collapse_key',
                
    //             notification: {
    //                 title: 'Swift', 
    //                 body: 'hello ayush testing push message',
    //             },
                
    //             data: {  //you can send only notification or only data(or include both)
    //                 my_key: 'dsfdf',
    //                 my_another_key: 'hello ayush testing push message'
    //             }
    //           };
    //           fcm.send(message, function(err, response){
    //             if(err){
    //               console.log("Something has gone wrong!");
    //               console.log(err);
    //             }else{
    //               console.log("Successfully sent with response: ", response);
    //             }
    //           });
    //         }
    //       }
    //   }else{
    //     //console.log("here");
    //     res.status(200)
    //           .send({
    //               error: true,
    //               success: false,
    //               errorMessage: "Détail de l'utilisateur non valide"
    //           });
    //   }
    // }catch(err){
    //   console.log(err);
    // }
  },
  getDriverSupport: async function(req, res,next)
  {
    //console.log(req.body);
    try{
      DriverSupportModel.findOne({  }).exec((err, userRecord)=>{
        if(err)
        {
          //console.log("herrrrrerr");
          console.log(err);
        }else{
          //console.log(userRecord);
          var return_response = {"error":false,errorMessage:"success","record":userRecord};
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err };
      res.status(200).send(return_response);
    }
  },

  getAllPushNotification:async function(req,res,next)
  {
    //console.log("get profile");
    try{
      var {driver_id} = req.body;
      if(driver_id == "" || driver_id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'identité est requise"
        });
      }else{
        DriverNotificationModel.find({driver_id:driver_id}).sort({created_at:-1}).exec((err,result)=>{
          if(err)
          {
            res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: err
            });
          }else{
            //console.log(result);
            DriverNotificationModel.updateMany({driver_id:driver_id,is_read:0},{"is_read":1},function(shipError,shipResult){
              if(shipError)
              {
                res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: shipError
                });
              }else{
                res.status(200)
                .send({
                    error: false,
                    success: true,
                    errorMessage: "Succès",
                    record:result
                });
              }
            });
          }
        });
      }
      
    }catch(err){
      console.log(err);
    }
  },
  getAllPushNotificationCount:async function(req,res,next)
  {
    //console.log("get profile");
    try{
      var {driver_id} = req.body;
      if(driver_id == "" || driver_id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'identité est requise"
        });
      }else{
        DriverNotificationModel.count({driver_id:driver_id,is_read:0}).exec((err,result)=>{
          if(err)
          {
            res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: err
            });
          }else{
            //console.log(result);
            res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Succès",
                record:result
            });
          }
        });
      }
      
    }catch(err){
      console.log(err);
    }
  },
  getSinglePushNotification:async function(req,res,next)
  {
    //console.log("get profile");
    try{
      var {id} = req.body;
      const queryJoin = [
        {
          path:'route_id',
          select:['routeName','totalDistance','totalTime','totalHour','totalMin','totalStop']
        }
      ];

      if(id == "" || id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'identité est requise"
        });
      }else{
        DriverNotificationModel.findOne({_id:id}).populate(queryJoin).exec((err,result)=>{
          if(err)
          {
            res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: err
            });
          }else{
            //console.log(result);
            res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Succès",
                record:result
            });
          }
        });
      }
      
    }catch(err){
      console.log(err);
    }
  },
  updateReadStatus:async function(req,res,next)
  {
    try{
      var {driver_id} = req.body;
      if(driver_id == "" || driver_id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'identité est requise"
        });
      }else{
        DriverNotificationModel.updateOne({ "driver_id":driver_id }, 
        {
          "is_read":1
        }, function (uerr, docs) {
          if (uerr){
            //console.log(uerr);
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: uerr
              });
          }else{
            var return_response = {"error":false,errorMessage:"Livré avec succès"};
            res.status(200).send(return_response);
          }
        });
      }
    }catch(error)
    {

      console.log(error);
    }
  },
  
  driverChangePasswordSubmit:async function(req,res,next)
  {
    try{
      //console.log(req.body);return false;
      var {driver_id,old_password,new_password} = req.body;
      
      var encryptedPassword = await bcrypt.hash(new_password, 10);

      
      if( !driver_id )
      {
        res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "La carte d'identité du conducteur est requise"
        });
      }

      if(old_password == new_password)
      {
        res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "L'ancien et le nouveau mot de passe sont les mêmes"
        });
      }else{
        DriversModel.findOne({ _id:driver_id },function(err,user){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
            //console.log("user record"+user);
            if(user)
            {
              bcrypt.compare(old_password, user.password, function(errPW, resPw) {
                if (errPW){
                  var return_response = {"error":true,success: false,errorMessage:errPW};
                    res.status(200)
                    .send(return_response);
                }else{
                  if(resPw)
                  {
                    //console.log("password match");
                    DriversModel.findByIdAndUpdate({ _id: driver_id },{
                      password: encryptedPassword,
                    }, function (err, user) {
                    if (err) {
                      res.status(200)
                      .send({
                          error: true,
                          success: false,
                          errorMessage: err,
                          errorOrignalMessage: err
                      });
                    } else {
                      //console.log("Record updated success-fully");
                      res.status(200)
                      .send({
                          error: false,
                          success: true,
                          errorMessage: "Votre mot de passe a été modifié avec succès. Vous pouvez maintenant vous connecter avec votre nouveau mot de passe."
                      });
                    }
                    });
                  }else{
                    //console.log("password not match");
                    res.status(200)
                      .send({
                        error: true,
                        success: false,
                        errorMessage: "Votre ancien mot de passe ne correspond pas"
                    });
                  }
                }
              });
            }else{
              res.status(200)
                .send({
                  error: true,
                  success: false,
                  errorMessage: "Connexion invalide"
              });
            }
        }
        });
      }
    }catch(error){
      console.log(error);
    }
  },
 

};