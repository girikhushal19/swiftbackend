const User = require("../../models/user/User");
const bycrypt = require("bcryptjs");
const mongoose=require("mongoose")
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const ejs = require('ejs');
const customConstant = require('../../helpers/customConstant');
const {sendmail}=require("../../modules/sendmail");
/////const hbs = require('nodemailer-express-handlebars');
const apptext_cmsmodel=require("../../models/admin/Apptext");

// var FCM = require('fcm-node');
// var serverKey = process.env.FCM_SERVER_KEY; //put your server key here
// var fcm = new FCM(serverKey);
const transport = nodemailer.createTransport({
    name: "Swift",
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    auth: {
      user: process.env.MAILER_EMAIL_ID,
      pass: process.env.MAILER_PASSWORD,
    }
});

module.exports = {

    addUserDeliveryNewAddress:async(req,res)=>{
        try{
            let{user_id,name,address,latitude,longitude,city,country,pobox,telephone,countryCode} = req.body;
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            if(!address)
            {
                return res.send({
                    status:false,
                    message:"L'adresse est requise"
                })
            }
            if(!latitude || !longitude)
            {
                return res.send({
                    status:false,
                    message:"L'adresse doit être une adresse Google"
                })
            }
            let location = {
                type:"point",
                coordinates:[latitude,longitude]
            };
            let insert_object = {
                countryCode:countryCode,
                name:name,
                address:address,
                location:location,
                city:city,
                country:country,
                pobox:pobox,
                telephone:telephone
            }
            let user_record = await User.findOne({_id:user_id},{address:1});
            if(!user_record)
            {
                return res.send({
                    status:false,
                    message:"ID utilisateur invalide"
                });
            }else{
                user_record.address.push(insert_object);
                //console.log("new_addre ", user_record.address);
                await User.updateOne({_id:user_id},{address:user_record.address}).then(async(result)=>{
                    let user_record_new = await User.findOne({_id:user_id},{address:1});
                    return res.send({
                        status:true,
                        message:"L'adresse a été ajoutée avec succès",
                        record:user_record_new.address
                    });
                }).catch((e)=>{
                    return res.send({
                        status:false,
                        message:e.message
                    });
                })
            }
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    getUserDeliveryNewAddress:async(req,res)=>{
        try{
            let user_id = req.params.id;
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            let user_record = await User.findOne({_id:user_id},{address:1});
            if(!user_record)
            {
                return res.send({
                    status:false,
                    message:"ID utilisateur invalide"
                });
            }
            return res.send({
                status:false,
                message:"Success",
                record:user_record.address
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },

    editUserDeliveryNewAddress:async(req,res)=>{
        try{
            let{id,name,address,latitude,longitude,city,country,pobox,telephone,user_id,countryCode} = req.body;
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            if(!id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant est requise"
                })
            }
            if(!address)
            {
                return res.send({
                    status:false,
                    message:"L'adresse est requise"
                })
            }
            if(!latitude || !longitude)
            {
                return res.send({
                    status:false,
                    message:"L'adresse doit être une adresse Google"
                })
            }
            let location = {
                type:"point",
                coordinates:[latitude,longitude]
            };
            let insert_object = {
                ...( countryCode && { countryCode:countryCode } ),
                ...( name && { name:name } ),
                ...( address && { address:address } ),
                ...( location && { location:location } ),
                ...( city && { city:city } ),
                ...( country && { country:country } ),
                ...( pobox && { pobox:pobox } ),
                ...( telephone && { telephone:telephone } )
            }
            let user_record = await User.findOne({_id:user_id},{address:1});
            if(!user_record)
            {
                return res.send({
                    status:false,
                    message:"ID utilisateur invalide"
                });
            }else{
                let new_address = user_record.address.map((val)=>{
                    let abc_obj = {};
                    if(val._id == id)
                    {
                        
                        val.countryCode = countryCode ? countryCode : val.countryCode;
                        val.name = name ? name : val.name;
                        val.address = address ? address : val.address;
                        val.location = location ? location : val.location;
                        val.city = city ? city : val.city;
                        val.country = country ? country : val.country;
                        val.pobox = pobox ? pobox : val.pobox;
                        val.telephone = telephone ? telephone : val.telephone;
                                           
                    }
                    return val;
                    //console.log("abc_obj ", abc_obj);
                });
                //console.log("new_address ", new_address);
                //console.log("new_addre ", user_record.address);
                await User.updateOne({_id:user_id},{address:new_address}).then(async(result)=>{
                    let user_record_new = await User.findOne({_id:user_id},{address:1});
                    return res.send({
                        status:true,
                        message:"L'adresse a été ajoutée avec succès",
                        record:user_record_new.address
                    });
                }).catch((e)=>{
                    return res.send({
                        status:false,
                        message:e.message
                    });
                })
            }
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    getUserDeliveryNewAddressSingle:async(req,res)=>{
        try{
            console.log(req.body);
            let {id,user_id} = req.body;
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            if(!id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant est requise"
                })
            }
            let record = await User.findOne({_id:user_id},{address:1});
            if(!record)
            {
                return res.send({
                    status:false,
                    message:"Identifiant invalide"
                })
            }
            console.log(record);
            let single_address = record.address.filter((val)=>{
                if( val._id == id)
                {
                    return val;
                }
            })
            if(single_address.length > 0)
            {
                return res.send({
                    status:true,
                    message:"Success",
                    record:single_address
                })
            }else{
                return res.send({
                    status:false,
                    message:"Identifiant invalide"
                })
            }
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    getUserDeliveryDeleteAddress:async(req,res)=>{
        try{
            console.log(req.body);
            let {id,user_id} = req.body;
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            if(!id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant est requise"
                })
            }
            let record = await User.findOne({_id:user_id},{address:1});
            if(!record)
            {
                return res.send({
                    status:false,
                    message:"Identifiant invalide"
                })
            }
            console.log(record);
            let single_address = record.address.filter((val)=>{
                if( val._id != id)
                {
                    return val;
                }
            })
            await User.updateOne({_id:user_id},{address:single_address}).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success"
                })
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message
                });
            })

            // if(single_address.length > 0)
            // {
            //     return res.send({
            //         status:true,
            //         message:"Success",
            //         record:single_address
            //     })
            // }else{
            //     return res.send({
            //         status:false,
            //         message:"Identifiant invalide"
            //     })
            // }
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    deleteUserAccount:async(req,res)=>{
        try{
            let user_id = req.params.id;
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            //console.log("user_id ", user_id);
            let record = await User.findOne({_id:user_id});
            if(!record)
            {
                return res.send({
                    status:false,
                    message:"Identifiant invalide"
                })
            }
            let randomNumber = Array.from(Array(20), () => Math.floor(Math.random() * 36).toString(36)).join('');
            let email = record.email+"_"+randomNumber+"_deleted";
            await User.updateOne({_id:user_id},{email:email,is_deleted:true,token:null}).then((result)=>{
                return res.send({
                    status:true,
                    message:"Votre compte a été supprimé avec succès"
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    makeDefaultDeliveryAddress:async(req,res)=>{
        try{
            let {user_id,id} = req.body;
            console.log(req.body);
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            if(!id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant est requise"
                })
            }
            let record = await User.findOne({_id:user_id});
            if(!record)
            {
                return res.send({
                    status:false,
                    message:"Identifiant invalide"
                })
            }
            let new_add = record.address.filter((val)=>{
                val.is_shipping_address = false;
                if(val._id == id)
                {
                    val.is_shipping_address = true;
                }
                return val;
            });
            //console.log("new_add ", new_add);
            await User.updateOne({_id:user_id},{address:new_add}).then((result)=>{
                return res.send({
                    status:true,
                    message:"L'adresse de livraison a été ajoutée avec succès"
                });
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message
                });
            });
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    },
    getUserDeliveryAddress:async(req,res)=>{
        try{
            let user_id = req.params.id;
            if(!user_id)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur est requise"
                })
            }
            let record = await User.findOne({_id:user_id});
            if(!record)
            {
                return res.send({
                    status:false,
                    message:"Identifiant invalide"
                })
            }
            let new_add = record.address.filter((val)=>{
                if(val.is_shipping_address = true)
                {
                    return val;
                }                
            });
            if(new_add.length > 0)
            {
                return res.send({
                    status:true,
                    message:"Success",
                    record:new_add[0]
                });
            }else{
                return res.send({
                    status:false,
                    message:"Aucune adresse de livraison n'a encore été sélectionnée"
                });
            }
            
        }catch(e){
            return res.send({
                status:false,
                message:e.message
            });
        }
    }
  
}