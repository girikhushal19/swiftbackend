const express =require('express');
const UserController = require('./UserController');
const UserSecondController = require("./UserSecondController");
const router = express.Router();
const verifyToken= require("../../middleware/authClient");
const multer=require("multer");
const path=require("path");
const user_path=process.env.USER_PATH;
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve(user_path))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });
router.post('/createuser',uploadTo.fields([
    { 
      name: 'photo', 
      maxCount: 1
    }
  ]
  ),UserController.createnewuser);

router.post('/userRegisteration',UserController.userRegisteration);

router.get("/markuserasactive/:id",verifyToken,UserController.markUserAsActive)
router.get("/getalluserforfilter",UserController.getalluserforfilter)
router.get("/markuserasinactive/:id",UserController.markUserAsInActive)
router.post('/login',UserController.login)
router.post('/forgotpassword',UserController.forgotpassword);

router.post('/userForgotPassword',UserController.userForgotPassword);
router.post("/userSubmitForgotPassword", UserController.userSubmitForgotPassword);

router.get("/logout/:id", UserController.logout);
router.get("/getuserprofile/:id", UserController.getuserprofile);
router.post("/markaddressasbillingaddress", UserController.markaddressasbillingaddress);
router.post("/markaddressasshippingaddress", UserController.markaddressasshippingaddress);
router.post("/getbillingaddress", UserController.getbillingaddress);
router.post("/getshippingaddress", UserController.getshippingaddress);
router.post("/deletemultipleuser", UserController.deletemultipleuser);
router.post("/updateaddress", UserController.updateaddress);
router.get("/reset_password",UserController.render_reset_password_template);
router.post("/reset_password",UserController.reset_password);
router.post("/deleteaddress",UserController.deleteaddress);
router.post("/emailVerifyCode", UserController.emailVerifyCode);
router.post("/mobileVerifyCode", UserController.mobileVerifyCode);
router.post("/emailVerifyCodeWeb", UserController.emailVerifyCodeWeb);
router.post("/changePassword", UserController.changePassword);

router.get("/push_testing/:id", UserController.push_testing);
router.post("/resendOtp", UserController.resendOtp);
router.post("/resendOtpWeb", UserController.resendOtpWeb);

router.post("/checkEmailExist", UserController.checkEmailExist);
router.post("/updateAddressDetail", UserController.updateAddressDetail);
router.post("/getUserSingleAddress", UserController.getUserSingleAddress);


router.post("/addUserDeliveryNewAddress", UserSecondController.addUserDeliveryNewAddress);
router.get("/getUserDeliveryNewAddress/:id", UserSecondController.getUserDeliveryNewAddress);
router.post("/getUserDeliveryNewAddressSingle", UserSecondController.getUserDeliveryNewAddressSingle);
router.post("/editUserDeliveryNewAddress", UserSecondController.editUserDeliveryNewAddress);
router.post("/getUserDeliveryDeleteAddress", UserSecondController.getUserDeliveryDeleteAddress);
router.get("/deleteUserAccount/:id", UserSecondController.deleteUserAccount);
router.post("/makeDefaultDeliveryAddress", UserSecondController.makeDefaultDeliveryAddress);
router.get("/getUserDeliveryAddress/:id", UserSecondController.getUserDeliveryAddress);


module.exports=router