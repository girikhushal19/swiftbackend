const Delivery = require("../../models/delivery/Delivery");

module.exports = {
    createDelivery: async (req, res) => {
        const {
            pickupdate,
            pickuptime,
            pointofpickup,
            pointofdelivery,
            latlongofpickup,
            numberofpackages,
            totalweight,
            maxlength,
            leplusheavy,
            productdesc,
            primaryref,
           
             
             freason,
             fdesc,
             fphotos,
             fsignature
         
        } = req.body;
        let images = [];
        if (req.files.photo) {
            req.files.photo?.map((image) => {
                let filename = "products/" + image.filename;
                images.push(filename);
            })
        }
        if (id) {
            await Product.findByIdAndUpdate(id, {
                ...(sku && { sku: sku }),
                ...(title && { title: title }),
                ...(description && { description: description }),
                ...(price && { price: price }),
                ...(images.length && { images: images })
            }).then((result) => {
                return res.send({
                    status: true,
                    message: "product updated successfully"
                })
            })

        } else {
            const product = new Product();
            product.sku = sku;
            product.title = title;
            product.description = description;
            product.price = price;
            product.images = images;
            product.save()
                .then((result) => {
                    return res.send({
                        status: true,
                        message: "product saved successfully"
                    })
                })
        }
    },
    deleteproduct: async (req, res) => {
        const id = req.body.id;
        await Product.findByIdAndDelete(id)
            .then((result) => {
                return res.send({
                    status: true,
                    message: "deleted successfully"
                })
            });
    },
    getallproducts: async(req,res)=>{
        const products=await Product.find({});
        return res.send({
            status:true,
            message:"fetched successfully",
            data:products
        })
    },
    getproductbyid: async(req,res)=>{
        const id=req.param.id;
        const product=await Product.findById(id);
        return res.send({
            status:true,
            message:"fetched successfully",
            data:product
        })
    }
}