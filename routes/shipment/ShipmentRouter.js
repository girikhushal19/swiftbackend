const express =require('express');
const ShipmentController = require('./ShipmentController');
const router = express.Router();
const multer=require("multer");
const path=require("path");



router.post('/addShipmentSubmit',ShipmentController.addShipmentSubmit);
router.post('/allShipment',ShipmentController.allShipment);
router.post('/allShipmentCount',ShipmentController.allShipmentCount);
router.post('/getShipmentDetail',ShipmentController.getShipmentDetail);

router.post('/allMerchantInvoice',ShipmentController.allMerchantInvoice);
router.post('/allMerchantInvoiceCount',ShipmentController.allMerchantInvoiceCount);



module.exports=router;