const mongoose=require("mongoose");
const EventbannerSchema=new mongoose.Schema({
   
    name:{type:String},
    second_text:{type:String,default:null},
    photos:{type:Array,default:[]}
},
{
    timestamps:true
}
);


module.exports=mongoose.model("eventbanner",EventbannerSchema);