const mongoose=require("mongoose");
const AddressSchema=new mongoose.Schema({
    latlong:{type:Array,default:[]},
    address:{type:String,default:""}

})
const organizerSchema=new mongoose.Schema({
   
    catagory:{type:String,default:""},
    name:{type:String,default:""},
    email:{type:String,unique:true},
    phone:{type:String,default:""},
    address:{type:AddressSchema,default:{}},
    bank:{type:String,default:""},
    accountname:{type:String,default:""},
    iban:{type:String,default:""},
    bic:{type:String,default:""},
    is_deleted:{type:Boolean,default:false},
    totalpaidamount:{type:Number,default:0},
  totalpendingamount:{type:Number,default:0},
},
{
    timestamps:true
}
);


module.exports=mongoose.model("organizer",organizerSchema);