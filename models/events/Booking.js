const mongoose = require("mongoose");
autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection(process.env.MONGO_URI);
autoIncrement.initialize(connection);
const stringGen = require("../../modules/randomString");
// const { ProductSchema } = require("../products/Product");

const EventBookingSchema = new mongoose.Schema({
    order_id: { type: Number, default: "" },
    combo_id:{type:String,default:""},
    event_id: {

        type: String
        , default: ""
    },
    type: {

        type: String
        , default: ""
    },
    event_name: {

        type: String
        , default: ""
    },
    org_name: {

        type: String
        , default: ""
    },
    org_id: {

        type: String
        , default: ""
    },
    edate: {

        type: Date
        , default: ""
    },
    etime: {

        type: String
        , default: ""
    },
    sdate: {

        type: Date
        , default: ""
    },
    stime: {

        type: String
        , default: ""
    },
    catagory: {

        type: String
        , default: ""
    },
    user_id: {
        required: true,
        type: String,
    },
    user_name: {
        required: true,
        type: String,
    },
    payment_status: {

        type: Boolean
        , default: false
    },
    promo_code: {

        type: String
        , default: ""
    },
    payment_method: {
        type: String
        , default: ""
    },
    transaction_id: {
        type: String,
        default: ""
    },
    tickets: {
        type: Array,
        default: ""
    },
    date_of_transaction: {
        type: Date,
        default: new Date().toISOString()

    },
    payment_amount: {
        type: Number,
        default: 0
    },
    purchased_ticket: {
        type: Number,
        default: 0
    },
    paid:{
        type:Boolean,
        default:false
    },
    discounted_amount: {
        type: Number,
        default: 0
    },
    status: {
        type: Number,
        default: 0,
    },

   

}, {
    timestamps: true
}
);
EventBookingSchema.plugin(autoIncrement.plugin, { model: 'orders', field: 'order_id', startAt: 1001 });

module.exports = mongoose.model("eventbooking", EventBookingSchema);