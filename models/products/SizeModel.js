const mongoose = require("mongoose");
const SizeSchema = new mongoose.Schema({
  name:{type:"String",default:null},
  status:{type:Number,default:1},
  created_at:{type:Date,default:Date.now}
});
module.exports = mongoose.model("sizes",SizeSchema);

