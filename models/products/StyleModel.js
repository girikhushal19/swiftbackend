const mongoose = require("mongoose");
const StyleSchema = new mongoose.Schema({
  name:{type:String,default:null},
  status:{type:Number,default:1},
  created_at:{type:Date,default:Date.now}
});
module.exports = mongoose.model("styles",StyleSchema);