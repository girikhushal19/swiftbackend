const mongoose=require("mongoose");
autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection(process.env.MONGO_URI);
autoIncrement.initialize(connection);
const stringGen= require("../../modules/randomString");
const { ProductSchema } = require("../products/Product");
const TrackingDetailsSchema=new mongoose.Schema({
  date:{type:Date,default:new Date()},
  time:{type:String,default:""},
  location:{type:String,default:""},
  comment:{type:String,default:""}
});
const SellerOrderNoteSchema = new mongoose.Schema({
  c_date:{type:Date,default:new Date() },
  message:{type:String,default:null}
});
const OrderSchema=new mongoose.Schema({
    order_id:{type:Number,default:""},
    combo_id:{type:String,default:""},
    products: [
        {
          product: {type:Object},
          part_id:{type:String,default:""},
          quantity: {
            type: Number,
            required: true,
          },
          varientPrice:{
            type:Number,
            default:0
          },
          discountedPrice:{
            type:Number,
            default:0
          },
          pricetouser:{
            type:Number,
            default:0
          },
          couponApplied:{
            type:String,
            default:""
          },
          tax: {
            type: Number,
            default:0
          },
          shipping_cost: {
            type: Number,
            default:0
          },
          commission: {
            type: Number,
            default:0
          },
          plusperproduct: {
            type: Number,
            default:0
          },
          finalprice: {
            type: Number,
            default:0
          },
          sellerprofit: {
            type: Number,
            default:0
          }, 
          is_shippable:{
            type:Boolean,
            default:true
          },
          variation_id:{
            type:String,
            default:""
          },
          variation_color:{type:String,default:""},
          variation_image:{type:String,default:""},
          variation_name:{type:String,default:""},
          variation_value:{type:String,default:""},

          provider_id:{
            type:String,
            default:""
          },
          is_pickedup:{
            type:Boolean,
            default:false
          },
          is_paid:{
            type:Boolean,
            default:false
          },
          provider_name:{
            type:String,
            default:""
          },
          returned_reason:{type:String,default:null},
          status:{
            type: Number,
            default: 0,
          },
          delivery_status:{
            type: Number,
            default: 0,
          },
          readytopickup:{type:Boolean,default:false}
        },
      ],
      payment_submit_web_type: { type: Number,required: 0}, // 0 = API , 1 = Web
      delivery_type: { type: Number,required: 1,}, //1=delivery , 2=pickup
      totalPrice: { type: Number,required: true,},
      totalcommission:{type: Number,required: true,},
      totalshipping:{type: Number,default:0},
      totalplusperproduct:{type: Number,default:0},
      totaltax:{type: Number,default:0},
      payment_amount: {type: Number,default:0},
      discounted_amount: { type: Number,default:0 },
      totalproductsinorder:{type:Number,default:0},
      totalproductsinqty:{type:Number,default:0},
      billingaddress: {type: Object,default:{}},
      shippingaddress: {type: Object,default:{}},
      userId: {required: true,type: String},
      payment_status: {type: Boolean,default:false   },
      promo_code: {type: String,default:""},
      payment_method: {type: String,default:"1"      }, //1=Stripe , 2=Papal
      transaction_id: {type: String,default:""     },
      payment_intent: {type: String,default:""     },
      date_of_transaction: {type: Date,default:new Date().toISOString() },
      status: {type: Number,default: 0, }, //status =1 delivered , 2 = cancel , 3 = returned , 6 = refund
      delivery_status:{type: Number,default: 0, },  // 1= pick up , 3= delivered , 4 = cancel from driver
      date_of_delivery: {type: Date,default:null   },
      returned_reason:{type:String,default:null},
      tracking_number:{type:String,default:stringGen(30)},
      tracking_details:{
        type:[TrackingDetailsSchema],
        default:[]
      },
      sellerNotes:{
        type:[SellerOrderNoteSchema],default:[]
      },
      shipping_details:{type:Object,default:{}},
      payment_details:{
        type:Object,default:{}
      },
      readytopickup:{type:Boolean,default:false},
      pickup_delivery_date:{type:String,default:null},
      pickup_delivery_time:{type:String,default:null},
      pickup_delivery_date_only:{type:Date,default:null}
},{
    timestamps:true
}
);
OrderSchema.plugin(autoIncrement.plugin, {model:'orders',field: 'order_id',startAt: 1001});

module.exports=mongoose.model("orders",OrderSchema);