const mongoose = require('mongoose');
const NotificationSchema = new mongoose.Schema({
    to_id: {type:String},
    user_type: {type:String,default:'user'}, // user , seller , admin
    title: {type:String,default:''},
    message: {type:String,default:''},
    status: {type:String,default:''},
    notification_type: {type:String,default:''},
    date: {type:Date,default: new Date() },
    createdd_at: {type:Date,default: new Date() },
    from_email: {type:String},
})
module.exports = mongoose.model('Notification', NotificationSchema);

 