const mongoose = require("mongoose");
const Schema = require("mongoose");

const PushnotificationmerchantsSchema = new mongoose.Schema({
  user_id: [{ type: Schema.Types.ObjectId,ref:"users" }],
  title: { type: String, default: null },
  description: { type: String, default: null },
  is_seen: { type: Number, default: 0 }, // 0 Not seen 1 seen
  is_checked: { type: Boolean, default: false }, // 0 Not seen 1 seen
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("pushnotificationmerchants", PushnotificationmerchantsSchema);

