const mongoose=require("mongoose");
const GlobalSettingsSchema=mongoose.Schema({
   
    tax:{type:Number,default:0},
    commission:{type:Number,default:0},
    plusperproduct:{type:Number,default:0},
    returnprice:{type:Number,default:0},
    eventcom:{type:Number,default:0},
    shipping_cost:{type:Number,default:0}, // 100 meter price
    imidiate_shipping_cost:{type:Number,default:0}, 
    estimate_time:{type:Number,default:0},
    return_max_day:{type:Number,default:0},
   
});
module.exports=mongoose.model("GlobalSettings",GlobalSettingsSchema);