const mongoose = require("mongoose");

const SettingSchema = new mongoose.Schema({
  attribute_key: { type: String, default: null },
  attribute_value: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("adminsettings", SettingSchema);