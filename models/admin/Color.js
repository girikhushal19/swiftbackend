const mongoose = require("mongoose");

const ColorSchema = new mongoose.Schema({
  color_name:{type:String,default:""},
  color_code:{type:String,default:""},
  status:{type:Number,defult:1},
  created_at:{type:Date,default:Date.now},
  updated_at:{type:Date,default:Date.now}
});

module.exports = mongoose.model("colors",ColorSchema);