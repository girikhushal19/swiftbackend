const mongoose = require("mongoose");
const AdminPayoutSchema = new mongoose.Schema({
   
   teacher_id: { type: String },
    
   payment_amount: { type: Number },
   payment_date: { type: Date }
    
    
});
module.exports = mongoose.model("AdminPayout", AdminPayoutSchema);