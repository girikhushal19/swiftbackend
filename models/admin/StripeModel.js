const mongoose = require("mongoose");
const Schema = require("mongoose");

const StripeSchema = new mongoose.Schema({
  stripe_key: { type: String, default: null },
  stripe_secret: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("stripes", StripeSchema);