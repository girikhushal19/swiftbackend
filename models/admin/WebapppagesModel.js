const mongoose = require("mongoose");
const Schema = require("mongoose");

const WebapppagesSchema = new mongoose.Schema({
  url_slug: { type: String, default: null },
  description: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("webapppages", WebapppagesSchema);