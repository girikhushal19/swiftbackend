const mongoose=require("mongoose");
const AnnouncementsSchema=new mongoose.Schema({
    title:{type:String},
    notificationtype:{type:String},
    usertype:{type:String},
    content:{type:String},
    sentTo:{type:Array},
    status:{type:Boolean,default:true}
},
{
    timestamps:true
}
);

module.exports=mongoose.model("Announcements",AnnouncementsSchema);