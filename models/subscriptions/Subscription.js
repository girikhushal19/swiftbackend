const mongoose=require('mongoose');
const SubscriptionsSchema=new mongoose.Schema({
   
    subscriber_name:{type:String,required:true},
    subscriber_email:{type:String,required:true},
    plusperproduct:{type:Number,default:0},
    extracommission:{type:Number,default:0},
    
    name_of_package:{type:String,required:true},
    package_id:{type:String,required:true},
    status:{type:Boolean,required:true},
    start_date:{type:Date,required:true},
    end_date:{type:Date,required:true},
    payment_status:{type:Boolean,default:false},
    payment_amount:{type:Number},
    payment_method:{type:String},
    date_of_transaction:{type:Date},
    transaction_id:{type:String},
    provider_id:{type:String,required:true},
    Numberofitems:{type:Number,default:0},
    duration:{type:Number,default:0},
    is_limited:{type:String,required:false},
    created_at: { type: Date, default: Date.now },
});
module.exports=mongoose.model('Subscriptions',SubscriptionsSchema);