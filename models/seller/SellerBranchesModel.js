const mongoose=require("mongoose");
const BranchSchema = new mongoose.Schema({
    seller_id:{type:String,default:""},
    shopname:{type:String,default:""},
    shopdesc:{type:String,default:""},
    shopphoto:{type:String,default:""},
    address:{type:String,default:""},
    location:{
        type:{type:String,default:""},
        coordinates:[]
    },
    main_branch:{type:Number,default:0},
    status:{type:Number,default:1} // 1 is Active , 0 is deactive , 2 is for delete
});
BranchSchema.index({location:'2dsphere'});
module.exports = mongoose.model("sellerbranches",BranchSchema)