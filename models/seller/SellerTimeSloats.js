const mongoose = require("mongoose");
const Schema = require("mongoose");

const SellerTimeSloatSchema = new mongoose.Schema({
    seller_id:{type:String,default:null},
    delivery_type:{type:Number,default:null},   //1 = Delivery , 2 = Pick up , 3 = Try On Program
    day_enable:{type:Boolean,default:true},
    day_name:{type:String,default:null},
    day_number:{type:Number,default:null},
    start_time:{type:Number,default:null},
    end_time:{type:Number,default:null},
    created_at:{type:Date,default:Date.now},
    updated_at:{type:Date,default:Date.now}
});

module.exports = mongoose.model("sellertimesloats",SellerTimeSloatSchema);


