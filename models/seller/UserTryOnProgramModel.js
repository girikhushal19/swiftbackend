const mongoose = require("mongoose");
const Schema = require("mongoose");

const UserTrySchema = new mongoose.Schema({
    seller_id:{type:String,default:null},
    product_id:{type:String,default:null},
    date:{type:String,default:null},
    time:{type:String,default:null},
    shop_id:{type:String,default:null},
    user_id:{type:String,default:null},
    status:{type:Number,default:1}, // 1=Booked , 2=Completed , 3= User cancled , 4= Seller cancled
    created_at:{type:Date,default:Date.now},
    updated_at:{type:Date,default:Date.now},
});
module.exports = mongoose.model("usertryonprograms",UserTrySchema);