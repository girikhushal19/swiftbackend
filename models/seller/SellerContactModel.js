const mongoose = require("mongoose");
const  SellercontactSchema = new mongoose.Schema({
  full_name:{ type: String, default: "" },
  email:{ type: String, default: "" },
  phone:{ type: String, default: "" },
  message:{ type: String, default: "" },
  created_at:{type:Date,default: new Date().toISOString()},
  updated_at:{type:Date,default: new Date().toISOString()},
});
module.exports = mongoose.model("sellercontacts", SellercontactSchema);