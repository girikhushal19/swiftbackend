const mongoose = require("mongoose");
const Schema = require("mongoose");

const SellerquerysSchema = new mongoose.Schema({
  seller_id: { type: String, default: null },
  request_purpose: { type: String, default: null },
  reason: { type: String, default: null },
  message: { type: String, default: null },
  photo: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("sellerquerys", SellerquerysSchema);