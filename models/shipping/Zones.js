const mongoose=require("mongoose");
const ZonesSchema=new mongoose.Schema({
    name:{type:String},
    cities:{type:Array},
    vendor_id:{type:String}
});


module.exports=mongoose.model("Zones",ZonesSchema);