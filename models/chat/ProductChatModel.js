const mongoose = require("mongoose");
const Schema = require("mongoose");

const ProductChatSchema = new mongoose.Schema({
  product_id: {type:String,default:null},
  room_id:{type: String, default: null},
  seller_id: {type:String,default:null},
  user_id: {type:String,default:null},
  message: { type: String, default: null },
  user_reason: { type: String, default: null },
  message_by: { type: Number, default: 1 }, //1 = user 2 =seller
  mark_read_seller: { type: Number, default: 0 },
  mark_read_user: { type: Number, default: 0 },
  del_flag_seller: { type: Number, default: 0 },
  del_flag_user: { type: Number, default: 0 },
  report_seller: { type: Number, default: 0 },
  report_user: { type: Number, default: 0 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("productchat", ProductChatSchema);