const mongoose=require("mongoose");
const RatingnreviewsSchema=new mongoose.Schema({
    user_id:{type:String},
    user_name:{type:String},
    provider_id:{type:String},
    provider_name:{type:String},
    product_name:{type:String},
    product_id:{type:String},
    order_id:{type:String},
    rating:{type:Number},
    review:{type:String},
    media:{type:Array},
    replies:{type:Array,default:[]}
    
},{
    timestamps:true
});
module.exports=mongoose.model("Ratingnreviews",RatingnreviewsSchema);