const mongoose = require('mongoose');
const NotificationPermissionSchema = new mongoose.Schema({
    id: {type:String, required:true},
    user_type:{type:String}, // user , seller 
    messagefromprovider:{type:Boolean,default:true},
    messagefromuser:{type:Boolean,default:true},
    messagepromocode:{type:Boolean,default:true},
    messagefromoblack:{type:Boolean,default:true},
    
    emailfromprovider:{type:Boolean,default:true},
    emailfromuser:{type:Boolean,default:true},
    emailpromocode:{type:Boolean,default:true},
    emailfromoblack:{type:Boolean,default:true},
    
    


})
module.exports = mongoose.model('NotificationPermission', NotificationPermissionSchema);