const mongoose=require("mongoose");
const {ProductSchema}=require("../products/Product");
const cartSchema=new mongoose.Schema({
    product:ProductSchema,
    quantity: {
        type: Number,
        required: true,
      },
    price: {
        type: Number,
        required: true,
      },
    // totalPrice:{
    //     type: Number,
    //     required: true,
    // }
})
const cart=mongoose.model("cart",cartSchema);
module.exports={
    cart,
    cartSchema
}
