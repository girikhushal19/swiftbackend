const jwt = require("jsonwebtoken");
const User = require("../models/user/User");
const Seller = require("../models/seller/Seller");
const Admin = require("../models/admin/Admin");
const config = process.env;

const verifyToken = async (req, res, next) => {
  const token =
    req.body.token || req.query.token || req.headers["x-access-token"];
  const type = req.headers["type"];
  let model;
  if (type == "seller") {
    model = Seller
  } else if (type == "user") {
    model = User
  } else if (type == "admin") {
    model = Admin
  } else {
    return res.status(401).send("A type header is required");
  }
  if (!token) {
    return res.status(403).send("A token is required for authentication");
  }
  try {
    const user = await model.findOne({ token: token });
    if (!user) { return res.status(401).send("Invalid Token"); }
  } catch (err) {
    return res.status(401).send("Invalid Token");
  }
  return next();
};

module.exports = verifyToken;